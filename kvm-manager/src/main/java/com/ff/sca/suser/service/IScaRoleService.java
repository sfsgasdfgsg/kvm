package com.ff.sca.suser.service;

import com.ff.sca.suser.entity.ScaRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * role 角色表 服务类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface IScaRoleService extends IService<ScaRole> {
	
}
