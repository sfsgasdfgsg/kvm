package com.ff.sca.suser.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.entity.ztree.ZtreeNode;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.suser.dao.ScaPermissionDao;
import com.ff.sca.suser.entity.ScaPermission;
import com.ff.sca.suser.entity.ScaRolePermission;
import com.ff.sca.suser.dao.ScaRolePermissionDao;
import com.ff.sca.suser.service.IScaRolePermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * role_permission 角色权限表 服务实现类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Service
public class ScaRolePermissionServiceImpl extends ServiceImpl<ScaRolePermissionDao, ScaRolePermission> implements IScaRolePermissionService {

    @Autowired
    private ScaPermissionDao permissionDao;
    @Autowired
    private ScaRolePermissionDao rolePermissionDao;

    @Override
    public List<ZtreeNode> getZtreePerminssionAuthInfo(Integer roleId) {
        List<ScaPermission> permisonList = permissionDao.getPerminssionZtree(0);
        return createZtreeNode(permisonList, roleId);
    }

    private List<ZtreeNode> createZtreeNode(List<ScaPermission> permisonList, Integer roleId) {
        List<ZtreeNode> nodes = new ArrayList<>();
        if (permisonList.size() > 0) {
            for (int i = 0; i < permisonList.size(); i++) {
                ScaPermission per = permisonList.get(i);
                ZtreeNode node = new ZtreeNode();
                node.setId(per.getId());
                node.setPid(per.getPid());
                node.setName(per.getName());
                node.setType(per.getType());
                List<ZtreeNode> cNode = createZtreeNode(permissionDao.getPerminssionZtree(per.getId()), roleId);
                node.setChildren(cNode);

                ScaRolePermission ew = new ScaRolePermission();
                ew.setRoleId(roleId);
                ew.setPermissionId(per.getId());
                ScaRolePermission spersionssion = rolePermissionDao.selectOne(ew);
                if (spersionssion != null) {
                    node.setChecked("true");
                }
                if (cNode.size() > 0) {
                    node.setIsParent(true);
                }
                node.setChilCount(cNode.size());
                nodes.add(node);
            }
        }
        return nodes;
    }
}
