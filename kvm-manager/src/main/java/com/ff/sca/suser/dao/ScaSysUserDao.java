package com.ff.sca.suser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.suser.entity.ScaPermission;
import com.ff.sca.suser.entity.ScaSysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * sca后台管理员用户 Mapper 接口
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface ScaSysUserDao extends BaseMapper<ScaSysUser> {

    public List<ScaPermission> getUserMenuList(@Param("userId") Integer userId, @Param("pid") Integer pid);

}