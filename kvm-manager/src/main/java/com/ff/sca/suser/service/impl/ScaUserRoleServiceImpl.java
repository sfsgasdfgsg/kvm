package com.ff.sca.suser.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ff.sca.suser.dao.ScaUserRoleDao;
import com.ff.sca.suser.entity.AuthInfo;
import com.ff.sca.suser.entity.ScaUserRole;
import com.ff.sca.suser.service.IScaUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * user_role 用户角色关联表 服务实现类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Service
public class ScaUserRoleServiceImpl extends ServiceImpl<ScaUserRoleDao, ScaUserRole> implements IScaUserRoleService {

    @Autowired
    private ScaUserRoleDao userRoleDao;

    @Override
    public List<AuthInfo> getUserAuthInfo(Integer roleId) {
        return userRoleDao.getUserAuthInfo(roleId);
    }
}
