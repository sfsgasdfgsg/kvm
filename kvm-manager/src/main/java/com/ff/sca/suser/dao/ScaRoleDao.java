package com.ff.sca.suser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.suser.entity.ScaRole;

/**
 * <p>
 * role 角色表 Mapper 接口
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface ScaRoleDao extends BaseMapper<ScaRole> {

}