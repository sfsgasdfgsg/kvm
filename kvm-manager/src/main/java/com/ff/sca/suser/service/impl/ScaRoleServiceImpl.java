package com.ff.sca.suser.service.impl;

import com.ff.sca.suser.entity.ScaRole;
import com.ff.sca.suser.dao.ScaRoleDao;
import com.ff.sca.suser.service.IScaRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * role 角色表 服务实现类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Service
public class ScaRoleServiceImpl extends ServiceImpl<ScaRoleDao, ScaRole> implements IScaRoleService {
	
}
