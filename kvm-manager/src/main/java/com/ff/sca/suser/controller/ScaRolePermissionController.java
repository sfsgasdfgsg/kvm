package com.ff.sca.suser.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.entity.ztree.ZtreeNode;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.suser.entity.ScaRolePermission;
import com.ff.sca.suser.service.IScaRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * role_permission 角色权限表 前端控制器
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Controller
@RequestMapping("/scaRolePermission")
public class ScaRolePermissionController extends BaseController {

    @Autowired
    private IScaRolePermissionService rolePermissionService;

    /**
     * 获取当前资源授权用户情况
     *
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/getZtreePerminssionAuthInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg getZtreePerminssionAuthInfo(Integer roleId) {
        try {
            List<ZtreeNode> ztreePerminssionAuthInfo = rolePermissionService.getZtreePerminssionAuthInfo(roleId);
            return resSuccess(ztreePerminssionAuthInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    @ControllerLog(
            description = "操作角色资源授权信息",
            module = OperateModule.SysUserManger,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/oprPermissionRole", method = RequestMethod.POST)
    public ResMsg oprPermissionRole(Integer roleId, String permissionIds) {
        try {
            if (CharUtil.isNotEmpty(roleId)) {
                if (CharUtil.isNotEmpty(permissionIds)) {
                    rolePermissionService.delete(new EntityWrapper<ScaRolePermission>().eq("role_id", roleId));
                    String[] addArr = permissionIds.split(",");
                    for (int i = 0; i < addArr.length; i++) {
                        ScaRolePermission aper = new ScaRolePermission();
                        aper.setCreateTime(new Date());
                        aper.setRoleId(roleId);
                        aper.setPermissionId(Integer.parseInt(addArr[i]));
                        rolePermissionService.insert(aper);
                    }
                }
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 单个角色授权资源add
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "增加角色授权资源",
            module = OperateModule.SysUserManger,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addRolePermission", method = RequestMethod.POST)
    public ResMsg addRolePermission(ScaRolePermission per) {
        try {
            if (CharUtil.isNotEmpty(per.getRoleId()) && CharUtil.isNotEmpty(per.getPermissionId())) {
                per.setCreateTime(new Date());
                boolean result = rolePermissionService.insert(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 单个解除角色资源授权delete
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "解除角色资源授权",
            module = OperateModule.SysUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delRolePermission", method = RequestMethod.POST)
    public ResMsg delRolePermission(ScaRolePermission per) {
        try {
            if (CharUtil.isNotEmpty(per.getRoleId()) && CharUtil.isNotEmpty(per.getPermissionId())) {
                boolean result = rolePermissionService.delete(new EntityWrapper<ScaRolePermission>().eq("permission_id", per.getPermissionId()).eq("role_id", per.getRoleId()));
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


}
