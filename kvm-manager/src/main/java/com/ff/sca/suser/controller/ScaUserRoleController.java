package com.ff.sca.suser.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.suser.entity.AuthInfo;
import com.ff.sca.suser.entity.ScaUserRole;
import com.ff.sca.suser.service.IScaUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * user_role 用户角色关联表 前端控制器
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Controller
@RequestMapping("/scaUserRole")
public class ScaUserRoleController extends BaseController {

    @Autowired
    private IScaUserRoleService userRoleService;

    /**
     * 获取当前角色授权用户情况
     *
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/getUserAuthInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg getUserAuthInfo(Integer roleId) {
        if (CharUtil.isNotEmpty(roleId)) {
            List<AuthInfo> authInfo = userRoleService.getUserAuthInfo(roleId);
            return resSuccess(authInfo);
        }
        return resfail(null);
    }

    @ControllerLog(
            description = "操作角色用户授权信息",
            module = OperateModule.SysUserManger,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/oprUserRole", method = RequestMethod.POST)
    public ResMsg oprUserRole(Integer roleId, String addUseIds, String delUserIds) {
        try {
            if (CharUtil.isNotEmpty(roleId)) {
                if (CharUtil.isNotEmpty(addUseIds)) {
                    String[] addArr = addUseIds.split(",");
                    for (int i = 0; i < addArr.length; i++) {
                        int count = userRoleService.selectCount(new EntityWrapper<ScaUserRole>().eq("role_id", roleId).eq("user_id", addArr[i]));
                        if (count == 0) {
                            ScaUserRole aper = new ScaUserRole();
                            aper.setCreateTime(new Date());
                            aper.setRoleId(roleId);
                            aper.setUserId(Integer.parseInt(addArr[i]));
                            userRoleService.insert(aper);
                        }
                    }
                }
                if (CharUtil.isNotEmpty(delUserIds)) {
                    String[] delArr = delUserIds.split(",");
                    for (int i = 0; i < delArr.length; i++) {
                        int count = userRoleService.selectCount(new EntityWrapper<ScaUserRole>().eq("role_id", roleId).eq("user_id", delArr[i]));
                        if (count > 0) {
                            userRoleService.delete(new EntityWrapper<ScaUserRole>().eq("role_id", roleId).eq("user_id", delArr[i]));
                        }
                    }
                }
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 单个用户角色授权add
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "增加用户角色授权",
            module = OperateModule.SysUserManger,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addUserRole", method = RequestMethod.POST)
    public ResMsg addUserRole(ScaUserRole per) {
        try {
            if (CharUtil.isNotEmpty(per.getRoleId()) && CharUtil.isNotEmpty(per.getUserId())) {
                per.setCreateTime(new Date());
                boolean result = userRoleService.insert(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 单个解除用户角色授权delete
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "解除用户角色授权",
            module = OperateModule.SysUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delUserRole", method = RequestMethod.POST)
    public ResMsg delUserRole(ScaUserRole per) {
        try {
            if (CharUtil.isNotEmpty(per.getRoleId()) && CharUtil.isNotEmpty(per.getUserId())) {
                boolean result = userRoleService.delete(new EntityWrapper<ScaUserRole>().eq("user_id", per.getUserId()).eq("role_id", per.getRoleId()));
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }
}
