package com.ff.sca.suser.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.CodeEnum;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.suser.entity.ScaRole;
import com.ff.sca.suser.entity.ScaRolePermission;
import com.ff.sca.suser.entity.ScaUserRole;
import com.ff.sca.suser.service.IScaRolePermissionService;
import com.ff.sca.suser.service.IScaRoleService;
import com.ff.sca.suser.service.IScaUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * role 角色表 前端控制器
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Controller
@RequestMapping("/scaRole")
public class ScaRoleController extends BaseController {

    @Autowired
    private IScaRoleService roleService;
    @Autowired
    private IScaUserRoleService userRoleService;
    @Autowired
    private IScaRolePermissionService rolePermissionService;

    /**
     * 查看角色信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/roleInfo", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg roleInfo(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(roleService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }

    }

    /**
     * 删除角色，附带删除和角色相关的信息
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除角色，以及与角色相关的授权信息",
            module = OperateModule.SysUserManger,
            opType = OperateType.delete
    )
    @RequestMapping(value = "/delRole", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg delRole(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                //删除角色
                roleService.deleteById(id);
                //删除角色与资源(permission)对应信息
                rolePermissionService.delete(new EntityWrapper<ScaRolePermission>().eq("role_id", id));
                //删除角色与用户(suser)对应信息
                userRoleService.delete(new EntityWrapper<ScaUserRole>().eq("role_id", id));
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改角色信息
     *
     * @param role
     * @return
     */
    @ControllerLog(
            description = "修改角色",
            module = OperateModule.SysUserManger,
            opType = OperateType.modify
    )
    @RequestMapping(value = "/updRole", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg updRole(ScaRole role) {
        try {
            if (CharUtil.isNotEmpty(role.getId())) {
                role.setUpdateTime(new Date());
                Boolean result = roleService.updateById(role);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    @ControllerLog(
            description = "新增角色",
            module = OperateModule.SysUserManger,
            opType = OperateType.create
    )
    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg addRole(ScaRole role) {
        try {
            if (CharUtil.isNotEmpty(role.getName())) {
                List<ScaRole> roleList = roleService.selectList(new EntityWrapper<ScaRole>().eq("name", role.getName()));
                if (roleList.size() > 0) {
                    return new ResMsg(CodeEnum.RES_700.getCode(), CodeEnum.RES_700.getMsg(), null);
                } else {
                    role.setCreateTime(new Date());
                    Boolean result = roleService.insert(role);
                    if (result) {
                        return resSuccess(null);
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 角色列表
     *
     * @param role
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/roleList", method = RequestMethod.POST)
    public LayuiTableResMsg roleList(HttpServletRequest request, Integer page, Integer size, ScaRole role) {
        try {
            Page<ScaRole> pageEntity = setPageInfo(page, size);
            EntityWrapper ew = new EntityWrapper<ScaRole>();
            if (CharUtil.isNotEmpty(role.getName())) {
                ew.like("name", role.getName());
            }
            Page<ScaRole> suserList = roleService.selectPage(pageEntity, ew);
            long total = roleService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}
