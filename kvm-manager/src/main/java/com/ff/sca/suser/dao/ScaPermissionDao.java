package com.ff.sca.suser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.suser.entity.ScaPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * permission 权限表 Mapper 接口
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface ScaPermissionDao extends BaseMapper<ScaPermission> {

    public List<ScaPermission> getPerminssionZtree(@Param("pid") Integer pid);

}