package com.ff.sca.suser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.suser.entity.AuthInfo;
import com.ff.sca.suser.entity.ScaUserRole;

import java.util.List;

/**
 * <p>
 * user_role 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface ScaUserRoleDao extends BaseMapper<ScaUserRole> {
    
    public List<AuthInfo> getUserAuthInfo(Integer roleId);

}