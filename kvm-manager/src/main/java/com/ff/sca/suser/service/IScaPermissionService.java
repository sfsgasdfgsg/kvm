package com.ff.sca.suser.service;

import com.baomidou.mybatisplus.service.IService;
import com.ff.sca.common.entity.ztree.ZtreeNode;
import com.ff.sca.suser.entity.ScaPermission;

import java.util.List;

/**
 * <p>
 * permission 权限表 服务类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface IScaPermissionService extends IService<ScaPermission> {

    /**
     * 资源树形菜单
     *
     * @param pid
     * @return
     */
    public List<ZtreeNode> getPerminssionZtree(Integer pid);

}
