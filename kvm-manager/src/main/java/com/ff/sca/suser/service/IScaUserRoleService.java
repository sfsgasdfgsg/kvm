package com.ff.sca.suser.service;

import com.baomidou.mybatisplus.service.IService;
import com.ff.sca.suser.entity.AuthInfo;
import com.ff.sca.suser.entity.ScaUserRole;

import java.util.List;

/**
 * <p>
 * user_role 用户角色关联表 服务类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface IScaUserRoleService extends IService<ScaUserRole> {

    /**
     * 获取用户授权信息
     *
     * @param roleId
     * @return
     */
    public List<AuthInfo> getUserAuthInfo(Integer roleId);

}
