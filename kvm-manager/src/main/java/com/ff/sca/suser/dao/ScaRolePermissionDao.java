package com.ff.sca.suser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.suser.entity.ScaRolePermission;

/**
 * <p>
 * role_permission 角色权限表 Mapper 接口
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface ScaRolePermissionDao extends BaseMapper<ScaRolePermission> {

}