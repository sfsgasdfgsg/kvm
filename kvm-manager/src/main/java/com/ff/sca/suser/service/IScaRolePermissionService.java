package com.ff.sca.suser.service;

import com.ff.sca.common.entity.ztree.ZtreeNode;
import com.ff.sca.suser.entity.ScaRolePermission;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * role_permission 角色权限表 服务类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface IScaRolePermissionService extends IService<ScaRolePermission> {

    /**
     * 获取资源授权信息ztree
     *
     * @param roleId
     * @return
     */
    public List<ZtreeNode> getZtreePerminssionAuthInfo(Integer roleId);

}
