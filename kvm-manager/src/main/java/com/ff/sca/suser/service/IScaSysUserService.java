package com.ff.sca.suser.service;

import com.baomidou.mybatisplus.service.IService;
import com.ff.sca.common.entity.ztree.MenuNode;
import com.ff.sca.common.entity.ztree.ZtreeNode;
import com.ff.sca.suser.entity.ScaSysUser;

import java.util.List;

/**
 * <p>
 * sca后台管理员用户 服务类
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
public interface IScaSysUserService extends IService<ScaSysUser> {


    /**
     * 树形菜单
     *
     * @param userId
     * @param pid
     * @return
     */
    public List<MenuNode> getUserMenuList(Integer userId, Integer pid);

}
