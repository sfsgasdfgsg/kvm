package com.ff.sca.suser.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.CodeEnum;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.entity.ztree.ZtreeNode;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.suser.entity.ScaPermission;
import com.ff.sca.suser.entity.ScaRolePermission;
import com.ff.sca.suser.service.IScaPermissionService;
import com.ff.sca.suser.service.IScaRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * permission 权限表 前端控制器
 * </p>
 *
 * @author gw
 * @since 2018-09-20
 */
@Controller
@RequestMapping("/scaPermission")
public class ScaPermissionController extends BaseController {

    @Autowired
    private IScaPermissionService permissionService;
    @Autowired
    private IScaRolePermissionService rolePermissionService;

    /**
     * 资源信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/perminssionInfo", method = RequestMethod.POST)
    public ResMsg perminssionInfo(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(permissionService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除资源，资源没有下级才能删除
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除资源，资源没有下级才能删除",
            module = OperateModule.SysUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delPerminssion", method = RequestMethod.POST)
    public ResMsg updPerminssion(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                int childrenCount = permissionService
                        .selectCount(new EntityWrapper<ScaPermission>().eq("pid", id));
                if (childrenCount > 0) {
                    return new ResMsg(CodeEnum.RES_701.getCode(), CodeEnum.RES_701.getMsg(), null);
                } else {
                    //删除资源
                    permissionService.deleteById(id);
                    //删除资源与角色关联表信息
                    rolePermissionService.delete(new EntityWrapper<ScaRolePermission>().eq("permission_id", id));
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 修改
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改资源信息",
            module = OperateModule.SysUserManger,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updPerminssion", method = RequestMethod.POST)
    public ResMsg updPerminssion(ScaPermission per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                ScaPermission permission = permissionService.selectById(per.getId());
                per.setCreateTime(permission.getCreateTime());
                per.setPermCode(permission.getPermCode());
                per.setUpdateTime(new Date());
                boolean result = permissionService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }

    }

    /**
     * 增加资源
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增资源",
            module = OperateModule.SysUserManger,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addPerminssion", method = RequestMethod.POST)
    public ResMsg addPerminssion(ScaPermission per) {
        try {
            if (CharUtil.isNotEmpty(per)) {
                per.setCreateTime(new Date());
                per.setPermCode(UUID.randomUUID().toString().toUpperCase());
                boolean result = permissionService.insert(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 资源ztree,异步加载
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPerminssionZtree", method = RequestMethod.POST)
    public ResMsg getPerminssionZtree(Integer id) {
        try {
            if (CharUtil.isEmpty(id)) {
                id = 0;
            }
            List<ZtreeNode> userRoles = permissionService.getPerminssionZtree(id);
            return resSuccess(userRoles);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 资源列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/perminssionList", method = RequestMethod.POST)
    public LayuiTableResMsg perminssionList(Integer page, Integer size, ScaPermission per) {
        try {
            Page<ScaPermission> pageEntity = setPageInfo(page, size);
            EntityWrapper ew = new EntityWrapper<ScaPermission>();
            if (CharUtil.isNotEmpty(per.getName())) {
                ew.like("name", per.getName());
            }
            if (CharUtil.isNotEmpty(per.getUrl())) {
                ew.like("url", per.getUrl());
            }
            if (CharUtil.isNotEmpty(per.getId())) {
                ew.eq("pid", per.getId());
            } else {
                ew.eq("pid", 0);
            }
            ew.orderBy("sort", true);
            Page<ScaPermission> suserList = permissionService.selectPage(pageEntity, ew);
            long total = permissionService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }


}
