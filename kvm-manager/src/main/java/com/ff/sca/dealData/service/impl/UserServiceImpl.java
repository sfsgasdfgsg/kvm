package com.ff.sca.dealData.service.impl;

import com.ff.sca.dealData.entity.User;
import com.ff.sca.dealData.dao.UserDao;
import com.ff.sca.dealData.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-23
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements IUserService {

}
