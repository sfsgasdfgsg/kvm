package com.ff.sca.dealData.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author gw123
 * @since 2018-12-23
 */
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 自身推广码
     */
    @TableField("user_id")
    private String userId;
    private String number;
    /**
     * 微信openid
     */
    private String openid;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像
     */
    private String headimg;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 电话
     */
    private String tel;
    /**
     * 交易密码
     */
    private String password;
    /**
     * 上级推广码
     */
    @TableField("recommend_id")
    private String recommendId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Integer createtime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Integer updatetime;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 美豆
     */
    private Double wallet;
    /**
     * 身份
     */
    private Integer level;
    /**
     * 购物金
     */
    private Double score;
    @TableField("score_2")
    private Double score2;
    @TableField("score_3")
    private Double score3;
    private Integer sign;
    private String address;
    @TableField("last_sign_time")
    private Integer lastSignTime;
    @TableField("count_days")
    private Integer countDays;
    /**
     * 团队礼包等级
     */
    private Integer star;
    /**
     * 团队商品等级
     */
    private Integer sale;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRecommendId() {
        return recommendId;
    }

    public void setRecommendId(String recommendId) {
        this.recommendId = recommendId;
    }

    public Integer getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Integer createtime) {
        this.createtime = createtime;
    }

    public Integer getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Integer updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getWallet() {
        return wallet;
    }

    public void setWallet(Double wallet) {
        this.wallet = wallet;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getScore2() {
        return score2;
    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public Double getScore3() {
        return score3;
    }

    public void setScore3(Double score3) {
        this.score3 = score3;
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getLastSignTime() {
        return lastSignTime;
    }

    public void setLastSignTime(Integer lastSignTime) {
        this.lastSignTime = lastSignTime;
    }

    public Integer getCountDays() {
        return countDays;
    }

    public void setCountDays(Integer countDays) {
        this.countDays = countDays;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public Integer getSale() {
        return sale;
    }

    public void setSale(Integer sale) {
        this.sale = sale;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "User{" +
        ", id=" + id +
        ", userId=" + userId +
        ", number=" + number +
        ", openid=" + openid +
        ", nickname=" + nickname +
        ", headimg=" + headimg +
        ", realname=" + realname +
        ", tel=" + tel +
        ", password=" + password +
        ", recommendId=" + recommendId +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        ", status=" + status +
        ", wallet=" + wallet +
        ", level=" + level +
        ", score=" + score +
        ", score2=" + score2 +
        ", score3=" + score3 +
        ", sign=" + sign +
        ", address=" + address +
        ", lastSignTime=" + lastSignTime +
        ", countDays=" + countDays +
        ", star=" + star +
        ", sale=" + sale +
        "}";
    }
}
