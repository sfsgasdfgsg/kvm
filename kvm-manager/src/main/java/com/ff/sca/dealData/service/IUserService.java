package com.ff.sca.dealData.service;

import com.ff.sca.dealData.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-23
 */
public interface IUserService extends IService<User> {

}
