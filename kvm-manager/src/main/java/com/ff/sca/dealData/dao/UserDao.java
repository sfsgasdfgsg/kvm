package com.ff.sca.dealData.dao;

import com.ff.sca.dealData.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-23
 */
public interface UserDao extends BaseMapper<User> {

}
