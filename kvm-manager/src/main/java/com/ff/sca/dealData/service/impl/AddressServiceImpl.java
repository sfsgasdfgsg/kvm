package com.ff.sca.dealData.service.impl;

import com.ff.sca.dealData.entity.Address;
import com.ff.sca.dealData.dao.AddressDao;
import com.ff.sca.dealData.service.IAddressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-23
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressDao, Address> implements IAddressService {

}
