package com.ff.sca.common.codeenum;

public enum OrderStatusEnum {


    APPROVAL(1, "待审核"),
    PAYMENT(3, "待支付"),
    DELIVERY(6, "待发货"),
    RECEIVE(9, "待收货"),
    COMPLETE(11, "已完成"),

    ;

    private int type;
    private String name;

    OrderStatusEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (OrderStatusEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
