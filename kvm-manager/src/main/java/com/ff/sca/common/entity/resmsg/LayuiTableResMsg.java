package com.ff.sca.common.entity.resmsg;

/**
 * layui表格返回数据模型
 * 
 * @ClassName: LayuiTableResMsg
 * @Description: TODO
 * @author gw
 * @date 2018年8月29日
 *
 */
public class LayuiTableResMsg {

	private String code;

	private String msg;

	private long count;

	private Object data;

	public LayuiTableResMsg() {
		super();
	}

	public LayuiTableResMsg(String code, String msg, long count, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.count = count;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
