package com.ff.sca.common.codeenum;

public enum OrderPaymentStatusEnum {


    PAYMENT(0, "待支付"),
    FAIL(2, "支付失败"),
    SUCCESS(4, "支付成功"),

    ;

    private int type;
    private String name;

    OrderPaymentStatusEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (OrderPaymentStatusEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
