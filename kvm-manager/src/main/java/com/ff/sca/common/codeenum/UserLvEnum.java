package com.ff.sca.common.codeenum;

public enum UserLvEnum {

    VISITOR(0, "大众会员"),
    GOLD(1, "黄金会员"),
    PLATINUM(2, "铂金会员"),
    DIAMONDS(3, "钻石会员"),
    ;

    private int type;
    private String name;

    UserLvEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (UserLvEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
