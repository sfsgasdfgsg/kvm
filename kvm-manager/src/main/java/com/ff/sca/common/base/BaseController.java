package com.ff.sca.common.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.admin.service.RedisService;
import com.ff.sca.common.codeenum.CodeEnum;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.common.util.IpUtil;
import com.ff.sca.common.util.JWT;
import com.ff.sca.suser.entity.ScaSysUser;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    @Autowired
    public RedisService redisService;

    /**
     * 获取当前登录用户
     *
     * @return
     */
    public ScaSysUser getCurLoginSysUser(HttpServletRequest req) {
        String userIp = IpUtil.getIpAddr(req);
        ScaSysUser user = JWT.unsign(redisService.getAppCache("manager_token_" + userIp), ScaSysUser.class);
        return user;
    }

    /**
     * set pageInfo
     *
     * @param page
     * @param size
     * @return
     */
    public Page setPageInfo(Integer page, Integer size) {
        Page pageInfo = null;
        if (CharUtil.isNotEmpty(page) && CharUtil.isNotEmpty(size)) {
            pageInfo = new Page(page, size);
        } else {
            pageInfo = new Page(1, 10);
        }
        return pageInfo;
    }

    public ResMsg resSuccess(Object data) {
        return new ResMsg(CodeEnum.HTTP_200.getCode(), CodeEnum.HTTP_200.getMsg(), data);
    }

    public ResMsg resError(Object data) {
        return new ResMsg(CodeEnum.HTTP_500.getCode(), CodeEnum.HTTP_500.getMsg(), data);
    }

    public ResMsg resfail(Object data) {
        return new ResMsg(CodeEnum.HTTP_400.getCode(), CodeEnum.HTTP_400.getMsg(), data);
    }

    public LayuiTableResMsg resTableSuccess(long count, Object data) {
        return new LayuiTableResMsg(CodeEnum.HTTP_200.getCode(), CodeEnum.HTTP_200.getMsg(), count, data);
    }

    public LayuiTableResMsg resTableError(long count, Object data) {
        return new LayuiTableResMsg(CodeEnum.HTTP_500.getCode(), CodeEnum.HTTP_500.getMsg(), count, data);
    }

    public LayuiTableResMsg resTablefail(long count, Object data) {
        return new LayuiTableResMsg(CodeEnum.HTTP_400.getCode(), CodeEnum.HTTP_400.getMsg(), count, data);
    }

    public ResMsg resAreadyExisted(Object data) {
        return new ResMsg(CodeEnum.RES_700.getCode(), CodeEnum.RES_700.getMsg(), data);
    }


}
