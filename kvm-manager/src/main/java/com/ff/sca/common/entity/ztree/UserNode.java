package com.ff.sca.common.entity.ztree;

import java.util.List;

/**
 * 会员ztree
 *
 * @author GOUWEI
 * @ClassName: ZtreeNode
 * @Description: TODO
 * @date 2018年8月29日
 */
public class UserNode {

    private Integer id;
    private String userCode;
    private String pollCode;
    private String name;
    private Integer status;
    private String isParent;
    private List<UserNode> children;
    private Integer chilCount;

    public String getIsParent() {
        return isParent;
    }

    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPollCode() {
        return pollCode;
    }

    public void setPollCode(String pollCode) {
        this.pollCode = pollCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<UserNode> getChildren() {
        return children;
    }

    public void setChildren(List<UserNode> children) {
        this.children = children;
    }

    public Integer getChilCount() {
        return chilCount;
    }

    public void setChilCount(Integer chilCount) {
        this.chilCount = chilCount;
    }
}
