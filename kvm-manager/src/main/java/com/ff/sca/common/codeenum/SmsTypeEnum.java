package com.ff.sca.common.codeenum;

public enum SmsTypeEnum {

    REGISTER(0, "注册"),
    LOGINPWD(1, "找回登录密码"),
    DEALPWD(2, "找回支付密码"),
    AUTH(3, "实名认证");

    private int type;
    private String name;

    SmsTypeEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (SmsTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
