package com.ff.sca.common.codeenum;

public enum ChangeTypeEnum {

    SHOPPING(1, "购物"),
    MD_RECHARGE(2, "美豆充值"),
    WX_RECHARGE(3, "微信充值"),
    ZFB_RECHARGE(4, "支付宝充值"),
    CASH(5, "提现"),
    GIFT_BOX_RETURN(6, "礼包返利"),
    GIFT_BOX_BONUS(7, "礼包分红"),
    PRODUCT_RETURN(8, "商品返利"),
    PRODUCT_BONUS(9, "商品分红"),
    REFUND(10, "退还"),
    ;

    private int type;
    private String name;

    ChangeTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (ChangeTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
