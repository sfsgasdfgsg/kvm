package com.ff.sca.common.entity.resmsg;

/**
 * 普通消息返回数据模型
 * 
 * @ClassName: ResMsg
 * @Description: TODO
 * @author gw
 * @date 2018年8月29日
 *
 */
public class ResMsg {

	private String code;

	private String msg;

	private Object data;

	public ResMsg() {
		super();
	}

	public ResMsg(String code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}