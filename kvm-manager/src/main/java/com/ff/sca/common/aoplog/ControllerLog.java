package com.ff.sca.common.aoplog;

import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;

import java.lang.annotation.*;

/**
 * 自定义注解
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ControllerLog {
    String description() default "";

    String module();

    OperateType opType();

}