package com.ff.sca.common.codeenum;

public enum CodeEnum {

    HTTP_199("199", "请求参数不合法"),
    HTTP_200("200", "成功"),
    HTTP_400("400", "失败"),
    HTTP_500("500", "服务器错误"),

    LOGIN_601("601", "登录成功"),
    LOGIN_602("602", "登录失败,稍后重试"),
    LOGIN_603("603", "密码错误"),
    LOGIN_604("604", "验证码错误"),
    LOGIN_605("605", "不存在用户"),
    LOGIN_606("606", "密码错误"),
    LOGIN_607("607", "请求错误"),
    LOGIN_608("608", "已经登录"),
    LOGIN_609("609", "未登录"),

    RES_700("700", "已经存在"),
    RES_701("701", "存在子级，不允许删除"),

    TOKEN_801("801", "token已经过期请重新登录"),


    UP_900("900", "文件超出大小限制"),
    UP_901("901", "文件上传为空"),

    ;

    /* 编码 */
    private String code;

    /* 描述消息 */
    private String msg;

    CodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String code() {
        return code;
    }

    public String msg() {
        return msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    // 普通方法
    public static String getMsg(String code) {
        for (CodeEnum en : CodeEnum.values()) {
            if (en.getCode() == code) {
                return en.getMsg();
            }
        }
        return null;
    }
}
