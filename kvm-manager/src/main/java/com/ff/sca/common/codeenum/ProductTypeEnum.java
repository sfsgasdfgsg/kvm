package com.ff.sca.common.codeenum;

public enum ProductTypeEnum {

    PRODUCT(0, "商品"),
    GIFT_BOX(1, "礼包"),

    ;

    private int type;
    private String name;

    ProductTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (ProductTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
