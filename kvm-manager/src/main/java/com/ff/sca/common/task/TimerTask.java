package com.ff.sca.common.task;

import com.ff.kvm.common.third_util.WXPublicUtil;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.HttpUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.sca.admin.service.RedisService;
import com.ff.sca.common.constant.Constant;
import com.ff.sca.common.util.DateUtil;
import com.ff.sca.statistical.service.IKvmReportService;
import com.ff.sca.statistical.service.IStatisticalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 系统定时任务
 *
 * @author gw
 * @ClassName: TimerTask
 * @Description: TODO
 * @date 2018年8月27日
 */
@Component
public class TimerTask {

    @Autowired
    private IStatisticalService statisticalService;
    @Autowired
    private IKvmReportService reportService;
    @Autowired
    private RedisService redisService;

    /**
     * 刷新微信公众号的授权token。（每2个小时执行一次）
     */
//    @Scheduled(cron = "0 0 0/2 * * ?")
//    public void getPublicAccessToken() {
//        redisService.putAppCache(Constants.WX_PUBLIC_ACCESS_TOKEN, WXPublicUtil.getPublicAccessToken());
//        System.out.println("[执行成功]系统定时任务：刷新微信公众号的授权token。执行时间（" + DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss") + "）");
//    }

    /**
     * 定时扫描未领的礼品是否过期（每一个小时执行一次）
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void scanningOverdueGift() {
        int count = statisticalService.scanningOverdueGiftCount();
        if (count > 0) {
            Boolean result = statisticalService.scanningOverdueGift();
            if (result) {
                System.out.println("[执行成功]系统定时任务：扫描过期未领取礼物。执行时间（" + DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss") + "）");
            } else {
                System.out.println("[执行失败]系统定时任务：扫描过期未领取礼物。执行时间（" + DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss") + "）");
            }
        }
    }

    /**
     * 定时统计平台收入支出报表记录（每天深夜零点之前执行统计当天的报表信息）
     */
    @Scheduled(cron = "0 59 23 * * ?")
    public void statisticsReport() {
        Boolean result = reportService.insert(statisticalService.reportStatistical());
        if (result) {
            System.out.println("[执行成功]系统定时任务：统计平台账务报表。执行时间（" + DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss") + "）");
        } else {
            System.out.println("[执行失败]系统定时任务：统计平台账务报表。执行时间（" + DateUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss") + "）");
        }
    }

}
