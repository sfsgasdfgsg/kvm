package com.ff.sca.common.codeenum;

public enum OrderTypeEnum {

    NORMAL(0, "普通订单"),
    GIFTBOX(1, "礼包订单"),
    ;

    private int type;
    private String name;

    OrderTypeEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (OrderTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
