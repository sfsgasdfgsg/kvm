package com.ff.sca.common.entity.ztree;

import java.util.List;

/**
 * @author GOUWEI
 * @ClassName: ZtreeNode
 * @Description: TODO
 * @date 2018年8月29日
 */
public class ZtreeNode {

    private Integer id;
    private Integer pid;
    private String name;
    private Integer type;
    private Boolean isParent;
    private List<ZtreeNode> children;
    private Integer chilCount;
    private String checked;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }

    public List<ZtreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<ZtreeNode> children) {
        this.children = children;
    }

    public Integer getChilCount() {
        return chilCount;
    }

    public void setChilCount(Integer chilCount) {
        this.chilCount = chilCount;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
