package com.ff.sca.common.constant;

public class Constant {
    //系统推荐码
    public static final String sysPollCode = "888888";
    //最小上传文件大小200M
    public static final long limitSize = 1024 * 1024 * 50;
}
