package com.ff.sca.admin.service;


import java.util.concurrent.TimeUnit;

public interface RedisService {

    void putAppCache(String key, String value);

    void putAppCache(String key, String value, long time, TimeUnit t);

    String getAppCache(String key);

    void delCache(String key);

}
