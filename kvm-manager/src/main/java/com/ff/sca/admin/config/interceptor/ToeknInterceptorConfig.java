package com.ff.sca.admin.config.interceptor;

import com.ff.sca.common.filter.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * token拦截器配置拦截路径
 */
@Configuration
public class ToeknInterceptorConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private TokenInterceptor tokenInterceptor;//使用注入的方式，在拦截器中注入的服务才不会为空

    public void addInterceptors(InterceptorRegistry registry) {
        //addPathPatterns 用于添加拦截规则
        //excludePathPatterns 用于排除拦截
        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/scaSysUser/vfcode") //获取验证码
                .excludePathPatterns("/scaSysUser/sysUserLogin")//登录
                .excludePathPatterns("/scaSysUser/loginOut")//注销
                .excludePathPatterns("/scaSysUser/getCurLoginSysUser")//获取登录用户
                .excludePathPatterns("/uf/uploadFile")//文件上传
                .excludePathPatterns("/dealData/data")//数据迁移
                .excludePathPatterns("/dealData/test")//导数据地方
        ;
        super.addInterceptors(registry);
    }

}
