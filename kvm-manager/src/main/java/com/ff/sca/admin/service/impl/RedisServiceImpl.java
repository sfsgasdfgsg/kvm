package com.ff.sca.admin.service.impl;

import com.ff.kvm.common.util.MyUtil;
import com.ff.sca.admin.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void putAppCache(String key, String value) {
        this.redisTemplate.opsForValue().set(key, value);
    }

    @Override
    public void putAppCache(String key, String value, long time, TimeUnit ty) {
        this.redisTemplate.opsForValue().set(key, value);
        this.redisTemplate.expire(key, time, ty);
    }

    @Override
    public String getAppCache(String key) {
        if (MyUtil.isBlank(key)) {
            return null;
        }
        Object obj = this.redisTemplate.opsForValue().get(key);
        if (obj != null) {
            return this.redisTemplate.opsForValue().get(key).toString();
        }
        return null;
    }

    @Override
    public void delCache(String key) {
        if (MyUtil.isBlank(key)) {
            return;
        }
        if (this.redisTemplate.hasKey(key))
            this.redisTemplate.delete(key);
    }

}
