package com.ff.sca.upload.controller;


import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.CodeEnum;
import com.ff.sca.common.constant.Constant;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.entity.resmsg.LayeditData;
import com.ff.sca.upload.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.Iterator;

/**
 * <p>
 * 文件上传
 * </p>
 *
 * @author gw
 * @since 2018-10-10
 */
@Controller
@RequestMapping("/uf")
public class FileUploadController extends BaseController {

    @Autowired
    private UploadService uploadService;


    /**
     * 文件上传
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public ResMsg uploadFile(HttpServletRequest request) {
        try {
            // 创建一个通用的多部分解析器
            CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
            // 判断 request 是否有文件上传,即多部分请求
            if (multipartResolver.isMultipart(request)) {
                // 转换成多部分request
                MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                // 取得request中的所有文件名
                Iterator<String> iterator = multiRequest.getFileNames();
                while (iterator.hasNext()) {
                    // 取得上传文件
                    MultipartFile file = multiRequest.getFile(iterator.next());
                    long fileSize = file.getSize();  //单位b
                    long limitSize = Constant.limitSize; //限制文件大小200
                    if (fileSize > limitSize) {
                        return new ResMsg(CodeEnum.UP_900.getCode(), CodeEnum.UP_900.getMsg(), fileSize);
                    }
                    if (file != null) {
                        InputStream inputStream = file.getInputStream();
                        String originalFileName = file.getOriginalFilename();
                        String fileSuffix = originalFileName.substring(originalFileName.lastIndexOf("."));
                        //String url = uploadService.uploadImg(inputStream, fileSuffix);
                        String url = uploadService.uploadImg2(inputStream, originalFileName);
                        LayeditData data = new LayeditData();
                        data.setSrc(url);
                        data.setTitle(originalFileName);
                        return new ResMsg("0", CodeEnum.HTTP_200.getMsg(), data);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
        return new ResMsg(CodeEnum.UP_901.getCode(), CodeEnum.UP_901.getMsg(), null);
    }

}
