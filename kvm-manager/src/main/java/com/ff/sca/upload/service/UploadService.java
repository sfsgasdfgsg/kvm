package com.ff.sca.upload.service;

import com.qcloud.cos.exception.CosClientException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public interface UploadService {

    public String uploadImg(InputStream inputStream, String fileType);

    public String uploadImg2(InputStream fileInputStream, String name);

}
