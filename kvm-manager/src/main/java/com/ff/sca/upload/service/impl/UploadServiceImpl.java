package com.ff.sca.upload.service.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

import com.ff.sca.common.util.NumberHelper;
import com.ff.sca.upload.service.UploadService;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.region.Region;
import org.springframework.stereotype.Service;

import com.aliyun.oss.OSSClient;

@Service
public class UploadServiceImpl implements UploadService {


    @Override
    public String uploadImg(InputStream inputStream, String fileType) {
        String endpoint = "http://oss-cn-beijing.aliyuncs.com";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录
        // https://ram.console.aliyun.com 创建
        String accessKeyId = "LTAIQw7ZUdneSpbh";
        String accessKeySecret = "NuHUGN3BX87HC1vVYnrKsrzrEHISaJ";

        String name = NumberHelper.generateRandomNum(2) + System.currentTimeMillis();//DataUtil.createData(15);
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject("dwsc", "dwscimg/" + name + fileType, inputStream);
        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 10);
        // 生成URL
        URL url = ossClient.generatePresignedUrl("dwsc", "dwscimg/" + name + fileType, expiration);
        // 关闭client
        ossClient.shutdown();
        return url.toString();
    }


    private static String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
    private static String accessKeyId = "LTAIi9hQC72WfTJ9";
    private static String accessKeySecret = "W381iqTAfXgFYBvigNBP4UmRtLilsQ";
    private static String bucketName = "kvm-mall";
    private static String url = "http://kvm-mall.oss-cn-hangzhou.aliyuncs.com/";

    public static OSSClient getOSSClient() {
        return new OSSClient(endpoint, accessKeyId, accessKeySecret);
    }

    @Override
    public String uploadImg2(InputStream fileInputStream, String name) {
        String fileType = name.substring(name.lastIndexOf("."));
        String fileName = UUID.randomUUID() + fileType;
        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
//        String accessKeyId = "<yourAccessKeyId>";
//        String accessKeySecret = "<yourAccessKeySecret>";
//        String bucketName = "kvm-product";
//        String objectName = "<yourObjectName>";
        OSSClient ossClient = getOSSClient();
        // 创建OSSClient实例。
//        ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        // 上传内容到指定的存储空间（bucketName）并保存为指定的文件名称（objectName）。
        ossClient.putObject(bucketName, fileName, fileInputStream);
        // 关闭OSSClient。
        ossClient.shutdown();
//        StringBuffer url = new StringBuffer(endpoint);

        return url + fileName;
    }
}

