package com.ff.sca.statistical.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.common.util.DateUtil;
import com.ff.sca.product.entity.KvmProductHot;
import com.ff.sca.statistical.entity.DealStatistical;
import com.ff.sca.statistical.entity.KvmReport;
import com.ff.sca.statistical.entity.StatisticalDV;
import com.ff.sca.statistical.service.IKvmReportService;
import com.ff.sca.statistical.service.IStatisticalService;
import com.ff.sca.system.entity.KvmSysCode;
import com.ff.sca.system.service.IKvmSysCodeService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-30
 */
@Controller
@RequestMapping("/statistical")
public class StatisticalController extends BaseController {

    @Autowired
    private IStatisticalService statisticalService;
    @Autowired
    private IKvmSysCodeService sysCodeService;
    @Autowired
    private IKvmReportService reportService;

    /**
     * 报表列表
     *
     * @param page
     * @param size
     * @param startDate
     * @param endDate
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public LayuiTableResMsg report(Integer page, Integer size, String startDate, String endDate) {
        try {
            Page<KvmReport> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmReport> ew = new EntityWrapper();
            /**
             * 商品编号
             */
            if (CharUtil.isEmpty(startDate) && CharUtil.isNotEmpty(endDate)) {
                ew.lt("time", DateUtil.stringToDate(endDate, "yyyy-MM-dd"));
            }
            if (CharUtil.isNotEmpty(startDate) && CharUtil.isEmpty(endDate)) {
                ew.gt("time", DateUtil.stringToDate(startDate, "yyyy-MM-dd"));
            }
            if (CharUtil.isNotEmpty(startDate) && CharUtil.isNotEmpty(endDate)) {
                ew.between("time", DateUtil.stringToDate(startDate, "yyyy-MM-dd"), DateUtil.stringToDate(endDate, "yyyy-MM-dd"));
            }
            Page<KvmReport> suserList = reportService.selectPage(pageEntity, ew);
            long total = reportService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }


    /**
     * 交易趋势统计图
     *
     * @param days
     * @param type
     * @param status
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/dealTrendStatistical", method = RequestMethod.POST)
    public ResMsg dealTrendStatistical(Integer days, Integer type, Integer status, Integer refresh) {
        try {
            if (CharUtil.isEmpty(days) || CharUtil.isEmpty(type) || CharUtil.isEmpty(status)) {
                return resfail(null);
            }
            String redis_statistical_info = redisService.getAppCache("home_page_dealst_info_" + days + type + status);
            if (CharUtil.isNotEmpty(redis_statistical_info) && CharUtil.isEmpty(refresh)) {
                JSONObject redis_ti = JSONObject.toJavaObject(JSONObject.parseObject(redis_statistical_info), JSONObject.class);
                return resSuccess(redis_ti);
            } else {
                JSONObject jsonobj = datacl(statisticalService.orderAmountStatistical(days, type, status), statisticalService.orderCountStatistical(days, type, status));
                KvmSysCode sysCode = sysCodeService.selectOne(new EntityWrapper<KvmSysCode>().eq("name", "sm_redis_cache_time"));
                int time = 10;
                if (sysCode != null) {
                    time = Integer.parseInt(sysCode.getCode());
                }
                redisService.putAppCache("home_page_dealst_info_" + days + type + status, JSONObject.toJSONString(jsonobj), time, TimeUnit.MINUTES);
                return resSuccess(jsonobj);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    private JSONObject datacl(List<DealStatistical> oas, List<DealStatistical> ocs) {
        JSONObject obj = new JSONObject();

        JSONObject obj1 = new JSONObject();
        List<String> title1 = new ArrayList();
        List<String> data1 = new ArrayList();
        if (oas.size() > 0) {
            for (int i = 0; i < oas.size(); ++i) {
                DealStatistical codedto = oas.get(i);
                title1.add(i, codedto.getDateTime());
                data1.add(i, codedto.getNum());
            }
        }
        obj1.put("title", title1);
        obj1.put("data", data1);

        JSONObject obj2 = new JSONObject();
        List<String> title2 = new ArrayList();
        List<String> data2 = new ArrayList();
        if (ocs.size() > 0) {
            for (int i = 0; i < ocs.size(); ++i) {
                DealStatistical codedto = ocs.get(i);
                title2.add(i, codedto.getDateTime());
                data2.add(i, codedto.getNum());
            }
        }
        obj2.put("title", title2);
        obj2.put("data", data2);

        obj.put("dealje", obj1);
        obj.put("ordersl", obj2);
        return obj;
    }


    /**
     * 首页统计信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/homePageStatistical", method = RequestMethod.POST)
    public ResMsg homePageStatistical() {
        try {
            String redis_statistical_info = redisService.getAppCache("home_page_Statistical_info");
            if (CharUtil.isNotEmpty(redis_statistical_info)) {
                StatisticalDV redis_ti = JSONObject.toJavaObject(JSONObject.parseObject(redis_statistical_info), StatisticalDV.class);
                return resSuccess(redis_ti);
            } else {
                StatisticalDV statisticalDV = statisticalService.homePageStatistical();
                if (CharUtil.isEmpty(statisticalDV.getOrderTotalMoney())) {
                    statisticalDV.setOrderTotalMoney(0);
                }
                if (CharUtil.isEmpty(statisticalDV.getCurdateOrderTotalMoney())) {
                    statisticalDV.setCurdateOrderTotalMoney(0);
                }
                KvmSysCode sysCode = sysCodeService.selectOne(new EntityWrapper<KvmSysCode>().eq("name", "sm_redis_cache_time"));
                int time = 10;
                if (sysCode != null) {
                    time = Integer.parseInt(sysCode.getCode());
                }
                redisService.putAppCache("home_page_Statistical_info", JSONObject.toJSONString(statisticalDV), time, TimeUnit.MINUTES);
                return resSuccess(statisticalDV);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

}

