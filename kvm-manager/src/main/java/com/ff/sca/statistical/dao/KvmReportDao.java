package com.ff.sca.statistical.dao;

import com.ff.sca.statistical.entity.KvmReport;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 平台报表记录 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-13
 */
public interface KvmReportDao extends BaseMapper<KvmReport> {

}
