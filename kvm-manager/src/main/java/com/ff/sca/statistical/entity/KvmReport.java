package com.ff.sca.statistical.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 平台报表记录
 * </p>
 *
 * @author gw123
 * @since 2018-12-13
 */
@TableName("kvm_report")
public class KvmReport extends Model<KvmReport> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 统计时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;
    /**
     * 购物
     */
    private Double shoping;
    /**
     * 美豆充值
     */
    @TableField("md_recharge")
    private Double mdRecharge;
    /**
     * 微信充值
     */
    @TableField("wx_recharge")
    private Double wxRecharge;
    /**
     * 支付宝充值
     */
    @TableField("zfb_recharge")
    private Double zfbRecharge;
    /**
     * 提现
     */
    private Double cas;
    /**
     * 礼包返利
     */
    @TableField("gift_box_return")
    private Double giftBoxReturn;
    /**
     * 礼包分红
     */
    @TableField("gift_box_bonus")
    private Double giftBoxBonus;
    /**
     * 商品返利
     */
    @TableField("product_normal_return")
    private Double productNormalReturn;
    /**
     * 商品分红
     */
    @TableField("product_bonus")
    private Double productBonus;
    /**
     * 退还
     */
    private Double refund;
    /**
     * 抢购商品返利
     */
    @TableField("product_rob_return")
    private Double productRobReturn;
    /**
     * 热销商品返利
     */
    @TableField("product_hot_return")
    private Double productHotReturn;
    /**
     * 团队礼包返利
     */
    @TableField("td_gift_box_return")
    private Double tdGiftBoxReturn;
    /**
     * 团队商品分红（团队重销f返利）
     */
    @TableField("td_product_bonus")
    private Double tdProductBonus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getShoping() {
        return shoping;
    }

    public void setShoping(Double shoping) {
        this.shoping = shoping;
    }

    public Double getMdRecharge() {
        return mdRecharge;
    }

    public void setMdRecharge(Double mdRecharge) {
        this.mdRecharge = mdRecharge;
    }

    public Double getWxRecharge() {
        return wxRecharge;
    }

    public void setWxRecharge(Double wxRecharge) {
        this.wxRecharge = wxRecharge;
    }

    public Double getZfbRecharge() {
        return zfbRecharge;
    }

    public void setZfbRecharge(Double zfbRecharge) {
        this.zfbRecharge = zfbRecharge;
    }

    public Double getCas() {
        return cas;
    }

    public void setCas(Double cas) {
        this.cas = cas;
    }

    public Double getGiftBoxReturn() {
        return giftBoxReturn;
    }

    public void setGiftBoxReturn(Double giftBoxReturn) {
        this.giftBoxReturn = giftBoxReturn;
    }

    public Double getGiftBoxBonus() {
        return giftBoxBonus;
    }

    public void setGiftBoxBonus(Double giftBoxBonus) {
        this.giftBoxBonus = giftBoxBonus;
    }

    public Double getProductNormalReturn() {
        return productNormalReturn;
    }

    public void setProductNormalReturn(Double productNormalReturn) {
        this.productNormalReturn = productNormalReturn;
    }

    public Double getProductBonus() {
        return productBonus;
    }

    public void setProductBonus(Double productBonus) {
        this.productBonus = productBonus;
    }

    public Double getRefund() {
        return refund;
    }

    public void setRefund(Double refund) {
        this.refund = refund;
    }

    public Double getProductRobReturn() {
        return productRobReturn;
    }

    public void setProductRobReturn(Double productRobReturn) {
        this.productRobReturn = productRobReturn;
    }

    public Double getProductHotReturn() {
        return productHotReturn;
    }

    public void setProductHotReturn(Double productHotReturn) {
        this.productHotReturn = productHotReturn;
    }

    public Double getTdGiftBoxReturn() {
        return tdGiftBoxReturn;
    }

    public void setTdGiftBoxReturn(Double tdGiftBoxReturn) {
        this.tdGiftBoxReturn = tdGiftBoxReturn;
    }

    public Double getTdProductBonus() {
        return tdProductBonus;
    }

    public void setTdProductBonus(Double tdProductBonus) {
        this.tdProductBonus = tdProductBonus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmReport{" +
        ", id=" + id +
        ", time=" + time +
        ", shoping=" + shoping +
        ", mdRecharge=" + mdRecharge +
        ", wxRecharge=" + wxRecharge +
        ", zfbRecharge=" + zfbRecharge +
        ", cas=" + cas +
        ", giftBoxReturn=" + giftBoxReturn +
        ", giftBoxBonus=" + giftBoxBonus +
        ", productNormalReturn=" + productNormalReturn +
        ", productBonus=" + productBonus +
        ", refund=" + refund +
        ", productRobReturn=" + productRobReturn +
        ", productHotReturn=" + productHotReturn +
        ", tdGiftBoxReturn=" + tdGiftBoxReturn +
        ", tdProductBonus=" + tdProductBonus +
        "}";
    }
}
