package com.ff.sca.statistical.service.impl;

import com.ff.sca.statistical.entity.KvmReport;
import com.ff.sca.statistical.dao.KvmReportDao;
import com.ff.sca.statistical.service.IKvmReportService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台报表记录 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-13
 */
@Service
public class KvmReportServiceImpl extends ServiceImpl<KvmReportDao, KvmReport> implements IKvmReportService {

}
