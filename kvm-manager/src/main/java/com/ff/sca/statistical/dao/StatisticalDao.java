package com.ff.sca.statistical.dao;

import com.ff.sca.common.entity.code.KeyValue;
import com.ff.sca.statistical.entity.DealStatistical;
import com.ff.sca.statistical.entity.KvmReport;
import com.ff.sca.statistical.entity.StatisticalDV;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-30
 */
public interface StatisticalDao {

    public StatisticalDV homePageStatistical();

    public List<DealStatistical> orderCountStatistical(@Param("days") Integer days, @Param("type") Integer type, @Param("status") Integer status);

    public List<DealStatistical> orderAmountStatistical(@Param("days") Integer days, @Param("type") Integer type, @Param("status") Integer status);

    public List<KeyValue> reportStatistical();

    public List<KeyValue> reportStatistical2();

    public boolean scanningOverdueGift();

    public int scanningOverdueGiftCount();

}
