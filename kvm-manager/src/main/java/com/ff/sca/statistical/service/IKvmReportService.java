package com.ff.sca.statistical.service;

import com.ff.sca.statistical.entity.KvmReport;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 平台报表记录 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-13
 */
public interface IKvmReportService extends IService<KvmReport> {

}
