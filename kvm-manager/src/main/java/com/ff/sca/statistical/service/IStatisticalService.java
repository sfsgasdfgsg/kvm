package com.ff.sca.statistical.service;

import com.ff.sca.common.entity.code.KeyValue;
import com.ff.sca.statistical.entity.DealStatistical;
import com.ff.sca.statistical.entity.KvmReport;
import com.ff.sca.statistical.entity.StatisticalDV;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-30
 */
public interface IStatisticalService {

    /**
     * 首页统计信息
     */
    public StatisticalDV homePageStatistical();

    /**
     * 订单数量统计
     */
    public List<DealStatistical> orderCountStatistical(Integer days, Integer type, Integer status);

    /**
     * 订单交易金额统计
     */
    public List<DealStatistical> orderAmountStatistical(Integer days, Integer type, Integer status);

    /**
     * 报表定时统计服务(统计账户)
     *
     * @return
     */
    public KvmReport reportStatistical();

    /**
     * 扫描过期礼物
     *
     * @return
     */
    public Boolean scanningOverdueGift();
    public int scanningOverdueGiftCount();

}
