package com.ff.sca.statistical.service.impl;

import com.ff.sca.common.entity.code.KeyValue;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.statistical.dao.KvmReportDao;
import com.ff.sca.statistical.dao.StatisticalDao;
import com.ff.sca.statistical.entity.DealStatistical;
import com.ff.sca.statistical.entity.KvmReport;
import com.ff.sca.statistical.entity.StatisticalDV;
import com.ff.sca.statistical.service.IStatisticalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-30
 */
@Service
public class StatisticalServiceImpl implements IStatisticalService {
    @Autowired
    private StatisticalDao statisticalDao;

    @Override
    public Boolean scanningOverdueGift() {
        return statisticalDao.scanningOverdueGift();
    }

    @Override
    public int scanningOverdueGiftCount() {
        return statisticalDao.scanningOverdueGiftCount();
    }


    @Override
    public StatisticalDV homePageStatistical() {
        return statisticalDao.homePageStatistical();
    }

    @Override
    public List<DealStatistical> orderCountStatistical(Integer days, Integer type, Integer status) {
        return statisticalDao.orderCountStatistical(days, type, status);
    }

    @Override
    public List<DealStatistical> orderAmountStatistical(Integer days, Integer type, Integer status) {
        return statisticalDao.orderAmountStatistical(days, type, status);
    }

    @Override
    public KvmReport reportStatistical() {
        KvmReport report = new KvmReport();
        report.setTime(new Date());
        /**
         * 统计账户日志表
         */
        List<KeyValue> rs1 = statisticalDao.reportStatistical();
        if (rs1.size() > 0) {
            for (int i = 0; i < rs1.size(); i++) {
                KeyValue keyValue = rs1.get(i);
                if (CharUtil.isNotEmpty(keyValue.getCkey())) {
                    Integer type = Integer.parseInt(keyValue.getCkey());
                    Double value = 0D;
                    if (CharUtil.isNotEmpty(keyValue.getCvalue())) {
                        value = Double.parseDouble(keyValue.getCvalue());
                    }
                    if (type == 1) {
                        report.setShoping(value);
                    } else if (type == 2) {
                        report.setMdRecharge(value);
                    } else if (type == 3) {
                        report.setWxRecharge(value);
                    } else if (type == 4) {
                        report.setZfbRecharge(value);
                    } else if (type == 5) {
                        report.setCas(value);
                    } else if (type == 6) {
                        report.setGiftBoxReturn(value);
                    } else if (type == 7) {
                        report.setGiftBoxBonus(value);
                    } else if (type == 8) {
                        report.setProductHotReturn(value);
                    } else if (type == 9) {
                        report.setProductBonus(value);
                    } else if (type == 10) {
                        report.setRefund(value);
                    } else if (type == 11) {
                        report.setProductRobReturn(value);
                    }
                }
            }

        }
        /**
         * 团队分红日志表
         */
        List<KeyValue> rs2 = statisticalDao.reportStatistical2();
        if (rs2.size() > 0) {
            for (int i = 0; i < rs2.size(); i++) {
                KeyValue keyValue = rs2.get(i);
                if (CharUtil.isNotEmpty(keyValue.getCkey())) {
                    Integer type = Integer.parseInt(keyValue.getCkey());
                    Double value = 0D;
                    if (CharUtil.isNotEmpty(keyValue.getCvalue())) {
                        value = Double.parseDouble(keyValue.getCvalue());
                    }
                    if (type == 1) {
                        report.setTdGiftBoxReturn(value);
                    } else if (type == 0) {
                        report.setTdProductBonus(value);
                    }
                }
            }
        }
        return report;
    }
}
