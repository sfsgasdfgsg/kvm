package com.ff.sca.statistical.entity;

/**
 * <p>
 *
 * </p>
 *
 * @author gw123
 * @since 2018-11-30
 */
public class StatisticalDV {

    /**
     * 注册总数
     */
    private Integer registerTotalNum;
    /**
     * 当日订单总数
     */
    private Integer curdateOrderTotalNum;
    /**
     * 总订单数
     */
    private Integer orderTotalNum;
    /**
     * 当日订单总金额
     */
    private Integer curdateOrderTotalMoney;
    /**
     * 订单总金额
     */
    private Integer orderTotalMoney;


    public Integer getRegisterTotalNum() {
        return registerTotalNum;
    }

    public void setRegisterTotalNum(Integer registerTotalNum) {
        this.registerTotalNum = registerTotalNum;
    }

    public Integer getCurdateOrderTotalNum() {
        return curdateOrderTotalNum;
    }

    public void setCurdateOrderTotalNum(Integer curdateOrderTotalNum) {
        this.curdateOrderTotalNum = curdateOrderTotalNum;
    }

    public Integer getOrderTotalNum() {
        return orderTotalNum;
    }

    public void setOrderTotalNum(Integer orderTotalNum) {
        this.orderTotalNum = orderTotalNum;
    }

    public Integer getCurdateOrderTotalMoney() {
        return curdateOrderTotalMoney;
    }

    public void setCurdateOrderTotalMoney(Integer curdateOrderTotalMoney) {
        this.curdateOrderTotalMoney = curdateOrderTotalMoney;
    }

    public Integer getOrderTotalMoney() {
        return orderTotalMoney;
    }

    public void setOrderTotalMoney(Integer orderTotalMoney) {
        this.orderTotalMoney = orderTotalMoney;
    }
}
