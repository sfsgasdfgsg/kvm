package com.ff.sca.product.service;

import com.ff.sca.product.entity.KvmProductPartition;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品分区 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
public interface IKvmProductPartitionService extends IService<KvmProductPartition> {

}
