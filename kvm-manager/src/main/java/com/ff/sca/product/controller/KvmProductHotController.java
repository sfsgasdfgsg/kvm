package com.ff.sca.product.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.CodeEnum;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.codeenum.ProductTypeEnum;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.product.entity.KvmProduct;
import com.ff.sca.product.entity.KvmProductHot;
import com.ff.sca.product.service.IKvmProductHotService;
import com.ff.sca.product.service.IKvmProductService;
import com.ff.sca.system.entity.KvmSysCode;
import com.ff.sca.system.service.IKvmSysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 热销专区商品 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Controller
@RequestMapping("/kvmProductHot")
public class KvmProductHotController extends BaseController {

    @Autowired
    private IKvmProductHotService productHotService;
    @Autowired
    private IKvmSysCodeService sysCodeService;
    @Autowired
    private IKvmProductService productService;

    /**
     * 查看详情
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productHot", method = RequestMethod.POST)
    public ResMsg productHot(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(productHotService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 热销专区设置首页显示
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "热销专区的商品设置在该专区首页显示",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/setHotPageTopShow", method = RequestMethod.POST)
    public ResMsg setHotPageTopShow(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                int count = productHotService.selectCount(new EntityWrapper<KvmProductHot>().eq("top", 1));
                KvmSysCode code = sysCodeService.selectOne(new EntityWrapper<KvmSysCode>().eq("name", "hot_page_product_num"));
                if (count < Integer.parseInt(code.getCode())) {
                    KvmProductHot phot = productHotService.selectById(id);
                    phot.setTop(1);
                    Boolean result = productHotService.updateById(phot);
                    if (result) {
                        return resSuccess(null);
                    }
                } else {
                    return new ResMsg(CodeEnum.HTTP_400.getCode(), "热销专区首页设置显示商品数量已达到上限，如需继续添加，可在系统参数（hot_page_product_num）设置！", null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    @ControllerLog(
            description = "取消热销专区的商品设置在该专区首页显示",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/setHotPageTopHide", method = RequestMethod.POST)
    public ResMsg setHotPageTopHide(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductHot phot = productHotService.selectById(id);
                phot.setTop(0);
                Boolean result = productHotService.updateById(phot);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改加入热销商品的信息
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改热销商品专区的商品信息",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updProductHot", method = RequestMethod.POST)
    public ResMsg delProductHot(KvmProductHot per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                KvmProductHot oldphot = productHotService.selectById(per.getId());
                per.setCreateTime(oldphot.getCreateTime());
                per.setProductId(oldphot.getProductId());
                per.setProductNo(oldphot.getProductNo());
                Boolean result = productHotService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 删除热销商品信息
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除热销专区商品",
            module = OperateModule.ProductManager,
            opType = OperateType.delete
    )
    @Transactional
    @ResponseBody
    @RequestMapping(value = "/delProductHot", method = RequestMethod.POST)
    public ResMsg delProductHot(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductHot productHot = productHotService.selectById(id);
                Integer productId = productHot.getProductId();
                Boolean result = productHotService.deleteById(id);
                if (result) {
                    KvmProduct product = productService.selectById(productId);
                    product.setType(ProductTypeEnum.PRODUCT.getType());
                    productService.updateById(product);
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 热销商品上架
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "热销商品上架操作",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/productHotPutaway", method = RequestMethod.POST)
    public ResMsg productHotPutaway(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductHot phot = productHotService.selectById(id);
                phot.setStatus(1);
                boolean result = productHotService.updateById(phot);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 热销商品下架
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "热销商品下架操作",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/productHotSoldOut", method = RequestMethod.POST)
    public ResMsg productHotSoldOut(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductHot phot = productHotService.selectById(id);
                phot.setStatus(0);
                boolean result = productHotService.updateById(phot);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 抢购专区商品列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productHotList", method = RequestMethod.POST)
    public LayuiTableResMsg productHotList(Integer page, Integer size, KvmProductHot per) {
        try {
            Page<KvmProductHot> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmProductHot> ew = new EntityWrapper();
            /**
             * 商品编号
             */
            if (CharUtil.isNotEmpty(per.getProductNo())) {
                ew.like("product_no", per.getProductNo());
            }
            /**
             * 商品分区
             */
            if (CharUtil.isNotEmpty(per.getPartitionId())) {
                ew.eq("partition_id", per.getPartitionId());
            }
            //上下架
            if (CharUtil.isNotEmpty(per.getStatus())) {
                ew.eq("status", per.getStatus());
            }
            Page<KvmProductHot> suserList = productHotService.selectPage(pageEntity, ew);
            long total = productHotService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

