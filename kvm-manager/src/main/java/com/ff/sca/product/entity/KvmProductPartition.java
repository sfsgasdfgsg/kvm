package com.ff.sca.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 商品分区
 * </p>
 *
 * @author gw123
 * @since 2018-12-24
 */
@TableName("kvm_product_partition")
public class KvmProductPartition extends Model<KvmProductPartition> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品分区名称
     */
    private String name;
    /**
     * 首页展示背景图
     */
    @TableField("back_ground_img")
    private String backGroundImg;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否删除；0：正常；1：删除；
     */
    private Integer status;
    /**
     * 分区描述
     */
    private String des;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 是否在首页专区模块一起显示0是，1否
     */
    private Integer top;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackGroundImg() {
        return backGroundImg;
    }

    public void setBackGroundImg(String backGroundImg) {
        this.backGroundImg = backGroundImg;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmProductPartition{" +
                ", id=" + id +
                ", name=" + name +
                ", backGroundImg=" + backGroundImg +
                ", sort=" + sort +
                ", status=" + status +
                ", des=" + des +
                ", createTime=" + createTime +
                ", top=" + top +
                "}";
    }
}
