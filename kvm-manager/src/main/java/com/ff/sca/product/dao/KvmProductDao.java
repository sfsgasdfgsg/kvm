package com.ff.sca.product.dao;

import com.ff.sca.product.entity.KvmProduct;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-26
 */
public interface KvmProductDao extends BaseMapper<KvmProduct> {

    public String getMaxProductNo();

}
