package com.ff.sca.product.service;

import com.ff.sca.product.entity.KvmProduct;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-26
 */
public interface IKvmProductService extends IService<KvmProduct> {

    /**
     * 获取最大No
     *
     * @return
     */
    public String getMaxProductNo();

}
