package com.ff.sca.product.dao;

import com.ff.sca.product.entity.KvmGiftProduct;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 签到赠送物品 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface KvmGiftProductDao extends BaseMapper<KvmGiftProduct> {

}
