package com.ff.sca.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 签到赠送物品
 * </p>
 *
 * @author gw123
 * @since 2018-12-20
 */
@TableName("kvm_gift_product")
public class KvmGiftProduct extends Model<KvmGiftProduct> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品id
     */
    @TableField("product_id")
    private Integer productId;
    /**
     * 商品名称
     */
    private String name;
    /**
     * 标题图
     */
    private String icon;
    /**
     * 展示图
     */
    private String img;
    /**
     * 库存
     */
    private Integer stock;
    /**
     * 单价
     */
    private Double price;
    /**
     * 详情展示
     */
    private String detail;
    /**
     * 邮费
     */
    private Double postage;
    /**
     * 领取次数
     */
    @TableField("get_times")
    private Integer getTimes;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String des;
    /**
     * 签到商品是否使用，1使用
     */
    private Integer use;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Double getPostage() {
        return postage;
    }

    public void setPostage(Double postage) {
        this.postage = postage;
    }

    public Integer getGetTimes() {
        return getTimes;
    }

    public void setGetTimes(Integer getTimes) {
        this.getTimes = getTimes;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUse() {
        return use;
    }

    public void setUse(Integer use) {
        this.use = use;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmGiftProduct{" +
        ", id=" + id +
        ", productId=" + productId +
        ", name=" + name +
        ", icon=" + icon +
        ", img=" + img +
        ", stock=" + stock +
        ", price=" + price +
        ", detail=" + detail +
        ", postage=" + postage +
        ", getTimes=" + getTimes +
        ", sort=" + sort +
        ", des=" + des +
        ", use=" + use +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
