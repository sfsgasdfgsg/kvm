package com.ff.sca.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 热销专区商品
 * </p>
 *
 * @author gw123
 * @since 2018-12-06
 */
@TableName("kvm_product_hot")
public class KvmProductHot extends Model<KvmProductHot> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品id
     */
    @TableField("product_id")
    private Integer productId;
    /**
     * 商品编号
     */
    @TableField("product_no")
    private String productNo;
    /**
     * 此状态，在热销专区是否上下架状态，非基本商品上下架状态;
0：下架
1：上架
     */
    private Integer status;
    /**
     * 热销价格
     */
    @TableField("price_hot")
    private Double priceHot;
    /**
     * 分区id
     */
    @TableField("partition_id")
    private Integer partitionId;
    /**
     * 是否在该专区首页显示；0：不展示，1展示
     */
    private Integer top;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getPriceHot() {
        return priceHot;
    }

    public void setPriceHot(Double priceHot) {
        this.priceHot = priceHot;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(Integer partitionId) {
        this.partitionId = partitionId;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmProductHot{" +
        ", id=" + id +
        ", productId=" + productId +
        ", productNo=" + productNo +
        ", status=" + status +
        ", priceHot=" + priceHot +
        ", partitionId=" + partitionId +
        ", top=" + top +
        ", createTime=" + createTime +
        "}";
    }
}
