package com.ff.sca.product.service;

import com.ff.sca.product.entity.KvmProductHot;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 热销专区商品 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
public interface IKvmProductHotService extends IService<KvmProductHot> {

}
