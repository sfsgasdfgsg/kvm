package com.ff.sca.product.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author gw123
 * @since 2018-12-14
 */
@TableName("kvm_product")
public class KvmProduct extends Model<KvmProduct> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品名称
     */
    private String name;
    /**
     * 等级
     */
    private Integer lv;
    /**
     * 编号
     */
    @TableField("product_no")
    private String productNo;
    /**
     * 历史库存
     */
    private Integer stock;
    /**
     * 当前库存
     */
    @TableField("current_stock")
    private Integer currentStock;
    /**
     * 商品分类（商品分区）
     */
    private Integer category;
    /**
     * 0普通商品，1礼包
     */
    private Integer type;
    /**
     * 单价
     */
    private Double price;
    private String img;
    /**
     * 详情展示
     */
    private String detail;
    /**
     * 参数
     */
    private String param;
    /**
     * 购买须知
     */
    @TableField("buy_notice")
    private String buyNotice;
    /**
     * 抢购价
     */
    @TableField("rob_price")
    private Double robPrice;
    /**
     * 热销价
     */
    @TableField("hot_price")
    private Double hotPrice;
    /**
     * 礼包享受折扣
     */
    private Double discount;
    /**
     * 展示图
     */
    private String icon;
    /**
     * 套餐销售
     */
    private String items;
    /**
     * 状态：0下架，1上架
     */
    private Integer status;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否首页展示：0否1是
     */
    private Integer top;
    /**
     * 邮费
     */
    @TableField("post_fee")
    private Double postFee;
    /**
     * 月销量
     */
    @TableField("month_sale")
    private Integer monthSale;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 简单描述
     */
    private String remark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLv() {
        return lv;
    }

    public void setLv(Integer lv) {
        this.lv = lv;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(Integer currentStock) {
        this.currentStock = currentStock;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getBuyNotice() {
        return buyNotice;
    }

    public void setBuyNotice(String buyNotice) {
        this.buyNotice = buyNotice;
    }

    public Double getRobPrice() {
        return robPrice;
    }

    public void setRobPrice(Double robPrice) {
        this.robPrice = robPrice;
    }

    public Double getHotPrice() {
        return hotPrice;
    }

    public void setHotPrice(Double hotPrice) {
        this.hotPrice = hotPrice;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public Double getPostFee() {
        return postFee;
    }

    public void setPostFee(Double postFee) {
        this.postFee = postFee;
    }

    public Integer getMonthSale() {
        return monthSale;
    }

    public void setMonthSale(Integer monthSale) {
        this.monthSale = monthSale;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmProduct{" +
        ", id=" + id +
        ", name=" + name +
        ", lv=" + lv +
        ", productNo=" + productNo +
        ", stock=" + stock +
        ", currentStock=" + currentStock +
        ", category=" + category +
        ", type=" + type +
        ", price=" + price +
        ", img=" + img +
        ", detail=" + detail +
        ", param=" + param +
        ", buyNotice=" + buyNotice +
        ", robPrice=" + robPrice +
        ", hotPrice=" + hotPrice +
        ", discount=" + discount +
        ", icon=" + icon +
        ", items=" + items +
        ", status=" + status +
        ", sort=" + sort +
        ", top=" + top +
        ", postFee=" + postFee +
        ", monthSale=" + monthSale +
        ", createTime=" + createTime +
        ", remark=" + remark +
        "}";
    }
}
