package com.ff.sca.product.service.impl;

import com.ff.sca.product.entity.KvmProductHot;
import com.ff.sca.product.dao.KvmProductHotDao;
import com.ff.sca.product.service.IKvmProductHotService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 热销专区商品 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Service
public class KvmProductHotServiceImpl extends ServiceImpl<KvmProductHotDao, KvmProductHot> implements IKvmProductHotService {

}
