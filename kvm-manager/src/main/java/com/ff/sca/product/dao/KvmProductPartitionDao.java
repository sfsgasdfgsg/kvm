package com.ff.sca.product.dao;

import com.ff.sca.product.entity.KvmProductPartition;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品分区 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
public interface KvmProductPartitionDao extends BaseMapper<KvmProductPartition> {

}
