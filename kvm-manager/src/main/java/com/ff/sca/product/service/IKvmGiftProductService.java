package com.ff.sca.product.service;

import com.ff.sca.product.entity.KvmGiftProduct;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 签到赠送物品 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface IKvmGiftProductService extends IService<KvmGiftProduct> {

}
