package com.ff.sca.product.dao;

import com.ff.sca.product.entity.KvmProductHot;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 热销专区商品 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
public interface KvmProductHotDao extends BaseMapper<KvmProductHot> {

}
