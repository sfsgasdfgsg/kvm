package com.ff.sca.product.service.impl;

import com.ff.sca.product.entity.KvmGiftProduct;
import com.ff.sca.product.dao.KvmGiftProductDao;
import com.ff.sca.product.service.IKvmGiftProductService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到赠送物品 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Service
public class KvmGiftProductServiceImpl extends ServiceImpl<KvmGiftProductDao, KvmGiftProduct> implements IKvmGiftProductService {

}
