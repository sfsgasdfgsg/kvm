package com.ff.sca.product.service.impl;

import com.ff.sca.product.entity.KvmProductRob;
import com.ff.sca.product.dao.KvmProductRobDao;
import com.ff.sca.product.service.IKvmProductRobService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抢购专区商品 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Service
public class KvmProductRobServiceImpl extends ServiceImpl<KvmProductRobDao, KvmProductRob> implements IKvmProductRobService {

}
