package com.ff.sca.product.service.impl;

import com.ff.sca.product.entity.KvmProductPartition;
import com.ff.sca.product.dao.KvmProductPartitionDao;
import com.ff.sca.product.service.IKvmProductPartitionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分区 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Service
public class KvmProductPartitionServiceImpl extends ServiceImpl<KvmProductPartitionDao, KvmProductPartition> implements IKvmProductPartitionService {

}
