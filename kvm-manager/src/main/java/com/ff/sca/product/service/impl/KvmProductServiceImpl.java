package com.ff.sca.product.service.impl;

import com.ff.sca.common.util.CharUtil;
import com.ff.sca.product.entity.KvmProduct;
import com.ff.sca.product.dao.KvmProductDao;
import com.ff.sca.product.service.IKvmProductService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-26
 */
@Service
public class KvmProductServiceImpl extends ServiceImpl<KvmProductDao, KvmProduct> implements IKvmProductService {

    @Autowired
    private KvmProductDao productDao;

    @Override
    public String getMaxProductNo() {
        String maxNo = productDao.getMaxProductNo();
        if (CharUtil.isNotEmpty(maxNo)) {
            maxNo = maxNo.substring(3);
            return "No-" + String.valueOf(Integer.parseInt(maxNo) + 1);
        } else {
            return "No-10000001";
        }
    }


}
