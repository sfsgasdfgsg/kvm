package com.ff.sca.product.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.product.entity.KvmGiftProduct;
import com.ff.sca.product.entity.KvmProduct;
import com.ff.sca.product.service.IKvmGiftProductService;
import com.ff.sca.product.service.IKvmProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * <p>
 * 签到赠送物品 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Controller
@RequestMapping("/kvmGiftProduct")
public class KvmGiftProductController extends BaseController {

    @Autowired
    private IKvmGiftProductService giftProductService;
    @Autowired
    private IKvmProductService productService;


    @ResponseBody
    @RequestMapping(value = "/gift", method = RequestMethod.POST)
    public ResMsg gift(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(giftProductService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 查询签到奖品列
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getGifts", method = RequestMethod.POST)
    public ResMsg getGifts() {
        try {
            return resSuccess(giftProductService.selectList(null));
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改奖品信息
     *
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updGiftProduct", method = RequestMethod.POST)
    public ResMsg updGiftProduct(KvmGiftProduct per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                KvmGiftProduct giftProduct = giftProductService.selectById(per.getId());
                per.setCreateTime(giftProduct.getCreateTime());
                per.setGetTimes(giftProduct.getGetTimes());
                per.setUpdateTime(new Date());
                Boolean result = giftProductService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除奖品
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delGiftProduct", method = RequestMethod.POST)
    public ResMsg delGiftProduct(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmGiftProduct giftProduct = giftProductService.selectById(id);
                if (giftProduct != null) {
                    KvmProduct product = productService.selectById(giftProduct.getProductId());
                    boolean result = giftProductService.deleteById(id);
                    if (result) {
                        product.setType(0);
                        productService.updateById(product);
                        return resSuccess(null);
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 奖品列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/giftProductList", method = RequestMethod.POST)
    public LayuiTableResMsg giftProductList(Integer page, Integer size, KvmGiftProduct per) {
        try {
            Page<KvmGiftProduct> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmGiftProduct> ew = new EntityWrapper();
            if (CharUtil.isNotEmpty(per.getName())) {
                ew.like("name", per.getName());
            }
            ew.orderBy("sort", true);
            Page<KvmGiftProduct> suserList = giftProductService.selectPage(pageEntity, ew);
            long total = giftProductService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

