package com.ff.sca.product.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.CodeEnum;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.codeenum.ProductTypeEnum;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.common.util.DateUtil;
import com.ff.sca.product.entity.KvmProduct;
import com.ff.sca.product.entity.KvmProductHot;
import com.ff.sca.product.entity.KvmProductRob;
import com.ff.sca.product.service.IKvmProductRobService;
import com.ff.sca.product.service.IKvmProductService;
import com.ff.sca.system.entity.KvmSysCode;
import com.ff.sca.system.service.IKvmSysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 抢购专区商品 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Controller
@RequestMapping("/kvmProductRob")
public class KvmProductRobController extends BaseController {

    @Autowired
    private IKvmProductRobService productRobService;
    @Autowired
    private IKvmProductService productService;
    @Autowired
    private IKvmSysCodeService sysCodeService;

    /**
     * 查看详情
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productRob", method = RequestMethod.POST)
    public ResMsg productRob(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(productRobService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 抢购专区设置首页显示
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "设置抢购专区商品为该专区首页显示",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/setRobPageTopShow", method = RequestMethod.POST)
    public ResMsg setRobPageTopShow(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                int count = productRobService.selectCount(new EntityWrapper<KvmProductRob>().eq("top", 1));
                KvmSysCode code = sysCodeService.selectOne(new EntityWrapper<KvmSysCode>().eq("name", "rob_page_product_num"));
                if (count < Integer.parseInt(code.getCode())) {
                    KvmProductRob phot = productRobService.selectById(id);
                    phot.setTop(1);
                    Boolean result = productRobService.updateById(phot);
                    if (result) {
                        return resSuccess(null);
                    }
                } else {
                    return new ResMsg(CodeEnum.HTTP_400.getCode(), "抢购专区首页设置显示商品数量已达到上限，如需继续添加，可在系统参数（rob_page_product_num）设置！", null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    @ControllerLog(
            description = "取消抢购专区的商品设置在该专区首页显示",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/setRobPageTopHide", method = RequestMethod.POST)
    public ResMsg setRobPageTopHide(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductRob prob = productRobService.selectById(id);
                prob.setTop(0);
                Boolean result = productRobService.updateById(prob);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改加入抢购商品的信息
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改抢购专区商品信息",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updProductRob", method = RequestMethod.POST)
    public ResMsg updProductRob(KvmProductRob per, String startDate, String endDate) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                if (CharUtil.isNotEmpty(startDate)) {
                    per.setStartTime(DateUtil.stringToDate(startDate, "yyyy-MM-dd HH:mm:ss"));
                }
                if (CharUtil.isNotEmpty(endDate)) {
                    per.setEndTime(DateUtil.stringToDate(endDate, "yyyy-MM-dd HH:mm:ss"));
                }
                KvmProductRob oldphot = productRobService.selectById(per.getId());
                per.setCreateTime(oldphot.getCreateTime());
                per.setProductId(oldphot.getProductId());
                per.setProductNo(oldphot.getProductNo());
                Boolean result = productRobService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 删除抢购商品信息
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除抢购专区商品",
            module = OperateModule.ProductManager,
            opType = OperateType.delete
    )
    @Transactional
    @ResponseBody
    @RequestMapping(value = "/delProductRob", method = RequestMethod.POST)
    public ResMsg delProductRob(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductRob productRob = productRobService.selectById(id);
                Integer productId = productRob.getProductId();
                Boolean result = productRobService.deleteById(id);
                if (result) {
                    KvmProduct product = productService.selectById(productId);
                    product.setType(ProductTypeEnum.PRODUCT.getType());
                    productService.updateById(product);
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 抢购商品上架
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "抢购专区商品上架",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/productRobPutaway", method = RequestMethod.POST)
    public ResMsg productRobPutaway(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductRob phot = productRobService.selectById(id);
                phot.setStatus(1);
                boolean result = productRobService.updateById(phot);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 抢购商品下架
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "抢购专区商品下架",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/productRobSoldOut", method = RequestMethod.POST)
    public ResMsg productRobSoldOut(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductRob phot = productRobService.selectById(id);
                phot.setStatus(0);
                boolean result = productRobService.updateById(phot);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 抢购专区商品列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productRobList", method = RequestMethod.POST)
    public LayuiTableResMsg productRobList(Integer page, Integer size, KvmProductRob per) {
        try {
            Page<KvmProductRob> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmProductRob> ew = new EntityWrapper();
            /**
             * 商品编号
             */
            if (CharUtil.isNotEmpty(per.getProductNo())) {
                ew.like("product_no", per.getProductNo());
            }
            /**
             * 商品分区
             */
            if (CharUtil.isNotEmpty(per.getPartitionId())) {
                ew.eq("partition_id", per.getPartitionId());
            }
            //上下架
            if (CharUtil.isNotEmpty(per.getStatus())) {
                ew.eq("status", per.getStatus());
            }
            Page<KvmProductRob> suserList = productRobService.selectPage(pageEntity, ew);
            long total = productRobService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }
}

