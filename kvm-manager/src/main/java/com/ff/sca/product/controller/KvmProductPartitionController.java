package com.ff.sca.product.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.product.entity.KvmProductPartition;
import com.ff.sca.product.service.IKvmProductPartitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * <p>
 * 商品分区 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Controller
@RequestMapping("/kvmProductPartition")
public class KvmProductPartitionController extends BaseController {

    @Autowired
    private IKvmProductPartitionService productPartitionService;

    /**
     * 分区下拉框
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productPartitions", method = RequestMethod.POST)
    public ResMsg productPartitions() {
        try {
            return resSuccess(productPartitionService.selectList(new EntityWrapper<KvmProductPartition>().eq("status", 0).orderBy("sort", true)));
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 查看分区信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productPartition", method = RequestMethod.POST)
    public ResMsg productPartition(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(productPartitionService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 新增分区
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增分区",
            module = OperateModule.ProductManager,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addProductPartition", method = RequestMethod.POST)
    public ResMsg addProductPartition(KvmProductPartition per) {
        try {
            if (CharUtil.isNotEmpty(per.getName())) {
                per.setCreateTime(new Date());
                per.setStatus(0);
                boolean result = productPartitionService.insert(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改商品分区信息
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改分区信息",
            module = OperateModule.ProductManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updProductPartition", method = RequestMethod.POST)
    public ResMsg delProductPartition(KvmProductPartition per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                KvmProductPartition productPartition = productPartitionService.selectById(per.getId());
                per.setStatus(productPartition.getStatus());
                per.setCreateTime(productPartition.getCreateTime());
                boolean result = productPartitionService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除分区
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除分区",
            module = OperateModule.ProductManager,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delProductPartition", method = RequestMethod.POST)
    public ResMsg delProductPartition(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmProductPartition productPartition = productPartitionService.selectById(id);
                productPartition.setStatus(1);
                boolean result = productPartitionService.updateById(productPartition);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 分区列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/productPartitionList", method = RequestMethod.POST)
    public LayuiTableResMsg productPartitionList(Integer page, Integer size, KvmProductPartition per) {
        try {
            Page<KvmProductPartition> pageEntity = setPageInfo(page, size);
            Page<KvmProductPartition> list = productPartitionService.selectPage(pageEntity, null);
            long total = productPartitionService.selectCount(null);
            return resTableSuccess(total, list.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }


}

