package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.admin.service.RedisService;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.system.entity.KvmSysCode;
import com.ff.sca.system.service.IKvmSysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 系统码表 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
@Controller
@RequestMapping("/kvmSysCode")
public class KvmSysCodeController extends BaseController {

    @Autowired
    private IKvmSysCodeService sysCodeService;
    @Autowired
    private RedisService redisService;

    
    /**
     * 查看直推信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/userGift", method = RequestMethod.POST)
    public ResMsg userGift() {
        try {
            return resSuccess(sysCodeService.selectOne(new EntityWrapper<KvmSysCode>().eq("name", "user_gift")));
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 配置直推奖品
     *
     * @param value
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/setUserGift", method = RequestMethod.POST)
    public ResMsg setUserGift(String value) {
        try {
            if (CharUtil.isNotEmpty(value)) {
                if (CharUtil.isNotEmpty(value) && value.split("-").length == 4) {
                    EntityWrapper<KvmSysCode> ew = new EntityWrapper();
                    ew.eq("name", "user_gift");
                    KvmSysCode code = new KvmSysCode();
                    code.setName("user_gift");
                    code.setCode(value);
                    boolean result = sysCodeService.update(code, ew);
                    if (result) {
                        return resSuccess(null);
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 修改code
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改系统参数",
            module = OperateModule.System,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updNotice", method = RequestMethod.POST)
    public ResMsg updNotice(KvmSysCode per) {
        try {
            if (CharUtil.isNotEmpty(per.getName())) {
                EntityWrapper<KvmSysCode> ew = new EntityWrapper();
                ew.eq("name", per.getName());
                boolean result = sysCodeService.update(per, ew);
                if (result) {
                    redisService.putAppCache(per.getName(), per.getCode());
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * code列表
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/sysCodeList", method = RequestMethod.POST)
    public ResMsg sysCodeList() {
        try {
            List<KvmSysCode> list = sysCodeService.selectList(null);
            return resSuccess(list);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

}

