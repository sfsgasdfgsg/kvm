package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.suser.entity.ScaSysUser;
import com.ff.sca.system.entity.KvmNotice;
import com.ff.sca.system.service.IKvmNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 公告 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
@Controller
@RequestMapping("/kvmNotice")
public class KvmNoticeController extends BaseController {

    @Autowired
    private IKvmNoticeService noticeService;

    @ResponseBody
    @RequestMapping(value = "/notice", method = RequestMethod.POST)
    public ResMsg notice(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmNotice result = noticeService.selectById(id);
                return resSuccess(result);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 新增公告
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增公告",
            module = OperateModule.System,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addNotice", method = RequestMethod.POST)
    public ResMsg delNotice(HttpServletRequest request, KvmNotice per) {
        try {
            if (CharUtil.isNotEmpty(per)) {
                ScaSysUser curUser = getCurLoginSysUser(request);
                per.setAuthor(curUser.getUserName());
                per.setCreateTime(new Date());
                boolean result = noticeService.insert(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改公告
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改公告信息",
            module = OperateModule.System,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updNotice", method = RequestMethod.POST)
    public ResMsg updNotice(KvmNotice per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                boolean result = noticeService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除公告
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除公告",
            module = OperateModule.System,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delNotice", method = RequestMethod.POST)
    public ResMsg delNotice(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                boolean result = noticeService.deleteById(id);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 公告列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/noticeList", method = RequestMethod.POST)
    public LayuiTableResMsg noticeList(Integer page, Integer size, KvmNotice per) {
        try {
            Page<KvmNotice> pageEntity = setPageInfo(page, size);
            Map<String, Object> map = new HashMap();

            Page<KvmNotice> suserList = noticeService.selectPage(pageEntity, new EntityWrapper<KvmNotice>().allEq(map));
            long total = noticeService.selectCount(new EntityWrapper<KvmNotice>().allEq(map));
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

