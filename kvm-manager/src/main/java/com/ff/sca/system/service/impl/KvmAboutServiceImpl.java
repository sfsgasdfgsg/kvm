package com.ff.sca.system.service.impl;

import com.ff.sca.system.entity.KvmAbout;
import com.ff.sca.system.dao.KvmAboutDao;
import com.ff.sca.system.service.IKvmAboutService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 关于我们 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
@Service
public class KvmAboutServiceImpl extends ServiceImpl<KvmAboutDao, KvmAbout> implements IKvmAboutService {

}
