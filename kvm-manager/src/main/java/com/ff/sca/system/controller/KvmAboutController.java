package com.ff.sca.system.controller;


import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.system.entity.KvmAbout;
import com.ff.sca.system.service.IKvmAboutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 关于我们 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
@Controller
@RequestMapping("/kvmAbout")
public class KvmAboutController extends BaseController {

    @Autowired
    private IKvmAboutService aboutService;

    /**
     * 修改关于信息
     *
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updAbout", method = RequestMethod.POST)
    public ResMsg updAbout(KvmAbout per) {
        try {
            per.setId(1);
            Boolean result = aboutService.updateById(per);
            if (result) {
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 查看关于信息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/abount", method = RequestMethod.POST)
    public ResMsg abount() {
        try {
            return resSuccess(aboutService.selectById(1));
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


}

