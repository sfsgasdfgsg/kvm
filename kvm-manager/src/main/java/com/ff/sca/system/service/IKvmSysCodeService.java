package com.ff.sca.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.ff.sca.system.entity.KvmSysCode;

/**
 * <p>
 * 系统码表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
public interface IKvmSysCodeService extends IService<KvmSysCode> {

}
