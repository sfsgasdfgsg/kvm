package com.ff.sca.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 签到礼物发放时间节点配置
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@TableName("kvm_signin_gift_seting")
public class KvmSigninGiftSeting extends Model<KvmSigninGiftSeting> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 连续签到第几天(不能大于系统配置最大签到天数)
     */
    @TableField("sign_in_no")
    private Integer signInNo;
    /**
     * 领取奖品id
     */
    @TableField("gift_product_id")
    private Integer giftProductId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSignInNo() {
        return signInNo;
    }

    public void setSignInNo(Integer signInNo) {
        this.signInNo = signInNo;
    }

    public Integer getGiftProductId() {
        return giftProductId;
    }

    public void setGiftProductId(Integer giftProductId) {
        this.giftProductId = giftProductId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmSigninGiftSeting{" +
        ", id=" + id +
        ", signInNo=" + signInNo +
        ", giftProductId=" + giftProductId +
        "}";
    }
}
