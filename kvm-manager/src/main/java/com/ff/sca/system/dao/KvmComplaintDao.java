package com.ff.sca.system.dao;

import com.ff.sca.system.entity.KvmComplaint;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 投诉 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
public interface KvmComplaintDao extends BaseMapper<KvmComplaint> {

}
