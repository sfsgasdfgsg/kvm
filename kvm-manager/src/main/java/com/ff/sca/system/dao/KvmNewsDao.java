package com.ff.sca.system.dao;

import com.ff.sca.system.entity.KvmNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 头条 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
public interface KvmNewsDao extends BaseMapper<KvmNews> {

}
