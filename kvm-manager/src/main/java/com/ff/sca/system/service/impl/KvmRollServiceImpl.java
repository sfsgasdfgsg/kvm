package com.ff.sca.system.service.impl;

import com.ff.sca.system.entity.KvmRoll;
import com.ff.sca.system.dao.KvmRollDao;
import com.ff.sca.system.service.IKvmRollService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页轮播 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-21
 */
@Service
public class KvmRollServiceImpl extends ServiceImpl<KvmRollDao, KvmRoll> implements IKvmRollService {

}
