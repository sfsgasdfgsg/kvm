package com.ff.sca.system.service;

import com.ff.sca.system.entity.KvmHelpCenter;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 帮助中心 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
public interface IKvmHelpCenterService extends IService<KvmHelpCenter> {

}
