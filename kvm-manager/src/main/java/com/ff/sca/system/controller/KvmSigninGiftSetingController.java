package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.system.entity.KvmSigninGiftSeting;
import com.ff.sca.system.service.IKvmSigninGiftSetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 签到礼物发放时间节点配置 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@Controller
@RequestMapping("/kvmSigninGiftSeting")
public class KvmSigninGiftSetingController extends BaseController {

    @Autowired
    private IKvmSigninGiftSetingService signinGiftSetingService;

    /**
     * 删除配置
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除签到获取商品时间节点配置",
            module = OperateModule.System,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delSigninGiftSeting", method = RequestMethod.POST)
    public ResMsg delSigninGiftSeting(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                Boolean result = signinGiftSetingService.deleteById(id);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 新增配置
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增签到获取商品时间节点配置",
            module = OperateModule.System,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/addSigninGiftSeting", method = RequestMethod.POST)
    public ResMsg addSigninGiftSeting(KvmSigninGiftSeting per) {
        try {
            int count = signinGiftSetingService.selectCount(new EntityWrapper<KvmSigninGiftSeting>().eq("sign_in_no", per.getSignInNo()));
            if (count == 0) {
                Boolean result = signinGiftSetingService.insert(per);
                if (result) {
                    return resSuccess(null);
                }
            } else {
                return resAreadyExisted(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 礼包领取节点list
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/signinGiftSetings", method = RequestMethod.POST)
    public ResMsg signinGiftSetings() {
        try {
            return resSuccess(signinGiftSetingService.selectList(new EntityWrapper<KvmSigninGiftSeting>().orderBy("sign_in_no", true)));
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

}

