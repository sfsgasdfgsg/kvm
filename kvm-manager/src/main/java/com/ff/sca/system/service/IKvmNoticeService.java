package com.ff.sca.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.ff.sca.system.entity.KvmNotice;

/**
 * <p>
 * 公告 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
public interface IKvmNoticeService extends IService<KvmNotice> {

}
