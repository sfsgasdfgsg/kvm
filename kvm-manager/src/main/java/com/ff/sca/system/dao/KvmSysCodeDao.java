package com.ff.sca.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.system.entity.KvmSysCode;

/**
 * <p>
 * 系统码表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
public interface KvmSysCodeDao extends BaseMapper<KvmSysCode> {

}
