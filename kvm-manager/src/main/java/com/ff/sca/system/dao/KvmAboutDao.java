package com.ff.sca.system.dao;

import com.ff.sca.system.entity.KvmAbout;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 关于我们 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
public interface KvmAboutDao extends BaseMapper<KvmAbout> {

}
