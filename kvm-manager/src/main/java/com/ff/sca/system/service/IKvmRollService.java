package com.ff.sca.system.service;

import com.ff.sca.system.entity.KvmRoll;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 首页轮播 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-21
 */
public interface IKvmRollService extends IService<KvmRoll> {

}
