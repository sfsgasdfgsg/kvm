package com.ff.sca.system.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ff.sca.system.dao.KvmSysCodeDao;
import com.ff.sca.system.entity.KvmSysCode;
import com.ff.sca.system.service.IKvmSysCodeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统码表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
@Service
public class KvmSysCodeServiceImpl extends ServiceImpl<KvmSysCodeDao, KvmSysCode> implements IKvmSysCodeService {

}
