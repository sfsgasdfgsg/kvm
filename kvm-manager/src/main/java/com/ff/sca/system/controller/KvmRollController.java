package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.system.entity.KvmRoll;
import com.ff.sca.system.service.IKvmRollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 首页轮播 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-21
 */
@Controller
@RequestMapping("/kvmRoll")
public class KvmRollController extends BaseController {

    @Autowired
    private IKvmRollService rollService;


    /**
     * 查看轮播信息
     */
    @ResponseBody
    @RequestMapping(value = "/rollByproductId", method = RequestMethod.POST)
    public ResMsg rollByproductId(Integer prodcutId) {
        try {
            if (CharUtil.isNotEmpty(prodcutId)) {
                return resSuccess(rollService.selectOne(new EntityWrapper<KvmRoll>().eq("product_id", prodcutId)));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 新增轮播
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增首页轮播",
            module = OperateModule.System,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addRoll", method = RequestMethod.POST)
    public ResMsg roll(KvmRoll per) {
        try {
            if (CharUtil.isNotEmpty(per.getProductId())) {
                List<KvmRoll> rollList = rollService.selectList(new EntityWrapper<KvmRoll>().eq("product_id", per.getProductId()));
                if (rollList.size() == 0) {
                    if (CharUtil.isEmpty(per.getSort())) {
                        per.setSort(0);
                    }
                    Boolean result = rollService.insert(per);
                    if (result) {
                        return resSuccess(null);
                    }
                } else {
                    return resAreadyExisted(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改首页轮播
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改首页轮播",
            module = OperateModule.System,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/updRoll", method = RequestMethod.POST)
    public ResMsg updRoll(KvmRoll per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                boolean result = rollService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除首页轮播
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除首页轮播",
            module = OperateModule.System,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/delRoll", method = RequestMethod.POST)
    public ResMsg delRoll(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                boolean result = rollService.deleteById(id);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 首页轮播列表（不分页）
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/rollList", method = RequestMethod.POST)
    public LayuiTableResMsg rollList() {
        try {
            List<KvmRoll> list = rollService.selectList(null);
            long total = rollService.selectCount(null);
            return resTableSuccess(total, list);
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

