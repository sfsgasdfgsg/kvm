package com.ff.sca.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.system.entity.KvmNotice;

/**
 * <p>
 * 公告 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
public interface KvmNoticeDao extends BaseMapper<KvmNotice> {

}
