package com.ff.sca.system.service;

import com.ff.sca.system.entity.KvmAbout;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 关于我们 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
public interface IKvmAboutService extends IService<KvmAbout> {

}
