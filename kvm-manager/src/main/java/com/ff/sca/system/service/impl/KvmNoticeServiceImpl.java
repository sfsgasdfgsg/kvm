package com.ff.sca.system.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ff.sca.system.dao.KvmNoticeDao;
import com.ff.sca.system.entity.KvmNotice;
import com.ff.sca.system.service.IKvmNoticeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公告 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
@Service
public class KvmNoticeServiceImpl extends ServiceImpl<KvmNoticeDao, KvmNotice> implements IKvmNoticeService {

}
