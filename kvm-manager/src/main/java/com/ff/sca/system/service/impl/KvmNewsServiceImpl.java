package com.ff.sca.system.service.impl;

import com.ff.sca.system.entity.KvmNews;
import com.ff.sca.system.dao.KvmNewsDao;
import com.ff.sca.system.service.IKvmNewsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 头条 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@Service
public class KvmNewsServiceImpl extends ServiceImpl<KvmNewsDao, KvmNews> implements IKvmNewsService {

}
