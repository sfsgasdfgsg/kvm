package com.ff.sca.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 系统码表
 * </p>
 *
 * @author gw123
 * @since 2018-09-30
 */
@TableName("kvm_sys_code")
public class KvmSysCode extends Model<KvmSysCode> {

    private static final long serialVersionUID = 1L;

    /**
     * 键
     */
    private String name;
    /**
     * 值
     */
    private String code;
    /**
     * 描述
     */
    private String des;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Override
    protected Serializable pkVal() {
        return this.name;
    }

    @Override
    public String toString() {
        return "ScaSysCode{" +
                ", name=" + name +
                ", code=" + code +
                ", des=" + des +
                "}";
    }
}
