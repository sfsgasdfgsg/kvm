package com.ff.sca.system.service;

import com.ff.sca.system.entity.KvmSigninGiftSeting;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 签到礼物发放时间节点配置 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
public interface IKvmSigninGiftSetingService extends IService<KvmSigninGiftSeting> {

}
