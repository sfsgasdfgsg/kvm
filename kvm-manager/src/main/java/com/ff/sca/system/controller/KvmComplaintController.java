package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.system.entity.KvmComplaint;
import com.ff.sca.system.service.IKvmComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 投诉 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
@Controller
@RequestMapping("/kvmComplaint")
public class KvmComplaintController extends BaseController {

    @Autowired
    private IKvmComplaintService complaintService;

    /**
     * 查看投诉
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/complaint", method = RequestMethod.POST)
    public ResMsg addHelpCenter(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(complaintService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 投诉列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/complaintList", method = RequestMethod.POST)
    public LayuiTableResMsg complaintList(Integer page, Integer size, KvmComplaint per) {
        try {
            Page<KvmComplaint> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmComplaint> ew = new EntityWrapper();
            if (CharUtil.isNotEmpty(per.getType())) {
                ew.eq("type", per.getType());
            }
            Page<KvmComplaint> suserList = complaintService.selectPage(pageEntity, ew);
            long total = complaintService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }
}

