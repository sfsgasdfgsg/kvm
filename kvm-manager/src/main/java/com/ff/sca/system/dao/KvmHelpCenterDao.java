package com.ff.sca.system.dao;

import com.ff.sca.system.entity.KvmHelpCenter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 帮助中心 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
public interface KvmHelpCenterDao extends BaseMapper<KvmHelpCenter> {

}
