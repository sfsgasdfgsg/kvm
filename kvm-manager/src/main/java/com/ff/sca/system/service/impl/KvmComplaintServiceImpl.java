package com.ff.sca.system.service.impl;

import com.ff.sca.system.entity.KvmComplaint;
import com.ff.sca.system.dao.KvmComplaintDao;
import com.ff.sca.system.service.IKvmComplaintService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 投诉 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
@Service
public class KvmComplaintServiceImpl extends ServiceImpl<KvmComplaintDao, KvmComplaint> implements IKvmComplaintService {

}
