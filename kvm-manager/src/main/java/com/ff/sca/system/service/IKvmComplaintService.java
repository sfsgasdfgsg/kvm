package com.ff.sca.system.service;

import com.ff.sca.system.entity.KvmComplaint;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 投诉 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
public interface IKvmComplaintService extends IService<KvmComplaint> {

}
