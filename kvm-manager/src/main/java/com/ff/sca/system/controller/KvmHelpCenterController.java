package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.system.entity.KvmHelpCenter;
import com.ff.sca.system.service.IKvmHelpCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * <p>
 * 帮助中心 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
@Controller
@RequestMapping("/kvmHelpCenter")
public class KvmHelpCenterController extends BaseController {

    @Autowired
    private IKvmHelpCenterService helpCenterService;


    /**
     * 查看信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/helpCenter", method = RequestMethod.POST)
    public ResMsg addHelpCenter(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(helpCenterService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 新增帮助
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增帮助",
            module = OperateModule.System,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/addHelpCenter", method = RequestMethod.POST)
    public ResMsg addHelpCenter(KvmHelpCenter per) {
        try {
            per.setCreateTime(new Date());
            boolean result = helpCenterService.insert(per);
            if (result) {
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改帮助
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改帮助",
            module = OperateModule.System,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updHelpCenter", method = RequestMethod.POST)
    public ResMsg updHelpCenter(KvmHelpCenter per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                boolean result = helpCenterService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除帮助
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除帮助",
            module = OperateModule.System,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delHelpCenter", method = RequestMethod.POST)
    public ResMsg delHelpCenter(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                boolean result = helpCenterService.deleteById(id);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 帮助中心列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/helpCenterList", method = RequestMethod.POST)
    public LayuiTableResMsg helpCenterList(Integer page, Integer size, KvmHelpCenter per) {
        try {
            Page<KvmHelpCenter> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmHelpCenter> ew = new EntityWrapper();
            if (CharUtil.isNotEmpty(per.getType())) {
                ew.eq("type", per.getType());
            }
            if (CharUtil.isNotEmpty(per.getTitle())) {
                ew.like("title", per.getTitle());
            }
            Page<KvmHelpCenter> suserList = helpCenterService.selectPage(pageEntity, ew);
            long total = helpCenterService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

