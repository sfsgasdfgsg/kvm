package com.ff.sca.system.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.system.entity.KvmNews;
import com.ff.sca.system.service.IKvmNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 头条 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@Controller
@RequestMapping("/kvmNews")
public class KvmNewsController extends BaseController {

    @Autowired
    private IKvmNewsService kvmNewsService;

    /**
     * 查看头条信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/news", method = RequestMethod.POST)
    public ResMsg news(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(kvmNewsService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除头条
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除头条",
            module = OperateModule.System,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delNews", method = RequestMethod.POST)
    public ResMsg delNews(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                boolean result = kvmNewsService.deleteById(id);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 修改头条信息
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改头条信息",
            module = OperateModule.System,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/updNews", method = RequestMethod.POST)
    public ResMsg updNews(KvmNews per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                KvmNews news = kvmNewsService.selectById(per.getId());
                per.setCreateTime(news.getCreateTime());
                boolean result = kvmNewsService.updateById(per);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 新增头条
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增头条",
            module = OperateModule.System,
            opType = OperateType.create
    )
    @ResponseBody
    @RequestMapping(value = "/addNews", method = RequestMethod.POST)
    public ResMsg addNews(KvmNews per) {
        try {
            boolean result = kvmNewsService.insert(per);
            if (result) {
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 头条列表
     */
    @ResponseBody
    @RequestMapping(value = "/newsList", method = RequestMethod.POST)
    public LayuiTableResMsg newsList(Integer page, Integer size, KvmNews per) {
        try {
            Page<KvmNews> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmNews> ew = new EntityWrapper();
            Page<KvmNews> suserList = kvmNewsService.selectPage(pageEntity, ew);
            long total = kvmNewsService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

