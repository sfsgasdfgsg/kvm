package com.ff.sca.system.service;

import com.ff.sca.system.entity.KvmNews;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 头条 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
public interface IKvmNewsService extends IService<KvmNews> {

}
