package com.ff.sca.system.dao;

import com.ff.sca.system.entity.KvmSigninGiftSeting;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 签到礼物发放时间节点配置 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
public interface KvmSigninGiftSetingDao extends BaseMapper<KvmSigninGiftSeting> {

}
