package com.ff.sca.system.service.impl;

import com.ff.sca.system.entity.KvmHelpCenter;
import com.ff.sca.system.dao.KvmHelpCenterDao;
import com.ff.sca.system.service.IKvmHelpCenterService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 帮助中心 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-07
 */
@Service
public class KvmHelpCenterServiceImpl extends ServiceImpl<KvmHelpCenterDao, KvmHelpCenter> implements IKvmHelpCenterService {

}
