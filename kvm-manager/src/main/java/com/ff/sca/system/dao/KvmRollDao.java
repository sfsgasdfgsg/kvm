package com.ff.sca.system.dao;

import com.ff.sca.system.entity.KvmRoll;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 首页轮播 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-21
 */
public interface KvmRollDao extends BaseMapper<KvmRoll> {

}
