package com.ff.sca.system.service.impl;

import com.ff.sca.system.entity.KvmSigninGiftSeting;
import com.ff.sca.system.dao.KvmSigninGiftSetingDao;
import com.ff.sca.system.service.IKvmSigninGiftSetingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到礼物发放时间节点配置 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@Service
public class KvmSigninGiftSetingServiceImpl extends ServiceImpl<KvmSigninGiftSetingDao, KvmSigninGiftSeting> implements IKvmSigninGiftSetingService {

}
