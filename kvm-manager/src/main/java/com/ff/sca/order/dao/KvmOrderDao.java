package com.ff.sca.order.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.order.entity.KvmOrder;
import com.ff.sca.order.entity.OrderInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface KvmOrderDao extends BaseMapper<KvmOrder> {

    public List<OrderInfoVo> orderList(@Param("start") Integer start, @Param("size") Integer size, @Param("orderInfoVo") OrderInfoVo orderInfoVo);

    public Long selectCount(@Param("orderInfoVo") OrderInfoVo orderInfoVo);

}
