package com.ff.sca.order.dao;

import com.ff.sca.order.entity.KvmOrderExpress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单物流信息 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface KvmOrderExpressDao extends BaseMapper<KvmOrderExpress> {

}
