package com.ff.sca.order.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.order.dao.KvmOrderDao;
import com.ff.sca.order.entity.KvmOrder;
import com.ff.sca.order.entity.OrderInfoVo;
import com.ff.sca.order.service.IKvmOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Service
public class KvmOrderServiceImpl extends ServiceImpl<KvmOrderDao, KvmOrder> implements IKvmOrderService {

    @Autowired
    private KvmOrderDao orderDao;

    @Override
    public List<OrderInfoVo> orderList(Integer page, Integer size, OrderInfoVo per) {
        if (CharUtil.isEmpty(page)) {
            page = 0;
        }
        if (CharUtil.isEmpty(size)) {
            size = 0;
        }
        Integer start = (page-1) * size;
        return orderDao.orderList(start, size, per);
    }

    @Override
    public Long selectCount(OrderInfoVo per) {
        return orderDao.selectCount(per);
    }
}
