package com.ff.sca.order.service.impl;

import com.ff.sca.order.entity.KvmOrderPrice;
import com.ff.sca.order.dao.KvmOrderPriceDao;
import com.ff.sca.order.service.IKvmOrderPriceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单价格 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Service
public class KvmOrderPriceServiceImpl extends ServiceImpl<KvmOrderPriceDao, KvmOrderPrice> implements IKvmOrderPriceService {

}
