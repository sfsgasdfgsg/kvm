package com.ff.sca.order.dao;

import com.ff.sca.order.entity.KvmOrderPrice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单价格 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface KvmOrderPriceDao extends BaseMapper<KvmOrderPrice> {

}
