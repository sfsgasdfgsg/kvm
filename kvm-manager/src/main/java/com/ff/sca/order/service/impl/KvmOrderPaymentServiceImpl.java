package com.ff.sca.order.service.impl;

import com.ff.sca.order.entity.KvmOrderPayment;
import com.ff.sca.order.dao.KvmOrderPaymentDao;
import com.ff.sca.order.service.IKvmOrderPaymentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户支付订单 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Service
public class KvmOrderPaymentServiceImpl extends ServiceImpl<KvmOrderPaymentDao, KvmOrderPayment> implements IKvmOrderPaymentService {

}
