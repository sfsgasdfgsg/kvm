package com.ff.sca.order.service.impl;

import com.ff.sca.order.entity.KvmOrderExpress;
import com.ff.sca.order.dao.KvmOrderExpressDao;
import com.ff.sca.order.service.IKvmOrderExpressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单物流信息 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Service
public class KvmOrderExpressServiceImpl extends ServiceImpl<KvmOrderExpressDao, KvmOrderExpress> implements IKvmOrderExpressService {

}
