package com.ff.sca.order.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 商户支付订单
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
@TableName("kvm_order_payment")
public class KvmOrderPayment extends Model<KvmOrderPayment> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 订单id
     */
    @TableField("order_id")
    private Integer orderId;
    /**
     * 订单NO
     */
    @TableField("order_no")
    private String orderNo;
    /**
     * 预交易流水号
     */
    @TableField("trade_no")
    private String tradeNo;
    /**
     * 支付渠道：0余额，1微信，2支付宝
     */
    @TableField("pay_channel")
    private Integer payChannel;
    /**
     * 支付状态
     */
    private Integer status;
    /**
     * ip
     */
    private String ip;
    /**
     * 签名
     */
    private String sign;
    /**
     * 订单描述
     */
    private String title;
    /**
     * 总金额单位分
     */
    private Integer total;
    /**
     * 备注
     */
    private String remark;
    /**
     * 支付时间
     */
    @TableField("pay_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date payTime;
    /**
     * 支付完成时间
     */
    @TableField("complete_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date completeTime;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public Integer getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(Integer payChannel) {
        this.payChannel = payChannel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmOrderPayment{" +
        ", id=" + id +
        ", orderId=" + orderId +
        ", orderNo=" + orderNo +
        ", tradeNo=" + tradeNo +
        ", payChannel=" + payChannel +
        ", status=" + status +
        ", ip=" + ip +
        ", sign=" + sign +
        ", title=" + title +
        ", total=" + total +
        ", remark=" + remark +
        ", payTime=" + payTime +
        ", completeTime=" + completeTime +
        ", createTime=" + createTime +
        "}";
    }
}
