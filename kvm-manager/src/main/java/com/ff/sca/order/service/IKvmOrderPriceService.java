package com.ff.sca.order.service;

import com.ff.sca.order.entity.KvmOrderPrice;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单价格 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface IKvmOrderPriceService extends IService<KvmOrderPrice> {

}
