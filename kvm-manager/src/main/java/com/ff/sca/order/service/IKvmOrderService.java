package com.ff.sca.order.service;

import com.baomidou.mybatisplus.service.IService;
import com.ff.sca.order.entity.KvmOrder;
import com.ff.sca.order.entity.OrderInfoVo;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface IKvmOrderService extends IService<KvmOrder> {

    /**
     * 总订单信息列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    public List<OrderInfoVo> orderList(Integer page, Integer size, OrderInfoVo per);

    public Long selectCount(OrderInfoVo per);

}
