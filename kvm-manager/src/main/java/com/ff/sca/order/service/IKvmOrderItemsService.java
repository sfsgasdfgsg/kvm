package com.ff.sca.order.service;

import com.ff.sca.order.entity.KvmOrderItems;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单商品表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface IKvmOrderItemsService extends IService<KvmOrderItems> {

}
