package com.ff.sca.order.service.impl;

import com.ff.sca.order.entity.KvmOrderItems;
import com.ff.sca.order.dao.KvmOrderItemsDao;
import com.ff.sca.order.service.IKvmOrderItemsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单商品表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Service
public class KvmOrderItemsServiceImpl extends ServiceImpl<KvmOrderItemsDao, KvmOrderItems> implements IKvmOrderItemsService {

}
