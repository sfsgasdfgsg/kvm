package com.ff.sca.order.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.codeenum.OrderExpressStatusEnum;
import com.ff.sca.common.codeenum.OrderStatusEnum;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.order.entity.*;
import com.ff.sca.order.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Controller
@RequestMapping("/kvmOrder")
public class KvmOrderController extends BaseController {

    @Autowired
    private IKvmOrderService orderService;
    @Autowired
    private IKvmOrderPriceService orderPriceService;
    @Autowired
    private IKvmOrderPaymentService orderPaymentService;
    @Autowired
    private IKvmOrderItemsService orderItemsService;
    @Autowired
    private IKvmOrderExpressService orderExpressService;

    @ControllerLog(
            description = "订单发货(自提)",
            module = OperateModule.OrderManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/ztOrdersSend", method = RequestMethod.POST)
    public ResMsg ztOrdersSend(KvmOrderExpress per) {
        try {
            if (CharUtil.isNotEmpty(per.getOrderId())) {
                KvmOrder order = orderService.selectById(per.getOrderId());
                KvmOrderExpress orderExpress = orderExpressService.selectOne(new EntityWrapper<KvmOrderExpress>().eq("order_id", per.getOrderId()));
                if (orderExpress != null) {
                    order.setStatus(OrderStatusEnum.RECEIVE.getType());
                    order.setExpressType(1);
                    Boolean orderResult = orderService.updateById(order);
                    if (orderResult) {
                        orderExpress.setType(1);
                        orderExpress.setExpressStatus(OrderExpressStatusEnum.RECEIVE.getType());
                        Boolean result = orderExpressService.updateById(orderExpress);
                        if (result) {
                            return resSuccess(null);
                        }
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 订单发货
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "订单发货(物流)",
            module = OperateModule.OrderManager,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/wlOrdersSend", method = RequestMethod.POST)
    public ResMsg ordersSend(KvmOrderExpress per, KvmOrderExpress oe) {
        try {
            if (CharUtil.isNotEmpty(per.getOrderId())) {
                KvmOrder order = orderService.selectById(per.getOrderId());
                KvmOrderExpress orderExpress = orderExpressService.selectOne(new EntityWrapper<KvmOrderExpress>().eq("order_id", per.getOrderId()));
                if (orderExpress != null) {
                    order.setStatus(OrderStatusEnum.RECEIVE.getType());
                    order.setExpressType(0);
                    Boolean orderResult = orderService.updateById(order);
                    if (orderResult) {
                        orderExpress.setType(0);
                        orderExpress.setExpressStatus(OrderExpressStatusEnum.RECEIVE.getType());
                        orderExpress.setExpressNo(oe.getExpressNo());
                        orderExpress.setExpressName(oe.getExpressName());
                        orderExpress.setSendTime(new Date());
                        Boolean result = orderExpressService.updateById(orderExpress);
                        if (result) {
                            return resSuccess(null);
                        }
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 订单物流信息
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderExpressInfo", method = RequestMethod.POST)
    public ResMsg orderExpressInfo(Integer orderId) {
        try {
            if (CharUtil.isNotEmpty(orderId)) {
                return resSuccess(orderExpressService.selectOne(new EntityWrapper<KvmOrderExpress>().eq("order_id", orderId)));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 查看订单商品
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderProductsInfo", method = RequestMethod.POST)
    public ResMsg orderProductsInfo(Integer orderId) {
        try {
            if (CharUtil.isNotEmpty(orderId)) {
                return resSuccess(orderItemsService.selectOne(new EntityWrapper<KvmOrderItems>().eq("order_id", orderId)));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 订单支付信息
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderPayInfo", method = RequestMethod.POST)
    public ResMsg orderPayInfo(Integer orderId) {
        try {
            if (CharUtil.isNotEmpty(orderId)) {
                return resSuccess(orderPaymentService.selectOne(new EntityWrapper<KvmOrderPayment>().eq("order_id", orderId)));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 订单价格信息
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderPriceInfo", method = RequestMethod.POST)
    public ResMsg orderPriceInfo(Integer orderId) {
        try {
            if (CharUtil.isNotEmpty(orderId)) {
                return resSuccess(orderPriceService.selectOne(new EntityWrapper<KvmOrderPrice>().eq("order_id", orderId)));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 订单列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderAllInfoList", method = RequestMethod.POST)
    public LayuiTableResMsg orderAllInfoList(Integer page, Integer size, OrderInfoVo per) {
        try {
            if (CharUtil.isEmpty(per.getNo())) {
                per.setNo(null);
            }
            if (CharUtil.isEmpty(per.getUserName())) {
                per.setUserName(null);
            }
            if (CharUtil.isEmpty(per.getType())) {
                per.setType(null);
            }
            if (CharUtil.isEmpty(per.getStatus())) {
                per.setStatus(null);
            }
            if (CharUtil.isEmpty(per.getMobile())) {
                per.setMobile(null);
            }
            if (CharUtil.isEmpty(per.getExpressNo())) {
                per.setExpressNo(null);
            }
            if (CharUtil.isEmpty(per.getExpressStatus())) {
                per.setExpressStatus(null);
            }
            if (CharUtil.isEmpty(per.getPayStatus())) {
                per.setPayStatus(null);
            }
            List<OrderInfoVo> orderList = orderService.orderList(page, size, per);
            long total = orderService.selectCount(per);
            return resTableSuccess(total, orderList);
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

