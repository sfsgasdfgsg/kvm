package com.ff.sca.order.service;

import com.ff.sca.order.entity.KvmOrderExpress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单物流信息 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface IKvmOrderExpressService extends IService<KvmOrderExpress> {

}
