package com.ff.sca.order.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 订单价格 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
@Controller
@RequestMapping("/kvmOrderPrice")
public class KvmOrderPriceController {

}

