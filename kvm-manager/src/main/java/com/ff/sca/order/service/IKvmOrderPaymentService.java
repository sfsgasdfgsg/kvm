package com.ff.sca.order.service;

import com.ff.sca.order.entity.KvmOrderPayment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商户支付订单 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface IKvmOrderPaymentService extends IService<KvmOrderPayment> {

}
