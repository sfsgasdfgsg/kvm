package com.ff.sca.order.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author gw123
 * @since 2018-12-13
 */
@TableName("kvm_order")
public class KvmOrder extends Model<KvmOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 编号
     */
    private String no;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 地址信息
     */
    @TableField("address_id")
    private Integer addressId;
    /**
     * 0物流，1自提
     */
    @TableField("express_type")
    private Integer expressType;
    /**
     * 支付信息
     */
    @TableField("payment_id")
    private Integer paymentId;
    /**
     * 价格信息
     */
    @TableField("price_id")
    private Integer priceId;
    /**
     * 0普通商品，1礼包
     */
    private Integer type;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getExpressType() {
        return expressType;
    }

    public void setExpressType(Integer expressType) {
        this.expressType = expressType;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getPriceId() {
        return priceId;
    }

    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmOrder{" +
        ", id=" + id +
        ", no=" + no +
        ", userId=" + userId +
        ", status=" + status +
        ", addressId=" + addressId +
        ", expressType=" + expressType +
        ", paymentId=" + paymentId +
        ", priceId=" + priceId +
        ", type=" + type +
        ", createTime=" + createTime +
        "}";
    }
}
