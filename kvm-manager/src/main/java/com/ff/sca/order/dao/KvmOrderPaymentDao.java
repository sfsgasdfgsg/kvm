package com.ff.sca.order.dao;

import com.ff.sca.order.entity.KvmOrderPayment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商户支付订单 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface KvmOrderPaymentDao extends BaseMapper<KvmOrderPayment> {

}
