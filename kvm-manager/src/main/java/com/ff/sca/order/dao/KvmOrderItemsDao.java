package com.ff.sca.order.dao;

import com.ff.sca.order.entity.KvmOrderItems;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单商品表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-28
 */
public interface KvmOrderItemsDao extends BaseMapper<KvmOrderItems> {

}
