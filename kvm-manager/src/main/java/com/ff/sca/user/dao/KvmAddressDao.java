package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 收货地址 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
public interface KvmAddressDao extends BaseMapper<KvmAddress> {

}
