package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmAddress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 收货地址 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
public interface IKvmAddressService extends IService<KvmAddress> {

}
