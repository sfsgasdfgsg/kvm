package com.ff.sca.user.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ff.sca.user.entity.KvmBlackList;

/**
 * <p>
 * 黑名单 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
public interface KvmBlackListDao extends BaseMapper<KvmBlackList> {

}
