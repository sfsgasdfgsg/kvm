package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 直推用户
 * </p>
 *
 * @author gw123
 * @since 2018-12-20
 */
@TableName("kvm_user_direct")
public class KvmUserDirect extends Model<KvmUserDirect> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 直推用户id
     */
    @TableField("direct_user_id")
    private Integer directUserId;
    /**
     * 是否已经领取礼包：0否，1是
     */
    private Integer status;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDirectUserId() {
        return directUserId;
    }

    public void setDirectUserId(Integer directUserId) {
        this.directUserId = directUserId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmUserDirect{" +
        ", id=" + id +
        ", userId=" + userId +
        ", directUserId=" + directUserId +
        ", status=" + status +
        ", createTime=" + createTime +
        "}";
    }
}
