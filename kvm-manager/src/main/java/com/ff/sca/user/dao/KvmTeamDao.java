package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmTeam;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 团队成员统计表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-27
 */
public interface KvmTeamDao extends BaseMapper<KvmTeam> {

}
