package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 美豆提现申请
 * </p>
 *
 * @author gw123
 * @since 2018-12-26
 */
@TableName("kvm_withdrawal")
public class KvmWithdrawal extends Model<KvmWithdrawal> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 提现用户id
     */
    @TableField("user_id")
    private Integer userId;
    private String mobile;
    /**
     * 提现到账类型；0：支付宝；1：银行卡
     */
    private Integer type;
    /**
     * 提现手续百分比
     */
    @TableField("fc_pg")
    private Double fcPg;
    /**
     * 手续费金额
     */
    @TableField("fc_num")
    private Double fcNum;
    /**
     * 提现总金额
     */
    private Double total;
    /**
     * 账号（支付宝，银行卡账号）
     */
    private String account;
    /**
     * 提现账户真实姓名
     */
    @TableField("account_name")
    private String accountName;
    /**
     * 提现状态；0：待审核；1：审核不通过；2：已取消，3提现成功
     */
    private Integer status;
    /**
     * 提现时间
     */
    @TableField("wd_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date wdTime;
    /**
     * 备注（失败原因，拒绝原因等信息）
     */
    private String des;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 银行名称
     */
    @TableField("bank_name")
    private String bankName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getFcPg() {
        return fcPg;
    }

    public void setFcPg(Double fcPg) {
        this.fcPg = fcPg;
    }

    public Double getFcNum() {
        return fcNum;
    }

    public void setFcNum(Double fcNum) {
        this.fcNum = fcNum;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getWdTime() {
        return wdTime;
    }

    public void setWdTime(Date wdTime) {
        this.wdTime = wdTime;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmWithdrawal{" +
        ", id=" + id +
        ", userId=" + userId +
        ", mobile=" + mobile +
        ", type=" + type +
        ", fcPg=" + fcPg +
        ", fcNum=" + fcNum +
        ", total=" + total +
        ", account=" + account +
        ", accountName=" + accountName +
        ", status=" + status +
        ", wdTime=" + wdTime +
        ", des=" + des +
        ", createTime=" + createTime +
        ", bankName=" + bankName +
        "}";
    }
}
