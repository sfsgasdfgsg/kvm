package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmUserDirect;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 直推用户 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-20
 */
public interface KvmUserDirectDao extends BaseMapper<KvmUserDirect> {

}
