package com.ff.sca.user.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.user.entity.KvmAchieLog;
import com.ff.sca.user.entity.KvmBlackList;
import com.ff.sca.user.entity.KvmUser;
import com.ff.sca.user.service.IKvmAchieLogService;
import com.ff.sca.user.service.IKvmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 业绩表 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-25
 */
@Controller
@RequestMapping("/kvmAchieLog")
public class KvmAchieLogController extends BaseController {


    @Autowired
    private IKvmAchieLogService achieLogService;
    @Autowired
    private IKvmUserService userService;

    @ResponseBody
    @RequestMapping(value = "/achieLogList", method = RequestMethod.POST)
    public LayuiTableResMsg achieLogList(Integer page, Integer size,String mobile) {
        try {
            Page<KvmAchieLog> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmAchieLog> ew = new EntityWrapper();
            if (CharUtil.isNotEmpty(mobile)) {
                KvmUser user = userService.selectOne(new EntityWrapper<KvmUser>().eq("user_code", mobile));
                if (user != null) {
                    ew.eq("user_id", user.getId());
                }
            }
            Page<KvmAchieLog> suserList = achieLogService.selectPage(pageEntity, ew);
            long total = achieLogService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

