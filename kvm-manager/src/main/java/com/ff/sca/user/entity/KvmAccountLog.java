package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 账户表
 * </p>
 *
 * @author gw123
 * @since 2018-11-30
 */
@TableName("kvm_account_log")
public class KvmAccountLog extends Model<KvmAccountLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 变化量
     */
    private Double num;
    /**
     * 余额
     */
    private Double amount;
    /**
     * 手续费
     */
    private Double fee;
    /**
     * 0购物金,1美豆
     */
    private Integer type;
    /**
     * 变动类型；1：购物；2：美豆充值；3：微信充值；4：支付宝充值；5：提现；6：礼包返利；7：礼包分红；8：商品返利；9：商品分红；10：退换
     */
    @TableField("change_type")
    private Integer changeType;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getChangeType() {
        return changeType;
    }

    public void setChangeType(Integer changeType) {
        this.changeType = changeType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmAccountLog{" +
        ", id=" + id +
        ", userId=" + userId +
        ", num=" + num +
        ", amount=" + amount +
        ", fee=" + fee +
        ", type=" + type +
        ", changeType=" + changeType +
        ", createTime=" + createTime +
        "}";
    }
}
