package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmAccountLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 账户表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
public interface KvmAccountLogDao extends BaseMapper<KvmAccountLog> {

}
