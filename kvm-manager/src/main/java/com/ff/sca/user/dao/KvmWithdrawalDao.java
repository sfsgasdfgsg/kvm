package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmWithdrawal;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 美豆提现申请 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
public interface KvmWithdrawalDao extends BaseMapper<KvmWithdrawal> {

}
