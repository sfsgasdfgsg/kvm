package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmAccountLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 账户表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
public interface IKvmAccountLogService extends IService<KvmAccountLog> {

}
