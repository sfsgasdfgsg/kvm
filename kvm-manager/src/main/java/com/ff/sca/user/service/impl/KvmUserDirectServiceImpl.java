package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmUserDirect;
import com.ff.sca.user.dao.KvmUserDirectDao;
import com.ff.sca.user.service.IKvmUserDirectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 直推用户 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-20
 */
@Service
public class KvmUserDirectServiceImpl extends ServiceImpl<KvmUserDirectDao, KvmUserDirect> implements IKvmUserDirectService {

}
