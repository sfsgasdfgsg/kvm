package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmUserLv;
import com.ff.sca.user.dao.KvmUserLvDao;
import com.ff.sca.user.service.IKvmUserLvService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 礼包直推配置表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@Service
public class KvmUserLvServiceImpl extends ServiceImpl<KvmUserLvDao, KvmUserLv> implements IKvmUserLvService {

}
