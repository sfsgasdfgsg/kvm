package com.ff.sca.user.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.user.entity.KvmAddress;
import com.ff.sca.user.service.IKvmAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 收货地址 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
@Controller
@RequestMapping("/kvmAddress")
public class KvmAddressController extends BaseController {

    @Autowired
    private IKvmAddressService addressService;

    /**
     * 通过用户id查看收货地址
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAdsByUserId", method = RequestMethod.POST)
    public ResMsg getAdsByUserId(Integer userId) {
        try {
            if (CharUtil.isNotEmpty(userId)) {
                List<KvmAddress> result = addressService.selectList(new EntityWrapper<KvmAddress>().eq("user_id", userId));
                return resSuccess(result);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

}

