package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 业绩表
 * </p>
 *
 * @author gw123
 * @since 2018-12-25
 */
@TableName("kvm_achie_log")
public class KvmAchieLog extends Model<KvmAchieLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 用户id
     */
    @TableField("down_user_id")
    private Integer downUserId;
    /**
     * 业绩
     */
    private Double total;
    /**
     * 0进货业绩,1礼包业绩
     */
    private Integer type;
    /**
     * 订单编号
     */
    @TableField("order_no")
    private String orderNo;
    @TableField("create_time")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDownUserId() {
        return downUserId;
    }

    public void setDownUserId(Integer downUserId) {
        this.downUserId = downUserId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmAchieLog{" +
        ", id=" + id +
        ", userId=" + userId +
        ", downUserId=" + downUserId +
        ", total=" + total +
        ", type=" + type +
        ", orderNo=" + orderNo +
        ", createTime=" + createTime +
        "}";
    }
}
