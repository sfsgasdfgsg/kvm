package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmBlackList;
import com.ff.sca.user.dao.KvmBlackListDao;
import com.ff.sca.user.service.IKvmBlackListService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 黑名单 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Service
public class KvmBlackListServiceImpl extends ServiceImpl<KvmBlackListDao, KvmBlackList> implements IKvmBlackListService {

}
