package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmBlackList;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 黑名单 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
public interface IKvmBlackListService extends IService<KvmBlackList> {

}
