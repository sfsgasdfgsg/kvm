package com.ff.sca.user.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.product.entity.KvmProduct;
import com.ff.sca.product.entity.KvmProductRob;
import com.ff.sca.user.entity.KvmBlackList;
import com.ff.sca.user.service.IKvmBlackListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 黑名单 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-27
 */
@Controller
@RequestMapping("/kvmBlackList")
public class KvmBlackListController extends BaseController {

    @Autowired
    private IKvmBlackListService blackListService;

    /**
     * 清除所有黑名单
     *
     * @return
     */
    @ControllerLog(
            description = "一键清除所有黑名单",
            module = OperateModule.SUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delAllBlack", method = RequestMethod.POST)
    public ResMsg delAllBlack() {
        try {
            Boolean result = blackListService.delete(null);
            if (result) {
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 黑名单删除
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "黑名单删除",
            module = OperateModule.SUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delBlack", method = RequestMethod.POST)
    public ResMsg delBlack(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                Boolean result = blackListService.deleteById(id);
                if (result) {
                    return resSuccess(null);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 黑名单列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/blackList", method = RequestMethod.POST)
    public LayuiTableResMsg blackList(Integer page, Integer size, KvmBlackList per) {
        try {
            Page<KvmBlackList> pageEntity = setPageInfo(page, size);
            EntityWrapper<KvmBlackList> ew = new EntityWrapper();
            if (CharUtil.isNotEmpty(per.getUserName())) {
                ew.like("user_name", per.getUserName());
            }
            Page<KvmBlackList> suserList = blackListService.selectPage(pageEntity, ew);
            long total = blackListService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

