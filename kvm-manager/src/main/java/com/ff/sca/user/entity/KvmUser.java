package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author gw123
 * @since 2018-12-26
 */
@TableName("kvm_user")
public class KvmUser extends Model<KvmUser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;
    /**
     * 用户电话
     */
    private String mobile;
    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;
    /**
     * 真实姓名
     */
    private String name;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 头像地址
     */
    @TableField("head_url")
    private String headUrl;
    /**
     * 登录密码
     */
    @TableField("login_pwd")
    private String loginPwd;
    /**
     * 二级密码
     */
    @TableField("deal_pwd")
    private String dealPwd;
    /**
     * 推荐码
     */
    @TableField("user_code")
    private String userCode;
    /**
     * 注册码
     */
    @TableField("poll_code")
    private String pollCode;
    /**
     * 用户状态 0.正常, 1冻结，2锁定
     */
    private Integer status;
    /**
     * 身份等级
     */
    private Integer lv;
    /**
     * 礼包团队等级
     */
    @TableField("team_gift_lv")
    private Integer teamGiftLv;
    /**
     * 商品团队等级（团队重销等级）
     */
    @TableField("team_product_lv")
    private Integer teamProductLv;
    /**
     * 角色默认0，普通会员
     */
    @TableField("role_id")
    private Integer roleId;
    /**
     * 微信号
     */
    private String wx;
    /**
     * 支付宝帐号
     */
    private String alipay;
    /**
     * 微信公众号下唯一标识
     */
    private String openid;
    /**
     * 微信开放平台下唯一标识
     */
    @TableField("op_openid")
    private String opOpenid;
    /**
     * 微信唯一标识
     */
    private String unionid;
    /**
     * 推广二维码地址
     */
    private String spread;
    /**
     * 上次登录时间
     */
    @TableField("login_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date loginTime;
    /**
     * 楼层
     */
    private Integer floor;
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    /**
     * 身份证号码
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 已经发放的直推奖品个数
     */
    @TableField("direct_gift_times")
    private Integer directGiftTimes;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getDealPwd() {
        return dealPwd;
    }

    public void setDealPwd(String dealPwd) {
        this.dealPwd = dealPwd;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPollCode() {
        return pollCode;
    }

    public void setPollCode(String pollCode) {
        this.pollCode = pollCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLv() {
        return lv;
    }

    public void setLv(Integer lv) {
        this.lv = lv;
    }

    public Integer getTeamGiftLv() {
        return teamGiftLv;
    }

    public void setTeamGiftLv(Integer teamGiftLv) {
        this.teamGiftLv = teamGiftLv;
    }

    public Integer getTeamProductLv() {
        return teamProductLv;
    }

    public void setTeamProductLv(Integer teamProductLv) {
        this.teamProductLv = teamProductLv;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getWx() {
        return wx;
    }

    public void setWx(String wx) {
        this.wx = wx;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getOpOpenid() {
        return opOpenid;
    }

    public void setOpOpenid(String opOpenid) {
        this.opOpenid = opOpenid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getSpread() {
        return spread;
    }

    public void setSpread(String spread) {
        this.spread = spread;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Integer getDirectGiftTimes() {
        return directGiftTimes;
    }

    public void setDirectGiftTimes(Integer directGiftTimes) {
        this.directGiftTimes = directGiftTimes;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmUser{" +
        ", id=" + id +
        ", userName=" + userName +
        ", mobile=" + mobile +
        ", nickName=" + nickName +
        ", name=" + name +
        ", sex=" + sex +
        ", headUrl=" + headUrl +
        ", loginPwd=" + loginPwd +
        ", dealPwd=" + dealPwd +
        ", userCode=" + userCode +
        ", pollCode=" + pollCode +
        ", status=" + status +
        ", lv=" + lv +
        ", teamGiftLv=" + teamGiftLv +
        ", teamProductLv=" + teamProductLv +
        ", roleId=" + roleId +
        ", wx=" + wx +
        ", alipay=" + alipay +
        ", openid=" + openid +
        ", opOpenid=" + opOpenid +
        ", unionid=" + unionid +
        ", spread=" + spread +
        ", loginTime=" + loginTime +
        ", floor=" + floor +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", idCard=" + idCard +
        ", directGiftTimes=" + directGiftTimes +
        "}";
    }
}
