package com.ff.sca.user.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.entity.ztree.UserNode;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.user.entity.KvmTeam;
import com.ff.sca.user.service.IKvmTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 团队成员统计表 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-27
 */
@Controller
@RequestMapping("/kvmTeam")
public class KvmTeamController extends BaseController {

    @Autowired
    private IKvmTeamService teamService;

    @ResponseBody
    @RequestMapping(value = "/teamInfo", method = RequestMethod.POST)
    public ResMsg teamInfo(String userId) {
        try {
            if (CharUtil.isNotEmpty(userId)) {
                return resSuccess(teamService.selectOne(new EntityWrapper<KvmTeam>().eq("user_id", userId)));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

}

