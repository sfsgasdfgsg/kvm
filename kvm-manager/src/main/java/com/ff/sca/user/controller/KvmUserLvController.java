package com.ff.sca.user.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.user.entity.KvmUserLv;
import com.ff.sca.user.service.IKvmUserLvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 礼包直推配置表 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@Controller
@RequestMapping("/kvmUserLv")
public class KvmUserLvController extends BaseController {

    @Autowired
    private IKvmUserLvService userLvService;

    /**
     * 修改等级重销配置数据
     *
     * @param json
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateLvSecond", method = RequestMethod.POST)
    public ResMsg updateLvSecond(String json) {
        try {
            if (CharUtil.isNotEmpty(json)) {
                JSONObject jsonObject = JSONObject.parseObject(json);
                Object obj = jsonObject.get("id");
                if (obj != null) {
                    KvmUserLv userLv = userLvService.selectById(Integer.parseInt(obj.toString()));
                    userLv.setSecond(jsonObject.get("data").toString());
                    Boolean result = userLvService.updateById(userLv);
                    if (result) {
                        return resSuccess(null);
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 查看礼包信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/userLv", method = RequestMethod.POST)
    public ResMsg userLv(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                return resSuccess(userLvService.selectById(id));
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 删除礼包直推配置
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "删除礼包配置",
            module = OperateModule.SUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/delUsrelv", method = RequestMethod.POST)
    public ResMsg updUsrelv(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                Boolean result = userLvService.deleteById(id);
                if (result != null) {
                    return resSuccess(result);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 修改礼包直推配置
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "修改礼包配置",
            module = OperateModule.SUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/updUsrelv", method = RequestMethod.POST)
    public ResMsg updUsrelv(KvmUserLv per) {
        try {
            if (CharUtil.isNotEmpty(per.getId())) {
                Boolean result = userLvService.updateById(per);
                if (result != null) {
                    return resSuccess(result);
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 新增礼包直推配置
     *
     * @param per
     * @return
     */
    @ControllerLog(
            description = "新增礼包配置",
            module = OperateModule.SUserManger,
            opType = OperateType.delete
    )
    @ResponseBody
    @RequestMapping(value = "/addUsrelv", method = RequestMethod.POST)
    public ResMsg userInfo(KvmUserLv per) {
        try {
            Boolean result = userLvService.insert(per);
            if (result != null) {
                return resSuccess(result);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 礼包直推配置列
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/lvList", method = RequestMethod.POST)
    public LayuiTableResMsg lvList(Integer page, Integer size, KvmUserLv per) {
        try {
            Page<KvmUserLv> pageEntity = setPageInfo(page, size);
            EntityWrapper ew = new EntityWrapper<KvmUserLv>();
            if (CharUtil.isNotEmpty(per.getName())) {
                ew.like("name", per.getName());
            }
            if (CharUtil.isNotEmpty(per.getLv())) {
                ew.eq("lv", per.getLv());
            }
            Page<KvmUserLv> suserList = userLvService.selectPage(pageEntity, ew);
            long total = userLvService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

