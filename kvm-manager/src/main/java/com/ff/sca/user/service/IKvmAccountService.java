package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmAccount;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 账户表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
public interface IKvmAccountService extends IService<KvmAccount> {

}
