package com.ff.sca.user.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.aoplog.ControllerLog;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.OperateModule;
import com.ff.sca.common.codeenum.OperateType;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.user.entity.KvmAccount;
import com.ff.sca.user.entity.KvmAccountLog;
import com.ff.sca.user.entity.KvmWithdrawal;
import com.ff.sca.user.service.IKvmAccountLogService;
import com.ff.sca.user.service.IKvmAccountService;
import com.ff.sca.user.service.IKvmWithdrawalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * <p>
 * 美豆提现申请 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
@Controller
@RequestMapping("/kvmWithdrawal")
public class KvmWithdrawalController extends BaseController {

    @Autowired
    private IKvmWithdrawalService withdrawalService;
    @Autowired
    private IKvmAccountService accountService;
    @Autowired
    private IKvmAccountLogService accountLogService;

    /**
     * 提现失败
     *
     * @param id
     * @param des
     * @return
     */
    @ControllerLog(
            description = "拒绝美豆提现",
            module = OperateModule.SUserManger,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/withdrawalFaild", method = RequestMethod.POST)
    public ResMsg withdrawal(Integer id, String des) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmWithdrawal withdrawal = withdrawalService.selectById(id);
                if (withdrawal.getStatus() == 0) {
                    withdrawal.setStatus(1);
                    withdrawal.setDes(des);
                    Boolean result = withdrawalService.updateById(withdrawal);
                    if (result) {
                        //退还
                        KvmAccount account = accountService.selectOne(new EntityWrapper<KvmAccount>().eq("user_id", withdrawal.getUserId()));
                        if (account != null) {
                            account.setMd(account.getMd() + withdrawal.getTotal());
                            accountService.updateById(account);
                            //记录日志
                            KvmAccountLog accountLog = new KvmAccountLog();
                            accountLog.setUserId(withdrawal.getUserId());
                            accountLog.setNum(withdrawal.getTotal());
                            accountLog.setAmount(account.getMd());
                            accountLog.setFee(withdrawal.getFcNum());
                            accountLog.setType(1);
                            accountLog.setChangeType(10);
                            accountLog.setCreateTime(new Date());
                            accountLogService.insert(accountLog);
                        }
                        return resSuccess(null);
                    }
                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 提现成功
     *
     * @param id
     * @return
     */
    @ControllerLog(
            description = "美豆提现",
            module = OperateModule.SUserManger,
            opType = OperateType.modify
    )
    @ResponseBody
    @RequestMapping(value = "/withdrawal", method = RequestMethod.POST)
    public ResMsg withdrawal(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmWithdrawal withdrawal = withdrawalService.selectById(id);
                if (withdrawal.getStatus() == 0) {
                    withdrawal.setStatus(3);
                    withdrawal.setWdTime(new Date());
                    Boolean result = withdrawalService.updateById(withdrawal);
                    if (result) {
                        return resSuccess(null);
                    }

                }
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 提现列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/withdrawalList", method = RequestMethod.POST)
    public LayuiTableResMsg withdrawalList(Integer page, Integer size, KvmWithdrawal per) {
        try {
            Page<KvmWithdrawal> pageEntity = setPageInfo(page, size);
            EntityWrapper ew = new EntityWrapper<KvmWithdrawal>();
            if (CharUtil.isNotEmpty(per.getMobile())) {
                ew.like("mobile", per.getMobile());
            }
            if (CharUtil.isNotEmpty(per.getStatus())) {
                ew.eq("status", per.getStatus());
            }
            ew.orderBy("create_time", false);
            Page<KvmWithdrawal> suserList = withdrawalService.selectPage(pageEntity, ew);
            long total = withdrawalService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }

}

