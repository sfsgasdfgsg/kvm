package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmWithdrawal;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 美豆提现申请 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
public interface IKvmWithdrawalService extends IService<KvmWithdrawal> {

}
