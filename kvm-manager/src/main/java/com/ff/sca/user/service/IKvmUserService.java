package com.ff.sca.user.service;

import com.ff.sca.common.entity.ztree.UserNode;
import com.ff.sca.user.entity.KvmUser;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
public interface IKvmUserService extends IService<KvmUser> {

    /**
     * 团队树形菜单
     *
     * @param userCode
     * @return
     */
    public List<UserNode> teamZtree(String userCode);

    /**
     * 团队下总人数
     *
     * @param userCode
     * @return
     */
    public Integer teamAllUserNum(String userCode);

}
