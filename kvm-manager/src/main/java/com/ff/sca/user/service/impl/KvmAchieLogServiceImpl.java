package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmAchieLog;
import com.ff.sca.user.dao.KvmAchieLogDao;
import com.ff.sca.user.service.IKvmAchieLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业绩表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-25
 */
@Service
public class KvmAchieLogServiceImpl extends ServiceImpl<KvmAchieLogDao, KvmAchieLog> implements IKvmAchieLogService {

}
