package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmUserDirect;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 直推用户 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-20
 */
public interface IKvmUserDirectService extends IService<KvmUserDirect> {

}
