package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmAccountLog;
import com.ff.sca.user.dao.KvmAccountLogDao;
import com.ff.sca.user.service.IKvmAccountLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
@Service
public class KvmAccountLogServiceImpl extends ServiceImpl<KvmAccountLogDao, KvmAccountLog> implements IKvmAccountLogService {

}
