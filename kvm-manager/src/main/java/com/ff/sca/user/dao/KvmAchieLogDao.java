package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmAchieLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 业绩表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-25
 */
public interface KvmAchieLogDao extends BaseMapper<KvmAchieLog> {

}
