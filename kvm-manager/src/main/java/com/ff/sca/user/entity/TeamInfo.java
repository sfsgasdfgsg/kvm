package com.ff.sca.user.entity;

/**
 * 团队信息
 */
public class TeamInfo {
    /**
     * 团队管理者
     */
    private String teamUserName;
    /**
     * 推广等级
     */
    private Integer lv;
    /**
     * 礼包团队等级
     */
    private Integer teamGiftLv;
    /**
     * 商品团队等级
     */
    private Integer teamProductLv;
    /**
     * 团队下直属人数
     */
    private Integer teamUserNum;
    /**
     * 团队下总人数
     */
    private Integer teamAllUserNum;

    public String getTeamUserName() {
        return teamUserName;
    }

    public void setTeamUserName(String teamUserName) {
        this.teamUserName = teamUserName;
    }

    public Integer getLv() {
        return lv;
    }

    public void setLv(Integer lv) {
        this.lv = lv;
    }

    public Integer getTeamGiftLv() {
        return teamGiftLv;
    }

    public void setTeamGiftLv(Integer teamGiftLv) {
        this.teamGiftLv = teamGiftLv;
    }

    public Integer getTeamProductLv() {
        return teamProductLv;
    }

    public void setTeamProductLv(Integer teamProductLv) {
        this.teamProductLv = teamProductLv;
    }

    public Integer getTeamUserNum() {
        return teamUserNum;
    }

    public void setTeamUserNum(Integer teamUserNum) {
        this.teamUserNum = teamUserNum;
    }

    public Integer getTeamAllUserNum() {
        return teamAllUserNum;
    }

    public void setTeamAllUserNum(Integer teamAllUserNum) {
        this.teamAllUserNum = teamAllUserNum;
    }
}
