package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 礼包直推配置表
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
@TableName("kvm_user_lv")
public class KvmUserLv extends Model<KvmUserLv> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 等级名称
     */
    private String name;
    /**
     * 等级
     */
    private Integer lv;
    /**
     * 返利比例
     */
    private Double point;
    /**
     * 单比最大获得
     */
    private Double amount;
    /**
     * 重消比例
     */
    private String second;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLv() {
        return lv;
    }

    public void setLv(Integer lv) {
        this.lv = lv;
    }

    public Double getPoint() {
        return point;
    }

    public void setPoint(Double point) {
        this.point = point;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmUserLv{" +
        ", id=" + id +
        ", name=" + name +
        ", lv=" + lv +
        ", point=" + point +
        ", amount=" + amount +
        ", second=" + second +
        "}";
    }
}
