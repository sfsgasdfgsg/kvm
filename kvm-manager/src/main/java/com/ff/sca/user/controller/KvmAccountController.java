package com.ff.sca.user.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ff.kvm.common.eunm.ChangeTypeEnum;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.codeenum.AccountTypeEnum;
import com.ff.sca.common.entity.resmsg.ResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.user.entity.KvmAccount;
import com.ff.sca.user.entity.KvmAccountLog;
import com.ff.sca.user.entity.KvmUser;
import com.ff.sca.user.service.IKvmAccountLogService;
import com.ff.sca.user.service.IKvmAccountService;
import com.ff.sca.user.service.IKvmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * <p>
 * 账户表 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
@Controller
@RequestMapping("/kvmAccount")
public class KvmAccountController extends BaseController {


    @Autowired
    private IKvmAccountService accountService;
    @Autowired
    private IKvmUserService userService;
    @Autowired
    private IKvmAccountLogService accountLogService;


    /**
     * 充值购物金,美豆
     *
     * @param mobile
     * @param num
     * @return
     */
    @Transactional
    @ResponseBody
    @RequestMapping(value = "/accountRecharge", method = RequestMethod.POST)
    public ResMsg accountRecharge(String mobile, String num, Integer type) {
        try {
            if (CharUtil.isNotEmpty(mobile) && CharUtil.isNotEmpty(num)) {
                KvmUser user = userService.selectOne(new EntityWrapper<KvmUser>().eq("mobile", mobile));
                //account
                KvmAccount account = accountService.selectOne(new EntityWrapper<KvmAccount>().eq("user_id", user.getId()));
                if (account != null) {
                    if (CharUtil.isEmpty(type)) {
                        Double oldGwj = account.getGwj();
                        if (CharUtil.isEmpty(oldGwj)) {
                            oldGwj = 0D;
                        }
                        account.setGwj(oldGwj + Double.parseDouble(num));
                        accountService.updateById(account);
                        //accountLog
                        KvmAccountLog accountLog = new KvmAccountLog();
                        accountLog.setUserId(user.getId());
                        accountLog.setCreateTime(new Date());
                        accountLog.setChangeType(ChangeTypeEnum.HT_GWJ_RECHARGE.getType());
                        accountLog.setNum(Double.parseDouble(num));
                        accountLog.setAmount(account.getGwj());
                        accountLog.setFee(0D);
                        accountLog.setType(0);
                        accountLogService.insert(accountLog);
                    } else {
                        Double oldMd = account.getMd();
                        if (CharUtil.isNotEmpty(oldMd)) {
                            oldMd = 0D;
                        }
                        account.setMd(oldMd + Double.parseDouble(num));
                        accountService.updateById(account);
                        //accountLog
                        KvmAccountLog accountLog = new KvmAccountLog();
                        accountLog.setUserId(user.getId());
                        accountLog.setCreateTime(new Date());
                        accountLog.setChangeType(ChangeTypeEnum.HT_MD_RECHARGE.getType());
                        accountLog.setNum(Double.parseDouble(num));
                        accountLog.setAmount(account.getMd());
                        accountLog.setFee(0D);
                        accountLog.setType(1);
                        accountLogService.insert(accountLog);
                    }
                } else {
                    return resAreadyExisted(null);
                }
                return resSuccess(null);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


    /**
     * 通过id查看账户信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public ResMsg account(Integer id) {
        try {
            if (CharUtil.isNotEmpty(id)) {
                KvmAccount result = accountService.selectById(id);
                return resSuccess(result);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }

    /**
     * 通过用户id查看账户信息
     *
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAccountByUserId", method = RequestMethod.POST)
    public ResMsg getAccountByUserId(Integer userId) {
        try {
            if (CharUtil.isNotEmpty(userId)) {
                KvmAccount result = accountService.selectOne(new EntityWrapper<KvmAccount>().eq("user_id", userId));
                return resSuccess(result);
            }
            return resfail(null);
        } catch (Exception e) {
            e.printStackTrace();
            return resError(e.getMessage());
        }
    }


}

