package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmAddress;
import com.ff.sca.user.dao.KvmAddressDao;
import com.ff.sca.user.service.IKvmAddressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货地址 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
@Service
public class KvmAddressServiceImpl extends ServiceImpl<KvmAddressDao, KvmAddress> implements IKvmAddressService {

}
