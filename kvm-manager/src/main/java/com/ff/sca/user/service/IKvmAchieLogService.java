package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmAchieLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 业绩表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-25
 */
public interface IKvmAchieLogService extends IService<KvmAchieLog> {

}
