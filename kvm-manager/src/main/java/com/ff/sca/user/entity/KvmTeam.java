package com.ff.sca.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 团队成员统计表
 * </p>
 *
 * @author gw123
 * @since 2018-12-27
 */
@TableName("kvm_team")
public class KvmTeam extends Model<KvmTeam> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 上级id
     */
    @TableField("up_user_id")
    private Integer upUserId;
    /**
     * 身份等级
     */
    private Integer lv;
    /**
     * 礼包团队等级
     */
    @TableField("g_lv")
    private Integer gLv;
    /**
     * 商品团队等级
     */
    @TableField("p_lv")
    private Integer pLv;
    /**
     * 直推游客
     */
    @TableField("d_lv0")
    private Integer dLv0;
    /**
     * 直推黄金
     */
    @TableField("d_lv1")
    private Integer dLv1;
    /**
     * 直推铂金
     */
    @TableField("d_lv2")
    private Integer dLv2;
    /**
     * 直推钻石
     */
    @TableField("d_lv3")
    private Integer dLv3;
    /**
     * 伞下游客
     */
    @TableField("t_lv0")
    private Integer tLv0;
    /**
     * 伞下黄金
     */
    @TableField("t_lv1")
    private Integer tLv1;
    /**
     * 伞下铂金
     */
    @TableField("t_lv2")
    private Integer tLv2;
    /**
     * 伞下钻石
     */
    @TableField("t_lv3")
    private Integer tLv3;
    /**
     * 礼包团队直推1星
     */
    @TableField("d_g_lv1")
    private Integer dGLv1;
    /**
     * 礼包团队直推2星
     */
    @TableField("d_g_lv2")
    private Integer dGLv2;
    /**
     * 礼包团队直推3星
     */
    @TableField("d_g_lv3")
    private Integer dGLv3;
    /**
     * 礼包团队直推4星
     */
    @TableField("d_g_lv4")
    private Integer dGLv4;
    /**
     * 礼包团队直推5星
     */
    @TableField("d_g_lv5")
    private Integer dGLv5;
    /**
     * 礼包团队直推6星
     */
    @TableField("d_g_lv6")
    private Integer dGLv6;
    /**
     * 礼包团队直推7星
     */
    @TableField("d_g_lv7")
    private Integer dGLv7;
    /**
     * 礼包团队伞下1星
     */
    @TableField("t_g_lv1")
    private Integer tGLv1;
    /**
     * 礼包团队伞下2星
     */
    @TableField("t_g_lv2")
    private Integer tGLv2;
    /**
     * 礼包团队伞下3星
     */
    @TableField("t_g_lv3")
    private Integer tGLv3;
    /**
     * 礼包团队伞下4星
     */
    @TableField("t_g_lv4")
    private Integer tGLv4;
    /**
     * 礼包团队伞下5星
     */
    @TableField("t_g_lv5")
    private Integer tGLv5;
    /**
     * 礼包团队伞下6星
     */
    @TableField("t_g_lv6")
    private Integer tGLv6;
    /**
     * 礼包团队伞下7星
     */
    @TableField("t_g_lv7")
    private Integer tGLv7;
    /**
     * 商品团队直推1级
     */
    @TableField("d_p_lv1")
    private Integer dPLv1;
    /**
     * 商品团队直推2级
     */
    @TableField("d_p_lv2")
    private Integer dPLv2;
    /**
     * 商品团队直推3级
     */
    @TableField("d_p_lv3")
    private Integer dPLv3;
    /**
     * 商品团队直推4级
     */
    @TableField("d_p_lv4")
    private Integer dPLv4;
    /**
     * 商品团队伞下1级
     */
    @TableField("t_p_lv1")
    private Integer tPLv1;
    /**
     * 商品团队伞下2级
     */
    @TableField("t_p_lv2")
    private Integer tPLv2;
    /**
     * 商品团队伞下3级
     */
    @TableField("t_p_lv3")
    private Integer tPLv3;
    /**
     * 商品团队伞下4级
     */
    @TableField("t_p_lv4")
    private Integer tPLv4;
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUpUserId() {
        return upUserId;
    }

    public void setUpUserId(Integer upUserId) {
        this.upUserId = upUserId;
    }

    public Integer getLv() {
        return lv;
    }

    public void setLv(Integer lv) {
        this.lv = lv;
    }

    public Integer getgLv() {
        return gLv;
    }

    public void setgLv(Integer gLv) {
        this.gLv = gLv;
    }

    public Integer getpLv() {
        return pLv;
    }

    public void setpLv(Integer pLv) {
        this.pLv = pLv;
    }

    public Integer getdLv0() {
        return dLv0;
    }

    public void setdLv0(Integer dLv0) {
        this.dLv0 = dLv0;
    }

    public Integer getdLv1() {
        return dLv1;
    }

    public void setdLv1(Integer dLv1) {
        this.dLv1 = dLv1;
    }

    public Integer getdLv2() {
        return dLv2;
    }

    public void setdLv2(Integer dLv2) {
        this.dLv2 = dLv2;
    }

    public Integer getdLv3() {
        return dLv3;
    }

    public void setdLv3(Integer dLv3) {
        this.dLv3 = dLv3;
    }

    public Integer gettLv0() {
        return tLv0;
    }

    public void settLv0(Integer tLv0) {
        this.tLv0 = tLv0;
    }

    public Integer gettLv1() {
        return tLv1;
    }

    public void settLv1(Integer tLv1) {
        this.tLv1 = tLv1;
    }

    public Integer gettLv2() {
        return tLv2;
    }

    public void settLv2(Integer tLv2) {
        this.tLv2 = tLv2;
    }

    public Integer gettLv3() {
        return tLv3;
    }

    public void settLv3(Integer tLv3) {
        this.tLv3 = tLv3;
    }

    public Integer getdGLv1() {
        return dGLv1;
    }

    public void setdGLv1(Integer dGLv1) {
        this.dGLv1 = dGLv1;
    }

    public Integer getdGLv2() {
        return dGLv2;
    }

    public void setdGLv2(Integer dGLv2) {
        this.dGLv2 = dGLv2;
    }

    public Integer getdGLv3() {
        return dGLv3;
    }

    public void setdGLv3(Integer dGLv3) {
        this.dGLv3 = dGLv3;
    }

    public Integer getdGLv4() {
        return dGLv4;
    }

    public void setdGLv4(Integer dGLv4) {
        this.dGLv4 = dGLv4;
    }

    public Integer getdGLv5() {
        return dGLv5;
    }

    public void setdGLv5(Integer dGLv5) {
        this.dGLv5 = dGLv5;
    }

    public Integer getdGLv6() {
        return dGLv6;
    }

    public void setdGLv6(Integer dGLv6) {
        this.dGLv6 = dGLv6;
    }

    public Integer getdGLv7() {
        return dGLv7;
    }

    public void setdGLv7(Integer dGLv7) {
        this.dGLv7 = dGLv7;
    }

    public Integer gettGLv1() {
        return tGLv1;
    }

    public void settGLv1(Integer tGLv1) {
        this.tGLv1 = tGLv1;
    }

    public Integer gettGLv2() {
        return tGLv2;
    }

    public void settGLv2(Integer tGLv2) {
        this.tGLv2 = tGLv2;
    }

    public Integer gettGLv3() {
        return tGLv3;
    }

    public void settGLv3(Integer tGLv3) {
        this.tGLv3 = tGLv3;
    }

    public Integer gettGLv4() {
        return tGLv4;
    }

    public void settGLv4(Integer tGLv4) {
        this.tGLv4 = tGLv4;
    }

    public Integer gettGLv5() {
        return tGLv5;
    }

    public void settGLv5(Integer tGLv5) {
        this.tGLv5 = tGLv5;
    }

    public Integer gettGLv6() {
        return tGLv6;
    }

    public void settGLv6(Integer tGLv6) {
        this.tGLv6 = tGLv6;
    }

    public Integer gettGLv7() {
        return tGLv7;
    }

    public void settGLv7(Integer tGLv7) {
        this.tGLv7 = tGLv7;
    }

    public Integer getdPLv1() {
        return dPLv1;
    }

    public void setdPLv1(Integer dPLv1) {
        this.dPLv1 = dPLv1;
    }

    public Integer getdPLv2() {
        return dPLv2;
    }

    public void setdPLv2(Integer dPLv2) {
        this.dPLv2 = dPLv2;
    }

    public Integer getdPLv3() {
        return dPLv3;
    }

    public void setdPLv3(Integer dPLv3) {
        this.dPLv3 = dPLv3;
    }

    public Integer getdPLv4() {
        return dPLv4;
    }

    public void setdPLv4(Integer dPLv4) {
        this.dPLv4 = dPLv4;
    }

    public Integer gettPLv1() {
        return tPLv1;
    }

    public void settPLv1(Integer tPLv1) {
        this.tPLv1 = tPLv1;
    }

    public Integer gettPLv2() {
        return tPLv2;
    }

    public void settPLv2(Integer tPLv2) {
        this.tPLv2 = tPLv2;
    }

    public Integer gettPLv3() {
        return tPLv3;
    }

    public void settPLv3(Integer tPLv3) {
        this.tPLv3 = tPLv3;
    }

    public Integer gettPLv4() {
        return tPLv4;
    }

    public void settPLv4(Integer tPLv4) {
        this.tPLv4 = tPLv4;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "KvmTeam{" +
        ", id=" + id +
        ", userId=" + userId +
        ", upUserId=" + upUserId +
        ", lv=" + lv +
        ", gLv=" + gLv +
        ", pLv=" + pLv +
        ", dLv0=" + dLv0 +
        ", dLv1=" + dLv1 +
        ", dLv2=" + dLv2 +
        ", dLv3=" + dLv3 +
        ", tLv0=" + tLv0 +
        ", tLv1=" + tLv1 +
        ", tLv2=" + tLv2 +
        ", tLv3=" + tLv3 +
        ", dGLv1=" + dGLv1 +
        ", dGLv2=" + dGLv2 +
        ", dGLv3=" + dGLv3 +
        ", dGLv4=" + dGLv4 +
        ", dGLv5=" + dGLv5 +
        ", dGLv6=" + dGLv6 +
        ", dGLv7=" + dGLv7 +
        ", tGLv1=" + tGLv1 +
        ", tGLv2=" + tGLv2 +
        ", tGLv3=" + tGLv3 +
        ", tGLv4=" + tGLv4 +
        ", tGLv5=" + tGLv5 +
        ", tGLv6=" + tGLv6 +
        ", tGLv7=" + tGLv7 +
        ", dPLv1=" + dPLv1 +
        ", dPLv2=" + dPLv2 +
        ", dPLv3=" + dPLv3 +
        ", dPLv4=" + dPLv4 +
        ", tPLv1=" + tPLv1 +
        ", tPLv2=" + tPLv2 +
        ", tPLv3=" + tPLv3 +
        ", tPLv4=" + tPLv4 +
        ", updateTime=" + updateTime +
        "}";
    }
}
