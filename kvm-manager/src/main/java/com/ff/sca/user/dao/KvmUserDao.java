package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
public interface KvmUserDao extends BaseMapper<KvmUser> {

    public List<KvmUser> teamZtree(@Param("pollCode") String pollCode);
}
