package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmTeam;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 团队成员统计表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-27
 */
public interface IKvmTeamService extends IService<KvmTeam> {

}
