package com.ff.sca.user.dao;

import com.ff.sca.user.entity.KvmUserLv;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 礼包直推配置表 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
public interface KvmUserLvDao extends BaseMapper<KvmUserLv> {

}
