package com.ff.sca.user.service;

import com.ff.sca.user.entity.KvmUserLv;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 礼包直推配置表 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-12-03
 */
public interface IKvmUserLvService extends IService<KvmUserLv> {

}
