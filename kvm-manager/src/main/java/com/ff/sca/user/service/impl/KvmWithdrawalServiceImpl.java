package com.ff.sca.user.service.impl;

import com.ff.sca.system.dao.KvmSysCodeDao;
import com.ff.sca.user.dao.KvmAccountDao;
import com.ff.sca.user.entity.KvmAccount;
import com.ff.sca.user.entity.KvmWithdrawal;
import com.ff.sca.user.dao.KvmWithdrawalDao;
import com.ff.sca.user.service.IKvmWithdrawalService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 美豆提现申请 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-29
 */
@Service
public class KvmWithdrawalServiceImpl extends ServiceImpl<KvmWithdrawalDao, KvmWithdrawal> implements IKvmWithdrawalService {

}
