package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmTeam;
import com.ff.sca.user.dao.KvmTeamDao;
import com.ff.sca.user.service.IKvmTeamService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 团队成员统计表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-12-27
 */
@Service
public class KvmTeamServiceImpl extends ServiceImpl<KvmTeamDao, KvmTeam> implements IKvmTeamService {

}
