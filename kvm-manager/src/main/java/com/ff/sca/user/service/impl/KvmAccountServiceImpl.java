package com.ff.sca.user.service.impl;

import com.ff.sca.user.entity.KvmAccount;
import com.ff.sca.user.dao.KvmAccountDao;
import com.ff.sca.user.service.IKvmAccountService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户表 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-11-16
 */
@Service
public class KvmAccountServiceImpl extends ServiceImpl<KvmAccountDao, KvmAccount> implements IKvmAccountService {

}
