package com.ff.sca.syslog.service;

import com.ff.sca.syslog.entity.ScaSysMangerLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 后台操作日志 服务类
 * </p>
 *
 * @author gw123
 * @since 2018-09-28
 */
public interface IScaSysMangerLogService extends IService<ScaSysMangerLog> {

}
