package com.ff.sca.syslog.service.impl;

import com.ff.sca.syslog.entity.ScaSysMangerLog;
import com.ff.sca.syslog.dao.ScaSysMangerLogDao;
import com.ff.sca.syslog.service.IScaSysMangerLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台操作日志 服务实现类
 * </p>
 *
 * @author gw123
 * @since 2018-09-28
 */
@Service
public class ScaSysMangerLogServiceImpl extends ServiceImpl<ScaSysMangerLogDao, ScaSysMangerLog> implements IScaSysMangerLogService {

}
