package com.ff.sca.syslog.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 后台操作日志
 * </p>
 *
 * @author gw123
 * @since 2018-12-27
 */
@TableName("sca_sys_manger_log")
public class ScaSysMangerLog extends Model<ScaSysMangerLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 操作人id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 操作人名称
     */
    @TableField("user_name")
    private String userName;
    private Integer mobile;
    private String ip;
    /**
     * 操作url
     */
    private String url;
    /**
     * 描述
     */
    private String description;
    /**
     * 操作模块
     */
    @TableField("operate_module")
    private String operateModule;
    /**
     * 操作类型
     */
    @TableField("operate_type")
    private String operateType;
    /**
     * 操作时间
     */
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    private String params;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getMobile() {
        return mobile;
    }

    public void setMobile(Integer mobile) {
        this.mobile = mobile;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperateModule() {
        return operateModule;
    }

    public void setOperateModule(String operateModule) {
        this.operateModule = operateModule;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ScaSysMangerLog{" +
        ", id=" + id +
        ", userId=" + userId +
        ", userName=" + userName +
        ", mobile=" + mobile +
        ", ip=" + ip +
        ", url=" + url +
        ", description=" + description +
        ", operateModule=" + operateModule +
        ", operateType=" + operateType +
        ", createTime=" + createTime +
        ", params=" + params +
        "}";
    }
}
