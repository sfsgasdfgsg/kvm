package com.ff.sca.syslog.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ff.sca.common.base.BaseController;
import com.ff.sca.common.entity.resmsg.LayuiTableResMsg;
import com.ff.sca.common.util.CharUtil;
import com.ff.sca.syslog.entity.ScaSysMangerLog;
import com.ff.sca.syslog.service.IScaSysMangerLogService;
import com.ff.sca.user.entity.KvmUser;
import com.ff.sca.user.service.IKvmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 后台操作日志 前端控制器
 * </p>
 *
 * @author gw123
 * @since 2018-09-28
 */
@Controller
@RequestMapping("/scaSysMangerLog")
public class ScaSysMangerLogController extends BaseController {

    @Autowired
    private IScaSysMangerLogService mangerLogService;
    @Autowired
    private IKvmUserService userService;

    /**
     * 系统日志列表
     *
     * @param page
     * @param size
     * @param per
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/managerLogList", method = RequestMethod.POST)
    public LayuiTableResMsg managerLogList(Integer page, Integer size, ScaSysMangerLog per) {
        try {
            Page<ScaSysMangerLog> pageEntity = setPageInfo(page, size);
            EntityWrapper ew = new EntityWrapper<ScaSysMangerLog>();
            if (CharUtil.isNotEmpty(per.getMobile())) {
                ew.eq("mobile", per.getMobile());
            }
            if (CharUtil.isNotEmpty(per.getDescription())) {
                ew.like("description", per.getDescription());
            }
            ew.orderBy("create_time", false);
            Page<ScaSysMangerLog> suserList = mangerLogService.selectPage(pageEntity, ew);
            long total = mangerLogService.selectCount(ew);
            return resTableSuccess(total, suserList.getRecords());

        } catch (Exception e) {
            e.printStackTrace();
            return resTableError(0, e.getMessage());
        }
    }


}

