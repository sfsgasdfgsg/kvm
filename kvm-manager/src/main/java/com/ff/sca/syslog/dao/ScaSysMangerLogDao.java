package com.ff.sca.syslog.dao;

import com.ff.sca.syslog.entity.ScaSysMangerLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 后台操作日志 Mapper 接口
 * </p>
 *
 * @author gw123
 * @since 2018-09-28
 */
public interface ScaSysMangerLogDao extends BaseMapper<ScaSysMangerLog> {

}
