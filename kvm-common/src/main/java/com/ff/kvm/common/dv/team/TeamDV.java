package com.ff.kvm.common.dv.team;

import java.io.Serializable;
import java.util.List;

public class TeamDV implements Serializable {

    private static final long serialVersionUID = -2893544934356090077L;

    private int total;

    private int giftLv;
    private String giftLvName;

    private int productLv;
    private String productLvName;

    private List<LvDV> user;
    private List<LvDV> giftUser;
    private List<LvDV> productUser;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getGiftLv() {
        return giftLv;
    }

    public void setGiftLv(int giftLv) {
        this.giftLv = giftLv;
    }

    public String getGiftLvName() {
        return giftLvName;
    }

    public void setGiftLvName(String giftLvName) {
        this.giftLvName = giftLvName;
    }

    public int getProductLv() {
        return productLv;
    }

    public void setProductLv(int productLv) {
        this.productLv = productLv;
    }

    public String getProductLvName() {
        return productLvName;
    }

    public void setProductLvName(String productLvName) {
        this.productLvName = productLvName;
    }

    public List<LvDV> getUser() {
        return user;
    }

    public void setUser(List<LvDV> user) {
        this.user = user;
    }

    public List<LvDV> getGiftUser() {
        return giftUser;
    }

    public void setGiftUser(List<LvDV> giftUser) {
        this.giftUser = giftUser;
    }

    public List<LvDV> getProductUser() {
        return productUser;
    }

    public void setProductUser(List<LvDV> productUser) {
        this.productUser = productUser;
    }
}
