package com.ff.kvm.common.dv.gift;

import java.io.Serializable;
import java.util.List;

public class GiftBoxDV implements Serializable {

    private static final long serialVersionUID = -9079462929688893667L;

    private int id;
    private String name;
    private double price;
    private double discount;
    private String remark;
    private String detail;
    private List<GiftBoxItemsDV> items;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public List<GiftBoxItemsDV> getItems() {
        return items;
    }

    public void setItems(List<GiftBoxItemsDV> items) {
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
