package com.ff.kvm.common.dv;

import java.io.Serializable;

public class NewsDetailDV implements Serializable {

    private static final long serialVersionUID = -6392217061309109672L;

    private String title;
    private String content;
    private String createTime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
