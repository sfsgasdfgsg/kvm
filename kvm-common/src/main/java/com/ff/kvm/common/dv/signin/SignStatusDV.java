package com.ff.kvm.common.dv.signin;

public class SignStatusDV {
    /**
     * 连续签到天数
     */
    private Integer num;
    /**
     * 当日是否签到
     */
    private Boolean curdateIsSignin;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Boolean getCurdateIsSignin() {
        return curdateIsSignin;
    }

    public void setCurdateIsSignin(Boolean curdateIsSignin) {
        this.curdateIsSignin = curdateIsSignin;
    }
}
