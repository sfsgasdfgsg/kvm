package com.ff.kvm.common.dv.team;

import java.io.Serializable;

public class LvDV implements Serializable {

    private static final long serialVersionUID = 682341383000286241L;

    private int lv;
    private String name;
    private int directPeople;
    private int treePeople;

    public LvDV() {
    }

    public LvDV(int lv, String name, int directPeople, int treePeople) {
        this.lv = lv;
        this.name = name;
        this.directPeople = directPeople;
        this.treePeople = treePeople;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDirectPeople() {
        return directPeople;
    }

    public void setDirectPeople(int directPeople) {
        this.directPeople = directPeople;
    }

    public int getTreePeople() {
        return treePeople;
    }

    public void setTreePeople(int treePeople) {
        this.treePeople = treePeople;
    }
}
