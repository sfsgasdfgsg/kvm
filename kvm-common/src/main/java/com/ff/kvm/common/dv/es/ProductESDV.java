package com.ff.kvm.common.dv.es;

import java.io.Serializable;
import java.util.Date;

public class ProductESDV implements Serializable {

    private static final long serialVersionUID = 2661459238807267506L;
    private int userId;
    private int orderId;
    private double total;
    private Date createTime;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
