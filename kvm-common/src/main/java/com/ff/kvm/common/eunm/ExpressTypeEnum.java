package com.ff.kvm.common.eunm;

public enum ExpressTypeEnum {

    SHIPPING(0, "物流"),
    TAKE_SELF(1, "自提"),
    ;

    private int type;
    private String name;

    ExpressTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (ExpressTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
