package com.ff.kvm.common.dv.account;

import java.io.Serializable;

public class AccountLogDV implements Serializable {

    private static final long serialVersionUID = -2184455911372653090L;

    private double num;
    private double amount;
    private double fee;
    private int changeType;
    private String changeTypeDes;
    private String createTime;


    public String getChangeTypeDes() {
        return changeTypeDes;
    }

    public void setChangeTypeDes(String changeTypeDes) {
        this.changeTypeDes = changeTypeDes;
    }

    public double getNum() {
        return num;
    }

    public void setNum(double num) {
        this.num = num;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getChangeType() {
        return changeType;
    }

    public void setChangeType(int changeType) {
        this.changeType = changeType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
