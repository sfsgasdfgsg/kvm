package com.ff.kvm.common.eunm;

public enum RechargeStatusEnum {



    PAYMENT(3, "待支付"),
    FAIL(4, "支付失败"),
    COMPLETE(11, "支付成功"),

    ;

    private int type;
    private String name;

    RechargeStatusEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (RechargeStatusEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
