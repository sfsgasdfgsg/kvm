package com.ff.kvm.common.dv.wx.spread;

import java.io.Serializable;

public class SceneStrDV implements Serializable {

    private static final long serialVersionUID = -7507103576042298852L;
    private String scene_str;

    public SceneStrDV(String scene_str) {
        this.scene_str = scene_str;
    }

    public String getScene_str() {
        return scene_str;
    }

    public void setScene_str(String scene_str) {
        this.scene_str = scene_str;
    }
}
