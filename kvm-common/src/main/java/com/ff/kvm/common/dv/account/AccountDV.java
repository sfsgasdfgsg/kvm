package com.ff.kvm.common.dv.account;

import java.io.Serializable;

public class AccountDV implements Serializable {

    private static final long serialVersionUID = 4153636656385817078L;

    private double md;
    private double gwj;

    public double getMd() {
        return md;
    }

    public void setMd(double md) {
        this.md = md;
    }

    public double getGwj() {
        return gwj;
    }

    public void setGwj(double gwj) {
        this.gwj = gwj;
    }
}
