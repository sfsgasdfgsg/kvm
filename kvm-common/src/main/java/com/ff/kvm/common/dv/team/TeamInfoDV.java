package com.ff.kvm.common.dv.team;

import java.io.Serializable;

public class TeamInfoDV implements Serializable {

    private static final long serialVersionUID = 374570220756238526L;

    private int teamGiftBoxLv;
    private String teamGiftBoxName;
    private double teamGiftBoxTotal;

    private int teamProductLv;
    private String teamProductName;
    private double teamProductTotal;

    public int getTeamGiftBoxLv() {
        return teamGiftBoxLv;
    }

    public void setTeamGiftBoxLv(int teamGiftBoxLv) {
        this.teamGiftBoxLv = teamGiftBoxLv;
    }

    public String getTeamGiftBoxName() {
        return teamGiftBoxName;
    }

    public void setTeamGiftBoxName(String teamGiftBoxName) {
        this.teamGiftBoxName = teamGiftBoxName;
    }

    public double getTeamGiftBoxTotal() {
        return teamGiftBoxTotal;
    }

    public void setTeamGiftBoxTotal(double teamGiftBoxTotal) {
        this.teamGiftBoxTotal = teamGiftBoxTotal;
    }

    public int getTeamProductLv() {
        return teamProductLv;
    }

    public void setTeamProductLv(int teamProductLv) {
        this.teamProductLv = teamProductLv;
    }

    public String getTeamProductName() {
        return teamProductName;
    }

    public void setTeamProductName(String teamProductName) {
        this.teamProductName = teamProductName;
    }

    public double getTeamProductTotal() {
        return teamProductTotal;
    }

    public void setTeamProductTotal(double teamProductTotal) {
        this.teamProductTotal = teamProductTotal;
    }
}
