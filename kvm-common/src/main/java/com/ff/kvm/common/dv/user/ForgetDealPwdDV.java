package com.ff.kvm.common.dv.user;

import java.io.Serializable;

public class ForgetDealPwdDV implements Serializable {


    private static final long serialVersionUID = -6136210206054401730L;
    private int userId;
    private String mobile;
    private String code;
    private String kaptchaCode;
    private String ip;
    private String dealPwd;

    public String getDealPwd() {
        return dealPwd;
    }

    public void setDealPwd(String dealPwd) {
        this.dealPwd = dealPwd;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKaptchaCode() {
        return kaptchaCode;
    }

    public void setKaptchaCode(String kaptchaCode) {
        this.kaptchaCode = kaptchaCode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
