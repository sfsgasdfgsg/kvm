package com.ff.kvm.common.dv;

import java.io.Serializable;

public class NewsDV implements Serializable {

    private static final long serialVersionUID = 481056759961833105L;

    private int id;
    private String title;
    private String createTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
