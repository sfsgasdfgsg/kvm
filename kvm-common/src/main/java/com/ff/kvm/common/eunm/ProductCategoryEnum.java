package com.ff.kvm.common.eunm;

public enum ProductCategoryEnum {

    DPHFZQ(1, "单排护肤专区"),
    KSZQ(2, "抗衰专区"),
    MBBSZQ(3, "美白补水专区"),
    MZZQ(4, "美妆专区"),
    YZZQ(5, "院装专区"),
    SPECIAL(6, "特色专区"),


    ;

    private int type;
    private String name;

    ProductCategoryEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (ProductCategoryEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
