package com.ff.kvm.common.dv.msg;

import java.io.Serializable;

public class Message implements Serializable {

    private static final long serialVersionUID = -8753869356862049093L;

    private int type;//0注册消息，1购买礼包消息
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
