package com.ff.kvm.common.dv.order;

import java.io.Serializable;
import java.util.List;

public class OrderDV implements Serializable {

    private static final long serialVersionUID = -5936342902006661734L;

    private int id;
    private int status;
    private String statusDes;
    private String no;
    private int quantity;
    private double amount;
    private double postFee;
    private double total;
    private String createTime;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusDes() {
        return statusDes;
    }

    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }

    private List<OrderItemsDV> items;

    public List<OrderItemsDV> getItems() {
        return items;
    }

    public void setItems(List<OrderItemsDV> items) {
        this.items = items;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public double getPostFee() {
        return postFee;
    }

    public void setPostFee(double postFee) {
        this.postFee = postFee;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
