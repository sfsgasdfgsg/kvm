package com.ff.kvm.common.util;

import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.vo.Response;


public class ResponseUtil {

    public static Response fail(){
        return new Response(StatusEnum.DATA_ERROR.code(),StatusEnum.DATA_ERROR.description());
    }

    public static Response success(){
        return new Response(StatusEnum.SUCCESS.code(),StatusEnum.SUCCESS.description());
    }


}
