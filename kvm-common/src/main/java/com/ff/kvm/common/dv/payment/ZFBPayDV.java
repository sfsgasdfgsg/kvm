package com.ff.kvm.common.dv.payment;

import java.io.Serializable;

public class ZFBPayDV implements Serializable {

    private static final long serialVersionUID = -8737653456967382430L;

    private String out_trade_no;//商户订单号,64个字符以内、只能包含字母、数字、下划线；需保证在商户端不重复
    private double total_amount;//金额
    private String subject;//订单标题

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(double total_amount) {
        this.total_amount = total_amount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
