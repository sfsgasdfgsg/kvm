package com.ff.kvm.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

public class UuidUtil {
    public static String uuid(){
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public static String nowDateStr() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
    }

    public static String nowDateUuid() {
        String str = nowDateStr();
        String str1 = uuid();
        String str2 = str + str1.substring(0, 4);
        return str2;
    }

    public static String uuid18(){
        return uuid().substring(0,18);
    }
    public static String uuid32(){
        return uuid().substring(0,31);
    }


}
