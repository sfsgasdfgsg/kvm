package com.ff.kvm.common.dv.home;

import java.io.Serializable;

public class HomeSpecialProductDV implements Serializable {

    private static final long serialVersionUID = -726685870841859128L;

    private String icon;
    private int id;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
