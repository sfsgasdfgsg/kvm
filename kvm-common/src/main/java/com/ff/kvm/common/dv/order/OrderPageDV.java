package com.ff.kvm.common.dv.order;

import java.io.Serializable;

public class OrderPageDV implements Serializable {

    private static final long serialVersionUID = -8186126773207163026L;

    private Integer userId;
    private Integer status;
    private Integer pageNum;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
}
