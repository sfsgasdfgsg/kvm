package com.ff.kvm.common.eunm;

public enum UserTeamProductLvEnum {

    LV0(0, "无"),
    LV1(1, "一级"),
    LV2(2, "二级"),
    LV3(3, "三级"),
    LV4(4, "四级"),
    ;

    private int type;
    private String name;

    UserTeamProductLvEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (UserTeamProductLvEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
