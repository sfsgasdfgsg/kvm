package com.ff.kvm.common.dv.shopping;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartDV implements Serializable {

    private static final long serialVersionUID = -2066828853195023577L;

    private int userId;
    private List<ItemsDV> items;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<ItemsDV> getItems() {
        if(items == null){
            return new ArrayList<>();
        }
        return items;
    }

    public void setItems(List<ItemsDV> items) {
        this.items = items;
    }
}
