package com.ff.kvm.common.dv.payment;

import java.io.Serializable;

public class PaymentResult implements Serializable {

    private static final long serialVersionUID = 2581759796636478535L;
    private String zfb_info;
    private String trade_no;

    private String partnerid;
    private String prepayid;
    private String noncestr;
    private String timestamp;
    private String sign;

    public String getZfb_info() {
        return zfb_info;
    }

    public void setZfb_info(String zfb_info) {
        this.zfb_info = zfb_info;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
