package com.ff.kvm.common.eunm;

public enum CashStatusEnum {


    APPROVAL(0, "待审核"),
    FAIL(1, "审核不通过"),
    CANCLE(2, "已取消"),
    SUCCESS(3, "提现成功"),

    ;

    private int type;
    private String name;

    CashStatusEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (CashStatusEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
