package com.ff.kvm.common.dv;

import java.io.Serializable;

public class IdsDV implements Serializable {

    private static final long serialVersionUID = 1158094772828650485L;

    private int userId;
    private int[] productIds;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int[] getProductIds() {
        return productIds;
    }

    public void setProductIds(int[] productIds) {
        this.productIds = productIds;
    }
}
