package com.ff.kvm.common.dv.signin;

import java.util.List;
import java.util.Map;

public class SigninGiftTimeNodeDV {
    /**
     * 签到最大天数
     */
    private Integer signInMaxNo;
    /**
     * 领取礼物时间节点   map（领取时间节点，领取的礼物id）
     */
    private List<SigninGiftSetingDV> giftTimeNode;

    public Integer getSignInMaxNo() {
        return signInMaxNo;
    }

    public void setSignInMaxNo(Integer signInMaxNo) {
        this.signInMaxNo = signInMaxNo;
    }

    public List<SigninGiftSetingDV> getGiftTimeNode() {
        return giftTimeNode;
    }

    public void setGiftTimeNode(List<SigninGiftSetingDV> giftTimeNode) {
        this.giftTimeNode = giftTimeNode;
    }
}
