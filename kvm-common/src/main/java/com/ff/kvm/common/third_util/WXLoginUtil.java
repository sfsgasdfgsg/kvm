package com.ff.kvm.common.third_util;

import com.ff.kvm.common.dv.wx.WXLoginDV;
import com.ff.kvm.common.dv.wx.WXUserInfoDV;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.HttpUtil;

public class WXLoginUtil {

    private static String appid = "wxdf844bbba0df4c36";
    private static String secret = "701a8e3db607697c9bf56983f83a1a86";
    private static String access_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+appid+"&secret="+secret+"&grant_type=authorization_code&code=";
    private static String userinfo_url = "https://api.weixin.qq.com/sns/userinfo?access_token=";
    private static String access_token_url_spread = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+secret;


    //access_token
    public static WXLoginDV getAccessToken(){
        String result = HttpUtil.sendGet(access_token_url_spread);
        System.out.println(result);
        return ConvertUtil.jsonToBean(result,WXLoginDV.class);
    }

    // auth2.0通过code 换取access_token
    public static WXLoginDV getAccessToken(String code){

        return ConvertUtil.jsonToBean(HttpUtil.sendGet(access_token_url+code),WXLoginDV.class);
    }

    // access_token 、openid 获取用户信息
    private static WXUserInfoDV getUserInfo(String accessToken,String openId){

        return ConvertUtil.jsonToBean( HttpUtil.sendGet(access_token_url+accessToken+"&openid="+openId), WXUserInfoDV.class);
    }

}
