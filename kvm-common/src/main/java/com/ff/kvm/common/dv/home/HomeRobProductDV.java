package com.ff.kvm.common.dv.home;

import java.io.Serializable;


public class HomeRobProductDV implements Serializable {

    private static final long serialVersionUID = 5559430685326590833L;


    private String icon;
    private double currentPrice;
    private String endTime;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }


    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
