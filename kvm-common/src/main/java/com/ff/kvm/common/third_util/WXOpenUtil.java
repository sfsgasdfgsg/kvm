package com.ff.kvm.common.third_util;

import com.ff.kvm.common.dv.wx.WXLoginDV;
import com.ff.kvm.common.dv.wx.WXUserInfoDV;
import com.ff.kvm.common.dv.wx.spread.ForeverQRCodeDV;
import com.ff.kvm.common.dv.wx.spread.TempQRCodeDV;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.HttpUtil;
import com.ff.kvm.common.util.MyUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class WXOpenUtil {

    private static String appid = "wxdf844bbba0df4c36";
    private static String appsecret = "caabe27c0eed427afa8cd3171e9adf10";
    private static String userinfo_url = "https://api.weixin.qq.com/sns/userinfo?";
    private static String access_token_url = "https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code&appid="+appid+"&secret="+appsecret+"&code=";
    private static String refresh_access_token_url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?grant_type=refresh_token&appid="+appid+"&refresh_token=";


    // auth2.0通过code 换取access_token
    public static WXLoginDV getAccessToken(String code){

        return ConvertUtil.jsonToBean(HttpUtil.sendGet(access_token_url+code),WXLoginDV.class);
    }


    public static WXUserInfoDV getUserInfo(String accessToken, String openId) {
        StringBuffer url = new StringBuffer(userinfo_url);
        url.append("access_token=").append(accessToken);
        url.append("&openid=").append(openId);
        return ConvertUtil.jsonToBean(HttpUtil.sendGet(url.toString()),WXUserInfoDV.class);
    }

    public static WXLoginDV refreshToken(String refreshToken) {
        return ConvertUtil.jsonToBean(HttpUtil.sendGet(refresh_access_token_url+refreshToken),WXLoginDV.class);
    }
}
