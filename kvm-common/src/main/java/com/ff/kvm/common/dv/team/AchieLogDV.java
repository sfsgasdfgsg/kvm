package com.ff.kvm.common.dv.team;

import java.io.Serializable;

public class AchieLogDV implements Serializable {

    private static final long serialVersionUID = 4895731731773544409L;

    private int userId;
    private String name;
    private double total;
    private String orderNo;
    private String createTime;

    private String giftBoxName;

    public String getGiftBoxName() {
        return giftBoxName;
    }

    public void setGiftBoxName(String giftBoxName) {
        this.giftBoxName = giftBoxName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
