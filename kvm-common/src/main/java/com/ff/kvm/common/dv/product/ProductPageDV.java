package com.ff.kvm.common.dv.product;

import java.io.Serializable;

public class ProductPageDV implements Serializable {

    private static final long serialVersionUID = 1455254378198733350L;

    private int userId;
    private Integer pageNum;
    private Integer category;
    private int type;//1普通商品，2礼品，3热销，4抢购，5特色商品
    private boolean isDiscount;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isDiscount() {
        return isDiscount;
    }

    public void setDiscount(boolean discount) {
        isDiscount = discount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
}
