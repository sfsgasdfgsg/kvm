package com.ff.kvm.common.dv.signin;

public class SigninGiftSetingDV {

    private Integer signinNo;
    private Integer giftProductId;

    public Integer getSigninNo() {
        return signinNo;
    }

    public void setSigninNo(Integer signinNo) {
        this.signinNo = signinNo;
    }

    public Integer getGiftProductId() {
        return giftProductId;
    }

    public void setGiftProductId(Integer giftProductId) {
        this.giftProductId = giftProductId;
    }
}
