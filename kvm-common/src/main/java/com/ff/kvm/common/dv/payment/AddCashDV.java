package com.ff.kvm.common.dv.payment;

import java.io.Serializable;

public class AddCashDV implements Serializable {

    private static final long serialVersionUID = -782977336345950086L;
    private int userId;
    private double total;

    private String bankName;
    private String branchBanckName;
    private String account;
    private String accountName;
    private int type;//0支付宝，1银行卡
    private String dealPwd;



    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDealPwd() {
        return dealPwd;
    }

    public void setDealPwd(String dealPwd) {
        this.dealPwd = dealPwd;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }


    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }


    public String getBranchBanckName() {
        return branchBanckName;
    }

    public void setBranchBanckName(String branchBanckName) {
        this.branchBanckName = branchBanckName;
    }
}
