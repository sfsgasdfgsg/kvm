package com.ff.kvm.common.dv.shopping;

import java.io.Serializable;

public class EditItemsDV implements Serializable {

    private static final long serialVersionUID = 1961220652877954472L;

    private int userId;
    private int productId;
    private int quantity;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
