package com.ff.kvm.common.dv.order;

import java.io.Serializable;

public class OrderExpressDV implements Serializable {

    private static final long serialVersionUID = 3036136499175466149L;

    private int expressStatus;
    private String expressStatusDes;
    private String expressName;
    private String expressNo;
    private String sendTime;
    private String signTime;

    public String getSignTime() {
        return signTime;
    }

    public void setSignTime(String signTime) {
        this.signTime = signTime;
    }

    public int getExpressStatus() {
        return expressStatus;
    }

    public void setExpressStatus(int expressStatus) {
        this.expressStatus = expressStatus;
    }

    public String getExpressStatusDes() {
        return expressStatusDes;
    }

    public void setExpressStatusDes(String expressStatusDes) {
        this.expressStatusDes = expressStatusDes;
    }

    public String getExpressName() {
        return expressName;
    }

    public void setExpressName(String expressName) {
        this.expressName = expressName;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }
}
