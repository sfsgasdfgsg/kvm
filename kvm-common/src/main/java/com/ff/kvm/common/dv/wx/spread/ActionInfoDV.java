package com.ff.kvm.common.dv.wx.spread;

import java.io.Serializable;

public class ActionInfoDV implements Serializable {

    private static final long serialVersionUID = 5273034122454881008L;

    private SceneStrDV scene;

    public ActionInfoDV(String code) {
        this.scene = new SceneStrDV(code);
    }

    public SceneStrDV getScene() {
        return scene;
    }

    public void setScene(SceneStrDV scene) {
        this.scene = scene;
    }
}
