package com.ff.kvm.common.dv.team;


import java.io.Serializable;
import java.util.List;

public class AchieInfoDV implements Serializable {

    private static final long serialVersionUID = 3560691444200124436L;

    private double productTotal;
    private double giftBoxTotal;
    private Long total;
    private Integer pageSize;

    private List<AchieLogDV> data;

    public double getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(double productTotal) {
        this.productTotal = productTotal;
    }

    public double getGiftBoxTotal() {
        return giftBoxTotal;
    }

    public void setGiftBoxTotal(double giftBoxTotal) {
        this.giftBoxTotal = giftBoxTotal;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<AchieLogDV> getData() {
        return data;
    }

    public void setData(List<AchieLogDV> data) {
        this.data = data;
    }
}
