package com.ff.kvm.common.dv.gift;

import java.io.Serializable;

public class GiftBoxItemsDV implements Serializable {

    private static final long serialVersionUID = 823563258936713275L;

    private String name;
    private int productId;
    private int quantity;
    private String price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
