package com.ff.kvm.common.dv.team;

import java.io.Serializable;

public class BonusLogDV implements Serializable {

    private static final long serialVersionUID = -3940600401174590514L;

    private int userId;
    private String name;
    private double total;
    private String createTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
