package com.ff.kvm.common.dv.lv;

import java.io.Serializable;

public class SecondDV implements Serializable {

    private static final long serialVersionUID = 946604663122025330L;

    private int lv;
    private double point;

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public double getPoint() {
        return point;
    }

    public void setPoint(double point) {
        this.point = point;
    }
}
