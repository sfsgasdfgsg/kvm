package com.ff.kvm.common.dv.shopping;

import java.io.Serializable;

public class ItemsDV implements Serializable {

    private static final long serialVersionUID = 7610256840407950603L;

    private int productId;
    private int quantity;

    public ItemsDV() {
    }

    public ItemsDV(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
