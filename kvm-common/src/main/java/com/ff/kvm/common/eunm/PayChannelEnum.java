package com.ff.kvm.common.eunm;

public enum PayChannelEnum {

    WGJ(0, "购物金"),
    WX(1, "微信"),
    ZFB(2, "支付宝"),
    MD(3, "美豆"),
    ;

    private int type;
    private String name;

    PayChannelEnum(int type, String name)
    {
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (PayChannelEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
