package com.ff.kvm.common.dv;

import java.io.Serializable;

public class RollListDV implements Serializable {

    private static final long serialVersionUID = 7434328438760252225L;

    private int id;
    private String img;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
