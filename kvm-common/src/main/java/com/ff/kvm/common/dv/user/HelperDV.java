package com.ff.kvm.common.dv.user;

import java.io.Serializable;

public class HelperDV implements Serializable {

    private static final long serialVersionUID = 1340076283251844336L;

    private int id;
    private String title;
    private String content;
    private String createTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
