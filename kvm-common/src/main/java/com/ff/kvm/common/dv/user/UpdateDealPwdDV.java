package com.ff.kvm.common.dv.user;

import java.io.Serializable;

public class UpdateDealPwdDV implements Serializable {
    private static final long serialVersionUID = 5500892449944635616L;

    private int userId;
    private String oldDealPwd;
    private String newDealPwd;



    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getOldDealPwd() {
        return oldDealPwd;
    }

    public void setOldDealPwd(String oldDealPwd) {
        this.oldDealPwd = oldDealPwd;
    }

    public String getNewDealPwd() {
        return newDealPwd;
    }

    public void setNewDealPwd(String newDealPwd) {
        this.newDealPwd = newDealPwd;
    }
}
