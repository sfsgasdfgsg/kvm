package com.ff.kvm.common.eunm;

public enum MessageTypeEnum {

    REGISTER(0, "新用户注册"),
    GIFTBOX(1, "礼包购买"),
    ;

    private int type;
    private String name;

    MessageTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (MessageTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
