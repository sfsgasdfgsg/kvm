package com.ff.kvm.common.eunm;

public enum ProductTypeEnum {

    PRODUCT(0, "商品"),
    GIFT_BOX(1, "礼包"),
    GIFT(2, "礼品"),
    HOT(3, "热销商品"),
    ROB(4, "抢购商品"),
    SIGIN_GIFY(6, "签到奖品"),
    GIFT_BOX_PRODUCT(7, "礼包商品");

    private int type;
    private String name;

    ProductTypeEnum(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (ProductTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
