package com.ff.kvm.common.dv.user;

import java.io.Serializable;

public class BindPhoneDV implements Serializable {

    private static final long serialVersionUID = 8595780541709256285L;

    private int userId;
    private String mobile;
    private String code;
    private String kaptchaCode;
    private String ip;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKaptchaCode() {
        return kaptchaCode;
    }

    public void setKaptchaCode(String kaptchaCode) {
        this.kaptchaCode = kaptchaCode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
