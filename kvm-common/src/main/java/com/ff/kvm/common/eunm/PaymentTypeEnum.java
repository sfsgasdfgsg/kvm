package com.ff.kvm.common.eunm;

public enum PaymentTypeEnum {

    SHOPPING(0, "购物"),
    RECHARGE(1, "充值"),
    ;

    private int type;
    private String name;

    PaymentTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (PaymentTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
