package com.ff.kvm.common.dv.product;

import java.io.Serializable;
import java.util.List;

public class CategoryDV implements Serializable {
    private static final long serialVersionUID = -3994523851813016384L;

    private int id;
    private String name;
    private String backGroundImg;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private List<ProductDV> data;

    public List<ProductDV> getData() {
        return data;
    }

    public void setData(List<ProductDV> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackGroundImg() {
        return backGroundImg;
    }

    public void setBackGroundImg(String backGroundImg) {
        this.backGroundImg = backGroundImg;
    }
}
