package com.ff.kvm.common.dv.wx.spread;

import java.io.Serializable;

public class TempQRCodeDV implements Serializable {

    private static final long serialVersionUID = 541872929578367671L;
    private String expire_seconds = "2591000";
    private String action_name = "QR_STR_SCENE";
    private ActionInfoDV action_info;

    public TempQRCodeDV(String code) {
        this.action_info = new ActionInfoDV(code);
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public ActionInfoDV getAction_info() {
        return action_info;
    }

    public void setAction_info(ActionInfoDV action_info) {
        this.action_info = action_info;
    }

    public String getExpire_seconds() {
        return expire_seconds;
    }

    public void setExpire_seconds(String expire_seconds) {
        this.expire_seconds = expire_seconds;
    }
}
