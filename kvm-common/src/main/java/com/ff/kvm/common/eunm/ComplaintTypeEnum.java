package com.ff.kvm.common.eunm;

public enum ComplaintTypeEnum {

    SUGGUEST(0, "建议/意见"),
    COMPLAINT(1, "投诉/举报"),
    ACCOUNT(2, "账号相关"),
    OTHER(3, "其他"),
    ;

    private int type;
    private String name;

    ComplaintTypeEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (ComplaintTypeEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
