package com.ff.kvm.common.util;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.ConverterRegistry;
import cn.hutool.core.util.XmlUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ff.kvm.common.dv.lv.SecondDV;
import com.ff.kvm.common.dv.wx.WXLoginDV;
import com.ff.kvm.common.third_util.WXPayUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.lang.reflect.Method;
import java.util.*;

public class ConvertUtil {
    public static <T> T convert(Object obj, Class<T> t){
        ConverterRegistry registry = ConverterRegistry.getInstance();
        return registry.convert(t, obj);
    }

    public static <T> List<T> convertList(List list, Class<T> t) {
        ConverterRegistry registry = ConverterRegistry.getInstance();
        List list2 = new ArrayList();
        for (Iterator localIterator = list.iterator(); localIterator.hasNext(); ) {
            Object obj = localIterator.next();
            Object obj2 = registry.convert(t, obj);
            list2.add(obj2);
        }
        return list2;
    }

    public static Map<String, Object> convertMap(Object obj) {
        return BeanUtil.beanToMap(obj);
    }

    public static <T> List<T> jsonToList(String json, Class<T> t){

        return JSON.parseArray(json, t);
    }
    public static <T> Map<Object,Object> jsonToMap(String json, Class<T> t,String key,String value){
        Map<Object,Object> map = new HashMap<>();
        List<T> list = JSON.parseArray(json, t);
        for (T t2 : list){
            map.put(getFieldValueByName(key,t2), getFieldValueByName(value,t2));
        }
        return map;
    }

    private static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            return value;
        } catch (Exception e) {

            return null;
        }
    }

    public static Map<String,String> xmlToMap(String xml){
//        System.out.println(xml);
        Map<String,String> map = new HashMap<>();
        xml = xml.replace("\n","").replace("<![CDATA[","").replace("]]>","");
//        System.out.println(xml);
        Document doc = XmlUtil.readXML(xml);
        NodeList nodeList = doc.getFirstChild().getChildNodes();
        for (int i = 0;i<nodeList.getLength();i++){
            Node node = nodeList.item(i);
            map.put(node.getNodeName(),node.getFirstChild().getNodeValue());
        }
        return map;
    }

    public static void main(String[] args){
        String json = "<xml><appid>wxdf844bbba0df4c36</appid><bank_type>CCB_DEBIT</bank_type><cash_fee>1</cash_fee><fee_type>CNY</fee_type><is_subscribe>N</is_subscribe><mch_id>1521489101</mch_id><nonce_str>F8AEA69F632A48C49F3227ABD273205</nonce_str><openid>ohlVz0dTRYmI_mFYMRLNlzK1p0c0</openid><out_trade_no>KVMRC201812222204008B72</out_trade_no><result_code>SUCCESS</result_code><return_code>SUCCESS</return_code><sign>A31F6267D22545A940218AABD88C4666</sign><time_end>20181222220416</time_end><total_fee>1</total_fee><trade_type>APP</trade_type><transaction_id>4200000229201812223554143498</transaction_id></xml>";
//        Map<Object,Object> map = jsonToMap(json,SecondDV.class,"lv","point");
//        System.out.println(map);
        Map<String,String> map = ConvertUtil.xmlToMap(json);
        WXPayUtil.checkSign(map);
    }

    public static <T> T jsonToBean(String json, Class<T> t) {

        return JSON.parseObject(json,t);
    }

    public static String toJSON(Object obj){
        if (obj == null) {
            return null;
        }
        return JSONObject.toJSONString(obj);
    }
}
