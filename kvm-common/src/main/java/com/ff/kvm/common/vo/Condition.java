package com.ff.kvm.common.vo;

import java.io.Serializable;

public class Condition implements Serializable {

    private static final long serialVersionUID = -6444230635724682321L;

    private String tokenId;
    private int pageNum;
    private int userId;
    private String startTime;
    private String endTime;
    private int tradeType;
    private String userName;
    private int buyUserId;
    private String mobile;
    private int uid;
    private int changeType;
    private int tradeStatus;
    private int upUserId;
    private int hostStatus;

    public int getHostStatus()
    {
        return this.hostStatus;
    }

    public void setHostStatus(int hostStatus) {
        this.hostStatus = hostStatus;
    }

    public int getUpUserId() {
        return this.upUserId;
    }

    public void setUpUserId(int upUserId) {
        this.upUserId = upUserId;
    }

    public String getTokenId() {
        return this.tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public int getUid() {
        return this.uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getTradeType() {
        return this.tradeType;
    }

    public void setTradeType(int tradeType) {
        this.tradeType = tradeType;
    }

    public int getTradeStatus() {
        return this.tradeStatus;
    }

    public void setTradeStatus(int tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public int getChangeType() {
        return this.changeType;
    }

    public void setChangeType(int changeType) {
        this.changeType = changeType;
    }

    public int getBuyUserId() {
        return this.buyUserId;
    }

    public void setBuyUserId(int buyUserId) {
        this.buyUserId = buyUserId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getPageNum()
    {
        return this.pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStartTime()
    {
        return this.startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return this.endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
