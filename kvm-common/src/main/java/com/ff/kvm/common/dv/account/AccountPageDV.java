package com.ff.kvm.common.dv.account;

import java.io.Serializable;

public class AccountPageDV implements Serializable {

    private static final long serialVersionUID = -8531455569939668276L;


    private int userId;
    private int type;
    private int changeType;
    private int pageNum;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getChangeType() {
        return changeType;
    }

    public void setChangeType(int changeType) {
        this.changeType = changeType;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
}
