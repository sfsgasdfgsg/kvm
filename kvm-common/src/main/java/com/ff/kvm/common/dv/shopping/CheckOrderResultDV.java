package com.ff.kvm.common.dv.shopping;

import java.io.Serializable;

public class CheckOrderResultDV implements Serializable {

    private static final long serialVersionUID = -6558870199996577389L;

    private int orderId;
    private String orderNO;
    private String paymentNO;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getPaymentNO() {
        return paymentNO;
    }

    public void setPaymentNO(String paymentNO) {
        this.paymentNO = paymentNO;
    }
}
