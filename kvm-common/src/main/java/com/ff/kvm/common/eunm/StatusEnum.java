package com.ff.kvm.common.eunm;

public enum StatusEnum {

    RE_LOGIN("100", "请先登录"),
    SUCCESS("200", "成功"),
    FAIL("400", "失败"),


    DATA_ERROR("500", "请稍后重试"),
    RULE_ERROR("501", "格式错误"),
    STATUS_ERROR("502", "状态错误"),


    POLL_USER_NOT_EXITS("451", "推广人不存在"),
    MOBILE_EXIST("452", "电话号码已经注册"),

    ACCOUNT_NOT_ENOUGH("454", "账户余额不足"),

    MIN_QUANTITY("457", "数量不能小于"),
    MAX_QUANTITY("457", "数量不能高于"),
    REAL_AUTH("458", "已实名认证"),
    REAL_AUTH_STATUS_ERROR("458", "状态错误"),
    OVER_MAX_QTY("460", "超出最大可卖出数量"),
    LOGIN_LOCK("461", "用户被锁定"),
    PWD_LOGIN_DEAL_("462", "登录密码、交易密码不能相同"),
    LOGIN_FROZEN("463", "用户被冻结"),
    MAX_TODAY_TRADE("464", "已达到日交易最大笔数"),
    SIGNED_IN("465", "今日已经签到"),



    PWD_ERROR("600", "密码不正确"),
    USER_OR_PWD_ERROR("601", "帐号或密码错误"),
    DEAL_PWD_ERROR("602", "交易密码错误"),
    AGAIN_PWD_ERROR("603", "两次输入密码不一致"),
    LOGIN_PWD_ERROR("604", "登录密码错误"),
    USER_NAME_NOT_EXITS("605", "账号不存在"),
    AUTH_FIRST("606", "请先实名认证"),
    IS_UPDATE_PAY("607", "只能修改一次"),

    TRADE_STATUS_CHANGE("700", "交易状态改变"),


    AUTH_ERROR("750", "权限不足"),
    AUTH_BLACK("701","禁止登录，已被加入黑名单"),


    VD_ERROR_QUANTITY("800", "数量不能为0"),
    VD_ERROR_KAPTCHA_CODE("800", "验证码错误"),
    VD_ERROR_DYNAMIC_CODE("800", "动态码错误"),
    VD_ERROR_MOBILE("800", "电话号码错误"),
    VD_ERROR_DEAL_PWD("800", "支付密码错误"),
    VD_ERROR_UP_FILE("800", "上传文件失败"),

    VD_EMPTY_UP_FILE("900", "上传文件为空"),
    VD_EMPTY_KAPTCHA_CODE("900", "验证码不能为空"),
    VD_EMPTY_MOBILE("900", "电话号码不能为空"),
    VD_EMPTY_CODE("900", "动态码不能为空"),
    VD_EMPTY_LOGIN_PWD("900", "登录密码不能为空"),
    VD_EMPTY_DEAL_PWD("900", "支付密码不能为空"),
    VD_EMPTY_POLL_CODE("900", "推广码不能为空"),
    VD_EMPTY_NAME("900", "名字不能为空"),
    VD_EMPTY_BLANK_USER_NAME("900", "持卡人姓名不能为空"),
    VD_EMPTY_BLANK_USER_ERROR("901", "持卡人姓名需与当前用户保持一致"),
    VD_EMPTY_ID_CARD("900", "身份证号码不能为空"),
    VD_EMPTY_ID_CARD_ABOVE("900", "身份证正面照片不能为空"),
    VD_EMPTY_ID_CARD_BELOW("900", "身份证背面照片不能为空"),
    VD_EMPTY_AUTH_EXITS("900", "该身份证号码已经认证"),
    VD_EMPTY_ID_WX("900", "微信号不能为空"),
    VD_EMPTY_ID_ALI_PAY("900", "支付宝账号不能为空"),
    VD_EMPTY_BLANK_NAME("900", "开户行名称不能为空"),
    VD_EMPTY_BLANK_NAME_BRANCH("900", "开户支行名称不能为空"),
    VD_EMPTY_BLANK_CARD("900", "银行卡账号不能为空"),
    VD_EMPTY_BLANK_CARD_ABOVE("900", "银行卡正面照片不能为空"),
    VD_EMPTY_CACHE("900", "键或值都不能为空"),

    NOT_EXIST_PAYEE("910", "收款人不存在"),
    NOT_EXIST_MOBILE("910", "手机号码不存在"),
    NOT_BUY_GIFT_BOX("910", "只能购买高于自己等级的礼包"),
    NOT_DIRECT_USER("910", "未达到标"),

    STOCK_NOT_ENOUGH("920", "已经售罄"),

    OVER_MAX_SHOPPING_ITEMS("1000", "购物车商品太多，请删除后重试"),
    ;



    private String code;
    private String description;

    StatusEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String code() {
        return this.code;
    }

    public String description() {
        return this.description;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static String getDescription(String code) {
        for (StatusEnum en : values()) {
            if (en.getCode() == code) {
                return en.getDescription();
            }
        }
        return null;
    }

}
