package com.ff.kvm.common.third_util;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayOpenPublicTemplateMessageIndustryModifyRequest;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayOpenPublicTemplateMessageIndustryModifyResponse;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradePayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.ff.kvm.common.dv.payment.PaymentResult;
import com.ff.kvm.common.dv.payment.ZFBPayDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.sun.org.apache.regexp.internal.RE;

import java.util.Map;

public class ZFBPayUtil {

    private static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoGqhbFSd2eKhtebYkzgzwRyqsMmkGj6iDDk69HOis3XBuN4U0eVaIso7jQMMejBEYxdbYCjqcX7yqoAHuVwlTg5V6HUufQfZpapn0sMlFNijASwFB29ey5Wy41q8BCyzQ6AvG7S3DPSYjSsuuSzU+uD+xcOy7WEC0+DQQSwBBlVUTMggKATR+zoYbdcJq61e+w58j8uFtCKXCyIs+mMOZHu35RJGx73Xbe396HwzyqQvUbNJKBa+tyxMQ5a0uoahR4u80t5AtsiwBOEpELWNb2Majv9Wy2RTCHj5ZxpxzoTLeiqJ7MiH0+PTSLgUhe5lIX6I+hcIOJG/qMW2ecL71QIDAQAB";//
    private static String APP_ID = "2018121662530984";//应用ID
    private static String mch_id = "2088331282224643";//商户号
    private static String url = "https://openapi.alipay.com/gateway.do";
    private static String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCDXapSuPMjKZYuDdSL/TqUcJYuAV4yVRVrCegttQhA6cHN5cfGm9ZjD2cGEel96WeFJHXuPsXlg2QAoTNc8udCI9DDGyaAFB19sA+T6/WEvefP9RRNHbMjqqT2xpgF3wEtR+98fUuIzVfyWHPLRAAcfJatMVGplb8v68v+pdtcMS9TeeeYfpwqgMo1EmLDtTuMa9mNyKP/DRlU0gcGvYdRXxEijay7lNjhx1pCzyAUnZ2/i3NTHH1/hlFA7vG1c0kV/GTUKPlbMw4b/cgnGkqX51Ah50D1vDN30F2lfCZpISERPRkpi6hWRjD6W2fp2UKx6RdzsFJApEZiJD+GdS+tAgMBAAECggEAUZE2MdkfN0UaOAxkXmvlFMh3p6c3yv7jpqvnG4Jd1piOuv8jNaIL00R8DtvOZF0p5xUFmOInVORpkq6FSwCUMZA1jv/rvvk+eo14jrujnkGgj5K4gU2grtPFTGlxeavHektBW8YcwZvnMNZVR/A4NXfywyVtdGRD60h5SOvJMgt36BrO8fsyM1etabgRaemlg8ax5JxMNzRjrV4yIbPKdzD64C17SmxX8Mpnk0kvbBLqPaDOI8fY+5G+KiMlQa58cDxgpaflFIg/u3wWBgwBOWbiZDvZeNbKKxhLd4ZXDQxfuwd6PJIkVPeu34TXVAt9epT4zokA+EjunTCgP0/boQKBgQDxJ7EniMSaA2/AEUvHVGG5cqgu2wrY+pZkfW5TZbeG41aMIeef/454mwOAz2rTdRcWpeFHhAGwfLSAKZ0h1uz+rGvLz8zM/ZvLBvXvFKaA8X9y5Bbj+bBcNPioRKdxmGmoHZITsc0l5TpfDgwAkRWBeSsc91mMqUfxl8FbDXRfxQKBgQCLc9SiNkB4pYqwvFk+Ul/mF/OBdW8ug0r8EdcLTrMl5jBQ59wcmdq9RortEwivaMKZPXdxID1qWlEREKsEGSiML8U/dt2WWPcEDES3buefULV75mLh9aWK8HcZqhJvtMT0mvhF9OHts9zleWm56rxvXm9bdvYZ3SDpzHK/1Y/myQKBgQCUHUjVe08lOQ8d7IGaf901xZI+lRJoiiBswA1O855xHpg9oWVeA9xBfxP7vWljTvtLUb636gAPochLH0P/UL5m/QeIOq0TCoDxhejvWsOZVrpsmmLkyqrjNzWyQ9iEEJR0SRU7O21A5TWgxEPi5TlMOFuegt1hY2don0iG4BCl8QKBgHGfYQMCHSmwB/7UmApVZ9m8biVvH7XQOAqGJqcJ6qHYmklxgUC0gytxDMT5mzwAqX3ePTGpSSEckazL3euq7AS874Q0P3u+5RUhQeQLsyF1y0k2gB8H3WgFYaVBk1/88/UxR1UIoATjZfp7II/hGMPNOAJYPVD5gVW9xUlUorkxAoGBAJAmpTNyMsuas8vwcGFeb8OBNZ81bUguCuV/XtTN6PmYvr3Os+g0XvXuZ7qUF2qOcxRYCWMys8g7bjuLVbvsEGyi+srAjIrBMi2+3MSyRPKqkHl0NKa9N2dl7UBLTcMoHeSn3KNdCGlx9GOgq5xMscHDA6bf4bcfdWRswRt0ph9H";



    private static String nonce_str;//随机字符串
    private static String body = "康丽美购物";//商品描述
    private static String trade_type = "APP";//交易类型
    private static String charset = "UTF-8";//编码
    private static String REC_PRODUCT_CODE = "REC_PRODUCT_CODE";
    private static String PRODUCT_CODE = "NORMAL_PRODUCT_CODE";
//    private static String notify_url = "http://wqadxmm.xicp.net:37388/payResult/zfb";//通知地址
    private static String notify_url = "http://47.96.148.139/kvm/payResult/zfb";//通知地址

    private static AlipayClient alipayClient ;

    private static synchronized  AlipayClient getAlipayClient(){
        if(alipayClient == null){
            alipayClient = new DefaultAlipayClient(url, APP_ID, APP_PRIVATE_KEY, "json", charset, ALIPAY_PUBLIC_KEY, "RSA2"); //获得初始化的AlipayClient

        }
        return alipayClient;
    }

    public static Response<PaymentResult> createPreOrder(ZFBPayDV zfbPayDV){
        Response<PaymentResult> rep = new Response<>();


//            AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
//            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
//
//            model.setBody(zfbPayDV.getSubject());
//            model.setSubject(zfbPayDV.getSubject());
//            model.setOutTradeNo(zfbPayDV.getOut_trade_no());
//            model.setTimeoutExpress("30m");
//            model.setTotalAmount(zfbPayDV.getTotal_amount());
//            model.setProductCode("QUICK_MSECURITY_PAY");
//            request.setBizModel(model);
//            request.setNotifyUrl(notify_url);
//            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
//            System.out.println(response.getBody());
//            rep.setData(response.getBody());
//            rep.setCode(StatusEnum.SUCCESS.code());
//            rep.setMsg(StatusEnum.SUCCESS.description());

        //实例化客户端
        AlipayClient alipayClient =  getAlipayClient();
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody(zfbPayDV.getSubject());
        model.setSubject(zfbPayDV.getSubject());
        //请保证OutTradeNo值每次保证唯一
        model.setOutTradeNo(zfbPayDV.getOut_trade_no());
        model.setTimeoutExpress("30m");
        model.setTotalAmount(zfbPayDV.getTotal_amount()+"");
        model.setProductCode("QUICK_MSECURITY_PAY");
        request.setBizModel(model);
        request.setNotifyUrl(notify_url);
        try {
            //这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            System.out.println(response.getBody());//就是orderString 可以直接给客户端请求，无需再做处理。
            PaymentResult paymentResult = new PaymentResult();
            paymentResult.setZfb_info(response.getBody());
            rep.setData(paymentResult);
            rep.setCode(StatusEnum.SUCCESS.code());
            rep.setMsg(StatusEnum.SUCCESS.description());
        } catch (AlipayApiException e) {
            e.printStackTrace();
            rep.setCode(StatusEnum.DATA_ERROR.code());
            rep.setMsg(e.getErrMsg());
            return rep;
        }

        return rep;
    }

    public static boolean checkSign(Map<String,String> params){
        try {
            return AlipaySignature.rsaCheckV1(params, ALIPAY_PUBLIC_KEY, charset,"RSA2");
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return false;
        }
    }

}
