package com.ff.kvm.common.dv.wx.spread;

import java.io.Serializable;

public class ForeverQRCodeDV implements Serializable {

    private static final long serialVersionUID = 4125130755720695010L;

    private String action_name = "QR_LIMIT_STR_SCENE";
    private ActionInfoDV action_info;

    public ForeverQRCodeDV(String code) {
        this.action_info = new ActionInfoDV(code);
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public ActionInfoDV getAction_info() {
        return action_info;
    }

    public void setAction_info(ActionInfoDV action_info) {
        this.action_info = action_info;
    }
}
