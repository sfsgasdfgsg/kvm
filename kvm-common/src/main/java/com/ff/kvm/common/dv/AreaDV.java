package com.ff.kvm.common.dv;

import java.io.Serializable;

public class AreaDV implements Serializable {

    private static final long serialVersionUID = -5880066909393269287L;

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
