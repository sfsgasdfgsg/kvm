package com.ff.kvm.common.dv.team;

import java.io.Serializable;

public class TeamPageDV implements Serializable {

    private static final long serialVersionUID = 5217115702721416882L;

    private int userId;
    private int pageNum;
    private int type;//0进货业绩，1礼包业绩/1身份，2礼包，3商品
    private int lv;

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
