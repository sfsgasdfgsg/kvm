package com.ff.kvm.common.util;



import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

public class MyUtil {

    public static boolean isBlank(String str){
        return StrUtil.isBlank(str);
    }

    public static String MD5encode(String password){
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        byte[] md5Bytes = md5.digest(password.getBytes());
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = md5Bytes[i] & 0xFF;
            if (val < 16) {
                hexValue.append("0");
            }

            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    public static Map<String, Object> beanToMap(Object bean){
        return BeanUtil.beanToMap(bean);
    }

    public static <T> T mapToBean(Map<?, ?> map, Class<T> beanClass){
        return BeanUtil.mapToBean(map,beanClass,true);
    }

    public static String formatDateYMDhms(Date date){
        if (date == null) {
            return null;
        }
        return DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
    }
    public static String formatDateYMD000(Date date){
        if (date == null) {
            return null;
        }
        return DateUtil.format(date, "yyyy-MM-dd 00:00:00");
    }

    public static String formatDateYMD(Date date) {
        if (date == null) {
            return null;
        }
        return DateUtil.format(date, "yyyy-MM-dd");
    }

    public static String formatDateYMD2(Date date){
        if (date == null) {
            return null;
        }
        return DateUtil.format(date, "yyyyMMdd");
    }

    public static Date parseDate(String str){
        if (str == null) {
            return null;
        }
        return DateUtil.parse(str);
    }

    public static Double float8(Double number){
        if (number == null) {
            return Double.valueOf(0.0D);
        }
        DecimalFormat df = new DecimalFormat("#.########");
        return Double.valueOf(df.format(number));
    }

    public static String float8Str(Double number) {
        if (number == null) {
            return "0";
        }
        DecimalFormat df = new DecimalFormat("#.########");
        return df.format(number);
    }

    public static Double float2(Double number){
        if (number == null) {
            return Double.valueOf(0.0D);
        }
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(number));
    }

    public static String toJSON(Object obj){
        if (obj == null) {
            return null;
        }
        return JSONObject.toJSONString(obj);
    }

    public static <T> T jsonToBean(String json, Class<T> t) {

        return JSON.parseObject(json,t);
    }

    public static String toJSONNull(Object obj){
        if (obj == null) {
            return null;
        }
        return JSONObject.toJSONString(obj, new SerializerFeature[] { SerializerFeature.WriteMapNullValue });
    }

    public static Date getDate(Date start, int day){
        Calendar date1 = Calendar.getInstance();
        date1.setTime(start);
        date1.add(5, day);
        return date1.getTime();
    }

    public static Date getDateM(Date start, int min) {
        Calendar date1 = Calendar.getInstance();
        date1.setTime(start);
        date1.add(12, min);
        return date1.getTime();
    }

    public static Date getDateH(Date start, int hour) {
        Calendar date1 = Calendar.getInstance();
        date1.setTime(start);
        date1.add(10, hour);
        return date1.getTime();
    }

    public static String rd4(){
        int rd = RandomUtil.randomInt(9999);
        DecimalFormat df = new DecimalFormat("0000");
        return formatDateYMD2(new Date()) + df.format(rd);
    }

    public static String formatStr(CharSequence template, Object[] params){
        return StrUtil.format(template, params);
    }

    public static boolean containDate(Date start, Date end, Date nowDate){
        Calendar date1 = Calendar.getInstance();
        date1.setTime(start);
        Calendar date2 = Calendar.getInstance();
        date2.setTime(end);
        Calendar date3 = Calendar.getInstance();
        date3.setTime(nowDate);

        return (date1.before(date3)) && (date3.before(date2));
    }

    // start 在end 前
    public static boolean before(Date start, Date end){
        Calendar date1 = Calendar.getInstance();
        date1.setTime(start);
        Calendar date2 = Calendar.getInstance();
        date2.setTime(end);
        return date1.before(date2);
    }



    public static String HMACSHA256(String data, String key){
        StringBuilder sb = new StringBuilder();
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] array = sha256_HMAC.doFinal(data.getBytes("UTF-8"));

            for (byte item : array) {
                sb.append(Integer.toHexString((item & 0xFF) | 0x100).substring(1, 3));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return sb.toString().toUpperCase();
    }

    private static String byteArrayToHexString(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int n = 0; b != null && n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1)
                hs.append('0');
            hs.append(stmp);
        }
        return hs.toString().toLowerCase();
    }

    public static String getTokenId(){
        return UuidUtil.uuid();
    }

    public static boolean isEmpty(Collection<?> collection){


        return  CollectionUtil.isEmpty(collection);
    }

    public static String getJSONValue(String key, String json) {
        JSONObject obj = (JSONObject) JSONObject.parse(json);
        return obj.getString(key);
    }

    //ascii 码排序
    public static List<String>  sortASCII(){
        List<String> list = new ArrayList<>();
        list.add("appid");
        list.add("partnerid");
        list.add("prepayid");
        list.add("package");
        list.add("noncestr");
        list.add("timestamp");
        Collections.sort(list);
        for (String str:list){
            System.out.println(str);
        }
        return list;
    }

    public static long betweenDay(Date start,Date end){

        String dateStr1 = formatDateYMD000(start);
        Date date1 = DateUtil.parse(dateStr1);

        String dateStr2 = formatDateYMD000(end);
        Date date2 = DateUtil.parse(dateStr2);

        return DateUtil.between(date1, date2, DateUnit.DAY);
    }

    public static void main(String[] args){

        betweenDay(parseDate("2018-12-26 22:33:23"),new Date());
    }
}
