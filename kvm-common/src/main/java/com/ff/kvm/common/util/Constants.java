package com.ff.kvm.common.util;

public class Constants {

    public static String USER = "user";
    public static String KAPTCHA = "kaptcha";
    public static String TOKENID = "token";
    public static String SHOPPINTG_CART = "shopping_cart";
    public static String GIFT_BOX_LV = "gift_box_lv_";//礼包等级key:名称-折扣
    public static String INCOME_SWITCH = "income_switch";//返利开关:0关闭，1开启
    public static String WX_PUBLIC_ACCESS_TOKEN = "wx_public_access_token";//微信公众号access_token-----time
    public static String WX_PUBLIC_ACCESS_TOKEN2 = "wx_public_access_token2";//微信公众号access_token-----time
    public static String WX_OPEN_REFRESH_TOKEN = "wx_open_refresh_token";//微信开放平台refresh_token
    public static String ROLL_LIST = "roll_list";//轮播图缓存


    //系统参数
    public static String PAGE_SIZE = "page_size";//分页大小
    public static String USER_FLOOR = "user_floor";//最高楼层
    public static String POLL_CODE = "poll_code";//默认推广码
    public static String GIFT_BOX_BONUS_LV = "gift_box_bonus_lv";//礼包分红最小奖励身份
    public static String GIFT_BOX_BONUS_CF = "gift_box_bonus_cf";//礼包分红：直推人数-特定星级-伞下人数-比例
    public static String PRODUCT_BONUS_LV = "product_bonus_lv";//重消团队奖最小奖励身份
    public static String PRODUCT_BONUS_CF = "product_bonus_cf";//重消团队奖分红:直推人数-伞下人数-比例
    public static String SMS_CF = "sms_cf";//短信配置：开关-模版-次数-默认验证码
    public static String USER_GIFT = "user_gift";//奖品领取设置：人数-商品id-数量-到期时间
    public static String PRODUCT_INCOME_BONUS = "product_income_bonus";//普通商品直推（原价的3.5）-抢购商品直推（支付价的10%）-普通商品团队计算比例（原价的3.5）
    public static String MAX_SHOPPING_ITEMS = "max_shopping_items";//购物车最多商品种数
    public static String TEAM_LAST_GIFT_BOX_SETTLE_TIME = "team_last_gift_box_settle_time";//上一次礼包分红时间
    public static String TEAM_GIFT_BOX_SETTLE_DAY = "team_gift_box_settle_day";//团队礼包结算间隔天数
    public static String TEAM_LAST_PRODUCT_SETTLE_TIME = "team_last_product_settle_time";//上一次商品分红时间
    public static String TEAM_PRODUCT_SETTLE_DAY = "team_product_settle_day";//团队商品结算间隔天数
    public static String CASH_FEE_POINT = "cash_fee_point";//提现手续费
    public static String GIFT_BOX_INCOME = "gift_box_income";//提现手续费


}
