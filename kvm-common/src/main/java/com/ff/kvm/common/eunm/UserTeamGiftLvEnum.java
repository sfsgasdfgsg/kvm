package com.ff.kvm.common.eunm;

public enum UserTeamGiftLvEnum {

    STAR0(0, "无"),
    STAR1(1, "初级合伙人"),
    STAR2(2, "中级合伙人"),
    STAR3(3, "高级合伙人"),
    STAR4(4, "资深合伙人"),
    STAR5(5, "功勋合伙人"),
    STAR6(6, "执行委员"),
    STAR7(7, "决策委员"),


    ;

    private int type;
    private String name;

    UserTeamGiftLvEnum(int type, String name){
        this.type = type;
        this.name = name;
    }
    public int type() {
        return this.type;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getName(int type) {
        for (UserTeamGiftLvEnum en : values()) {
            if (en.getType() == type) {
                return en.getName();
            }
        }
        return null;
    }
}
