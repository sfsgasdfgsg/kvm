package com.ff.kvm.common.dv.shopping;

import java.io.Serializable;
import java.util.List;

public class CheckOrderDV implements Serializable {

    private static final long serialVersionUID = -9103444945093770415L;

    private int userId;
    private List<ItemsDV> items;
    private int addressId;
    private int expressType;
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getExpressType() {
        return expressType;
    }

    public void setExpressType(int expressType) {
        this.expressType = expressType;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<ItemsDV> getItems() {
        return items;
    }

    public void setItems(List<ItemsDV> items) {
        this.items = items;
    }
}
