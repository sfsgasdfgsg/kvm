package com.ff.kvm.common.third_util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.vo.Response;

public class AliyunSmsUtil {
    public static final String product = "Dysmsapi";
    public static final String domain = "dysmsapi.aliyuncs.com";
    public static final String accessKeyId = "LTAIyYqeWozkp9Jv";
    public static final String accessKeySecret = "StHw9YQYJ6BZbiS7AJRobrcyWXPHYI";
    public static final String connectTimeOut = "sun.net.client.defaultConnectTimeout";
    public static final String connectTime = "10000";
    public static final String readTimeOut = "sun.net.client.defaultReadTimeout";
    public static final String readTime = "10000";


    public static IAcsClient getIAcsClient() throws ClientException {
        System.setProperty(connectTimeOut, "10000");
        System.setProperty(readTimeOut, "10000");

        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        return new DefaultAcsClient(profile);
    }
    public static Response<String> sendSms(String uMoblie, String smsType)
            throws ClientException {
//        System.setProperty(connectTimeOut, "10000");
//        System.setProperty(readTimeOut, "10000");
//
//        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
//        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
//        IAcsClient acsClient = new DefaultAcsClient(profile);
        IAcsClient acsClient = getIAcsClient();
        SendSmsRequest request = new SendSmsRequest();
        request.setPhoneNumbers(uMoblie);
        request.setSignName("德诚佳际商贸");
        request.setTemplateCode(smsType);

        String radomStr = String.valueOf(Math.random());
        String code = radomStr.substring(2, 6);
        String jsonStr = "{\"code\":\"" + code + "\"}";

        request.setTemplateParam(jsonStr);
        Response<String> response = new Response<>();
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
        if ("OK".equals(sendSmsResponse.getCode())){
            response.setCode(StatusEnum.SUCCESS.getCode());
            response.setData(code);
            return response;
        }

        if ("isv.OUT_OF_SERVICE".equals(sendSmsResponse.getCode())) {
            response.setCode(StatusEnum.ACCOUNT_NOT_ENOUGH.code());
            response.setMsg(StatusEnum.ACCOUNT_NOT_ENOUGH.description());
            return response;
        }
        response.setCode(StatusEnum.FAIL.code());
        response.setMsg(sendSmsResponse.getCode());
        return response;
    }

    public static void main(String[] args)
    {
        try
        {
            sendSms("18782104331", "SMS_154390103");
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
