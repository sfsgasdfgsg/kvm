package com.ff.kvm.common.dv.product;

import java.io.Serializable;

public class ProductSpecialDV implements Serializable {
    private static final long serialVersionUID = -3309593053843955538L;

    private String name;
    private String icon;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
