package com.ff.kvm.common.third_util;

import com.ff.kvm.common.dv.wx.spread.ForeverQRCodeDV;
import com.ff.kvm.common.dv.wx.spread.TempQRCodeDV;
import com.ff.kvm.common.dv.wx.WXUserInfoDV;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.HttpUtil;
import com.ff.kvm.common.util.MyUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class WXPublicUtil {

    private static String appid = "wxcf5d23e321d5383f";
    private static String appsecret = "701a8e3db607697c9bf56983f83a1a86";
    private static String token = "6807263487738247087";

    private static String appid2 = "wxd6d4812cca746bdb";
    private static String appsecret2 = "b5504ac052b89750fa2ab95682373967";
    private static String token2 = "wexin";
    private static String public_access_token_url2 = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid2+"&secret="+appsecret2;

    private static String spread_url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=";
    private static String userinfo_url = "https://api.weixin.qq.com/cgi-bin/user/info?lang=zh_CN";
    private static String public_access_token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+appsecret;
    private static String public_union_id_url = "https://api.weixin.qq.com/cgi-bin/user/info?lang=zh_CN&access_token=";


    //type:0临时二维码，1永久二维码
    public static String getSpreadUrl(int type,String pollCode,String accessToken) {

        String params;
        if(type == 0){
            params = MyUtil.toJSON(new TempQRCodeDV(pollCode));
        }else{
            params = MyUtil.toJSON(new ForeverQRCodeDV(pollCode));
        }
        return MyUtil.getJSONValue("url",HttpUtil.sendPost(spread_url + accessToken,params));
    }

    public static WXUserInfoDV getUserInfo(String accessToken, String openId) {
        StringBuffer url = new StringBuffer(userinfo_url);
        url.append("&access_token=").append(accessToken);
        url.append("&openid=").append(openId);
        return ConvertUtil.jsonToBean(HttpUtil.sendGet(url.toString()),WXUserInfoDV.class);
    }

    public static boolean checkSignature(String signature, String timestamp, String nonce) {

        System.out.println("signature:" + signature + "timestamp:" + timestamp + "nonc:" + nonce);
        String[] arr = new String[]{token, timestamp, nonce};
        // 将token、timestamp、nonce三个参数进行字典序排序
        Arrays.sort(arr);
        StringBuilder content = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            content.append(arr[i]);
        }
        MessageDigest md = null;
        String tmpStr = null;

        try {
            md = MessageDigest.getInstance("SHA-1");
            // 将三个参数字符串拼接成一个字符串进行sha1加密
            byte[] digest = md.digest(content.toString().getBytes());
            tmpStr = byteToStr(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        content = null;
        // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信
        System.out.println(tmpStr.equals(signature.toUpperCase()));
        return tmpStr != null ? tmpStr.equals(signature.toUpperCase()) : false;

    }


    private static String byteToStr(byte[] byteArray) {
        String strDigest = "";
        for (int i = 0; i < byteArray.length; i++) {
            strDigest += byteToHexStr(byteArray[i]);
        }
        return strDigest;
    }

    private static String byteToHexStr(byte mByte) {
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char[] tempArr = new char[2];
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
        tempArr[1] = Digit[mByte & 0X0F];

        String s = new String(tempArr);
        return s;
    }

    public static String getPublicAccessToken(){

        return MyUtil.getJSONValue("access_token",HttpUtil.sendGet(public_access_token_url));
    }

    public static String getUnionId(String accessToken,String openId){

        return MyUtil.getJSONValue("unionid",HttpUtil.sendGet(public_union_id_url+accessToken+"&openid="+openId));
    }

}
