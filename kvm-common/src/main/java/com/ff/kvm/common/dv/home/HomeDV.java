package com.ff.kvm.common.dv.home;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import java.io.Serializable;
import java.util.List;

public class HomeDV implements Serializable {

    private static final long serialVersionUID = 5820354696265367493L;

    private String propVideo;//视频

    private List<RollListDV> rollList;//轮播商品

    private List<NewsDV> newsList;//资讯

    private List<HomeSpecialProductDV> specialList;//特色商品

    private HomeRobProductDV robProduct;


    public String getPropVideo() {
        return propVideo;
    }

    public void setPropVideo(String propVideo) {
        this.propVideo = propVideo;
    }

    public List<HomeSpecialProductDV> getSpecialList() {
        return specialList;
    }

    public void setSpecialList(List<HomeSpecialProductDV> specialList) {
        this.specialList = specialList;
    }

    public HomeRobProductDV getRobProduct() {
        return robProduct;
    }

    public void setRobProduct(HomeRobProductDV robProduct) {
        this.robProduct = robProduct;
    }

    public List<RollListDV> getRollList() {
        return rollList;
    }

    public void setRollList(List<RollListDV> rollList) {
        this.rollList = rollList;
    }

    public List<NewsDV> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<NewsDV> newsList) {
        this.newsList = newsList;
    }
}
