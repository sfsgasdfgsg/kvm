package com.ff.kvm.common.dv.user;

import java.io.Serializable;

public class UserInfoDV implements Serializable {

    private static final long serialVersionUID = 6681732512287531053L;

    private String name;
    private String realName;
    private String mobile;
    private int lv;
    private String lvName;
    private String headUrl;
    private int dealPwd;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public int getDealPwd() {
        return dealPwd;
    }

    public void setDealPwd(int dealPwd) {
        this.dealPwd = dealPwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public String getLvName() {
        return lvName;
    }

    public void setLvName(String lvName) {
        this.lvName = lvName;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }
}
