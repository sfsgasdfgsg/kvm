package com.ff.kvm.common.dv.es;

import java.io.Serializable;

public class UserESLVDV implements Serializable {

    private static final long serialVersionUID = 1517482415434326703L;

    private int userId;
    private int upUserId;
    private int lv;
    private int floor;
    private int teamGiftLV;
    private int teamProductLV;

    private int dlv0;
    private int dlv1;
    private int dlv2;
    private int dlv3;

    private int tlv0;
    private int tlv1;
    private int tlv2;
    private int tlv3;



    private int dglv1;
    private int dglv2;
    private int dglv3;
    private int dglv4;
    private int dglv5;
    private int dglv6;
    private int dglv7;

    private int tglv1;
    private int tglv2;
    private int tglv3;
    private int tglv4;
    private int tglv5;
    private int tglv6;
    private int tglv7;

    private int dplv1;
    private int dplv2;
    private int dplv3;
    private int dplv4;

    private int tplv1;
    private int tplv2;
    private int tplv3;
    private int tplv4;

    public int getDirect(){
        return dlv0 + dlv1 + dlv2 + dlv3;
    }
    public int getDirect2(){
        return dlv2 + dlv3;//铂金以上的直推
    }

    public int getTree(){
        return tlv0 + tlv1 + tlv2 + tlv3;
    }


    public int getDlv0() {
        return dlv0;
    }

    public void setDlv0(int dlv0) {
        this.dlv0 = dlv0;
    }

    public int getDlv1() {
        return dlv1;
    }

    public void setDlv1(int dlv1) {
        this.dlv1 = dlv1;
    }

    public int getDlv2() {
        return dlv2;
    }

    public void setDlv2(int dlv2) {
        this.dlv2 = dlv2;
    }

    public int getDlv3() {
        return dlv3;
    }

    public void setDlv3(int dlv3) {
        this.dlv3 = dlv3;
    }

    public int getTlv0() {
        return tlv0;
    }

    public void setTlv0(int tlv0) {
        this.tlv0 = tlv0;
    }

    public int getTlv1() {
        return tlv1;
    }

    public void setTlv1(int tlv1) {
        this.tlv1 = tlv1;
    }

    public int getTlv2() {
        return tlv2;
    }

    public void setTlv2(int tlv2) {
        this.tlv2 = tlv2;
    }

    public int getTlv3() {
        return tlv3;
    }

    public void setTlv3(int tlv3) {
        this.tlv3 = tlv3;
    }

    public int getProductDirect(){
        return dplv1 + dplv2 + dplv3 + dplv4;
    }
    public int getProductTree(){
        return tplv1 + tplv2 + tplv3 + tplv4;
    }

    public int getDplv1() {
        return dplv1;
    }

    public void setDplv1(int dplv1) {
        this.dplv1 = dplv1;
    }

    public int getDplv2() {
        return dplv2;
    }

    public void setDplv2(int dplv2) {
        this.dplv2 = dplv2;
    }

    public int getDplv3() {
        return dplv3;
    }

    public void setDplv3(int dplv3) {
        this.dplv3 = dplv3;
    }

    public int getDplv4() {
        return dplv4;
    }

    public void setDplv4(int dplv4) {
        this.dplv4 = dplv4;
    }

    public int getTplv1() {
        return tplv1;
    }

    public void setTplv1(int tplv1) {
        this.tplv1 = tplv1;
    }

    public int getTplv2() {
        return tplv2;
    }

    public void setTplv2(int tplv2) {
        this.tplv2 = tplv2;
    }

    public int getTplv3() {
        return tplv3;
    }

    public void setTplv3(int tplv3) {
        this.tplv3 = tplv3;
    }

    public int getTplv4() {
        return tplv4;
    }

    public void setTplv4(int tplv4) {
        this.tplv4 = tplv4;
    }

    public int getGift1Tree(){
        return tglv1+tglv2+tglv3+tglv4+tglv5+tglv6+tglv7;
    }
    public int getGift2Tree(){
        return tglv2+tglv3+tglv4+tglv5+tglv6+tglv7;
    }
    public int getGift3Tree(){
        return tglv3+tglv4+tglv5+tglv6+tglv7;
    }
    public int getGift4Tree(){
        return tglv4+tglv5+tglv6+tglv7;
    }
    public int getGift5Tree(){
        return tglv5+tglv6+tglv7;
    }
    public int getGift6Tree(){
        return tglv6+tglv7;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUpUserId() {
        return upUserId;
    }

    public void setUpUserId(int upUserId) {
        this.upUserId = upUserId;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getTeamGiftLV() {
        return teamGiftLV;
    }

    public void setTeamGiftLV(int teamGiftLV) {
        this.teamGiftLV = teamGiftLV;
    }

    public int getTeamProductLV() {
        return teamProductLV;
    }

    public void setTeamProductLV(int teamProductLV) {
        this.teamProductLV = teamProductLV;
    }

    public int getDglv1() {
        return dglv1;
    }

    public void setDglv1(int dglv1) {
        this.dglv1 = dglv1;
    }

    public int getDglv2() {
        return dglv2;
    }

    public void setDglv2(int dglv2) {
        this.dglv2 = dglv2;
    }

    public int getDglv3() {
        return dglv3;
    }

    public void setDglv3(int dglv3) {
        this.dglv3 = dglv3;
    }

    public int getDglv4() {
        return dglv4;
    }

    public void setDglv4(int dglv4) {
        this.dglv4 = dglv4;
    }

    public int getDglv5() {
        return dglv5;
    }

    public void setDglv5(int dglv5) {
        this.dglv5 = dglv5;
    }

    public int getDglv6() {
        return dglv6;
    }

    public void setDglv6(int dglv6) {
        this.dglv6 = dglv6;
    }

    public int getDglv7() {
        return dglv7;
    }

    public void setDglv7(int dglv7) {
        this.dglv7 = dglv7;
    }

    public int getTglv1() {
        return tglv1;
    }

    public void setTglv1(int tglv1) {
        this.tglv1 = tglv1;
    }

    public int getTglv2() {
        return tglv2;
    }

    public void setTglv2(int tglv2) {
        this.tglv2 = tglv2;
    }

    public int getTglv3() {
        return tglv3;
    }

    public void setTglv3(int tglv3) {
        this.tglv3 = tglv3;
    }

    public int getTglv4() {
        return tglv4;
    }

    public void setTglv4(int tglv4) {
        this.tglv4 = tglv4;
    }

    public int getTglv5() {
        return tglv5;
    }

    public void setTglv5(int tglv5) {
        this.tglv5 = tglv5;
    }

    public int getTglv6() {
        return tglv6;
    }

    public void setTglv6(int tglv6) {
        this.tglv6 = tglv6;
    }

    public int getTglv7() {
        return tglv7;
    }

    public void setTglv7(int tglv7) {
        this.tglv7 = tglv7;
    }
}
