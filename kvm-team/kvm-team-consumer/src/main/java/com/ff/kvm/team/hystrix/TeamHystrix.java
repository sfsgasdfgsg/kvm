package com.ff.kvm.team.hystrix;

import com.ff.kvm.common.dv.es.GiftBoxESDV;
import com.ff.kvm.common.dv.es.ProductESDV;
import com.ff.kvm.common.dv.es.UserESDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.team.consumer.TeamConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TeamHystrix implements TeamConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<Boolean> addUser(UserESDV userESDV) {
        log.error("/kvm-team-provider/addUser  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> updateUserLv(UserESDV userESDV) {
        log.error("/kvm-team-provider/updateUserLv  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> addProduct(ProductESDV productESDV) {
        log.error("/kvm-team-provider/addProduct  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> addGiftBox(GiftBoxESDV giftBoxESDV) {
        log.error("/kvm-team-provider/addGiftBox  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> updateUser(int userId) {
        log.error("/kvm-team-provider/userId  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
