package com.ff.kvm.team.consumer;

import com.ff.kvm.common.dv.es.GiftBoxESDV;
import com.ff.kvm.common.dv.es.ProductESDV;
import com.ff.kvm.common.dv.es.UserESDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.team.hystrix.TeamHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="kvm-team-provider", fallback= TeamHystrix.class)
public interface TeamConsumer {

    @RequestMapping("/addUser")
    Response<Boolean> addUser(@RequestBody UserESDV userESDV);

    @RequestMapping("/updateUserLv")
    Response<Boolean> updateUserLv(@RequestBody UserESDV userESDV);

    @RequestMapping("/addProduct")
    Response<Boolean> addProduct(@RequestBody ProductESDV productESDV);

    @RequestMapping("/addGiftBox")
    Response<Boolean> addGiftBox(@RequestBody GiftBoxESDV giftBoxESDV);

    @RequestMapping("/updateUser")
    Response<Boolean> updateUser(@RequestParam("userId") int userId);

}
