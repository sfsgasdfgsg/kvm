package com.ff.kvm.team.exception;

import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.vo.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public Response defaultErrorHandler(Exception e) throws Exception { this.logger.error("异常拦截", e);
        Response response = new Response();

        if ((e instanceof NoHandlerFoundException)) {
            response.setCode(StatusEnum.RULE_ERROR.code());
            response.setMsg(StatusEnum.RULE_ERROR.description());
        } else {
            response.setCode(StatusEnum.DATA_ERROR.code());
            response.setMsg(StatusEnum.DATA_ERROR.description());
        }

        return response;
    }
}
