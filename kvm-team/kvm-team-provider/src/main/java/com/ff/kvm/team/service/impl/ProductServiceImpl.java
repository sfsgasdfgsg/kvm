package com.ff.kvm.team.service.impl;

import com.ff.kvm.common.dv.es.ProductESDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.team.common.TeamProductDV;
import com.ff.kvm.team.es.entity.ProductES;
import com.ff.kvm.team.es.entity.UserES;
import com.ff.kvm.team.es.repository.ESProductRepository;
import com.ff.kvm.team.es.util.ESUtil;
import com.ff.kvm.team.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ESProductRepository productRepository;

    @Autowired
    private RedisService redisService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserService userService;

    @Autowired
    private SysCodeService sysCodeService;


    @Override
    public Response<Boolean> addProduct(ProductESDV productESDV) {
        productRepository.save(ConvertUtil.convert(productESDV, ProductES.class));
        return ResponseUtil.success();
    }

    @Override
    public void exeProduct() {
        Date start = MyUtil.parseDate(redisService.getCache(Constants.TEAM_LAST_PRODUCT_SETTLE_TIME));
        Date end = new Date();
//        long day = MyUtil.betweenDay(start,end);
//        log.info("距离上一次商品团队分红天数...",day);
//        if(day < Integer.valueOf(redisService.getCache(Constants.TEAM_PRODUCT_SETTLE_DAY))){
//            return;
//        }
        log.info("执行商品团队分红...start");


        int maxFloor = userService.getMaxFloor();
        String[] cf = userService.getProdctTeamCF();
        //存放绩效
        Map<Integer, Double> totalMap = new HashMap<>();
        for(int i = maxFloor;i>0;i--){
            List<UserES> userList = userService.findUserESByFloor(i);
            clbBonus(totalMap,userList,start,end,cf);
        }

        //记录上这次分红时间，为下次开始时间
        redisService.putCache(Constants.TEAM_LAST_PRODUCT_SETTLE_TIME,MyUtil.formatDateYMDhms(end));
        sysCodeService.updateSysCode(Constants.TEAM_LAST_PRODUCT_SETTLE_TIME,MyUtil.formatDateYMDhms(end));
        log.info("执行商品团队分红...end");
    }

    @Transactional
    public void clbBonus(Map<Integer, Double> totalMap, List<UserES> userList, Date start, Date end,String[] cf) {
        for(UserES userES : userList){
            if(userES.getUpUserId() == 0 ){
                continue;
            }
            UserES upUser = userService.findUserESById(userES.getUpUserId());
            List<ProductES> productList = findOrderESByUserId(userES.getUserId(),start,end);
            double total = sumTotal(productList);
            if(totalMap.containsKey(userES.getUserId())){
                total = total + totalMap.get(userES.getUserId());
            }
            if(total == 0){
                continue;
            }

            //上级<=下级不返利
            if(upUser.getTeamProductLv() <= userES.getTeamProductLv()){
                continue;
            }
            System.out.println("userId：" +userES.getUserId()+",total:" + total);
            if(totalMap.containsKey(userES.getUpUserId())){
                System.out.println("userupId：" +userES.getUpUserId()+",total:" + (totalMap.get(userES.getUpUserId()) + total));
                totalMap.put(userES.getUpUserId(),totalMap.get(userES.getUpUserId()) + total);
            }else{
                System.out.println("userupId：" +userES.getUpUserId()+",total:" + total);
                totalMap.put(userES.getUpUserId(), total);
            }


            double point = 0;
            if(userES.getTeamProductLv() != 0){
                point = Double.valueOf(cf[userES.getTeamProductLv()-1].split("-")[2]);
            }
            total = MyUtil.float2(total);
            double upPoint = Double.valueOf(cf[upUser.getTeamProductLv()-1].split("-")[2]);
            double md = MyUtil.float2(total*(upPoint - point));
            accountService.productBonus(upUser.getUserId(),upUser.getTeamProductLv(),upPoint,userES.getUserId(),userES.getTeamProductLv(),point,md,total);

            if(totalMap.containsKey(userES.getUserId())){
                totalMap.remove(userES.getUserId());
            }

        }

    }

    private double sumTotal(List<ProductES> productList) {
        double total = 0;
        for (ProductES productES : productList){
            total += productES.getTotal();
        }
        return total;
    }


    private List<ProductES> findOrderESByUserId(int userId,Date start, Date end) {

        return productRepository.search(ESUtil.getSearchQuery(userId,start,end)).getContent();
    }


    @Override
    public Response<Boolean> executeProduct() {
        new Runnable() {
            @Override
            public void run() {
                exeProduct();
            }
        };
        return ResponseUtil.success();
    }

}
