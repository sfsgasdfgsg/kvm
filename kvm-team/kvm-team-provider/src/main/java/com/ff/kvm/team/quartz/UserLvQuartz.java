package com.ff.kvm.team.quartz;

import com.ff.kvm.team.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UserLvQuartz {

    @Autowired
    private UserService userService;

    @Scheduled(cron="0 0 1 * * ?")//每天凌晨1点执行
    public void execute(){
        userService.exeUserLV();
    }
}
