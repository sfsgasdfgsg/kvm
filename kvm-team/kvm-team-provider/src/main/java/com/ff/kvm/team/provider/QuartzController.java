package com.ff.kvm.team.provider;

import com.ff.kvm.common.vo.Response;
import com.ff.kvm.team.service.GiftBoxService;
import com.ff.kvm.team.service.ProductService;
import com.ff.kvm.team.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nobody")
public class QuartzController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private GiftBoxService giftBoxService;

    @RequestMapping("/updateUser")
    public Response<Boolean> updateUser(@RequestParam int userId){
        return userService.updateUser(userId);
    }

    @RequestMapping("/executeLV")
    public Response<Boolean> executeLV(){

        return userService.executeLV();
    }

    @RequestMapping("/executeProduct")
    public Response<Boolean> executeProduct(){

        return productService.executeProduct();
    }

    @RequestMapping("/executeGiftBox")
    public Response<Boolean> executeGiftBox(){

        return giftBoxService.executeGiftBox();
    }
}
