package com.ff.kvm.team.service.impl;

import com.ff.kvm.dao.mapper.SysCodeMapper;
import com.ff.kvm.team.service.SysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysCodeServiceImpl implements SysCodeService {

    @Autowired
    private SysCodeMapper sysCodeMapper;


    @Override
    public void updateSysCode(String name, String code) {
        sysCodeMapper.updateValue(name,code);
    }
}
