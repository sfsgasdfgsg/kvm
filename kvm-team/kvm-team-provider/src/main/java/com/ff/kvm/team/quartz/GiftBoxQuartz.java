package com.ff.kvm.team.quartz;

import com.ff.kvm.team.service.GiftBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class GiftBoxQuartz {

    @Autowired
    private GiftBoxService giftBoxService;

    @Scheduled(cron="0 0 2 ? * 2")//每周一凌晨2点执行
    public void execute(){
        giftBoxService.exeGiftBox();
    }
}
