//package com.ff.kvm.team.config.amqp;
//
//import com.ff.kvm.team.service.impl.MsgDealServiceImpl;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
//import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sound.midi.Receiver;
//
//@Configuration
//public class RabbitMQConfig {
//    public static final String QUEUE_NAME = "kvm-team";
//
//    //消息接收配置
//    @Bean
//    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
//                                             MessageListenerAdapter listenerAdapter) {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory);
//        container.setQueueNames(QUEUE_NAME);
//        container.setMessageListener(listenerAdapter);
//        return container;
//    }
//    @Bean
//    MessageListenerAdapter listenerAdapter(MsgDealServiceImpl receiver) {
//        return new MessageListenerAdapter(receiver, "receiveMessage");
//    }
//
//
//}
