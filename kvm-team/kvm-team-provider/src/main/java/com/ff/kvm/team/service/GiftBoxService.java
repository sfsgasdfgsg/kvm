package com.ff.kvm.team.service;

import com.ff.kvm.common.dv.es.GiftBoxESDV;
import com.ff.kvm.common.vo.Response;

public interface GiftBoxService {

    Response<Boolean> addGiftBox(GiftBoxESDV giftBoxESDV);

    void exeGiftBox();

    Response<Boolean> executeGiftBox();
}
