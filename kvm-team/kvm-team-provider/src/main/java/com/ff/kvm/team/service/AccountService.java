package com.ff.kvm.team.service;


public interface AccountService {

    void giftBonus(int userId,int userLv,double point,int downUserId,int downUserLv,double downPoint,double md, double total);

    void productBonus(int userId,int userLv,double point,int downUserId,int downUserLv,double downPoint,double md, double total);

}
