//package com.ff.kvm.team.service.impl;
//
//import com.ff.kvm.common.dv.es.UserESDV;
//import com.ff.kvm.common.dv.msg.Message;
//import com.ff.kvm.common.eunm.MessageTypeEnum;
//import com.ff.kvm.common.util.ConvertUtil;
//import com.ff.kvm.team.config.amqp.RabbitMQConfig;
//import com.ff.kvm.team.service.UserService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class MsgDealServiceImpl{
//
//    private final Logger log = LoggerFactory.getLogger(getClass());
//
//    @Autowired
//    private UserService userService;
//
//    @RabbitListener(queues = RabbitMQConfig.QUEUE_NAME)
//    public void receiveMessage(String msg) {
//        log.info("Received msg :{}",msg);
//        Message message = ConvertUtil.jsonToBean(msg,Message.class);
//        UserESDV userESDV = ConvertUtil.jsonToBean(message.getBody(),UserESDV.class);
//        if(message.getType() == MessageTypeEnum.REGISTER.type()){
//
//        }else if(message.getType() == MessageTypeEnum.GIFTBOX.type()){
//
//        }else{
//            log.info("UNKNOWN MESSAGE TYPE:{}",message.getType());
//        }
//
//    }
//
//}
