package com.ff.kvm.team.es.repository;

import com.ff.kvm.team.es.entity.GiftBoxES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ESGiftBoxRepository extends ElasticsearchRepository<GiftBoxES, Integer> {
}
