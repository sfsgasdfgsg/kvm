package com.ff.kvm.team.provider;

import com.ff.kvm.common.dv.es.GiftBoxESDV;
import com.ff.kvm.common.dv.es.ProductESDV;
import com.ff.kvm.common.dv.es.UserESDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.team.service.GiftBoxService;
import com.ff.kvm.team.service.ProductService;
import com.ff.kvm.team.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamProvider {


    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private GiftBoxService giftBoxService;

    @RequestMapping("/addUser")
    public Response<Boolean> addUser(@RequestBody UserESDV userESDV){
        return userService.addUser(userESDV);
    }

    @RequestMapping("/updateUserLv")
    public Response<Boolean> updateUserLv(@RequestBody UserESDV userESDV){
        return userService.updateUserLv(userESDV);
    }

    @RequestMapping("/addProduct")
    public Response<Boolean> addProduct(@RequestBody ProductESDV productESDV){
        return productService.addProduct(productESDV);
    }

    @RequestMapping("/addGiftBox")
    public Response<Boolean> addGiftBox(@RequestBody GiftBoxESDV giftBoxESDV){
        return giftBoxService.addGiftBox(giftBoxESDV);
    }


    @RequestMapping("/updateUser")
    public Response<Boolean> updateUser(@RequestParam int userId){
        return userService.updateUser(userId);
    }


    @RequestMapping("/updateAllUser")
    public Response<Boolean> updateAllUser(){
        return userService.updateAllUser();
    }
}
