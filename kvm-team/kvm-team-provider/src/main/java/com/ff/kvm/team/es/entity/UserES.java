package com.ff.kvm.team.es.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

@Document(indexName="kvm_index", type="kvm_user")
public class UserES implements Serializable {

    private static final long serialVersionUID = 5630923998531782165L;

    @Id
    private int userId;
    private int upUserId;
    private int status;
    private int lv;
    private int teamGiftLv;
    private int teamProductLv;
    private int floor;
    private Date createTime;


    public int getTeamGiftLv() {
        return teamGiftLv;
    }

    public void setTeamGiftLv(int teamGiftLv) {
        this.teamGiftLv = teamGiftLv;
    }

    public int getTeamProductLv() {
        return teamProductLv;
    }

    public void setTeamProductLv(int teamProductLv) {
        this.teamProductLv = teamProductLv;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUpUserId() {
        return upUserId;
    }

    public void setUpUserId(int upUserId) {
        this.upUserId = upUserId;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }
}
