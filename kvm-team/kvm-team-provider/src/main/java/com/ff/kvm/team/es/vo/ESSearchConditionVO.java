package com.ff.kvm.team.es.vo;

import java.io.Serializable;
import java.util.List;

public class ESSearchConditionVO implements Serializable {

    private static final long serialVersionUID = -1310202766224026165L;

    private List<ESParamVO> shouldParams;
    private List<ESParamVO> mustParams;
    private ESParamVO includeMin;

    public ESSearchConditionVO(){
    }

    public ESSearchConditionVO(List<ESParamVO> shouldParams, List<ESParamVO> mustParams, ESParamVO includeMin){
        this.shouldParams = shouldParams;
        this.mustParams = mustParams;
        this.includeMin = includeMin;
    }

    public ESParamVO getIncludeMin() {
        return this.includeMin;
    }

    public void setIncludeMin(ESParamVO includeMin) {
        this.includeMin = includeMin;
    }

    public List<ESParamVO> getShouldParams() {
        return this.shouldParams;
    }

    public void setShouldParams(List<ESParamVO> shouldParams) {
        this.shouldParams = shouldParams;
    }

    public List<ESParamVO> getMustParams() {
        return this.mustParams;
    }

    public void setMustParams(List<ESParamVO> mustParams) {
        this.mustParams = mustParams;
    }
}
