package com.ff.kvm.team.es.repository;

import com.ff.kvm.team.es.entity.UserES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ESUserRepository extends ElasticsearchRepository<UserES, Integer> {
}
