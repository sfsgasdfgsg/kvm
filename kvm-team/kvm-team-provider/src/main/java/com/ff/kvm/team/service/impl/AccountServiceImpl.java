package com.ff.kvm.team.service.impl;

import com.ff.kvm.common.eunm.AccountTypeEnum;
import com.ff.kvm.common.eunm.BonusTypeEnum;
import com.ff.kvm.common.eunm.ChangeTypeEnum;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.dao.mapper.AccountLogMapper;
import com.ff.kvm.dao.mapper.AccountMapper;
import com.ff.kvm.dao.mapper.BonusLogMapper;
import com.ff.kvm.dao.model.AccountLog;
import com.ff.kvm.dao.model.BonusLog;
import com.ff.kvm.team.common.TeamGiftUserDV;
import com.ff.kvm.team.common.TeamProductDV;
import com.ff.kvm.team.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private BonusLogMapper bonusLogMapper;

    @Autowired
    private AccountLogMapper accountLogMapper;

    @Override
    public void productBonus(int userId,int userLv,double point,int downUserId,int downUserLv,double downPoint,double md, double total) {
        accountMapper.bonus(userId,md);

        Date nowTime = new Date();
        //记录日志
        BonusLog bonusLog = new BonusLog();
        bonusLog.setUserId(userId);
        bonusLog.setUserLv(userLv);
        bonusLog.setUserPoint(point);
        bonusLog.setDownUserId(downUserId);
        bonusLog.setDownUserLv(downUserLv);
        bonusLog.setDownUserPoint(downPoint);
        bonusLog.setTotal(total);
        bonusLog.setMd(md);
        bonusLog.setType(BonusTypeEnum.PRODUCT.type());
        bonusLog.setCreateTime(nowTime);
        bonusLogMapper.insert(bonusLog);

        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(0D);
        accountLog.setNum(md);
        accountLog.setAmount(accountMapper.getMdByUserId(userId));
        accountLog.setCreateTime(nowTime);
        accountLog.setType(AccountTypeEnum.MD.type());
        accountLog.setChangeType(ChangeTypeEnum.PRODUCT_BONUS.type());
        accountLogMapper.insert(accountLog);
    }


    @Override
    public void giftBonus(int userId,int userLv,double point,int downUserId,int downUserLv,double downPoint,double md, double total) {

        Date nowTime = new Date();
        accountMapper.bonus(userId,md);
        //记录日志
        BonusLog bonusLog = new BonusLog();
        bonusLog.setUserId(userId);
        bonusLog.setUserLv(userLv);
        bonusLog.setUserPoint(point);
        bonusLog.setDownUserId(downUserId);
        bonusLog.setDownUserLv(downUserLv);
        bonusLog.setDownUserPoint(downPoint);
        bonusLog.setTotal(total);
        bonusLog.setMd(md);
        bonusLog.setType(BonusTypeEnum.GIFTBOX.type());
        bonusLog.setCreateTime(nowTime);
        bonusLogMapper.insert(bonusLog);

        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(0D);
        accountLog.setNum(md);
        accountLog.setAmount(accountMapper.getMdByUserId(userId));
        accountLog.setCreateTime(nowTime);
        accountLog.setType(AccountTypeEnum.MD.type());
        accountLog.setChangeType(ChangeTypeEnum.GIFT_BOX_BONUS.type());
        accountLogMapper.insert(accountLog);

    }


}
