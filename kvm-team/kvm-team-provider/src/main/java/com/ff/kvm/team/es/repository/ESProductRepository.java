package com.ff.kvm.team.es.repository;

import com.ff.kvm.team.es.entity.ProductES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ESProductRepository extends ElasticsearchRepository<ProductES, Integer> {
}
