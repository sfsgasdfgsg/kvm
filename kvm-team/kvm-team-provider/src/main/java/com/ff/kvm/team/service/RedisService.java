package com.ff.kvm.team.service;

public interface RedisService {

    String getCache(String key);

    void putCache(String key, String value);
}
