package com.ff.kvm.team.quartz;

import com.ff.kvm.team.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ProductQuartz {

    @Autowired
    private ProductService productService;

    @Scheduled(cron="0 0 3 1 * ?")//每月1号凌晨3点执行
    public void execute(){
        productService.exeProduct();
    }
}
