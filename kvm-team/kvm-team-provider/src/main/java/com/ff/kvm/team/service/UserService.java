package com.ff.kvm.team.service;

import com.ff.kvm.common.dv.es.UserESDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.team.es.entity.UserES;

import java.util.List;

public interface UserService {

    Response<Boolean> addUser(UserESDV userESDV);

    List<UserES> findUserESByFloor(int floor);

    String[] getGiftBoxTeamCF();

    String[] getProdctTeamCF();

    void exeUserLV();

    UserES findUserESById(int userId);

    Response<Boolean> updateUserLv(UserESDV userESDV);

    Response<Boolean> updateUser(int userId);

    Response<Boolean> executeLV();

    Response<Boolean> updateAllUser();

    int getMaxFloor();
}
