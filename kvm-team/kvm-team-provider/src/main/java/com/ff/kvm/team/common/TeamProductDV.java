package com.ff.kvm.team.common;

import java.io.Serializable;

public class TeamProductDV implements Serializable {

    private static final long serialVersionUID = 8028868411106131000L;


    private int userId;
    private int upUserId;
    private int lv;
    private int point;//两位小数
    private int myTotal;//两位小数
    private int total;//两位小数

    public int getUpUserId() {
        return upUserId;
    }

    public void setUpUserId(int upUserId) {
        this.upUserId = upUserId;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getMyTotal() {
        return myTotal;
    }

    public void setMyTotal(int myTotal) {
        this.myTotal = myTotal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
