//package com.ff.kvm.team.config.thread;
//
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//
//import java.util.List;
//import java.util.concurrent.RecursiveTask;
//
//public class ForkJoinPeopleThread extends RecursiveTask<Integer> {
//    private static final int size = 500;
//    private EsUserService esUserService;
//    private BoolQueryBuilder builder;
//
//    public ForkJoinPeopleThread(BoolQueryBuilder builder, EsUserService esUserService)
//    {
//        this.builder = builder;
//        this.esUserService = esUserService;
//    }
//
//    protected Integer compute()
//    {
//        Integer total = 0;
//        List<UserES> list = this.esUserService.findUserByBuilder(this.builder);
//        if (list.size() <= 0) {
//            return 0;
//        }
//        if ((0 < list.size()) && (list.size() <= 500))
//        {
//            BoolQueryBuilder builder1 = QueryBuilders.boolQuery();
//            for (UserES userES : list) {
//                builder1.should(QueryBuilders.termQuery("upUserId", userES.getUserId()));
//            }
//            ForkJoinPeopleThread thread1 = new ForkJoinPeopleThread(builder1, this.esUserService);
//            thread1.fork();
//            return list.size() + thread1.join();
//        }if (list.size() > 500) {
//            int middle = list.size() / 2;
//            BoolQueryBuilder builder2 = QueryBuilders.boolQuery();
//            BoolQueryBuilder builder3 = QueryBuilders.boolQuery();
//            for (int i = 0; i < middle; i++) {
//                builder2.should(QueryBuilders.termQuery("upUserId", list.get(i).getUserId()));
//            }
//            for (int i = middle; i < list.size(); i++) {
//                builder3.should(QueryBuilders.termQuery("upUserId", list.get(i).getUserId()));
//            }
//
//            ForkJoinPeopleThread thread2 = new ForkJoinPeopleThread(builder2, this.esUserService);
//            thread2.fork();
//            ForkJoinPeopleThread thread3 = new ForkJoinPeopleThread(builder3, this.esUserService);
//            thread3.fork();
//            return list.size() + thread2.join() + thread3.join();
//        }
//
//        return total;
//    }
//}
