package com.ff.kvm.team.service;

import com.ff.kvm.common.dv.es.ProductESDV;
import com.ff.kvm.common.vo.Response;

public interface ProductService {

    Response<Boolean> addProduct(ProductESDV productESDV);

    void exeProduct();

    Response<Boolean> executeProduct();
}
