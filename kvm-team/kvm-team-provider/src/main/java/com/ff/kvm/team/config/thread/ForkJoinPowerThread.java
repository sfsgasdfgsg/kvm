//package com.ff.kvm.team.config.thread;
//
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//
//import java.util.Map;
//import java.util.concurrent.RecursiveTask;
//
//public class ForkJoinPowerThread extends RecursiveTask<Double> {
//
//    private static final int size = 800;
//    private EsUserService esUserService;
//    private BoolQueryBuilder builder;
//    private Map<Integer, Double> hostMap;
//    private int lv;
//
//    public ForkJoinPowerThread(BoolQueryBuilder builder, Map<Integer, Double> hostMap, EsUserService esUserService, int lv)
//    {
//        this.builder = builder;
//        this.hostMap = hostMap;
//        this.esUserService = esUserService;
//        this.lv = lv;
//    }
//
//    protected Double compute()
//    {
//        double total = 0D;
//        List<UserES> list = this.esUserService.findUserByBuilder(this.builder);
//        if (list.size() <= 0) {
//            return 0D;
//        }
//        this.lv += 1;
//        total = sumPower(total, list, this.hostMap);
//        if (this.lv >= 10) {
//            return total;
//        }
//        if ((0 < list.size()) && (list.size() <= 800)) {
//            BoolQueryBuilder builder1 = QueryBuilders.boolQuery();
//            for (UserES userES : list) {
//                builder1.should(QueryBuilders.termQuery("upUserId", userES.getUserId()));
//            }
//
//            ForkJoinPowerThread thread1 = new ForkJoinPowerThread(builder1, this.hostMap, this.esUserService, this.lv);
//            thread1.fork();
//            return total + thread1.join();
//        }if (list.size() > 800){
//            int middle = list.size() / 2;
//            BoolQueryBuilder builder2 = QueryBuilders.boolQuery();
//            BoolQueryBuilder builder3 = QueryBuilders.boolQuery();
//            for (int i = 0; i < middle; i++) {
//                builder2.should(QueryBuilders.termQuery("upUserId", list.get(i).getUserId()));
//            }
//
//            for (int i = middle; i < list.size(); i++) {
//                builder3.should(QueryBuilders.termQuery("upUserId", list.get(i).getUserId()));
//            }
//
//            ForkJoinPowerThread thread2 = new ForkJoinPowerThread(builder2, this.hostMap, this.esUserService, this.lv);
//            thread2.fork();
//            ForkJoinPowerThread thread3 = new ForkJoinPowerThread(builder3, this.hostMap, this.esUserService, this.lv);
//            thread3.fork();
//            return total + thread2.join() + thread3.join();
//        }
//
//        return total;
//    }
//
//    private Double sumPower(Double total, List<UserES> list, Map<Integer, Double> hostMap)
//    {
//        for (UserES userES : list) {
//            if (userES.getTotalPower() != null) {
//                total = Double.valueOf(total + userES.getTotalPower());
//            }else {
//                for (UserHostES userHostES : userES.getUserHostList()) {
//                    if (userHostES.getStatus().intValue() == HostStatusEnum.NORMAL.type()) {
//                        total = total + hostMap.get(userHostES.getType());
//                    }
//                }
//            }
//        }
//        return total;
//    }
//}
