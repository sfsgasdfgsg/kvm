package com.ff.kvm.team.service.impl;

import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.team.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisServiceImpl implements RedisService {


    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public String getCache(String key) {
        if(MyUtil.isBlank(key)){
            return null;
        }
        Object obj = redisTemplate.opsForValue().get(key);
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    @Override
    public void putCache(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }
}
