package com.ff.kvm.team.es.vo;

import java.io.Serializable;

public class ESParamVO implements Serializable {
    private static final long serialVersionUID = -7454688109594605720L;
    private String name;
    private Object value;

    public ESParamVO(String name, Object value){
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return this.value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
