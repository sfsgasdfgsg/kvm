package com.ff.kvm.team.es.util;

import cn.hutool.core.collection.CollectionUtil;
import com.ff.kvm.team.es.vo.ESParamVO;
import com.ff.kvm.team.es.vo.ESSearchConditionVO;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ESUtil {
    private static Pageable pageable = new PageRequest(0, 10000);

    public static SearchQuery getBoolQueryBuilder(List<ESParamVO> conditions)
    {
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        for (ESParamVO paramVO : conditions) {
            if ((paramVO.getValue() instanceof Integer))
                builder.must(QueryBuilders.termQuery(paramVO.getName(), Integer.valueOf(paramVO.getValue().toString())));
            else if ((paramVO.getValue() instanceof String))
                builder.must(QueryBuilders.termQuery(paramVO.getName(), paramVO.getValue().toString()));
            else if ((paramVO.getValue() instanceof Boolean)) {
                builder.must(QueryBuilders.termQuery(paramVO.getName(), Boolean.valueOf(paramVO.getValue().toString())));
            }
        }

        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(builder).build();
    }

    public static SearchQuery getBoolQueryBuilder(String key, List<Object> paramList){
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        for (Iterator localIterator = paramList.iterator(); localIterator.hasNext(); ) { Object stat = localIterator.next();
            if ((stat instanceof Integer))
                builder.should(QueryBuilders.termQuery(key, Integer.valueOf(stat.toString())));
            else if ((stat instanceof String))
                builder.should(QueryBuilders.termQuery(key, stat.toString()));
            else if ((stat instanceof Boolean)) {
                builder.should(QueryBuilders.termQuery(key, Boolean.valueOf(stat.toString())));
            }
        }

        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(builder).build();
    }

    public static SearchQuery getIndicesQueryBuilder(String key, Object param)
    {
        QueryBuilder builder = null;
        if ((param instanceof Integer))
            builder = QueryBuilders.termQuery(key, Integer.valueOf(param.toString()));
        else if ((param instanceof String))
            builder = QueryBuilders.termQuery(key, param.toString());
        else if ((param instanceof Boolean)) {
            builder = QueryBuilders.termQuery(key, Boolean.valueOf(param.toString()));
        }

        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(QueryBuilders.indicesQuery(builder, new String[0])).build();
    }

    public static SearchQuery getWildcardQueryBuilder(String key, String param)
    {
        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(QueryBuilders.wildcardQuery(key, "*" + param + "*")).build();
    }

    public static SearchQuery getBoolSearchQuery(ESSearchConditionVO conditionVO)
    {
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        shouldQueryBuilder(builder, conditionVO.getShouldParams());
        mustQueryBuilder(builder, conditionVO.getMustParams());
        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(QueryBuilders.indicesQuery(builder, new String[0])).build();
    }

    public static BoolQueryBuilder shouldQueryBuilder(BoolQueryBuilder builder, List<ESParamVO> shouldParams)
    {
        if (CollectionUtil.isEmpty(shouldParams)) {
            return builder;
        }
        for (ESParamVO paramVO : shouldParams) {
            builder.should(QueryBuilders.termQuery(paramVO.getName(), paramVO.getValue()));
        }
        return builder;
    }

    public static BoolQueryBuilder mustQueryBuilder(BoolQueryBuilder builder, List<ESParamVO> mustParams)
    {
        if (CollectionUtil.isEmpty(mustParams)) {
            return builder;
        }
        for (ESParamVO paramVO : mustParams) {
            builder.must(QueryBuilders.termQuery(paramVO.getName(), paramVO.getValue()));
        }
        return builder;
    }

    public static SearchQuery getBoolSearchQuery(BoolQueryBuilder builder){
        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(QueryBuilders.indicesQuery(builder, new String[0])).build();
    }



    //一个参数精确匹配
    public static BoolQueryBuilder getBoolQueryBuilder(String key,Object floor){
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        builder.must(QueryBuilders.termQuery(key, floor));
        return builder;
    }
    //范围查询
    public static BoolQueryBuilder getRangeQueryBuilder(BoolQueryBuilder builder,String cloum,String start,String end){

        // 包含上下边界
        return builder.filter(QueryBuilders.rangeQuery(cloum).from(start).to(end).includeLower(false).includeUpper(true));
    }

    public static SearchQuery getSearchQuery(BoolQueryBuilder builder){
        return new NativeSearchQueryBuilder().withPageable(pageable).withQuery(QueryBuilders.indicesQuery(builder, new String[0])).build();
    }


    //专用kvm查询
    public static SearchQuery getSearchQuery(int userId, Date start, Date end){
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        builder.must(QueryBuilders.termQuery("userId", userId));
        builder.must(QueryBuilders.rangeQuery("createTime").gt(start.getTime()).lt(end.getTime()).includeLower(false).includeUpper(true));
        return new NativeSearchQueryBuilder().withQuery(builder).build();
    }
}
