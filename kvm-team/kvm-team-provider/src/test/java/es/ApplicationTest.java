package es;

import com.ff.kvm.team.service.GiftBoxService;
import com.ff.kvm.team.service.ProductService;
import com.ff.kvm.team.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@ComponentScan("com.ff.kvm")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTest.class)
public class ApplicationTest {

    @Autowired
    private UserService userService;

    @Autowired
    private GiftBoxService giftBoxService;

    @Autowired
    private ProductService productService;

    @Test
    public void testUserLv(){
        userService.exeUserLV();
    }

    @Test
    public void testTeamGiftBox(){
        giftBoxService.exeGiftBox();
    }

    @Test
    public void testTeamProduct(){
        productService.exeProduct();
    }
}
