package com.ff.kvm.web;

import com.ff.kvm.common.dv.order.OrderDV;
import com.ff.kvm.common.dv.order.OrderDetailDV;
import com.ff.kvm.common.dv.order.OrderExpressDV;
import com.ff.kvm.common.dv.order.OrderPageDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.OrderConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/private/order")
public class OrderController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private OrderConsumer orderConsumer;

    //订单列表
    @RequestMapping(value = "/orderList",method = RequestMethod.POST)
    public Response<PageResult<OrderDV>> orderList(OrderPageDV orderPageDV, HttpServletRequest request){
        orderPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return orderConsumer.orderList(orderPageDV);
    }

    //订单详情
    @RequestMapping(value = "/orderDetail",method = RequestMethod.POST)
    public Response<OrderDetailDV> orderDetail(@RequestParam("orderId") int orderId){

        return orderConsumer.orderDetail(orderId);
    }

    //物流详情
    @RequestMapping(value = "/expressDetail",method = RequestMethod.POST)
    public Response<OrderExpressDV> expressDetail(@RequestParam("orderId") int orderId){

        return orderConsumer.expressDetail(orderId);
    }

    //订单签收
    @RequestMapping(value = "/signOrder",method = RequestMethod.POST)
    public Response<Boolean> signOrder(HttpServletRequest request,@RequestParam("orderId") int orderId){

        return orderConsumer.signOrder(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),orderId);
    }

    //取消订单
    @RequestMapping(value = "/cancleOrder",method = RequestMethod.POST)
    public Response<Boolean> cancleOrder(HttpServletRequest request,@RequestParam("orderId") int orderId){

        return orderConsumer.cancleOrder(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),orderId);
    }

}
