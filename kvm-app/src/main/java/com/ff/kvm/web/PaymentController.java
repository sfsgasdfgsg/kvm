package com.ff.kvm.web;

import com.ff.kvm.common.dv.payment.AddCashDV;
import com.ff.kvm.common.dv.payment.CashDV;
import com.ff.kvm.common.dv.payment.PayOrderDV;
import com.ff.kvm.common.dv.payment.PaymentResult;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.PaymentConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/private/pay")
public class PaymentController {

    @Autowired
    private PaymentConsumer paymentConsumer;

    @Autowired
    private RedisService redisService;

    //支付下单
    @RequestMapping(value = "/confirmPay",method = RequestMethod.POST)
    public Response<PaymentResult> confirmPayOrder(PayOrderDV payOrderDV, HttpServletRequest request){
        payOrderDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        payOrderDV.setIp(request.getRemoteHost());
        return paymentConsumer.confirmPayOrder(payOrderDV);
    }

    //体现
    @RequestMapping(value = "/addCash",method = RequestMethod.POST)
    public Response<Boolean> addCash(AddCashDV addCashDV, HttpServletRequest request){
        addCashDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return paymentConsumer.addCash(addCashDV);
    }

    //体现列表
    @RequestMapping(value = "/cashList",method = RequestMethod.POST)
    public Response<PageResult<CashDV>> cashList(HttpServletRequest request, @RequestParam("pageNum") int pageNum){

        return paymentConsumer.cashList(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),pageNum);
    }


}
