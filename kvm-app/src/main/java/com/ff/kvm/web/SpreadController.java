package com.ff.kvm.web;

import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.service.RedisService;
import com.ff.kvm.service.SpreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/spread")
public class SpreadController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private SpreadService spreadService;

    //推广
    @RequestMapping("/spread")
    public void spread(HttpServletRequest request, HttpServletResponse response){

        spreadService.spread(request,response);
    }

    //获取推广二维码
//    @RequestMapping(value="/private/spreadQRCode", method= RequestMethod.POST)
    public Response<String> spreadQRCode(HttpServletRequest request){

        return spreadService.spreadQRCode(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

}
