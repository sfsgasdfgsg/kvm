package com.ff.kvm.web;

import com.ff.kvm.common.dv.IdsDV;
import com.ff.kvm.common.dv.shopping.*;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.ShoppingCartConsumer;
import com.ff.kvm.service.RedisService;
import com.ff.kvm.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/private/shoppingCart")
public class ShoppingCartController {


    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private ShoppingCartConsumer shoppingCartConsumer;

    @Autowired
    private RedisService redisService;


    //加入购物车
//    @RequestMapping(value = "/addItems",method = RequestMethod.POST)
    public Response<Boolean> addItems(@RequestBody EditItemsDV editItemsDV, HttpServletRequest request){
        editItemsDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return shoppingCartService.addItems(editItemsDV);
    }


    //修改数量
//    @RequestMapping(value = "/editItems",method = RequestMethod.POST)
    public Response<Boolean> editItems(@RequestBody EditItemsDV editItemsDV, HttpServletRequest request){
        editItemsDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return shoppingCartService.editItems(editItemsDV);
    }

    //删除商品
//    @RequestMapping(value = "/delItems",method = RequestMethod.POST)
    public Response<Boolean> delItems(@RequestBody IdsDV ids, HttpServletRequest request){
        ids.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return shoppingCartService.delItems(ids);
    }

    //创建购物订单
    @RequestMapping(value = "/checkOrder",method = RequestMethod.POST)
    public Response<CheckOrderResultDV> checkOrder(@RequestBody CheckOrderDV checkOrderDV, HttpServletRequest request){
        checkOrderDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return shoppingCartConsumer.checkOrder(checkOrderDV);
    }

    //购物车商品列表
    @RequestMapping(value = "/iteamsList",method = RequestMethod.POST)
    public Response<List<ShoppingProductDV>> iteamsList(HttpServletRequest request,@RequestParam("productIds") String productIds){
        return shoppingCartConsumer.iteamsList(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),productIds);
    }

}
