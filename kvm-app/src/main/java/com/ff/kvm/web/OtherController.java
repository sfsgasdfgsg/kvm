package com.ff.kvm.web;

import com.ff.kvm.common.dv.user.ComplaintDV;
import com.ff.kvm.common.dv.user.HelperDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.OtherConsumer;
import com.ff.kvm.service.RedisService;
import com.ff.kvm.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/private/other")
public class OtherController {

    @Autowired
    private OtherConsumer otherConsumer;

    @Autowired
    private RedisService redisService;

    @Autowired
    private UtilService utilService;

    //帮助中心列表
    @RequestMapping(value = "/helperList",method = RequestMethod.POST)
    public Response<List<HelperDV>> helperList(){

        return otherConsumer.helperList();
    }

    //帮助中心
    @RequestMapping(value = "/helperDetail",method = RequestMethod.POST)
    public Response<HelperDV> helperDetail(@RequestParam("id")int id){

        return otherConsumer.helperDetail(id);
    }

    //关于我们
    @RequestMapping(value = "/about",method = RequestMethod.POST)
    public Response<HelperDV> about(){

        return otherConsumer.about();
    }

    //投诉
    @RequestMapping(value = "/addComplaint",method = RequestMethod.POST)
    public Response<Boolean> addComplaint(HttpServletRequest request,ComplaintDV complaintDV){
        complaintDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return otherConsumer.addComplaint(complaintDV);
    }

    //投诉列表
    @RequestMapping(value = "/complaintList",method = RequestMethod.POST)
    public Response<List<ComplaintDV>> complaintList(HttpServletRequest request){

        return otherConsumer.complaintList(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }


    //投诉详情
    @RequestMapping(value = "/complaintDetail",method = RequestMethod.POST)
    public Response<ComplaintDV> complaintDetail(@RequestParam("id") int id){

        return otherConsumer.complaintDetail(id);
    }

    //上传图片
    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public Response<String> upload(HttpServletRequest request){

        return utilService.upload(request);
    }
}
