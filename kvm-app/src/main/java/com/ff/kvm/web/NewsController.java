package com.ff.kvm.web;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.NewsDetailDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.NewsConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/private/news")
public class NewsController {

    @Autowired
    private NewsConsumer newsConsumer;

    //资讯列表
    @RequestMapping(value = "/newsList",method = RequestMethod.POST)
    public Response<List<NewsDV>> newsList(){

        return newsConsumer.newsList();
    }

    //资讯详情
    @RequestMapping(value = "/newsDetail",method = RequestMethod.POST)
    public Response<NewsDetailDV> newsDetail(@RequestParam("id") int id){

        return newsConsumer.newsDetail(id);
    }
}
