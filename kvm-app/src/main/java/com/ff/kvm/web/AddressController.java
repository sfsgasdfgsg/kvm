package com.ff.kvm.web;

import com.ff.kvm.common.dv.AddressDV;
import com.ff.kvm.common.dv.AreaDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.AddressConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/private/address")
public class AddressController {


    @Autowired
    private AddressConsumer addressConsumer;

    @Autowired
    private RedisService redisService;

    //新增收货地址
    @RequestMapping(value = "/newAddress",method = RequestMethod.POST)
    public Response<Boolean> newAddress(HttpServletRequest request,AddressDV addressDV){
        addressDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return addressConsumer.newAddress(addressDV);
    }

    //修改收货地址
    @RequestMapping(value = "/updateAddress",method = RequestMethod.POST)
    public Response<Boolean> updateAddress(HttpServletRequest request,AddressDV addressDV){
        addressDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return addressConsumer.updateAddress(addressDV);
    }

    //设置默认收货地址
    @RequestMapping(value = "/updateDefaultAddress",method = RequestMethod.POST)
    public Response<Boolean> updateDefaultAddress(HttpServletRequest request){

        return addressConsumer.updateDefaultAddress(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),Integer.valueOf(request.getParameter("id")));
    }


    //获取默认收货地址
    @RequestMapping(value = "/defaultAddress",method = RequestMethod.POST)
    public Response<AddressDV> defaultAddress(HttpServletRequest request){

        return addressConsumer.defaultAddress(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

    //获取收货地址列表
    @RequestMapping(value = "/userAddress",method = RequestMethod.POST)
    public Response<List<AddressDV>> userAddress(HttpServletRequest request){

        return addressConsumer.userAddress(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

    //获取地址
    @RequestMapping(value = "/areaList",method = RequestMethod.POST)
    public Response<List<AreaDV>> areaList(int id){

        return addressConsumer.areaList(id);
    }

    //删除收货地址
    @RequestMapping(value = "/delAddress",method = RequestMethod.POST)
    public Response<Boolean> delAddress(HttpServletRequest request){

        return addressConsumer.delAddress(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),Integer.valueOf(request.getParameter("id")));
    }

}
