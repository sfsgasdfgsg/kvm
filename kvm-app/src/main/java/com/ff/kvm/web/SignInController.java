package com.ff.kvm.web;

import com.ff.kvm.common.dv.signin.GetGiftDV;
import com.ff.kvm.common.dv.signin.PersonalProductDV;
import com.ff.kvm.common.dv.signin.SignStatusDV;
import com.ff.kvm.common.dv.signin.SigninGiftTimeNodeDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.SignInConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/private/signIn")
public class SignInController {

    @Autowired
    private SignInConsumer signInConsumer;
    @Autowired
    private RedisService redisService;

    @RequestMapping(value = "/getGiftPostage", method = RequestMethod.POST)
    public Response<String> getGiftPostage(HttpServletRequest request) {
        return signInConsumer.getGiftPostage(Integer.valueOf(request.getParameter("id")));
    }

    /**
     * 用户领取奖品(生成订单支付成功后调用)
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getGift", method = RequestMethod.POST)
    public Response getGift(HttpServletRequest request) {
        return signInConsumer.getGift(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))), Integer.valueOf(request.getParameter("id")));
    }

    /**
     * 签到
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/signIn", method = RequestMethod.POST)
    public Response<GetGiftDV> signIn(HttpServletRequest request) {
        return signInConsumer.signIn(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

    /**
     * 个人首页签到状态获取
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/signinStatus", method = RequestMethod.POST)
    public Response<SignStatusDV> signinStatus(HttpServletRequest request) {
        return signInConsumer.signinStatus(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

    /**
     * 签到礼物领取时间节点
     *
     * @return
     */
    @RequestMapping(value = "/signinGiftNode", method = RequestMethod.POST)
    public Response<SigninGiftTimeNodeDV> signinGiftNode() {
        return signInConsumer.signinGiftNode();
    }

    /**
     * 签到个人礼品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/personalGiftList", method = RequestMethod.POST)
    public Response<PageResult<PersonalProductDV>> personalGiftList(HttpServletRequest request) {
        return signInConsumer.personalGiftList(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))), Integer.valueOf(request.getParameter("PageNum")), Integer.valueOf(request.getParameter("status")));
    }

}
