package com.ff.kvm.web;

import com.ff.kvm.common.dv.account.AccountDV;
import com.ff.kvm.common.dv.account.AccountLogDV;
import com.ff.kvm.common.dv.account.AccountPageDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.AccountConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/private/account")
public class AccountController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private AccountConsumer accountConsumer;

    @RequestMapping(value = "/financialDetail",method = RequestMethod.POST)
    public Response<PageResult<AccountLogDV>> financialDetail(HttpServletRequest request,AccountPageDV accountPageDV){
        accountPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return accountConsumer.financialDetail(accountPageDV);
    }

    @RequestMapping(value = "/account",method = RequestMethod.POST)
    public Response<AccountDV> account(HttpServletRequest request){
        return accountConsumer.account(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }


}
