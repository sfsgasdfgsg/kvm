package com.ff.kvm.web;

import com.ff.kvm.common.dv.team.*;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.TeamConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/private/team")
public class TeamController {

    @Autowired
    private TeamConsumer teamConsumer;

    @Autowired
    private RedisService redisService;

    //团队信息
    @RequestMapping(value = "/team",method = RequestMethod.POST)
    public Response<TeamDV> team(HttpServletRequest request){
        return teamConsumer.team(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

    //直推列表
    @RequestMapping(value = "/teamUser",method = RequestMethod.POST)
    public Response<PageResult<TeamUserDV>> teamUser(HttpServletRequest request, TeamPageDV teamPageDV){
        teamPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return teamConsumer.teamUser(teamPageDV);
    }

    //业绩
    @RequestMapping(value = "/achieInfo",method = RequestMethod.POST)
    public Response<AchieInfoDV> achieInfo(HttpServletRequest request, TeamPageDV teamPageDV){
        teamPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return teamConsumer.achieInfo(teamPageDV);
    }

    //团队报表
    @RequestMapping(value = "/teamInfo",method = RequestMethod.POST)
    public Response<TeamInfoDV> teamInfo(HttpServletRequest request){

        return teamConsumer.teamInfo(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }


    //团队分红明细
    @RequestMapping(value = "/bonusLog",method = RequestMethod.POST)
    public Response<PageResult<BonusLogDV>> bonusLog(HttpServletRequest request, TeamPageDV teamPageDV){
        teamPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return teamConsumer.bonusLog(teamPageDV);
    }
}
