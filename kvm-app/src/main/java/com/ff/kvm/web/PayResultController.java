package com.ff.kvm.web;

import com.ff.kvm.service.PayResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/payResult")
public class PayResultController {

    @Autowired
    private PayResultService payResultService;

    //微信支付回调
    @RequestMapping(value = "/wx",method = RequestMethod.POST)
    public void wxPayRT(HttpServletRequest request, HttpServletResponse response){

        payResultService.wxPayRT(request,response);
    }

    //支付宝支付回调
    @RequestMapping(value = "/zfb",method = RequestMethod.POST)
    public void zfbPayRT(HttpServletRequest request, HttpServletResponse response){
        payResultService.zfbPayRT(request,response);
    }
}
