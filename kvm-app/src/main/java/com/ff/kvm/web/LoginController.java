package com.ff.kvm.web;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.service.LoginService;
import com.ff.kvm.common.vo.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;


    //获取验证码
    @RequestMapping("/kaptcha")
    public void getKaptcha(HttpServletRequest request, HttpServletResponse response) {

        try{
            if (request.getSession() == null) {
                return;
            }
            String mobile = request.getParameter("mobile");
            BufferedImage challenge = loginService.createBufferedImage(mobile);
            ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
            ImageIO.write(challenge, "jpg", jpegOutputStream);

            byte[] captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
            response.setContentType("image/jpeg");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0L);
            ServletOutputStream out = response.getOutputStream();
            out.write(captchaChallengeAsJpeg);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    //推广
//    @RequestMapping(value="/spread", method= RequestMethod.POST)
//    public Response<Boolean> spread(LoginDV loginDV){
//
//        return loginService.spread(loginDV);
//    }

    //注册发送短信
    @RequestMapping(value="/registerSendCode", method= RequestMethod.POST)
    public Response<Boolean> registerSendCode(HttpServletRequest request, LoginDV loginDV){

        loginDV.setIp(request.getRemoteHost());
        return loginService.registerSendCode(loginDV);
    }

    //注册
    @RequestMapping(value="/register", method= RequestMethod.POST)
    public Response<Boolean> register(LoginDV loginDV){

        return loginService.register(loginDV);
    }


    //帐号密码登陆
    @RequestMapping(value="/login", method= RequestMethod.POST)
    public Response<String> login(LoginDV loginDV){

        return loginService.login(loginDV);
    }


    //找回登陆密码发送短信
    @RequestMapping(value="/forgetPwdSendCode", method= RequestMethod.POST)
    public Response<Boolean> forgetPwdSendCode(LoginDV loginDV){

        return loginService.forgetPwdSendCode(loginDV);
    }

    //找回登陆密码
    @RequestMapping(value="/forgetPwd", method= RequestMethod.POST)
    public Response<Boolean> forgetPwd(LoginDV loginDV){

        return loginService.forgetPwd(loginDV);
    }

    //微信登陆
    @RequestMapping(value="/wxLogin", method= RequestMethod.POST)
    public Response<String> wxLogin(LoginDV loginDV){

        return loginService.wxLogin(loginDV);
    }


    //手机号码登陆发送短信
    @RequestMapping(value="/phoneLoginSendCode", method= RequestMethod.POST)
    public Response<Boolean> phoneLoginSendCode(LoginDV loginDV){

        return loginService.phoneLoginSendCode(loginDV);
    }

    //手机号码登陆
    @RequestMapping(value="/phoneLogin", method= RequestMethod.POST)
    public Response<String> phoneLogin(LoginDV loginDV){

        return loginService.phoneLogin(loginDV);
    }

    //注销登录
    @RequestMapping(value="/loginOut", method= RequestMethod.POST)
    public Response<Boolean> loginOut(HttpServletRequest request){

        return loginService.loginOut(request.getHeader(Constants.TOKENID));
    }

}
