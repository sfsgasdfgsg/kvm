package com.ff.kvm.web;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.dv.home.HomeDV;
import com.ff.kvm.common.dv.product.CategoryDV;
import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductSpecialDV;
import com.ff.kvm.common.dv.product.ProductDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.HomeConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/private/home")
public class HomeController {

    @Autowired
    private HomeConsumer homeConsumer;

    //首页轮播图
    @RequestMapping(value = "/rollList",method = RequestMethod.POST)
    public Response<List<RollListDV>> rollList(){

        return homeConsumer.rollList();
    }


    //宣传视频
    @RequestMapping(value = "/propVideo",method = RequestMethod.POST)
    public Response<String> propVideo(){

        return homeConsumer.propVideo();
    }

    //头条
    @RequestMapping(value = "/hotList",method = RequestMethod.POST)
    public Response<List<NewsDV>> hotList(){

        return homeConsumer.hotList();
    }

    //特色商品
    @RequestMapping(value = "/special",method = RequestMethod.POST)
    public Response<List<ProductSpecialDV>> special(){
        return homeConsumer.special();
    }

    //热销/抢购展示
    @RequestMapping(value = "/active",method = RequestMethod.POST)
    public Response<ProductActiveDV> active(@RequestParam("type") int type){
        return homeConsumer.active(type);
    }

    //系列专区
    @RequestMapping(value = "/category",method = RequestMethod.POST)
    public Response<List<CategoryDV>> category(){
        return homeConsumer.category();
    }


    //首页整合接口
    @RequestMapping(value = "/home",method = RequestMethod.POST)
    public Response<HomeDV> home(){
        return homeConsumer.home();
    }



}
