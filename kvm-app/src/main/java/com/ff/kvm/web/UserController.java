package com.ff.kvm.web;

import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.user.*;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.UserConsumer;
import com.ff.kvm.service.LoginService;
import com.ff.kvm.service.RedisService;
import com.ff.kvm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/private/user")
public class UserController {

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserConsumer userConsumer;

    @Autowired
    private UserService userService;


    //绑定手机号
    @RequestMapping(value="/bindPhoneSendCode", method= RequestMethod.POST)
    public Response<Boolean> bindPhoneSendCode( BindPhoneDV bindPhoneDV){

        return userService.bindPhoneSendCode(bindPhoneDV);
    }

    //绑定手机号
    @RequestMapping(value="/bindPhone", method= RequestMethod.POST)
    public Response<Boolean> bindPhone(BindPhoneDV bindPhoneDV,HttpServletRequest request){
        bindPhoneDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return userService.bindPhone(bindPhoneDV);
    }

    //领取直推礼包
    @RequestMapping(value = "/directGift",method = RequestMethod.POST)
    public Response<CheckOrderResultDV> directGift(HttpServletRequest request){

        return userConsumer.directGift(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),Integer.valueOf(request.getParameter("product")));
    }



    //修改交易密码
    @RequestMapping(value = "/updateDealPwd",method = RequestMethod.POST)
    public Response<Boolean> updateDealPwd(HttpServletRequest request, UpdateDealPwdDV updateDealPwdDV){
        updateDealPwdDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return userConsumer.updateDealPwd(updateDealPwdDV);
    }

    //找回交易密码发送短信
    @RequestMapping(value = "/forgetDealPwdSendCode",method = RequestMethod.POST)
    public Response<Boolean> forgetDealPwdSendCode(HttpServletRequest request,ForgetDealPwdDV forgetDealPwdDV){
        forgetDealPwdDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        forgetDealPwdDV.setIp(request.getRemoteHost());
        return userService.forgetDealPwdSendCode(forgetDealPwdDV);
    }

    //找回交易密码
    @RequestMapping(value = "/forgetDealPwd",method = RequestMethod.POST)
    public Response<Boolean> forgetDealPwd(HttpServletRequest request,ForgetDealPwdDV forgetDealPwdDV){
        forgetDealPwdDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return userService.forgetDealPwd(forgetDealPwdDV);
    }

    //用户信息
    @RequestMapping(value = "/userInfo",method = RequestMethod.POST)
    public Response<UserInfoDV> userInfo(HttpServletRequest request){

        return userConsumer.userInfo(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }

    //设置交易密码发送短信
    @RequestMapping(value = "/dealPwdSendCode",method = RequestMethod.POST)
    public Response<Boolean> dealPwdSendCode(HttpServletRequest request,DealPwdDV dealPwdDV){
        dealPwdDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        dealPwdDV.setIp(request.getRemoteHost());
        return userService.dealPwdSendCode(dealPwdDV);
    }

    //设置交易密码
    @RequestMapping(value = "/dealPwd",method = RequestMethod.POST)
    public Response<Boolean> dealPwd(HttpServletRequest request,DealPwdDV dealPwdDV){
        dealPwdDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return userService.dealPwd(dealPwdDV);
    }

    //获取推广二维码
    @RequestMapping(value="/spreadQRCode", method= RequestMethod.POST)
    public Response<String> spreadQRCode(HttpServletRequest request){

        return userConsumer.spreadQRCode(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
    }


}
