package com.ff.kvm.web;

import com.ff.kvm.common.dv.gift.GiftBoxDV;
import com.ff.kvm.common.dv.gift.GiftBoxItemsDV;
import com.ff.kvm.common.dv.product.*;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.ProductConsumer;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/private/product")
public class ProductController {

    @Autowired
    private ProductConsumer productConsumer;

    @Autowired
    private RedisService redisService;


    //商品列表，普通折扣
    @RequestMapping(value = "/productList",method = RequestMethod.POST)
    public Response<PageResult<ProductDV>> productList(HttpServletRequest request,ProductPageDV productPageDV){
        productPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return productConsumer.productList(productPageDV);
    }

    //商品详情
    @RequestMapping(value = "/productDetail",method = RequestMethod.POST)
    public Response<ProductDetailDV> productDetail(HttpServletRequest request,@RequestParam("id") int id){
        return productConsumer.productDetail(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))),id);
    }

    //立即购买
    @RequestMapping(value = "/buyProduct",method = RequestMethod.POST)
    public Response<CheckOrderResultDV> buyProduct(HttpServletRequest request,BuyProductDV buyProductDV){
        buyProductDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return productConsumer.buyProduct(buyProductDV);
    }

    //签到礼品购买
    @RequestMapping(value = "/buySigninGiftProduct",method = RequestMethod.POST)
    public Response<CheckOrderResultDV> buySigninGiftProduct(HttpServletRequest request,BuyProductDV buyProductDV){
        buyProductDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return productConsumer.buySigninGiftProduct(buyProductDV);
    }

    //礼包列表
    @RequestMapping(value = "/giftBoxList",method = RequestMethod.POST)
    public Response<List<GiftBoxDV>> giftBoxList(){

        return productConsumer.giftBoxList();
    }

    //礼包商品列表
    @RequestMapping("/giftBoxItems")
    public Response<List<GiftBoxItemsDV>> giftBoxItems(@RequestParam("id") int id){

        return productConsumer.giftBoxItems(id);
    }

    //专区名称列表
    @RequestMapping(value = "/categoryList",method = RequestMethod.POST)
    public Response<List<CategoryDV>> categoryList(){
        return productConsumer.categoryList();
    }


    //活动列表
    @RequestMapping(value = "/productActiveList",method = RequestMethod.POST)
    public Response<PageResult<ProductActiveDV>> productActiveList(ProductPageDV productPageDV){
        return productConsumer.productActiveList(productPageDV);
    }

    //商品列表 专属折扣
    @RequestMapping(value = "/productDiscountList",method = RequestMethod.POST)
    public Response<PageResult<ProductDV>> productDiscountList(HttpServletRequest request,ProductPageDV productPageDV){
        productPageDV.setUserId(Integer.valueOf(redisService.getCache(request.getHeader(Constants.TOKENID))));
        return productConsumer.productDiscountList(productPageDV);
    }



}
