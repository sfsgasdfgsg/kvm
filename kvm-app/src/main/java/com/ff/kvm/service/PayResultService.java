package com.ff.kvm.service;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface PayResultService {

    void wxPayRT(HttpServletRequest request, HttpServletResponse response);

    void zfbPayRT(HttpServletRequest request, HttpServletResponse response);
}
