package com.ff.kvm.service;


import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.dv.user.BindPhoneDV;
import com.ff.kvm.common.dv.user.ForgetDealPwdDV;
import com.ff.kvm.common.vo.Response;

import java.awt.image.BufferedImage;

public interface LoginService {

    BufferedImage createBufferedImage(String mobile);

    Response<Boolean> registerSendCode(LoginDV loginDV);

    Response<Boolean> register(LoginDV loginDV);

    Response<String> login(LoginDV loginDV);

    Response<Boolean> forgetPwdSendCode(LoginDV loginDV);

    Response<Boolean> forgetPwd(LoginDV loginDV);

    Response<Boolean> spread(LoginDV loginDV);

    Response<String> wxLogin(LoginDV loginDV);

    Response<String> phoneLogin(LoginDV loginDV);

    Response<Boolean> phoneLoginSendCode(LoginDV loginDV);


    boolean checkKaptchaCode(String mobile, String kaptchaCode);

    Response<Boolean> loginOut(String token);
}
