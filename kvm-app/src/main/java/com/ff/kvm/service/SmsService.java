package com.ff.kvm.service;


import com.aliyuncs.exceptions.ClientException;
import com.ff.kvm.common.vo.Response;

public interface SmsService {

    boolean checkCode(String mobile, String code);

    boolean sendCode(String mobile,int type,String ip) throws ClientException;

    Response<Boolean> send(String mobile, int type, String ip);
}
