package com.ff.kvm.service.impl;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.dv.wx.WXLoginDV;
import com.ff.kvm.common.dv.wx.WXUserInfoDV;
import com.ff.kvm.common.eunm.SmsTypeEnum;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.third_util.WXOpenUtil;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.config.kaptcha.KaptchaProducer;
import com.ff.kvm.mall.consumer.LoginConsumer;
import com.ff.kvm.mall.consumer.UserConsumer;
import com.ff.kvm.service.LoginService;
import com.ff.kvm.service.RedisService;
import com.ff.kvm.service.SmsService;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;

@Service
public class LoginServiceImpl implements LoginService {


    @Autowired
    private RedisService redisService;

    @Autowired
    private KaptchaProducer kaptchaProducer;

    @Autowired
    private LoginConsumer loginConsumer;

    @Autowired
    private UserConsumer userConsumer;

    @Autowired
    private SmsService smsService;

    @Override
    public BufferedImage createBufferedImage(String mobile) {
        DefaultKaptcha defaultKaptcha = kaptchaProducer.getDefaultKaptcha();
        String text = defaultKaptcha.createText();
        BufferedImage bufferedImage = defaultKaptcha.createImage(text);
        redisService.putKaptcha(mobile, text);
        return bufferedImage;
    }

    @Override
    public Response<Boolean> registerSendCode(LoginDV loginDV) {

        Response<Boolean> response = new Response<>();
        if (MyUtil.isBlank(loginDV.getMobile())) {
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }

        String kaptchaCode = redisService.getKaptcha(loginDV.getMobile());
        redisService.delCache(loginDV.getMobile());
        if ((kaptchaCode == null) || (!kaptchaCode.equals(loginDV.getKaptchaCode().toLowerCase()))) {
            response.setCode(StatusEnum.VD_ERROR_KAPTCHA_CODE.code());
            response.setMsg(StatusEnum.VD_ERROR_KAPTCHA_CODE.description());
            return response;
        }
        response = userConsumer.checkMobile(loginDV.getMobile());
        if(response.getData()){
            response.setCode(StatusEnum.MOBILE_EXIST.getCode());
            response.setMsg(StatusEnum.MOBILE_EXIST.description());
            response.setData(false);
            return response;
        }

        response = smsService.send(loginDV.getMobile(), SmsTypeEnum.REGISTER.type(),loginDV.getIp());
        if(!response.getData()){
            response.setCode(StatusEnum.FAIL.code());
            response.setMsg(StatusEnum.FAIL.description());
            response.setData(false);
            return response;
        }

        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Response<Boolean> register(LoginDV loginDV) {

        Response<Boolean> response = new Response<>();
        if (MyUtil.isBlank(loginDV.getMobile())) {
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }

        if (MyUtil.isBlank(loginDV.getCode())) {
            response.setCode(StatusEnum.VD_EMPTY_CODE.code());
            response.setMsg(StatusEnum.VD_EMPTY_CODE.description());
            response.setData(false);
            return response;
        }

        if (MyUtil.isBlank(loginDV.getLoginPwd())) {
            response.setCode(StatusEnum.VD_EMPTY_LOGIN_PWD.code());
            response.setMsg(StatusEnum.VD_EMPTY_LOGIN_PWD.description());
            response.setData(false);
            return response;
        }

        if(!smsService.checkCode(loginDV.getMobile(),loginDV.getCode())){
            response.setCode(StatusEnum.VD_ERROR_DYNAMIC_CODE.code());
            response.setMsg(StatusEnum.VD_ERROR_DYNAMIC_CODE.description());
            response.setData(false);
            return response;
        }

        return loginConsumer.register(loginDV);
    }

    @Override
    public Response<String> login(LoginDV loginDV) {

        Response<String> response = new Response<>();
        if (MyUtil.isBlank(loginDV.getUserName())) {
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            return response;
        }

        if (MyUtil.isBlank(loginDV.getLoginPwd())) {
            response.setCode(StatusEnum.VD_EMPTY_LOGIN_PWD.code());
            response.setMsg(StatusEnum.VD_EMPTY_LOGIN_PWD.description());
            return response;
        }

        return loginConsumer.login(loginDV);
    }

    @Override
    public Response<Boolean> forgetPwdSendCode(LoginDV loginDV) {

        Response<Boolean> response = new Response<>();
        if (MyUtil.isBlank(loginDV.getMobile())) {
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }

        String kaptchaCode = redisService.getKaptcha(loginDV.getMobile());
        redisService.delCache(loginDV.getMobile());
        if ((kaptchaCode == null) || (!kaptchaCode.equals(loginDV.getKaptchaCode().toLowerCase()))) {
            response.setCode(StatusEnum.VD_ERROR_KAPTCHA_CODE.code());
            response.setMsg(StatusEnum.VD_ERROR_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }

        response = userConsumer.checkMobile(loginDV.getMobile());
        if(!response.getData()){
            response.setCode(StatusEnum.NOT_EXIST_MOBILE.getCode());
            response.setMsg(StatusEnum.NOT_EXIST_MOBILE.description());
            response.setData(false);
            return response;
        }
        response = smsService.send(loginDV.getMobile(), SmsTypeEnum.LOGINPWD.type(),loginDV.getIp());

        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Response<Boolean> forgetPwd(LoginDV loginDV) {

        Response<Boolean> response = new Response<>();
        if (MyUtil.isBlank(loginDV.getMobile())) {
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }
        if (MyUtil.isBlank(loginDV.getCode())) {
            response.setCode(StatusEnum.VD_EMPTY_CODE.code());
            response.setMsg(StatusEnum.VD_EMPTY_CODE.description());
            response.setData(false);
            return response;
        }
        if (MyUtil.isBlank(loginDV.getLoginPwd())) {
            response.setCode(StatusEnum.VD_EMPTY_LOGIN_PWD.code());
            response.setMsg(StatusEnum.VD_EMPTY_LOGIN_PWD.description());
            response.setData(false);
            return response;
        }
        if(smsService.checkCode(loginDV.getMobile(),loginDV.getCode())){
            response.setCode(StatusEnum.VD_ERROR_DYNAMIC_CODE.code());
            response.setMsg(StatusEnum.VD_ERROR_DYNAMIC_CODE.description());
            response.setData(false);
            return response;
        }

        return loginConsumer.forgetPwd(loginDV);
    }

    @Override
    public Response<Boolean> spread(LoginDV loginDV) {
        return loginConsumer.spread(loginDV);
    }

    @Override
    public Response<String> wxLogin(LoginDV loginDV) {
        Response<String> response = new Response<>();

        WXLoginDV wxLoginDV = getOpenRefreshToken(loginDV.getWxCode());
        if(!MyUtil.isBlank(wxLoginDV.getErrcode())){
            response.setCode(wxLoginDV.getErrcode());
            response.setMsg(wxLoginDV.getErrmsg());
            return response;
        }
        loginDV.setAccessToken(wxLoginDV.getAccess_token());
        loginDV.setOpenId(wxLoginDV.getOpenid());
        return loginConsumer.wxLogin(loginDV);
    }

    private WXLoginDV getOpenRefreshToken(String wxCode) {
//        WXLoginDV wxLoginDV = WXOpenUtil.getAccessToken(wxCode);
//        String refreshToken = redisService.getCache(Constants.WX_OPEN_REFRESH_TOKEN);
//        if(MyUtil.isBlank(refreshToken)){
//            wxLoginDV = WXOpenUtil.getAccessToken(wxCode);
//            if(MyUtil.isBlank(wxLoginDV.getErrcode())){
//                redisService.putCache(Constants.WX_OPEN_REFRESH_TOKEN,wxLoginDV.getRefresh_token());
//            }
//            return wxLoginDV;
//        }
//        return WXOpenUtil.refreshToken(wxLoginDV);
        return WXOpenUtil.getAccessToken(wxCode);
    }


    @Override
    public Response<String> phoneLogin(LoginDV loginDV) {
        Response<String> response = new Response<>();
        if(MyUtil.isBlank(loginDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            return response;
        }
        if(!smsService.checkCode(loginDV.getMobile(),loginDV.getCode())){
            response.setCode(StatusEnum.VD_ERROR_DYNAMIC_CODE.code());
            response.setMsg(StatusEnum.VD_ERROR_DYNAMIC_CODE.description());
            return response;
        }

        return loginConsumer.phoneLogin(loginDV);
    }


    @Override
    public Response<Boolean> phoneLoginSendCode(LoginDV loginDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(loginDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.code());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            return response;
        }

        if(!checkKaptchaCode(loginDV.getMobile(),loginDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_ERROR_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }
        response = userConsumer.checkMobile(loginDV.getMobile());
        if(!response.getData()){
            response.setCode(StatusEnum.NOT_EXIST_MOBILE.getCode());
            response.setMsg(StatusEnum.NOT_EXIST_MOBILE.description());
            response.setData(false);
            return response;
        }

        response = smsService.send(loginDV.getMobile(), SmsTypeEnum.LOGINPWD.type(),loginDV.getIp());
        if(!response.getData()){
            return response;
        }

        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }




    @Override
    public boolean checkKaptchaCode(String mobile, String kaptchaCode) {

        String o = redisService.getKaptcha(mobile);

        if(o == null){
            return false;
        }
        if(!o.equals(kaptchaCode.toLowerCase())){
            return false;
        }

        redisService.delKaptcha(mobile);
        return true;
    }

    @Override
    public Response<Boolean> loginOut(String token) {

        String userId = redisService.getCache(token);
        if(!MyUtil.isBlank(userId)){
            redisService.delCache(userId);
        }
        redisService.delCache(token);
        Response<Boolean> response = new Response<>();
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }
}
