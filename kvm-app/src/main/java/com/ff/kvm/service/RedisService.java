package com.ff.kvm.service;


import com.ff.kvm.common.dv.shopping.ShoppingCartDV;

public interface RedisService {

    void putAppCache(String key, String value);

    String getAppCache(String key);

    void delCache(String key);

    void putKaptcha(String key, String value);

    void delKaptcha(String key);

    String getKaptcha(String key);

    String getCache(String key);

    void putCache(String key,String value);

    boolean isContain(String key);

    void putSMSCode(String mobile, String code);

    String getSMSCode(String mobile);

    void delSMSCode(String mobile);

    ShoppingCartDV getShoppingCart(int userId);

    void putShoppingCart(ShoppingCartDV shoppingCartDV);
}
