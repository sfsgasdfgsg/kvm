package com.ff.kvm.service.impl;

import com.ff.kvm.common.dv.shopping.ShoppingCartDV;
import com.ff.kvm.common.util.BeanConvertUtils;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisServiceImpl implements RedisService {

    private String kaptcha = "kaptcha_";

    private String sms = "sms_";

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void putAppCache(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, 5, TimeUnit.HOURS);
    }

    @Override
    public String getAppCache(String key) {
        if(MyUtil.isBlank(key)){
            return null;
        }
        Object obj = redisTemplate.opsForValue().get(key);
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    @Override
    public void delCache(String key) {
        if(MyUtil.isBlank(key)){
            return ;
        }
        if (redisTemplate.hasKey(key).booleanValue()){
            redisTemplate.delete(key);
        }
    }

    @Override
    public void putKaptcha(String key, String value) {
        key = kaptcha+key;
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key, 5, TimeUnit.MINUTES);
    }

    @Override
    public void delKaptcha(String key) {
        if(MyUtil.isBlank(key)){
            return ;
        }
        key = kaptcha+key;
        if (redisTemplate.hasKey(key)){
            redisTemplate.delete(key);
        }

    }

    @Override
    public String getKaptcha(String key) {
        if(MyUtil.isBlank(key)){
            return null;
        }
        Object obj = redisTemplate.opsForValue().get(kaptcha+key);
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    @Override
    public String getCache(String key) {
        if(MyUtil.isBlank(key)){
            return null;
        }
        Object obj = redisTemplate.opsForValue().get(key);
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    @Override
    public void putCache(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    @Override
    public boolean isContain(String key) {
        if (redisTemplate.hasKey(key)){
            return true;
        }
        return false;
    }

    @Override
    public void putSMSCode(String mobile, String code) {
        String key = sms + mobile;
        redisTemplate.opsForValue().set(key, code);
        redisTemplate.expire(key, 5, TimeUnit.MINUTES);
    }

    @Override
    public String getSMSCode(String mobile) {

        if(MyUtil.isBlank(mobile)){
            return null;
        }
        Object obj = redisTemplate.opsForValue().get(sms + mobile);
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }


    @Override
    public void delSMSCode(String mobile) {

        if(MyUtil.isBlank(mobile)){
            return ;
        }
        String key = sms + mobile;
        if (redisTemplate.hasKey(key)){
            redisTemplate.delete(key);
        }
    }

    @Override
    public ShoppingCartDV getShoppingCart(int userId) {
        String key = Constants.SHOPPINTG_CART+userId;
        if (redisTemplate.hasKey(key)){
            Object obj = redisTemplate.opsForValue().get(key);
            return BeanConvertUtils.convert(obj, ShoppingCartDV.class);
        }
        return null;
    }

    @Override
    public void putShoppingCart(ShoppingCartDV shoppingCartDV) {
        redisTemplate.opsForValue().set(Constants.SHOPPINTG_CART+shoppingCartDV.getUserId(), MyUtil.toJSON(shoppingCartDV));
    }


}
