package com.ff.kvm.service.impl;

import com.ff.kvm.common.dv.user.BindPhoneDV;
import com.ff.kvm.common.dv.user.DealPwdDV;
import com.ff.kvm.common.dv.user.ForgetDealPwdDV;
import com.ff.kvm.common.eunm.SmsTypeEnum;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.UserConsumer;
import com.ff.kvm.service.LoginService;
import com.ff.kvm.service.SmsService;
import com.ff.kvm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserConsumer userConsumer;

    @Autowired
    private SmsService smsService;

    @Autowired
    private LoginService loginService;

    @Override
    public Response<Boolean> bindPhoneSendCode(BindPhoneDV bindPhoneDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(bindPhoneDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }
        if(MyUtil.isBlank(bindPhoneDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_EMPTY_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }

        if(!loginService.checkKaptchaCode(bindPhoneDV.getMobile(),bindPhoneDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_ERROR_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }
        response = userConsumer.checkMobile(bindPhoneDV.getMobile());
        if(response.getData()){
            response.setCode(StatusEnum.MOBILE_EXIST.getCode());
            response.setMsg(StatusEnum.MOBILE_EXIST.description());
            response.setData(false);
            return response;
        }

        response = smsService.send(bindPhoneDV.getMobile(), SmsTypeEnum.BIND.type(),bindPhoneDV.getIp());

        return response;
    }

    @Override
    public Response<Boolean> bindPhone(BindPhoneDV bindPhoneDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(bindPhoneDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }

        if(!smsService.checkCode(bindPhoneDV.getMobile(),bindPhoneDV.getCode())){
            response.setCode(StatusEnum.VD_ERROR_DYNAMIC_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_DYNAMIC_CODE.description());
            response.setData(false);
            return response;
        }

        return userConsumer.bindPhone(bindPhoneDV.getUserId(),bindPhoneDV.getMobile());
    }


    @Override
    public Response<Boolean> forgetDealPwdSendCode(ForgetDealPwdDV forgetDealPwdDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(forgetDealPwdDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }
        if(MyUtil.isBlank(forgetDealPwdDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_EMPTY_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }

        if(!loginService.checkKaptchaCode(forgetDealPwdDV.getMobile(),forgetDealPwdDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_ERROR_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }

        Response<String> rep = userConsumer.getMobile(forgetDealPwdDV.getUserId());
        if(!rep.getData().equals(forgetDealPwdDV.getMobile())){
            response.setCode(StatusEnum.VD_ERROR_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_MOBILE.description());
            response.setData(false);
            return response;
        }
        response = smsService.send(rep.getData(), SmsTypeEnum.DEALPWD.type(),forgetDealPwdDV.getIp());

        return response;
    }

    @Override
    public Response<Boolean> forgetDealPwd(ForgetDealPwdDV forgetDealPwdDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(forgetDealPwdDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }
        if(!smsService.checkCode(forgetDealPwdDV.getMobile(),forgetDealPwdDV.getCode())){
            response.setCode(StatusEnum.VD_ERROR_DYNAMIC_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_DYNAMIC_CODE.description());
            response.setData(false);
            return response;
        }

        return userConsumer.forgetDealPwd(forgetDealPwdDV.getUserId(),forgetDealPwdDV.getDealPwd());
    }

    @Override
    public Response<Boolean> dealPwdSendCode(DealPwdDV dealPwdDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(dealPwdDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }
        if(MyUtil.isBlank(dealPwdDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_EMPTY_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }

        if(!loginService.checkKaptchaCode(dealPwdDV.getMobile(),dealPwdDV.getKaptchaCode())){
            response.setCode(StatusEnum.VD_ERROR_KAPTCHA_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_KAPTCHA_CODE.description());
            response.setData(false);
            return response;
        }



        Response<String> rep = userConsumer.getMobile(dealPwdDV.getUserId());
        if(!rep.getData().equals(dealPwdDV.getMobile())){
            response.setCode(StatusEnum.VD_ERROR_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_MOBILE.description());
            response.setData(false);
            return response;
        }
        response = smsService.send(rep.getData(), SmsTypeEnum.DEALPWD.type(),dealPwdDV.getIp());

        return response;
    }

    @Override
    public Response<Boolean> dealPwd(DealPwdDV dealPwdDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(dealPwdDV.getMobile())){
            response.setCode(StatusEnum.VD_EMPTY_MOBILE.getCode());
            response.setMsg(StatusEnum.VD_EMPTY_MOBILE.description());
            response.setData(false);
            return response;
        }
        if(!smsService.checkCode(dealPwdDV.getMobile(),dealPwdDV.getCode())){
            response.setCode(StatusEnum.VD_ERROR_DYNAMIC_CODE.getCode());
            response.setMsg(StatusEnum.VD_ERROR_DYNAMIC_CODE.description());
            response.setData(false);
            return response;
        }

        return userConsumer.dealPwd(dealPwdDV);
    }


}
