package com.ff.kvm.service;

import com.ff.kvm.common.dv.shopping.EditItemsDV;
import com.ff.kvm.common.dv.IdsDV;
import com.ff.kvm.common.vo.Response;

public interface ShoppingCartService {
    
    Response<Boolean> editItems(EditItemsDV editItemsDV);

    Response<Boolean> delItems(IdsDV ids);

    Response<Boolean> addItems(EditItemsDV editItemsDV);
}
