package com.ff.kvm.service.impl;

import com.ff.kvm.common.dv.shopping.EditItemsDV;
import com.ff.kvm.common.dv.IdsDV;
import com.ff.kvm.common.dv.shopping.ItemsDV;
import com.ff.kvm.common.dv.shopping.ShoppingCartDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.service.RedisService;
import com.ff.kvm.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private RedisService redisService;

    @Override
    public Response<Boolean> editItems(EditItemsDV editItemsDV) {
        Response<Boolean> response = new Response<>();
        if(editItemsDV.getQuantity() == 0){
            response.setCode(StatusEnum.VD_ERROR_QUANTITY.code());
            response.setMsg(StatusEnum.VD_ERROR_QUANTITY.description());
            response.setData(false);
            return response;
        }

        ShoppingCartDV shoppingCart = redisService.getShoppingCart(editItemsDV.getUserId());
        if(shoppingCart == null){
            response.setCode(StatusEnum.DATA_ERROR.code());
            response.setMsg(StatusEnum.DATA_ERROR.description());
            response.setData(false);
            return response;
        }
        List<ItemsDV> items = editShoppingCart(shoppingCart.getItems(),editItemsDV);
        shoppingCart.setItems(items);
        redisService.putShoppingCart(shoppingCart);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    private List<ItemsDV> editShoppingCart(List<ItemsDV> items,EditItemsDV editItemsDV) {
        for (ItemsDV item:items){
            if(item.getProductId() == editItemsDV.getProductId()){
                item.setQuantity(editItemsDV.getQuantity());
            }
        }
        return items;
    }

    @Override
    public Response<Boolean> delItems(IdsDV ids) {
        Response<Boolean> response = new Response<>();
        ShoppingCartDV shoppingCart = redisService.getShoppingCart(ids.getUserId());
        if(shoppingCart == null){
            response.setCode(StatusEnum.DATA_ERROR.code());
            response.setMsg(StatusEnum.DATA_ERROR.description());
            response.setData(false);
            return response;
        }
        List<ItemsDV> items = delShoppingCart(ids.getProductIds(),shoppingCart.getItems());
        shoppingCart.setItems(items);
        redisService.putShoppingCart(shoppingCart);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Response<Boolean> addItems(EditItemsDV editItemsDV) {

        Response<Boolean> response = new Response<>();
        ShoppingCartDV shoppingCart = redisService.getShoppingCart(editItemsDV.getUserId());
        if(shoppingCart == null){
            response.setCode(StatusEnum.DATA_ERROR.code());
            response.setMsg(StatusEnum.DATA_ERROR.description());
            response.setData(false);
            return response;
        }
        response = checkMaxItems(shoppingCart.getItems());
        if(!response.getData()){
            return response;
        }
        List<ItemsDV> items = addShoppingCart(shoppingCart.getItems(),editItemsDV);
        shoppingCart.setItems(items);
        redisService.putShoppingCart(shoppingCart);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    private Response<Boolean> checkMaxItems(List<ItemsDV> items) {
        int max = Integer.valueOf(redisService.getCache(Constants.MAX_SHOPPING_ITEMS));
        Response<Boolean> response = new Response<>();
        if(items.size() > max){
            response.setCode(StatusEnum.OVER_MAX_SHOPPING_ITEMS.code());
            response.setMsg(StatusEnum.OVER_MAX_SHOPPING_ITEMS.description());
            response.setData(false);
        }
        response.setData(true);
        return response;
    }

    private List<ItemsDV> addShoppingCart(List<ItemsDV> items,EditItemsDV editItemsDV) {
        for (ItemsDV item:items){
            if(item.getProductId() == editItemsDV.getProductId()){
                item.setQuantity(item.getQuantity() + editItemsDV.getQuantity());
            }
        }
        return items;
    }

    private List<ItemsDV> delShoppingCart(int[] productIds, List<ItemsDV> items) {
        for (ItemsDV item:items){
            for(int productId : productIds){
                if(item.getProductId() == productId){
                    items.remove(item);
                }
            }

        }
        return items;
    }
}
