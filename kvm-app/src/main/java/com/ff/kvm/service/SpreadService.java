package com.ff.kvm.service;

import com.ff.kvm.common.vo.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SpreadService {
    
    Response<String> spreadQRCode(int userId);

    void spread(HttpServletRequest request, HttpServletResponse response);
}
