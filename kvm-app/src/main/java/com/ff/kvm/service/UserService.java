package com.ff.kvm.service;

import com.ff.kvm.common.dv.user.BindPhoneDV;
import com.ff.kvm.common.dv.user.DealPwdDV;
import com.ff.kvm.common.dv.user.ForgetDealPwdDV;
import com.ff.kvm.common.vo.Response;

public interface UserService {

    Response<Boolean> bindPhone(BindPhoneDV bindPhoneDV);

    Response<Boolean> bindPhoneSendCode(BindPhoneDV bindPhoneDV);

    Response<Boolean> forgetDealPwdSendCode(ForgetDealPwdDV forgetDealPwdDV);

    Response<Boolean> forgetDealPwd(ForgetDealPwdDV forgetDealPwdDV);

    Response<Boolean> dealPwdSendCode(DealPwdDV dealPwdDV);

    Response<Boolean> dealPwd(DealPwdDV dealPwdDV);
}
