package com.ff.kvm.service;

import com.ff.kvm.common.vo.Response;

import javax.servlet.http.HttpServletRequest;

public interface UtilService {

    Response<String> upload(HttpServletRequest request);
}
