package com.ff.kvm.config.filter;

import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class ParamFilter implements Filter {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)servletRequest;
//        ServletRequest requestWrapper = new ParameterRequestWrapper(request);
        log.info("token:{},path:{},params:{}", request.getHeader(Constants.TOKENID),request.getRequestURI(), MyUtil.toJSON(request.getParameterMap()));
//        log.info("token:{},path:{},params:{}", request.getHeader(Constants.TOKENID),request.getRequestURI(), (new ParameterRequestWrapper(request)).getBodyString());

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
