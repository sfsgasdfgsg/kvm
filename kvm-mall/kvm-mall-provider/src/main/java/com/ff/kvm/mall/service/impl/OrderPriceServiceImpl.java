package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.shopping.ShoppingCartDV;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.dao.mapper.OrderPriceMapper;
import com.ff.kvm.dao.model.Order;
import com.ff.kvm.dao.model.OrderPrice;
import com.ff.kvm.mall.service.OrderPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderPriceServiceImpl implements OrderPriceService {



    @Autowired
    private OrderPriceMapper orderPriceMapper;

    @Override
    public double clbOrderPrice(ShoppingCartDV shoppingCart, int[] productIds) {

        //todo
        return 0;
    }

    @Override
    public void saveOrderPrice(int orderId, double discount, double amount, Double postFee) {
        OrderPrice orderPrice = new OrderPrice();
        amount = MyUtil.float2(amount*discount);
        orderPrice.setTotal(MyUtil.float2(amount+postFee));
        orderPrice.setAmount(amount);
        orderPrice.setDiscount(discount);
        orderPrice.setPostFee(postFee);
        orderPrice.setOrderId(orderId);

        orderPrice.setCreateTime(new Date());
        orderPriceMapper.insert(orderPrice);
    }

    @Override
    public OrderPrice findOrderPriceById(Integer priceId) {
        return orderPriceMapper.selectByPrimaryKey(priceId);
    }

    @Override
    public double findOrderPriceTotalByOrderId(int orderId) {
        return orderPriceMapper.findOrderPriceTotalByOrderId(orderId);
    }

    @Override
    public OrderPrice findOrderPriceByOrderId(int orderId) {
        return orderPriceMapper.findOrderPriceByOrderId(orderId);
    }

}
