package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.account.AccountDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.AccountMapper;
import com.ff.kvm.dao.model.Account;
import com.ff.kvm.mall.service.AccountLogService;
import com.ff.kvm.mall.service.AccountService;
import com.ff.kvm.mall.service.AchieLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountLogService accountLogService;

    @Autowired
    private AchieLogService achieLogService;

    @Override
    public void createAccount(Integer id) {
        Account account = new Account();
        account.setUserId(id);
        account.setMd(0D);
        account.setGwj(0D);
        account.setCreateTime(new Date());
        accountMapper.insert(account);
    }

    @Override
    public boolean gwjPay(int userId,double total) {
        Account account = accountMapper.findAccountByUserId(userId);
        if(total > account.getGwj()){
            return false;
        }
        accountMapper.gwjPay(userId,total);
        accountLogService.gwjPay(userId,total);
        return true;
    }

    @Override
    public double getGwjByUserId(int userId) {

        return accountMapper.getGwjByUserId(userId);
    }

    @Override
    public double getMdByUserId(int userId) {

        return accountMapper.getMdByUserId(userId);
    }

    @Override
    public void income(int userId,double income, int changeType,String orderNo) {
        accountMapper.income(userId,income);
        accountLogService.income(userId,income,changeType);
    }

    @Override
    public boolean mdRecharge(int userId, double total) {
        Account account = accountMapper.findAccountByUserId(userId);
        if(total > account.getMd()){
            return false;
        }
        accountMapper.mdRecharge(userId,total);
        accountLogService.mdRecharge(userId,total);
        return true;
    }

    @Override
    public Response<AccountDV> account(int userId) {
        Response<AccountDV> response = new Response<>();
        Account account = accountMapper.findAccountByUserId(userId);
        AccountDV accountDV = new AccountDV();
        accountDV.setMd(account.getMd());
        accountDV.setGwj(account.getGwj());
        response.setData(accountDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public boolean wxzfgRecharge(int userId, double total,int changeType) {
        accountMapper.wxzfgRecharge(userId,total);
        accountLogService.wxzfgRecharge(userId,total,changeType);
        return true;
    }

    @Override
    public boolean cashMD(int userId,double total, double amount,double fee) {

        Account account = accountMapper.findAccountByUserId(userId);
        if(account.getMd() < total){
            return false;
        }
        accountMapper.mdCash(userId,total);
        accountLogService.mdCash(userId,amount,fee);
        return true;
    }

}
