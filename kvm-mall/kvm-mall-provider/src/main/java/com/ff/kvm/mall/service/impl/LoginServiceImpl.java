package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.dv.wx.WXUserInfoDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.third_util.WXOpenUtil;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.SysCodeMapper;
import com.ff.kvm.dao.model.PreRegister;
import com.ff.kvm.dao.model.User;
import com.ff.kvm.mall.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private SysCodeMapper sysCodeMapper;

    @Autowired
    private SpreadService spreadService;


    @Transactional
    public int register(LoginDV loginDV,WXUserInfoDV wxUserInfoDV) {
        PreRegister preRegister = spreadService.findByUnionId(wxUserInfoDV.getUnionid());
        String openId = null;
        if (preRegister == null) {
            loginDV.setPollCode(redisService.getCache(Constants.POLL_CODE));
        }else{
            loginDV.setPollCode(preRegister.getPollCode());
            openId = preRegister.getOpenId();
        }

        String[] gift = redisService.getCache(Constants.USER_GIFT).split("-");
        User upUser = userService.findUserByUserCode(loginDV.getPollCode());
        int floor = 1;
        int upId = 0;
        if(upUser != null){
            floor = upUser.getLv();
            upId = upUser.getId();
            userService.checkDirectGift(loginDV.getPollCode(),upId,gift,1);
        }

        User user = getUser(loginDV,floor,wxUserInfoDV,openId);
        int id = userService.createUser(user);

        userService.registerGift(id,gift,2);
        //保存到es
        userService.sendUserMsg(user,upId,user.getLv());
        return id;
    }

    private User getUser(LoginDV loginDV,int floor,WXUserInfoDV wxUserInfoDV,String openId) {

        User user = new User();
        user.setNickName(wxUserInfoDV.getNickname());
        user.setSex(wxUserInfoDV.getSex());
        user.setUnionId(wxUserInfoDV.getUnionid());
        user.setHeadUrl(wxUserInfoDV.getHeadimgurl());
        user.setOpenId(openId);
        user.setOpOpenId(wxUserInfoDV.getOpenid());
        user.setLv(0);
        user.setStatus(0);
        user.setLv(0);
        user.setTeamGiftLv(0);
        user.setTeamProductLv(0);
        user.setRoleId(0);
        user.setDirectGiftTimes(0);
        user.setPollCode(loginDV.getPollCode());
        user.setCreateTime(new Date());
        checkFloor(user,floor);

        return user;
    }

    private void checkFloor(User user,int floor) {

        user.setFloor(floor+1);
        int maxFloor = Integer.valueOf(redisService.getCache(Constants.USER_FLOOR));
        if(user.getFloor() > maxFloor){
            redisService.putCache(Constants.USER_FLOOR,user.getFloor()+"");
            sysCodeMapper.updateValue(Constants.USER_FLOOR,user.getFloor()+"");
        }
    }

    @Override
    public Response<String> login(LoginDV loginDV) {
        Response<String> response = new Response<>();
        User user = userService.findUserByMobile(loginDV.getMobile());
        if (userService.isExistsBlack(user.getId())) {
            response.setCode(StatusEnum.AUTH_BLACK.code());
            response.setMsg(StatusEnum.AUTH_BLACK.description());
            return response;
        }
        if (!user.getLoginPwd().equals(MyUtil.MD5encode(loginDV.getLoginPwd()))) {
            response.setCode(StatusEnum.USER_OR_PWD_ERROR.code());
            response.setMsg(StatusEnum.USER_OR_PWD_ERROR.description());
            return response;
        }
        String tokenId = login(user.getId());
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(tokenId);
        return response;
    }

    private String login(Integer id) {
        userService.updateLoginTime(id);
        String tokenId = MyUtil.getTokenId();
        redisService.slo(id+"",tokenId);
        return tokenId;
    }

    @Transactional
    @Override
    public Response<Boolean> forgetPwd(LoginDV loginDV) {
        Response<Boolean> response = new Response<>();
        Integer id = userService.getUserIdByMobile(loginDV.getMobile());
        if (id == null) {
            response.setCode(StatusEnum.NOT_EXIST_MOBILE.code());
            response.setMsg(StatusEnum.NOT_EXIST_MOBILE.description());
            response.setData(false);
            return response;
        }
        userService.updateLoginPwd(id, loginDV.getLoginPwd());
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Response<Boolean> spread(LoginDV loginDV) {
        Response<Boolean> response = new Response<>();
        if(MyUtil.isBlank(loginDV.getOpenId())){
            response.setCode(StatusEnum.FAIL.code());
            response.setMsg(StatusEnum.FAIL.description());
            response.setData(false);
            return response;
        }
        if(MyUtil.isBlank(loginDV.getOpenId())){
            response.setCode(StatusEnum.FAIL.code());
            response.setMsg(StatusEnum.FAIL.description());
            response.setData(false);
            return response;
        }

        Integer id = userService.getIdByUserCode(loginDV.getPollCode());
        if(id == null){
            response.setCode(StatusEnum.FAIL.code());
            response.setMsg(StatusEnum.FAIL.description());
            response.setData(false);
            return response;
        }

        return spreadService.preRegister(loginDV.getOpenId(),loginDV.getPollCode());
    }

    @Override
    public Response<String> wxLogin(LoginDV loginDV) {
        Response<String> response = new Response<>();
        WXUserInfoDV wxUserInfoDV = WXOpenUtil.getUserInfo(loginDV.getAccessToken(),loginDV.getOpenId());
        Integer id = userService.getIdByUnionId(wxUserInfoDV.getUnionid());
        if(id == null){//去注册
            id = register(loginDV,wxUserInfoDV);
        }
        String tokenId = login(id);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(tokenId);
        return response;

    }

    @Override
    public Response<String> phoneLogin(LoginDV loginDV) {
        Response<String> response = new Response<>();

        String tokenId = login(userService.getUserIdByMobile(loginDV.getMobile()));
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(tokenId);
        return response;
    }

}
