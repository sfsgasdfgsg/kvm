package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.gift.GiftBoxDV;
import com.ff.kvm.common.dv.gift.GiftBoxItemsDV;
import com.ff.kvm.common.dv.product.*;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.eunm.ProductTypeEnum;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.ProductHotService;
import com.ff.kvm.mall.service.ProductRobService;
import com.ff.kvm.mall.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductProvider {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRobService productRobService;

    @Autowired
    private ProductHotService productHotService;

    @RequestMapping("/productList")
    public Response<PageResult<ProductDV>> productList(@RequestBody ProductPageDV productPageDV){
        productPageDV.setDiscount(false);
        return productService.productList(productPageDV);
    }

    @RequestMapping("/productDetail")
    public Response<ProductDetailDV> productDetail(@RequestParam("userId") int userId,@RequestParam("id") int id){

        return productService.productDetail(userId,id);
    }

    @RequestMapping("/buyProduct")
    public Response<CheckOrderResultDV> buyProduct(@RequestBody BuyProductDV buyProductDV){

        return productService.buyProduct(buyProductDV);
    }

    @RequestMapping("/buySigninGiftProduct")
    public Response<CheckOrderResultDV> buySigninGiftProduct(@RequestBody BuyProductDV buyProductDV){

        return productService.buySigninGiftProduct(buyProductDV);
    }

    @RequestMapping("/giftBoxList")
    public Response<List<GiftBoxDV>> giftBoxList(){

        return productService.giftBoxList();
    }

    @RequestMapping("/giftBoxItems")
    public Response<List<GiftBoxItemsDV>> giftBoxItems(@RequestParam("id") int id){

        return productService.giftBoxItems(id);
    }

    @RequestMapping("/categoryList")
    public Response<List<CategoryDV>> categoryList(){
        return productService.categoryList();
    }

    @RequestMapping("/productRobList")
    public Response<PageResult<ProductActiveDV>> productRobList(@RequestBody ProductPageDV productPageDV){
        return productRobService.productRobList(productPageDV);
    }

    @RequestMapping("/productActiveList")
    public Response<PageResult<ProductActiveDV>> productActiveList(@RequestBody ProductPageDV productPageDV){
        if(productPageDV.getType() == ProductTypeEnum.HOT.type()){
            return productHotService.productHotList(productPageDV);
        }

        return productRobService.productRobList(productPageDV);
    }

    @RequestMapping("/productDiscountList")
    public Response<PageResult<ProductDV>> productDiscountList(@RequestBody ProductPageDV productPageDV){
        productPageDV.setDiscount(true);
        return productService.productList(productPageDV);
    }

}
