package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.PreRegisterMapper;
import com.ff.kvm.dao.model.PreRegister;
import com.ff.kvm.mall.service.SpreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpreadServiceImpl implements SpreadService {

    @Autowired
    private PreRegisterMapper preRegisterMapper;

    @Override
    public Response<Boolean> preRegister(String openId, String pollCode) {
        Response<Boolean> response = new Response<>();
        PreRegister preRegister = new PreRegister();
        preRegister.setOpenId(openId);
        preRegister.setPollCode(pollCode);
        preRegister.setStatus(0);
        preRegisterMapper.insert(preRegister);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public PreRegister findByOpenId(String openId) {
        return preRegisterMapper.findByOpenId(openId);
    }

    @Override
    public PreRegister findByUnionId(String unionId) {
        return preRegisterMapper.findByUnionId(unionId);
    }
}
