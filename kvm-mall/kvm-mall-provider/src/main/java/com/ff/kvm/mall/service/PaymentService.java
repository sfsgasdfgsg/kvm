package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.payment.*;
import com.ff.kvm.common.vo.Response;

import java.util.Date;

public interface PaymentService {

    Response<PaymentResult> confirmPayOrder(PayOrderDV payOrderDV);

    Response<Boolean> wxPayRT(WXPayRTDV wxPayRTDV);

    Response<Boolean> zfbPayRT(ZFBPayRTDV zfbPayRTDV);

    Date getPayTime(int orderId);
}
