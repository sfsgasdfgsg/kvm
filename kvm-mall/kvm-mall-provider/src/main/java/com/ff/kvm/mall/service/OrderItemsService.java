package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.order.OrderItemsDV;
import com.ff.kvm.common.dv.shopping.ItemsDV;
import com.ff.kvm.dao.model.OrderItems;

import java.util.List;

public interface OrderItemsService {

    void createOrderItems(int orderId, ItemsDV itemsDV,double discount,double price);

    List<OrderItems> findOrderItemsByOrderId(int orderId);

    List<OrderItemsDV> getItemsDV(int orderId);

    List<OrderItemsDV> getGiftBoxItemsDV(int orderId);

    int findGiftLvByOrderId(int orderId);

    List<OrderItems> finOrderItems(int orderId);

    double findGiftBoxPriceByOrderId(int orderId);

    OrderItems findGiftBoxByOrderId(int orderId);
}
