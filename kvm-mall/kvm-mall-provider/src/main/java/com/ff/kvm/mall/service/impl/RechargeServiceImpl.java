package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.payment.PayOrderDV;
import com.ff.kvm.common.eunm.RechargeStatusEnum;
import com.ff.kvm.dao.mapper.RechargeMapper;
import com.ff.kvm.dao.model.Recharge;
import com.ff.kvm.mall.service.RechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class RechargeServiceImpl implements RechargeService {

    @Autowired
    private RechargeMapper rechargeMapper;

    @Override
    public void saveRecharge(PayOrderDV payOrderDV,int type) {
        Recharge recharge = new Recharge();
        recharge.setRechargeNo(payOrderDV.getOrderNo());
        recharge.setTotal(payOrderDV.getTotal());
        recharge.setUserId(payOrderDV.getUserId());
        recharge.setCreateTime(new Date());
        recharge.setStatus(type);
        rechargeMapper.insert(recharge);
    }

    @Override
    public void updateRechargeStatus(String rechargeNo) {
        rechargeMapper.updateRechargeStatus(rechargeNo);
    }
}
