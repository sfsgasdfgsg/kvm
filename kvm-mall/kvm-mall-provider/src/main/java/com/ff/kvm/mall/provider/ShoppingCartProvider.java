package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.shopping.CheckOrderDV;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.shopping.ShoppingProductDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.helper.ShoppingCartHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ShoppingCartProvider {

    @Autowired
    private ShoppingCartHelper shoppingCartHelper;


    @RequestMapping("/checkOrder")
    public Response<CheckOrderResultDV> checkOrder(@RequestBody CheckOrderDV checkOrderDV){
        return shoppingCartHelper.checkOrder(checkOrderDV);
    }

    @RequestMapping("/iteamsList")
    public Response<List<ShoppingProductDV>> iteamsList(@RequestParam("userId") int userId,@RequestParam("productIds")String productIds){
        return shoppingCartHelper.iteamsList(userId,productIds);
    }
}
