package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.signin.GetGiftDV;
import com.ff.kvm.common.dv.signin.PersonalProductDV;
import com.ff.kvm.common.dv.signin.SignStatusDV;
import com.ff.kvm.common.dv.signin.SigninGiftTimeNodeDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;

public interface SignInService {

    /**
     * 获取奖品的邮费
     *
     * @param id
     * @return
     */
    Response<String> getGiftPostage(Integer id);

    /**
     * 用户领取商品
     *
     * @param userId
     * @param id
     * @return
     */
    Response<Boolean> getGift(Integer userId, Integer id);

    /**
     * 签到
     *
     * @param userId
     * @return
     */
    Response<GetGiftDV> signIn(Integer userId);

    /**
     * 签到状态
     *
     * @param userId
     * @return
     */
    Response<SignStatusDV> signinStatus(Integer userId);

    /**
     * 签到礼物领取时间节点
     *
     * @return
     */
    Response<SigninGiftTimeNodeDV> signinGiftNode();


    /**
     * 签到奖品列表
     *
     * @return
     */
    Response<PageResult<PersonalProductDV>> personalGiftList(Integer userId, Integer PageNum, Integer status);


}