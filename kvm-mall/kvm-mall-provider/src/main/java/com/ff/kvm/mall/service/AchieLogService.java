package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.team.AchieInfoDV;
import com.ff.kvm.common.dv.team.TeamPageDV;
import com.ff.kvm.common.vo.Response;

public interface AchieLogService {

    void saveLog(int downUserId, double income, int type,String orderNo,int orderId);

    Response<AchieInfoDV> achieInfo(TeamPageDV teamPageDV);
}
