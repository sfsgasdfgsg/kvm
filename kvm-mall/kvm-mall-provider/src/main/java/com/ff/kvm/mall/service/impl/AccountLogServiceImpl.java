package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.account.AccountLogDV;
import com.ff.kvm.common.dv.account.AccountPageDV;
import com.ff.kvm.common.eunm.AccountTypeEnum;
import com.ff.kvm.common.eunm.ChangeTypeEnum;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.AccountLogMapper;
import com.ff.kvm.dao.model.AccountLog;
import com.ff.kvm.mall.service.AccountLogService;
import com.ff.kvm.mall.service.AccountService;
import com.ff.kvm.mall.service.RedisService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class AccountLogServiceImpl implements AccountLogService {

    @Autowired
    private AccountLogMapper accountLogMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private AccountService accountService;

    @Override
    public Response<PageResult<AccountLogDV>> financialDetail(AccountPageDV accountPageDV) {
        Response<PageResult<AccountLogDV>> response = new Response<>();
        PageResult<AccountLogDV> result = new PageResult<>();
        List<AccountLogDV> listDV = new ArrayList<>();
        int pageSize = Integer.valueOf(redisService.getCache(Constants.PAGE_SIZE));
        Page page = PageHelper.startPage(accountPageDV.getPageNum(),pageSize);
        Map<String,Object> condition = ConvertUtil.convertMap(accountPageDV);
        List<AccountLog> list = accountLogMapper.findAccountLog(condition);
        for(AccountLog accountLog : list){
            AccountLogDV accountLogDV = new AccountLogDV();
            accountLogDV.setFee(accountLog.getFee());
            accountLogDV.setAmount(accountLog.getAmount());
            accountLogDV.setNum(accountLog.getNum());
            accountLogDV.setChangeType(accountLog.getChangeType());
            accountLogDV.setChangeTypeDes(ChangeTypeEnum.getName(accountLog.getChangeType()));
            accountLogDV.setCreateTime(MyUtil.formatDateYMDhms(accountLog.getCreateTime()));
            listDV.add(accountLogDV);
        }
        result.setData(listDV);
        result.setTotal(page.getTotal());
        result.setPageSize(pageSize);
        response.setData(result);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public void gwjPay(int userId, double total) {

        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(0D);
        accountLog.setNum(-total);
        accountLog.setAmount(accountService.getGwjByUserId(userId));
        accountLog.setCreateTime(new Date());
        accountLog.setType(AccountTypeEnum.GWJ.type());
        accountLog.setChangeType(ChangeTypeEnum.SHOPPING.type());
        accountLogMapper.insert(accountLog);
    }

    @Override
    public void income(int userId, double total,int changeType) {
        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(0D);
        accountLog.setNum(total);
        accountLog.setAmount(accountService.getMdByUserId(userId));
        accountLog.setCreateTime(new Date());
        accountLog.setType(AccountTypeEnum.MD.type());
        accountLog.setChangeType(changeType);
        accountLogMapper.insert(accountLog);
    }

    @Override
    public void mdRecharge(int userId, double total) {
        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(0D);
        accountLog.setNum(-total);
        accountLog.setAmount(accountService.getMdByUserId(userId));
        accountLog.setCreateTime(new Date());
        accountLog.setType(AccountTypeEnum.MD.type());
        accountLog.setChangeType(ChangeTypeEnum.MD_RECHARGE.type());
        accountLogMapper.insert(accountLog);

        AccountLog accountLog2 = new AccountLog();
        accountLog2.setUserId(userId);
        accountLog2.setFee(0D);
        accountLog2.setNum(total);
        accountLog2.setAmount(accountService.getGwjByUserId(userId));
        accountLog2.setCreateTime(new Date());
        accountLog2.setType(AccountTypeEnum.GWJ.type());
        accountLog2.setChangeType(ChangeTypeEnum.MD_RECHARGE.type());
        accountLogMapper.insert(accountLog2);
    }

    @Override
    public void wxzfgRecharge(int userId, double total,int changeType) {
        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(0D);
        accountLog.setNum(total);
        accountLog.setAmount(accountService.getGwjByUserId(userId));
        accountLog.setCreateTime(new Date());
        accountLog.setType(AccountTypeEnum.GWJ.type());
        accountLog.setChangeType(changeType);
        accountLogMapper.insert(accountLog);
    }

    @Override
    public void mdCash(int userId, double total, double fee) {
        AccountLog accountLog = new AccountLog();
        accountLog.setUserId(userId);
        accountLog.setFee(-fee);
        accountLog.setNum(-total);
        accountLog.setAmount(accountService.getMdByUserId(userId));
        accountLog.setCreateTime(new Date());
        accountLog.setType(AccountTypeEnum.MD.type());
        accountLog.setChangeType(ChangeTypeEnum.CASH.type());
        accountLogMapper.insert(accountLog);
    }


}
