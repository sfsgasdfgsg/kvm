package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.shopping.ShoppingCartDV;
import com.ff.kvm.dao.model.OrderPrice;

public interface OrderPriceService {

    double clbOrderPrice(ShoppingCartDV shoppingCart, int[] productIds);

    void saveOrderPrice(int orderId, double discount, double amount, Double postFee);

    OrderPrice findOrderPriceById(Integer priceId);

    double findOrderPriceTotalByOrderId(int orderId);

    OrderPrice findOrderPriceByOrderId(int orderId);
}
