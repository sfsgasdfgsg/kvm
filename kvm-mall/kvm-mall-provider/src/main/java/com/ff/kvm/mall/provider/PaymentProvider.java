package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.payment.*;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.CashService;
import com.ff.kvm.mall.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentProvider {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CashService cashService;

    @RequestMapping("/confirmPayOrder")
    public Response<PaymentResult> confirmPayOrder(@RequestBody PayOrderDV payOrderDV){

        return paymentService.confirmPayOrder(payOrderDV);
    }

    @RequestMapping("/wxPayRT")
    public Response<Boolean> wxPayRT(@RequestBody WXPayRTDV wxPayRTDV){

        return paymentService.wxPayRT(wxPayRTDV);
    }

    @RequestMapping("/zfbPayRT")
    public Response<Boolean> zfbPayRT(@RequestBody ZFBPayRTDV zfbPayRTDV){

        return paymentService.zfbPayRT(zfbPayRTDV);
    }

    @RequestMapping("/addCash")
    public Response<Boolean> addCash(@RequestBody AddCashDV addCashDV){
        return cashService.addCash(addCashDV);
    }

    @RequestMapping("/cashList")
    public Response<PageResult<CashDV>> cashList(@RequestParam("userId") int userId, @RequestParam("pageNum") int pageNum){
        return cashService.cashList(userId,pageNum);
    }

}
