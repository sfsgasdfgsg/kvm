package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.msg.Message;

public interface MsgSendService {

    void send(int type,String body);
}
