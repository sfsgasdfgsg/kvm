package com.ff.kvm.mall.config;

import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.SysCodeService;
import com.ff.kvm.mall.service.UserLvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class SysCodeRunner implements ApplicationRunner {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private SysCodeService sysCodeService;

    @Autowired
    private UserLvService userLvService;

    public void run(ApplicationArguments applicationArguments) {

        log.info("初始化系统参数...start");
        sysCodeService.loadSysCode();

        log.info("初始化礼包等级参数...start");
        userLvService.loadLv();

    }
}
