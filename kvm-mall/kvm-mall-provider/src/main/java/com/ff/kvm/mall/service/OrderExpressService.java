package com.ff.kvm.mall.service;

import com.ff.kvm.dao.model.OrderExpress;

import java.util.Date;

public interface OrderExpressService {

    OrderExpress findOrderExpressById(Integer expressId);

    int createOrderExpress(int orderId,String orderNo,int expressType);

    OrderExpress findOrderExpressByOrderId(int orderId);

    int sign(int orderId);

    Date getSendTime(int orderId);

    void updateExpressStatus(int orderId, int status);
}
