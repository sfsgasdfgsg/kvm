package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.user.HelperDV;
import com.ff.kvm.common.vo.Response;

import java.util.List;

public interface HelperCenterService {
    
    Response<List<HelperDV>> helperList();

    Response<HelperDV> helperDetail(int id);

    Response<HelperDV> about();
}
