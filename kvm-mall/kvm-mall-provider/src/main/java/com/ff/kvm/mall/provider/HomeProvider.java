package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.dv.home.HomeDV;
import com.ff.kvm.common.dv.product.CategoryDV;
import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductSpecialDV;
import com.ff.kvm.common.dv.product.ProductDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.helper.HomeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeProvider {

    @Autowired
    private HomeHelper homeHelper;

    @RequestMapping("/rollList")
    public Response<List<RollListDV>> rollList(){

        return homeHelper.rollList();
    }

    @RequestMapping("/hotList")
    public Response<List<NewsDV>> hotList(){

        return homeHelper.hotList();
    }

    @RequestMapping("/propVideo")
    public Response<String> propVideo(){
        return homeHelper.propVideo();
    }

    @RequestMapping("/category")
    public Response<List<CategoryDV>> category(){
        return homeHelper.category();
    }

    @RequestMapping("/active")
    public Response<ProductActiveDV> active(@RequestParam("type")int type){
        return homeHelper.active(type);
    }

    @RequestMapping("/special")
    public Response<List<ProductSpecialDV>> special(){
        return homeHelper.special();
    }

    @RequestMapping("/home")
    public Response<HomeDV> home(){
        return homeHelper.home();
    }

}
