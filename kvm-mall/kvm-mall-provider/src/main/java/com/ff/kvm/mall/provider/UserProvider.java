package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.user.DealPwdDV;
import com.ff.kvm.common.dv.user.UpdateDealPwdDV;
import com.ff.kvm.common.dv.user.UserInfoDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserProvider {

    @Autowired
    private UserService userService;

    //检查电话号码是否存在
    @RequestMapping("/checkMobile")
    public Response<Boolean> checkMobile(@RequestParam("mobile") String mobile){
        return userService.checkMobile(mobile);
    }

    //直推领取礼品
    @RequestMapping("/directGift")
    public Response<CheckOrderResultDV> directGift(@RequestParam("userId") int userId, @RequestParam("productId")int productId){
        return userService.directGift(userId,productId);
    }

    @RequestMapping("/bindPhone")
    public Response<Boolean> bindPhone(@RequestParam("userId") int userId,@RequestParam("mobile") String mobile){
        return userService.bindPhone(userId,mobile);
    }

    @RequestMapping("/updateDealPwd")
    public Response<Boolean> updateDealPwd(@RequestBody UpdateDealPwdDV updateDealPwdDV){
        return userService.updateDealPwd(updateDealPwdDV);
    }

    @RequestMapping("/getMobile")
    public Response<String> getMobile(@RequestParam("userId")int userId){
        return userService.getMobile(userId);
    }

    @RequestMapping("/forgetDealPwd")
    public Response<Boolean> forgetDealPwd(@RequestParam("userId") int userId,@RequestParam("dealPwd") String dealPwd){
        return userService.forgetDealPwd(userId,dealPwd);
    }

    @RequestMapping("/userInfo")
    public Response<UserInfoDV> userInfo(@RequestParam("userId")int userId){
        return userService.userInfo(userId);
    }

    @RequestMapping("/dealPwd")
    public Response<Boolean> dealPwd(@RequestBody DealPwdDV dealPwdDV){
        return userService.dealPwd(dealPwdDV);
    }

    @RequestMapping("/spreadQRCode")
    public Response<String> spreadQRCode(@RequestParam("userId")int userId){
        return userService.spreadQRCode(userId);
    }
}
