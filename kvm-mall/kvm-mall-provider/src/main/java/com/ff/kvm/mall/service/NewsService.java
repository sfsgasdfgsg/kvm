package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.NewsDetailDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.News;

import java.util.List;

public interface NewsService {

    List<News> hotList();

    List<News> findVideo();

    Response<List<NewsDV>> newsList();

    Response<NewsDetailDV> newsDetail(Integer id);
}
