package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.account.AccountDV;
import com.ff.kvm.common.vo.Response;

public interface AccountService {

    void createAccount(Integer id);

    boolean gwjPay(int userId,double total);

    double getGwjByUserId(int userId);

    double getMdByUserId(int userId);

    void income(int userId,double income, int changeType,String orderNo);

    boolean mdRecharge(int userId, double total);

    Response<AccountDV> account(int userId);

    boolean wxzfgRecharge(int userId, double total,int changeType);

    boolean  cashMD(int userId,double total, double amount,double fee);
}
