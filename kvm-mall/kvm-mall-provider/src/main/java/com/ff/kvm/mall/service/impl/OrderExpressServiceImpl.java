package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.eunm.ExpressTypeEnum;
import com.ff.kvm.common.eunm.OrderExpressChannelEnum;
import com.ff.kvm.common.eunm.OrderExpressStatusEnum;
import com.ff.kvm.dao.mapper.OrderExpressMapper;
import com.ff.kvm.dao.model.OrderExpress;
import com.ff.kvm.mall.service.OrderExpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderExpressServiceImpl implements OrderExpressService {

    @Autowired
    private OrderExpressMapper orderExpressMapper;


    @Override
    public OrderExpress findOrderExpressById(Integer expressId) {
        return orderExpressMapper.selectByPrimaryKey(expressId);
    }

    @Override
    public int createOrderExpress(int orderId,String orderNo,int expressType) {

        Date nowTime = new Date();
        OrderExpress orderExpress = new OrderExpress();

        if(expressType == ExpressTypeEnum.TAKE_SELF.type()){
            orderExpress.setExpressName(OrderExpressChannelEnum.getName(expressType));
            orderExpress.setExpressStatus(OrderExpressStatusEnum.COMPLETE.type());
            orderExpress.setSendTime(nowTime);
        }else{
            orderExpress.setExpressStatus(OrderExpressStatusEnum.INIT.type());
        }
        orderExpress.setOrderNo(orderNo);
        orderExpress.setType(expressType);
        orderExpress.setOrderId(orderId);
        orderExpress.setCreateTime(nowTime);
        orderExpressMapper.insert(orderExpress);
        return orderId;
    }

    @Override
    public OrderExpress findOrderExpressByOrderId(int orderId) {
        return orderExpressMapper.findOrderExpressByOrderId(orderId);
    }

    @Override
    public int sign(int orderId) {
        return orderExpressMapper.sign(orderId,OrderExpressStatusEnum.COMPLETE.type());
    }

    @Override
    public Date getSendTime(int orderId) {
        return orderExpressMapper.getSendTime(orderId);
    }

    @Override
    public void updateExpressStatus(int orderId, int status) {
        orderExpressMapper.updateExpressStatus(orderId,status);
    }
}
