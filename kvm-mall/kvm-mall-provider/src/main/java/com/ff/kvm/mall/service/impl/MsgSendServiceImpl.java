//package com.ff.kvm.mall.service.impl;
//
//import com.ff.kvm.common.dv.msg.Message;
//import com.ff.kvm.common.util.ConvertUtil;
//import com.ff.kvm.mall.config.amqp.RabbitMQConfig;
//import com.ff.kvm.mall.service.MsgSendService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class MsgSendServiceImpl implements MsgSendService {
//
//    private final Logger log = LoggerFactory.getLogger(getClass());
//
//    @Autowired
//    private AmqpTemplate rabbitTemplate;
//
//    @Override
//    public void send(int type,String body) {
//        Message msg = new Message();
//        msg.setType(type);
//        msg.setBody(body);
//        log.info("send msg :{}",body);
//        rabbitTemplate.convertAndSend(RabbitMQConfig.QUEUE_NAME,  ConvertUtil.toJSON(msg));
//    }
//
//}
