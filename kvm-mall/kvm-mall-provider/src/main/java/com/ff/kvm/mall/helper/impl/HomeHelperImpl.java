package com.ff.kvm.mall.helper.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.dv.home.HomeDV;
import com.ff.kvm.common.dv.home.HomeRobProductDV;
import com.ff.kvm.common.dv.home.HomeSpecialProductDV;
import com.ff.kvm.common.dv.product.CategoryDV;
import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductDV;
import com.ff.kvm.common.dv.product.ProductSpecialDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.BeanConvertUtils;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.*;
import com.ff.kvm.mall.helper.HomeHelper;
import com.ff.kvm.mall.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HomeHelperImpl implements HomeHelper {

    @Autowired
    private RollService rollService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRobService productRobService;

    @Autowired
    private ProductHotService productHotService;

    @Autowired
    private ProductPartitionService productPartitionService;

    @Autowired
    private RedisService redisService;


    @Override
    public Response<List<RollListDV>> rollList() {
        Response<List<RollListDV>> response = new Response<>();
        List<RollListDV> listDV = rollService.rollList();
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(listDV);
//        msgSendService.send(1,"test");
        return response;
    }

    @Override
    public Response<List<NewsDV>> hotList() {
        Response<List<NewsDV>> response = new Response<>();
        List<News> list = newsService.hotList();
        List<NewsDV> listDV = BeanConvertUtils.convertList(list, NewsDV.class);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(listDV);
        return response;
    }

    @Override
    public Response<String> propVideo() {
        Response<String> response = new Response<>();
        List<News> list = newsService.findVideo();
        if(CollectionUtil.isNotEmpty(list)){
            response.setData(list.get(0).getImg());
        }
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<List<CategoryDV>> category() {
        Response<List<CategoryDV>> response = new Response<>();
        List<CategoryDV> listDV = new ArrayList<>();
        double discount = Double.valueOf(redisService.getCache(Constants.GIFT_BOX_LV+0).split("-")[1]);
        List<ProductPartition> list = productPartitionService.findProductPartitionTop();
        for (ProductPartition productPartition : list){
            List<Product>  productList = productService.findProductTopByCategory(productPartition.getId());
            List<ProductDV> productDVList = new ArrayList<>();
            for (Product product : productList){
                ProductDV productDV = new ProductDV();
                productDV.setStock(product.getStock());
                productDV.setRemark(product.getRemark());
                productDV.setPrice(product.getPrice());
                productDV.setIcon(product.getIcon());
                productDV.setName(product.getName());
                productDV.setId(product.getId());
                productDV.setMonthSale(product.getMonthSale());
                productDV.setCurrentPrice(MyUtil.float2(product.getPrice()*discount));
                productDVList.add(productDV);
            }
            CategoryDV categoryDV = new CategoryDV();
            categoryDV.setId(productPartition.getId());
            categoryDV.setName(productPartition.getName());
            categoryDV.setBackGroundImg(productPartition.getBackGroundImg());
            categoryDV.setData(productDVList);
            listDV.add(categoryDV);
        }

        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<ProductActiveDV> active(int type) {
        Response<ProductActiveDV> response = new Response<>();
        ProductActiveDV productActiveDV = new ProductActiveDV();
        int productId = 0;
        if(type == 0){
            ProductRob productRob = productRobService.findTopRob();
            productActiveDV.setCurrentPrice(productRob.getPriceRob());
            productActiveDV.setProductId(productRob.getProductId());

            productActiveDV.setStartTime(MyUtil.formatDateYMDhms(productRob.getStartTime()));
            productActiveDV.setEndTime(MyUtil.formatDateYMDhms(productRob.getEndTime()));
            productId = productRob.getProductId();
        }else{
            ProductHot productHot = productHotService.findTopHot();
            productActiveDV.setCurrentPrice(productHot.getPriceHot());
            productActiveDV.setProductId(productHot.getProductId());
            productId = productHot.getProductId();
        }
        Product product = productService.findProductById(productId);
        productActiveDV.setName(product.getName());
        productActiveDV.setIcon(product.getIcon());
        productActiveDV.setPrice(product.getPrice());
        productActiveDV.setStock(product.getStock());
        productActiveDV.setRemark(product.getRemark());
        response.setData(productActiveDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<List<ProductSpecialDV>> special() {
        Response<List<ProductSpecialDV>> response = new Response<>();
        List<Product>  list = productService.findProductSpecialTop();
        List<ProductSpecialDV> listDV = new ArrayList<>();
        for(Product product : list){
            ProductSpecialDV productSpecialDV = new ProductSpecialDV();
            productSpecialDV.setIcon(product.getIcon());
            productSpecialDV.setName(product.getName());
            productSpecialDV.setId(product.getId());
            listDV.add(productSpecialDV);
        }
        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<HomeDV> home() {
        Response<HomeDV> response = new Response<>();
        HomeDV homeDV = new HomeDV();
        //轮播
        List<RollListDV> rollList = rollService.rollList();
        homeDV.setRollList(rollList);

        //抢购
        ProductRob productRob = productRobService.findTopRob();
        if(productRob != null){
            Product product = productService.findProductNormal(productRob.getProductId());
            HomeRobProductDV robProduct = new HomeRobProductDV();
            robProduct.setIcon(product.getIcon());
            robProduct.setCurrentPrice(productRob.getPriceRob());
            robProduct.setEndTime(MyUtil.formatDateYMDhms(productRob.getEndTime()));
            homeDV.setRobProduct(robProduct);
        }

        //资讯
        List<NewsDV> hostList = BeanConvertUtils.convertList(newsService.hotList(), NewsDV.class);
        homeDV.setNewsList(hostList);

        //视频
        List<News> video = newsService.findVideo();
        if(!MyUtil.isEmpty(video)){
            homeDV.setPropVideo(video.get(0).getImg());
        }

        //特色商品
        List<Product> productList = productService.findProductSpecialTop();
        List<HomeSpecialProductDV> listDV = new ArrayList<>();
        for(Product produt : productList){
            HomeSpecialProductDV specialProduct = new HomeSpecialProductDV();
            specialProduct.setIcon(produt.getIcon());
            specialProduct.setId(produt.getId());
            listDV.add(specialProduct);
        }
        homeDV.setSpecialList(listDV);
        response.setData(homeDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }


}
