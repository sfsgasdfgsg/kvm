package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.account.AccountDV;
import com.ff.kvm.common.dv.account.AccountLogDV;
import com.ff.kvm.common.dv.account.AccountPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.AccountLogService;
import com.ff.kvm.mall.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountProvider {

    @Autowired
    private AccountLogService accountLogService;

    @Autowired
    private AccountService accountService;

    @RequestMapping("/financialDetail")
    public Response<PageResult<AccountLogDV>> financialDetail(@RequestBody AccountPageDV accountPageDV){

        return accountLogService.financialDetail(accountPageDV);
    }

    @RequestMapping("/account")
    public Response<AccountDV> account(@RequestParam("userId") int userId){

        return accountService.account(userId);
    }
}
