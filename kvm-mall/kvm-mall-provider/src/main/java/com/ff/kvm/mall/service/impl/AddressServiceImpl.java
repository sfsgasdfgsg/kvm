package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.AddressDV;
import com.ff.kvm.common.dv.AreaDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.BeanConvertUtils;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.AddressMapper;
import com.ff.kvm.dao.mapper.AreaMapper;
import com.ff.kvm.dao.model.Address;
import com.ff.kvm.dao.model.Area;
import com.ff.kvm.mall.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AreaMapper areaMapper;

    @Override
    public Address findDefaultAddress(int userId) {
        return addressMapper.findDefaultAddress(userId);
    }

    @Override
    public Response<AddressDV> defaultAddress(int userId) {
        Response<AddressDV> response = new Response<>();
        Address address = addressMapper.findDefaultAddress(userId);
        AddressDV addressDV = ConvertUtil.convert(address,AddressDV.class);

        response.setData(addressDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<List<AddressDV>> userAddress(int userId) {

        Response<List<AddressDV>> response = new Response<>();
        List<Address> list = addressMapper.findAddressByUserId(userId);
        List<AddressDV> listDV = BeanConvertUtils.convertList(list,AddressDV.class);
        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Transactional
    @Override
    public Response<Boolean> newAddress(AddressDV addressDV) {
        Response<Boolean> response = new Response<>();
        Address address = new Address();



        Integer id = addressMapper.findDefaultAddressId(addressDV.getUserId());
        if(id == null){
            address.setIsDefault(1);
        }else{
            if(addressDV.getIsDefault() == 1){
                addressMapper.cancleDefaultAddress(id);
                address.setIsDefault(1);
            }else{
                address.setIsDefault(0);
            }
        }

        address.setUserId(addressDV.getUserId());
        address.setAlias(addressDV.getAlias());
        address.setArea(addressDV.getArea());
        address.setCity(addressDV.getCity());
        address.setProvince(addressDV.getProvince());
        address.setDetail(addressDV.getDetail());
        address.setName(addressDV.getName());
        address.setMobile(addressDV.getMobile());
        addressMapper.insert(address);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Transactional
    @Override
    public Response<Boolean> updateAddress(AddressDV addressDV) {
        Response<Boolean> response = new Response<>();
        Address address = addressMapper.selectByPrimaryKey(addressDV.getId());
        address.setArea(addressDV.getArea());
        address.setCity(addressDV.getCity());
        address.setProvince(addressDV.getProvince());
        address.setDetail(addressDV.getDetail());
        address.setName(addressDV.getName());
        address.setMobile(addressDV.getMobile());
        addressMapper.updateByPrimaryKey(address);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Transactional
    @Override
    public Response<Boolean> updateDefaultAddress(int userId, int id) {
        Response<Boolean> response = new Response<>();
        addressMapper.cancleDefaultAddress(addressMapper.findDefaultAddressId(userId));
        addressMapper.updateDefaultAddress(id);

        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Response<List<AreaDV>> areaList(int id) {
        Response<List<AreaDV>> response = new Response<>();
        List<AreaDV> listDV = new ArrayList<>();
        List<Area> list = areaMapper.findAreaList(id);
        for (Area area : list){
            AreaDV cityDV = new AreaDV();
            cityDV.setId(area.getId());
            cityDV.setName(area.getName());
            listDV.add(cityDV);
        }
        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<Boolean> delAddress(int userId, int id) {
        addressMapper.deleteByPrimaryKey(id);
        Response<Boolean> response = new Response<>();
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Address findAddressById(int addressId) {
        return addressMapper.selectByPrimaryKey(addressId);
    }
}
