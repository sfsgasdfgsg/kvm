package com.ff.kvm.mall.helper;

import com.ff.kvm.common.vo.Response;

public interface SpreadHelper {

    Response<Boolean> spread(String pollCode, String openId);

    String getAccessToken();
}
