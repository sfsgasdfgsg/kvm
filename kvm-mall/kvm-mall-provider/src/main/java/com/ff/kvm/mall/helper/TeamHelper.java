package com.ff.kvm.mall.helper;

import com.ff.kvm.common.dv.team.TeamDV;
import com.ff.kvm.common.vo.Response;

public interface TeamHelper {

    Response<TeamDV> team(int userId);
}
