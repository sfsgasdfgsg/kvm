package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.user.ComplaintDV;
import com.ff.kvm.common.vo.Response;

import java.util.List;

public interface ComplaintService {

    Response<Boolean> addComplaint(ComplaintDV complaintDV);

    Response<List<ComplaintDV>> complaintList(int userId);

    Response<ComplaintDV> complaintDetail(int id);
}
