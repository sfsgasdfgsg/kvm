package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginProvider {

    @Autowired
    private LoginService loginService;

    @RequestMapping("/register")
    public Response<Boolean> register(@RequestBody LoginDV loginDV){
        return null;
    }

    @RequestMapping("/wxLogin")
    public Response<String> wxLogin(@RequestBody LoginDV loginDV){
        return loginService.wxLogin(loginDV);
    }

    @RequestMapping("/phoneLogin")
    public Response<String> phoneLogin(@RequestBody LoginDV loginDV){
        return loginService.phoneLogin(loginDV);
    }

    @RequestMapping("/login")
    public Response<String> login(@RequestBody LoginDV loginDV){
        return loginService.login(loginDV);
    }


    @RequestMapping("/forgetPwd")
    public Response<Boolean> forgetPwd(@RequestBody LoginDV loginDV){
        return loginService.forgetPwd(loginDV);
    }

}
