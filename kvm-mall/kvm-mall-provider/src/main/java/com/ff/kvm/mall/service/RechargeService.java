package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.payment.PayOrderDV;

public interface RechargeService {

    void saveRecharge(PayOrderDV payOrderDV,int type);

    void updateRechargeStatus(String rechargeNo);
}
