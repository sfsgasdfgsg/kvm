package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.NewsDetailDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NewsProvider {

    @Autowired
    private NewsService newsService;

    @RequestMapping("/newsList")
    public Response<List<NewsDV>> newsList(){

        return newsService.newsList();
    }

    @RequestMapping("/newsDetail")
    public Response<NewsDetailDV> newsDetail(@RequestParam("id") Integer id){

        return newsService.newsDetail(id);
    }
}
