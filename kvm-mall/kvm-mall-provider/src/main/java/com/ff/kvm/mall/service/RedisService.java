package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.shopping.ShoppingCartDV;

public interface RedisService {

    String getCache(String key);

    void putCache(String key,String value);

    void delCache(String key);

    ShoppingCartDV getShoppingCart(int userId);

    void putShoppingCart(ShoppingCartDV shoppingCartDV);

    void slo(String userId, String tokenId);
}
