package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.team.TeamDV;
import com.ff.kvm.common.dv.team.TeamPageDV;
import com.ff.kvm.common.dv.team.TeamUserDV;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.user.DealPwdDV;
import com.ff.kvm.common.dv.user.ForgetDealPwdDV;
import com.ff.kvm.common.dv.user.UpdateDealPwdDV;
import com.ff.kvm.common.dv.user.UserInfoDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.User;


public interface UserService {

    void saveUser(User user);

    User findUserByMobile(String mobile);

    Integer getUserIdByMobile(String mobile);

    void updateLoginPwd(int id, String loginPwd);

    int createUser(User user);

    int getUserLvByUserId(int userId);

    /**
     * 是否存在于黑名单
     *
     * @param userId
     * @return
     */
    Boolean isExistsBlack(int userId);

    User findUserById(int userId);

    User findUserByUserCode(String userCode);

    int upLv(int userId,int orderId);

    Integer findUpUserLvById(int userId);

    User findUpUserById(int userId);

    int getFloorByUserCode(String userCode);

    void saveUserES(User user,int upUserId);


    Integer getIdByUserCode(String userCode);

    void updateLoginTime(int id);

    Integer getIdByOpOpenId(String openId);

    String getSpreadUrlByUserId(int userId);

    Response<Boolean> checkMobile(String mobile);

    Response<CheckOrderResultDV> directGift(int userId, int productId);

    void sendUserMsg(User user, int upId,int lv);

    Response<Boolean> bindPhone(int userId,String mobile);

    Response<PageResult<TeamUserDV>> teamUser(TeamPageDV teamPageDV);

    String getNameByUserId(int userId);

    User findNormalUser(int userId);

    Response<Boolean> updateDealPwd(UpdateDealPwdDV updateDealPwdDV);

    Response<Boolean> forgetDealPwd(int userId,String dealPwd);

    Response<String> getMobile(int userId);

    Response<UserInfoDV> userInfo(int userId);

    Response<Boolean> dealPwd(DealPwdDV dealPwdDV);

    String getUserDealPwd(int userId);


    Response<String> spreadQRCode(int userId);

    void checkDirectGift(String pollCode, int userId,String[] gift,int channle);

    Integer getUpUserId(int userId);

    Integer getIdByUnionId(String unionId);

    void registerGift(int userId,String[] gift,int channle);
}
