package com.ff.kvm.mall.service;

import com.ff.kvm.dao.model.Order;


public interface OrderPayMentService {

    String createOrderPayment(Order order);
}
