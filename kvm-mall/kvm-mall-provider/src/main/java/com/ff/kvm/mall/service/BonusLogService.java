package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.team.BonusLogDV;
import com.ff.kvm.common.dv.team.TeamInfoDV;
import com.ff.kvm.common.dv.team.TeamPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;

public interface BonusLogService {

    Response<TeamInfoDV> teamInfo(int userId);

    Response<PageResult<BonusLogDV>> bonusLog(TeamPageDV teamPageDV);
}
