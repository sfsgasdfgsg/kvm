package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.team.AchieInfoDV;
import com.ff.kvm.common.dv.team.AchieLogDV;
import com.ff.kvm.common.dv.team.TeamPageDV;
import com.ff.kvm.common.eunm.ChangeTypeEnum;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.AchieLogMapper;
import com.ff.kvm.dao.model.AchieLog;
import com.ff.kvm.mall.service.AchieLogService;
import com.ff.kvm.mall.service.ProductService;
import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.UserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AchieLogServiceImpl implements AchieLogService {

    @Autowired
    private AchieLogMapper achieLogMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;


    /**
     *
     * @param downUserId
     * @param income
     * @param type 0进货业绩，1礼包业绩
     * @param orderNo
     */
    @Override
    public void saveLog(int downUserId, double income, int type,String orderNo,int orderId) {

        Integer userId = userService.getUpUserId(downUserId);
        if(userId == null){
            return;
        }

        AchieLog achieLog = new AchieLog();
        achieLog.setUserId(userId);
        achieLog.setDownUserId(downUserId);
        achieLog.setType(type);
        achieLog.setTotal(income);
        achieLog.setOrderNo(orderNo);
        achieLog.setOrderId(orderId);
        achieLog.setCreateTime(new Date());
        achieLogMapper.insert(achieLog);
    }

    @Override
    public Response<AchieInfoDV> achieInfo(TeamPageDV teamPageDV) {
        AchieInfoDV achieInfoDV = new AchieInfoDV();
        Double productTotal = achieLogMapper.sumTotal(0);
        productTotal = productTotal == null?0:productTotal;
        Double giftBoxTotal = achieLogMapper.sumTotal(1);
        giftBoxTotal = giftBoxTotal == null?0:giftBoxTotal;
        achieInfoDV.setProductTotal(productTotal);
        achieInfoDV.setGiftBoxTotal(giftBoxTotal);
        List<AchieLogDV> listDV = new ArrayList<>();
        int pageSize = Integer.valueOf(redisService.getCache(Constants.PAGE_SIZE));
        Page page = PageHelper.startPage(teamPageDV.getPageNum(),pageSize);
        List<AchieLog> list = achieLogMapper.findAchieLog(teamPageDV.getUserId(),teamPageDV.getType());
        for(AchieLog achieLog : list){
            AchieLogDV achieLogDV = new AchieLogDV();
            achieLogDV.setName(userService.getNameByUserId(achieLog.getDownUserId()));
            if(teamPageDV.getType() == 1){
                achieLogDV.setGiftBoxName(productService.getGiftNameByOrderId(achieLog.getOrderId()));
            }
            achieLogDV.setUserId(achieLog.getDownUserId());
            achieLogDV.setOrderNo(achieLog.getOrderNo());
            achieLogDV.setTotal(achieLog.getTotal());
            achieLogDV.setCreateTime(MyUtil.formatDateYMDhms(achieLog.getCreateTime()));
            listDV.add(achieLogDV);
        }

        achieInfoDV.setTotal(page.getTotal());
        achieInfoDV.setPageSize(pageSize);
        achieInfoDV.setData(listDV);
        achieInfoDV.setData(listDV);
        Response<AchieInfoDV> response = new Response<>();
        response.setData(achieInfoDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
