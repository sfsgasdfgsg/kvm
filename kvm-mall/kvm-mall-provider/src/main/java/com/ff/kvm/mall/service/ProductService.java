package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.gift.GiftBoxDV;
import com.ff.kvm.common.dv.gift.GiftBoxItemsDV;
import com.ff.kvm.common.dv.order.OrderItemsDV;
import com.ff.kvm.common.dv.product.*;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.Product;

import java.util.List;

public interface ProductService {

    Product findProductById(int productId);

    Response<PageResult<ProductDV>> productList(ProductPageDV productPageDV);

    Response<ProductDetailDV> productDetail(int userId,int id);

    Response<CheckOrderResultDV> buyProduct(BuyProductDV buyProductDV);

    Response<CheckOrderResultDV> buySigninGiftProduct(BuyProductDV buyProductDV);

    Response<List<GiftBoxDV>> giftBoxList();

    Response<List<GiftBoxItemsDV>> giftBoxItems(int id);

    double getPriceById(Integer productId);

    List<Product>  findProductTopByType(int type);

    List<Product> findProductSpecialTop();

    List<Product> findProductTopByCategory(int category);

    Product findProductByIdForCheckOrder(int productId);


    Response<List<CategoryDV>> categoryList();

    Product findProductNormal(int productId);

    void sendGiftBoxES(int userId,int orderId, Product product);

    Product findProductNormalByOrderId(int orderId);

    String getGiftItemsByOrderId(int orderId);

    String getGiftNameByOrderId(int orderId);
}
