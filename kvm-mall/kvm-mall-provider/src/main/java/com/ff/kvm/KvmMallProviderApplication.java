package com.ff.kvm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//import org.springframework.context.annotation.ComponentScan;

@EnableZuulProxy
@MapperScan("com.ff.kvm.dao.mapper")
//@ComponentScan("com.lhxy.wk")
@EnableTransactionManagement
@EnableEurekaClient
@EnableScheduling
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class KvmMallProviderApplication extends SpringBootServletInitializer {
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
    {
        return builder.sources(new Class[] { KvmMallProviderApplication.class });
    }

    public static void main(String[] args) {
        SpringApplication.run(KvmMallProviderApplication.class, args);
    }
}
