package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.signin.GetGiftDV;
import com.ff.kvm.common.dv.signin.PersonalProductDV;
import com.ff.kvm.common.dv.signin.SignStatusDV;
import com.ff.kvm.common.dv.signin.SigninGiftTimeNodeDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SignInProvider {

    @Autowired
    private SignInService signInService;

    @RequestMapping("/getGiftPostage")
    public Response<String> getGiftPostage(Integer id) {
        return signInService.getGiftPostage(id);
    }

    @RequestMapping("/getGift")
    public Response<Boolean> getGift(Integer userId, Integer giftId) {
        return signInService.getGift(userId, giftId);
    }

    @RequestMapping("/signIn")
    public Response<GetGiftDV> signIn(Integer userId) {
        return signInService.signIn(userId);
    }

    @RequestMapping("/signinStatus")
    public Response<SignStatusDV> signinStatus(Integer userId) {
        return signInService.signinStatus(userId);
    }

    @RequestMapping("/signinGiftNode")
    public Response<SigninGiftTimeNodeDV> signinGiftNode() {
        return signInService.signinGiftNode();
    }

    @RequestMapping("/personalGiftList")
    public Response<PageResult<PersonalProductDV>> personalGiftList(Integer userId, Integer PageNum, Integer status) {
        return signInService.personalGiftList(userId, PageNum, status);
    }

}
