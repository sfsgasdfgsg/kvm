package com.ff.kvm.mall.helper;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.dv.home.HomeDV;
import com.ff.kvm.common.dv.product.CategoryDV;
import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductSpecialDV;
import com.ff.kvm.common.vo.Response;

import java.util.List;

public interface HomeHelper {

    Response<List<RollListDV>> rollList();

    Response<List<NewsDV>> hotList();

    Response<String> propVideo();

    Response<List<CategoryDV>> category();

    Response<ProductActiveDV> active(int type);

    Response<List<ProductSpecialDV>> special();

    Response<HomeDV> home();
}
