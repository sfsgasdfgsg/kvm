package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.account.AccountLogDV;
import com.ff.kvm.common.dv.account.AccountPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;

public interface AccountLogService {

    Response<PageResult<AccountLogDV>> financialDetail(AccountPageDV accountPageDV);

    void gwjPay(int userId, double total);

    void income(int userId, double total,int changeType);

    void mdRecharge(int userId, double total);

    void wxzfgRecharge(int userId, double total,int changeType);

    void mdCash(int userId, double total, double fee);
}
