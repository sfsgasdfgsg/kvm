package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.dao.mapper.RollMapper;
import com.ff.kvm.dao.model.Roll;
import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.RollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RollServiceImpl implements RollService {

    @Autowired
    private RollMapper rollMapper;

    @Autowired
    private RedisService redisService;

    @Override
    public List<RollListDV> rollList() {
        List<RollListDV> listDV ;
        String rollList = redisService.getCache(Constants.ROLL_LIST);
        if(!MyUtil.isBlank(rollList)){
            return ConvertUtil.jsonToList(rollList,RollListDV.class);
        }
        listDV = new ArrayList<>();
        List<Roll> list = rollMapper.rollList();

        for (Roll roll : list){
            RollListDV rollListDV = new RollListDV();
            rollListDV.setId(roll.getId());
            List<String> imgs = ConvertUtil.jsonToList(roll.getImg(), String.class);
            if (!MyUtil.isEmpty(imgs)) {
                rollListDV.setImg(imgs.get(0));
            }

            listDV.add(rollListDV);
        }
        redisService.putCache(Constants.ROLL_LIST,MyUtil.toJSON(listDV));
        return listDV;
    }
}
