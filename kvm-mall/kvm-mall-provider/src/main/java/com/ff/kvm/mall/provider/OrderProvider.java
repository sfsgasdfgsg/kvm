package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.order.OrderDV;
import com.ff.kvm.common.dv.order.OrderDetailDV;
import com.ff.kvm.common.dv.order.OrderExpressDV;
import com.ff.kvm.common.dv.order.OrderPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderProvider {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/orderList")
    public Response<PageResult<OrderDV>> orderList(@RequestBody OrderPageDV orderPageDV){

        return orderService.orderList(orderPageDV);
    }

    @RequestMapping("/orderDetail")
    public Response<OrderDetailDV> orderDetail(@RequestParam("orderId") int orderId){

        return orderService.orderDetail(orderId);
    }

    @RequestMapping("/expressDetail")
    public Response<OrderExpressDV> expressDetail(@RequestParam("orderId") int orderId){

        return orderService.expressDetail(orderId);
    }

    @RequestMapping("/signOrder")
    public Response<Boolean> signOrder(@RequestParam("userId") int userId, @RequestParam("orderId") int orderId){
        return orderService.signOrder(userId,orderId);
    }

    @RequestMapping("/cancleOrder")
    public Response<Boolean> cancleOrder(@RequestParam("userId") int userId, @RequestParam("orderId") int orderId){
        return orderService.cancleOrder(userId,orderId);
    }
}
