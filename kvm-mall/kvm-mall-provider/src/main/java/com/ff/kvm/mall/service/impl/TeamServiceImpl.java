package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.team.LvDV;
import com.ff.kvm.common.dv.team.TeamDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.eunm.UserLvEnum;
import com.ff.kvm.common.eunm.UserTeamGiftLvEnum;
import com.ff.kvm.common.eunm.UserTeamProductLvEnum;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.TeamMapper;
import com.ff.kvm.dao.model.Team;
import com.ff.kvm.mall.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamMapper teamMapper;


    @Override
    public Response<TeamDV> team(int userId) {
        Response<TeamDV> response = new Response<>();
        TeamDV teamDV = new TeamDV();

        Team team = teamMapper.findTeamByUserId(userId);
        if(team == null){
            response.setCode(StatusEnum.SUCCESS.code());
            response.setMsg(StatusEnum.SUCCESS.description());
            return response;
        }
        teamDV.setTotal(team.getTotal());
        teamDV.setGiftLv(team.getgLv());
        teamDV.setGiftLvName(UserTeamGiftLvEnum.getName(team.getgLv()));
        teamDV.setProductLv(team.getpLv());
        teamDV.setProductLvName(UserTeamProductLvEnum.getName(team.getpLv()));
        List<LvDV> user = new ArrayList<>();
        user.add(new LvDV(UserLvEnum.DIAMONDS.type(), UserLvEnum.DIAMONDS.getName(),team.getdLv3(),team.gettLv3()));
        user.add(new LvDV(UserLvEnum.PLATINUM.type(), UserLvEnum.PLATINUM.getName(),team.getdLv2(),team.gettLv2()));
        user.add(new LvDV(UserLvEnum.GOLD.type(), UserLvEnum.GOLD.getName(),team.getdLv1(),team.gettLv1()));
        user.add(new LvDV(UserLvEnum.VISITOR.type(), UserLvEnum.VISITOR.getName(),team.getdLv0(),team.gettLv0()));
        teamDV.setUser(user);
        List<LvDV> giftUser = new ArrayList<>();
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR7.type(), UserTeamGiftLvEnum.STAR7.getName(),team.getdGLv7(),team.gettGLv7()));
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR6.type(), UserTeamGiftLvEnum.STAR6.getName(),team.getdGLv6(),team.gettGLv6()));
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR5.type(), UserTeamGiftLvEnum.STAR5.getName(),team.getdGLv5(),team.gettGLv5()));
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR4.type(), UserTeamGiftLvEnum.STAR4.getName(),team.getdGLv4(),team.gettGLv4()));
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR3.type(), UserTeamGiftLvEnum.STAR3.getName(),team.getdGLv3(),team.gettGLv3()));
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR2.type(), UserTeamGiftLvEnum.STAR2.getName(),team.getdGLv2(),team.gettGLv2()));
        giftUser.add(new LvDV(UserTeamGiftLvEnum.STAR1.type(), UserTeamGiftLvEnum.STAR1.getName(),team.getdGLv1(),team.gettGLv1()));
        teamDV.setGiftUser(giftUser);
        List<LvDV> productUser = new ArrayList<>();
        productUser.add(new LvDV(UserTeamProductLvEnum.LV4.type(), UserTeamProductLvEnum.LV4.getName(),team.getdPLv4(),team.gettPLv4()));
        productUser.add(new LvDV(UserTeamProductLvEnum.LV3.type(), UserTeamProductLvEnum.LV3.getName(),team.getdPLv3(),team.gettPLv3()));
        productUser.add(new LvDV(UserTeamProductLvEnum.LV2.type(), UserTeamProductLvEnum.LV2.getName(),team.getdPLv2(),team.gettPLv2()));
        productUser.add(new LvDV(UserTeamProductLvEnum.LV1.type(), UserTeamProductLvEnum.LV1.getName(),team.getdPLv1(),team.gettPLv1()));
        teamDV.setProductUser(productUser);
        response.setData(teamDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
