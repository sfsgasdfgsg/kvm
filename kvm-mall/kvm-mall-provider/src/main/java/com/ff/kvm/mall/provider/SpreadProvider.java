package com.ff.kvm.mall.provider;

import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.helper.SpreadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpreadProvider {

    @Autowired
    private SpreadHelper spreadHelper;


//    public Response<String> spreadQRCode(@RequestParam("userId") int userId){
//        return spreadHelper.spreadQRCode(userId);
//    }

    @RequestMapping("/spread")
    public Response<Boolean> spread(@RequestParam("pollCode")String pollCode, @RequestParam("openId")String openId){
        return spreadHelper.spread(pollCode,openId);
    }
}
