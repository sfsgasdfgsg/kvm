package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.team.*;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.AchieLogService;
import com.ff.kvm.mall.service.BonusLogService;
import com.ff.kvm.mall.service.TeamService;
import com.ff.kvm.mall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamProvider {


    @Autowired
    private UserService userService;

    @Autowired
    private AchieLogService achieLogService;

    @Autowired
    private BonusLogService bonusLogService;

    @Autowired
    private TeamService teamService;

    @RequestMapping("/teamUser")
    public Response<PageResult<TeamUserDV>> teamUser(@RequestBody TeamPageDV teamPageDV){
        return userService.teamUser(teamPageDV);
    }

    @RequestMapping("/achieInfo")
    public Response<AchieInfoDV> achieInfo(@RequestBody TeamPageDV teamPageDV){
        return achieLogService.achieInfo(teamPageDV);
    }

    @RequestMapping("/teamInfo")
    public Response<TeamInfoDV> teamInfo(@RequestParam("userId")int userId){

        return bonusLogService.teamInfo(userId);
    }

    @RequestMapping("/bonusLog")
    public Response<PageResult<BonusLogDV>> bonusLog(@RequestBody TeamPageDV teamPageDV){
        return bonusLogService.bonusLog(teamPageDV);
    }

    @RequestMapping("/team")
    public Response<TeamDV> team(@RequestParam("userId")int userId){
        return teamService.team(userId);
    }
}
