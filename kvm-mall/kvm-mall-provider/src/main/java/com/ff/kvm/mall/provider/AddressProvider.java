package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.AddressDV;
import com.ff.kvm.common.dv.AreaDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.AddressService;
import feign.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddressProvider {

    @Autowired
    private AddressService addressService;

    @RequestMapping("/defaultAddress")
    public Response<AddressDV> defaultAddress(@Param("userId") int userId){

        return addressService.defaultAddress(userId);
    }

    @RequestMapping("/userAddress")
    public Response<List<AddressDV>> userAddress(@Param("userId") int userId){

        return addressService.userAddress(userId);
    }


    @RequestMapping("/newAddress")
    public Response<Boolean> newAddress(@RequestBody AddressDV addressDV){

        return addressService.newAddress(addressDV);
    }

    @RequestMapping("/updateAddress")
    public Response<Boolean> updateAddress(@RequestBody AddressDV addressDV){

        return addressService.updateAddress(addressDV);
    }

    @RequestMapping("/updateDefaultAddress")
    public Response<Boolean> updateDefaultAddress(@Param("userId") int userId, @Param("id") int id){

        return addressService.updateDefaultAddress(userId,id);
    }


    @RequestMapping("/delAddress")
    public Response<Boolean> delAddress(@Param("userId") int userId, @Param("id") int id){

        return addressService.delAddress(userId,id);
    }


    @RequestMapping("/areaList")
    public Response<List<AreaDV>> areaList(@RequestParam("id")int id){
        return addressService.areaList(id);
    }
}
