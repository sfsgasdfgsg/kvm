package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.vo.Response;

public interface LoginService {

//    Response<Boolean> register(LoginDV loginDV);

    Response<String> login(LoginDV loginDV);

    Response<Boolean> forgetPwd(LoginDV loginDV);

    Response<Boolean> spread(LoginDV loginDV);

    Response<String> wxLogin(LoginDV loginDV);

    Response<String> phoneLogin(LoginDV loginDV);

}
