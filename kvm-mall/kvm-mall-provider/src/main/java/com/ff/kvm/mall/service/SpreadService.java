package com.ff.kvm.mall.service;

import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.PreRegister;

public interface SpreadService {

    Response<Boolean> preRegister(String openId, String pollCode);

    PreRegister findByOpenId(String openId);

    PreRegister findByUnionId(String unionId);
}
