package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.order.OrderDV;
import com.ff.kvm.common.dv.order.OrderDetailDV;
import com.ff.kvm.common.dv.order.OrderExpressDV;
import com.ff.kvm.common.dv.order.OrderPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.Order;

public interface OrderService {

    Order createOrder(int userId,int addressId,int expressType,String remark,int type);

    Response<PageResult<OrderDV>> orderList(OrderPageDV orderPageDV);

    Response<OrderDetailDV> orderDetail(int orderId);

    Response<OrderExpressDV> expressDetail(int orderId);

    Order findOrderById(int orderId);

    Integer updateOrder(Order order);

    int getOrderIdByOrderNo(String orderNo);

    Response<Boolean> signOrder(int userId, int orderId);

    Order findOrderByNo(String orderNo);

    int updateStatus(int userId, int orderId,int status);

    Response<Boolean> cancleOrder(int userId, int orderId);
}
