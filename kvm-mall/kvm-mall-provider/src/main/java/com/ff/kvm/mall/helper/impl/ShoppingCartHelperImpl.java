package com.ff.kvm.mall.helper.impl;

import com.ff.kvm.common.dv.shopping.*;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.Order;
import com.ff.kvm.dao.model.Product;
import com.ff.kvm.mall.helper.ShoppingCartHelper;
import com.ff.kvm.mall.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShoppingCartHelperImpl implements ShoppingCartHelper {

    @Autowired
    private RedisService redisService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderItemsService orderItemsService;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderPriceService orderPriceService;

    @Autowired
    private UserService userService;

    @Transactional
    @Override
    public Response<CheckOrderResultDV> checkOrder(CheckOrderDV checkOrderDV) {
        Response<CheckOrderResultDV> response = new Response<>();
        if(MyUtil.isEmpty(checkOrderDV.getItems())){
            response.setCode(StatusEnum.FAIL.code());
            response.setMsg(StatusEnum.FAIL.description());
            return response;
        }
        for (ItemsDV itemsDV:checkOrderDV.getItems()){
            if(itemsDV.getQuantity() <= 0){
                response.setCode(StatusEnum.FAIL.code());
                response.setMsg(StatusEnum.FAIL.description());
                return response;
            }
        }



        double discount = Double.valueOf(redisService.getCache(Constants.GIFT_BOX_LV+userService.getUserLvByUserId(checkOrderDV.getUserId())).split("-")[1]);
        double amount = 0D;
        Order order = orderService.createOrder(checkOrderDV.getUserId(),checkOrderDV.getAddressId(),checkOrderDV.getExpressType(),checkOrderDV.getRemark(),0);
        double postFee = 0D;
        for (ItemsDV itemsDV:checkOrderDV.getItems()){
            Product product = productService.findProductNormal(itemsDV.getProductId());
            if(product.getStock() <= 0){
                response.setCode(StatusEnum.STOCK_NOT_ENOUGH.code());
                response.setMsg(StatusEnum.STOCK_NOT_ENOUGH.description());
                return response;
            }
            postFee = product.getPostFee() > postFee?product.getPostFee():postFee;
            orderItemsService.createOrderItems(order.getId(),itemsDV,discount,0);
            amount += itemsDV.getQuantity()*product.getPrice();
        }
//        order.setType(0);
//        orderService.updateOrder(order);

        //存储价格打折信息
        orderPriceService.saveOrderPrice(order.getId(),discount,MyUtil.float2(amount),0D);


        CheckOrderResultDV checkOrderResultDV = new CheckOrderResultDV();
        checkOrderResultDV.setOrderId(order.getId());
        checkOrderResultDV.setOrderNO(order.getNo());
        response.setData(checkOrderResultDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }


    @Override
    public Response<List<ShoppingProductDV>> iteamsList(int userId,String productIds) {

        double discount = Double.valueOf(redisService.getCache(Constants.GIFT_BOX_LV+userService.getUserLvByUserId(userId)).split("-")[1]);
        Response<List<ShoppingProductDV>> response = new Response<>();
        String[] ids = productIds.split(",");
        List<ShoppingProductDV> listDV = new ArrayList<>();
        for (String id : ids){
            if(!MyUtil.isBlank(id)){
                ShoppingProductDV shoppingProductDV = new ShoppingProductDV();
                Product product = productService.findProductById(Integer.valueOf(id));
                shoppingProductDV.setId(product.getId());
                shoppingProductDV.setName(product.getName());
                shoppingProductDV.setIcon(product.getIcon());
                shoppingProductDV.setRemark(product.getRemark());
                shoppingProductDV.setCurrentPrice(MyUtil.float2(product.getPrice()*discount));
                listDV.add(shoppingProductDV);
            }
        }
        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
