package com.ff.kvm.mall.service;

import com.ff.kvm.dao.model.ProductPartition;

import java.util.List;

public interface ProductPartitionService {

    List<ProductPartition> findProductPartition();

    List<ProductPartition> findProductPartitionTop();
}
