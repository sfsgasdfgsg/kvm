package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.util.Constants;
import com.ff.kvm.dao.mapper.UserLvMapper;
import com.ff.kvm.dao.model.UserLv;
import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.UserLvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserLvServiceImpl implements UserLvService {

    @Autowired
    private UserLvMapper userLvMapper;

    @Autowired
    private RedisService redisService;

    @Override
    public UserLv findUserLvByLv(int lv) {
        return userLvMapper.findUserLvByLv(lv);
    }

    @Override
    public void loadLv() {
        List<UserLv> list = userLvMapper.findAll();
        for (UserLv userLv : list){
            redisService.putCache(Constants.GIFT_BOX_LV+userLv.getLv(),userLv.getName()+"-"+userLv.getPoint());
        }
    }
}
