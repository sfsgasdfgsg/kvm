package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.AddressDV;
import com.ff.kvm.common.dv.AreaDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.Address;

import java.util.List;

public interface AddressService {

    Address findDefaultAddress(int userId);

    Response<AddressDV> defaultAddress(int userId);

    Response<List<AddressDV>> userAddress(int userId);

    Response<Boolean> newAddress(AddressDV addressDV);

    Response<Boolean> updateAddress(AddressDV addressDV);

    Response<Boolean> updateDefaultAddress(int userId, int id);

    Response<List<AreaDV>> areaList(int id);

    Response<Boolean> delAddress(int userId, int id);

    Address findAddressById(int addressId);
}
