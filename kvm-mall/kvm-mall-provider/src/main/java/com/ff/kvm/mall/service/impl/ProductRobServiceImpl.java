package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductDV;
import com.ff.kvm.common.dv.product.ProductPageDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.ProductRobMapper;
import com.ff.kvm.dao.model.Product;
import com.ff.kvm.dao.model.ProductRob;
import com.ff.kvm.dao.model.po.ProductActivePO;
import com.ff.kvm.mall.service.ProductRobService;
import com.ff.kvm.mall.service.RedisService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductRobServiceImpl implements ProductRobService {

    @Autowired
    private ProductRobMapper productRobMapper;

    @Autowired
    private RedisService redisService;

    @Override
    public ProductRob findTopRob() {
        return productRobMapper.findTopRob();
    }

    @Override
    public Response<PageResult<ProductActiveDV>> productRobList(ProductPageDV productPageDV) {

        Response<PageResult<ProductActiveDV>> response = new Response<>();
        PageResult<ProductActiveDV> result = new PageResult<>();
        int pageSize = Integer.valueOf(redisService.getCache(Constants.PAGE_SIZE));
        List<ProductActiveDV> listDV = new ArrayList<>();
        Page page = PageHelper.startPage(productPageDV.getPageNum(),pageSize);
        List<ProductActivePO> list = productRobMapper.findProductRobList();
        for (ProductActivePO productActivePO:list){
            ProductActiveDV productActiveDV = new ProductActiveDV();
            productActiveDV.setProductId(productActivePO.getProductId());
            productActiveDV.setName(productActivePO.getName());
            List<String> imgs = ConvertUtil.jsonToList(productActivePO.getImg(), String.class);
            if (!MyUtil.isEmpty(imgs)) {
                productActiveDV.setIcon(imgs.get(0));
            }
            productActiveDV.setPrice(productActivePO.getPrice());
            productActiveDV.setCurrentPrice(MyUtil.float2(productActivePO.getCurrentPrice()));
            productActiveDV.setRemark(productActivePO.getRemark());
            productActiveDV.setStock(productActivePO.getStock());
            productActiveDV.setStartTime(MyUtil.formatDateYMDhms(productActivePO.getStartTime()));
            productActiveDV.setEndTime(MyUtil.formatDateYMDhms(productActivePO.getEndTime()));
            listDV.add(productActiveDV);
        }
        result.setData(listDV);
        result.setPageSize(pageSize);
        result.setTotal(page.getTotal());
        response.setData(result);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public ProductRob findProductRob(int productId) {

        return productRobMapper.findProductRob(productId);
    }

    @Override
    public void updateStock(int productId,int quantity) {
        productRobMapper.updateStock(productId,quantity);
    }
}
