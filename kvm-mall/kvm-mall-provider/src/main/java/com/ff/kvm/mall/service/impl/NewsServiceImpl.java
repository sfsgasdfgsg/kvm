package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.NewsDetailDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.BeanConvertUtils;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.NewsMapper;
import com.ff.kvm.dao.model.News;
import com.ff.kvm.mall.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public List<News> hotList() {
        return newsMapper.hotList();
    }

    @Override
    public List<News> findVideo() {
        return newsMapper.findVideo();
    }

    @Override
    public Response<List<NewsDV>> newsList() {
        Response<List<NewsDV>> response = new Response<>();
        List<News> list = newsMapper.newsList();
        List<NewsDV> listDV = new ArrayList<>();
        for(News news : list){
            NewsDV newsDV = new NewsDV();
            newsDV.setId(news.getId());
            newsDV.setTitle(news.getTitle());
            newsDV.setCreateTime(MyUtil.formatDateYMDhms(news.getCreateTime()));
            listDV.add(newsDV);
        }
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(listDV);
        return response;
    }

    @Override
    public Response<NewsDetailDV> newsDetail(Integer id) {
        Response<NewsDetailDV> response = new Response<>();
        News news = newsMapper.selectByPrimaryKey(id);
        NewsDetailDV newsDetailDV = new NewsDetailDV();
        newsDetailDV.setTitle(news.getTitle());
        newsDetailDV.setContent(news.getContent());
        newsDetailDV.setCreateTime(MyUtil.formatDateYMDhms(news.getCreateTime()));
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(newsDetailDV);
        return response;
    }
}
