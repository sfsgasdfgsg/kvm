package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.payment.AddCashDV;
import com.ff.kvm.common.dv.payment.CashDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;

public interface CashService {

    Response<Boolean> addCash(AddCashDV addCashDV);

    Response<PageResult<CashDV>> cashList(int userId, int pageNum);
}
