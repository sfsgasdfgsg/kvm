//package com.ff.kvm.mall.config.amqp;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//
//@Configuration
//public class RabbitMQConfig {
//    public static final String QUEUE_NAME = "kvm-team";
//    public static final String QUEUE_EXCHANGE_NAME = "kvm-exchange";
//
//    @Bean
//    public Queue queue() {
//        // 是否持久化
//        boolean durable = true;
//        // 仅创建者可以使用的私有队列，断开后自动删除
//        boolean exclusive = false;
//        // 当所有消费客户端连接断开后，是否自动删除队列
//        boolean autoDelete = false;
//        return new Queue(QUEUE_NAME, durable, exclusive, autoDelete);
//    }
//
//    @Bean
//    public TopicExchange exchange() {
//        // 是否持久化
//        boolean durable = true;
//        // 当所有消费客户端连接断开后，是否自动删除队列
//        boolean autoDelete = false;
//        return new TopicExchange(QUEUE_EXCHANGE_NAME, durable, autoDelete);
//    }
//
//    @Bean
//    public Binding binding(Queue queue, TopicExchange exchange) {
//        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME);
//    }
//
//
//}
