package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.user.HelperDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.HelpCenterMapper;
import com.ff.kvm.dao.model.HelpCenter;
import com.ff.kvm.mall.service.HelperCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HelperCenterServiceImpl implements HelperCenterService {

    @Autowired
    private HelpCenterMapper helpCenterMapper;

    @Override
    public Response<List<HelperDV>> helperList() {
        Response<List<HelperDV>> response = new Response<>();
        List<HelperDV> listDV = new ArrayList<>();
        List<HelpCenter> list = helpCenterMapper.findHelperCenter();
        for(HelpCenter helpCenter : list){
            HelperDV helperDV = new HelperDV();
            helperDV.setId(helpCenter.getId());
            helperDV.setTitle(helpCenter.getTitle());
            listDV.add(helperDV);
        }
        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<HelperDV> helperDetail(int id) {
        Response<HelperDV> response = new Response<>();
        HelpCenter helpCenter = helpCenterMapper.selectByPrimaryKey(id);
        HelperDV helperDV = new HelperDV();
        helperDV.setId(helpCenter.getId());
        helperDV.setTitle(helpCenter.getTitle());
        helperDV.setContent(helpCenter.getContent());
        helperDV.setCreateTime(MyUtil.formatDateYMDhms(helpCenter.getCreateTime()));
        response.setData(helperDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<HelperDV> about() {
        Response<HelperDV> response = new Response<>();
        List<HelpCenter> list = helpCenterMapper.findAbout();
        HelperDV helperDV = new HelperDV();
        if(!MyUtil.isEmpty(list)){
            HelpCenter helpCenter = list.get(0);
            helperDV.setId(helpCenter.getId());
            helperDV.setTitle(helpCenter.getTitle());
            helperDV.setContent(helpCenter.getContent());
            helperDV.setCreateTime(MyUtil.formatDateYMDhms(helpCenter.getCreateTime()));
        }

        response.setData(helperDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
