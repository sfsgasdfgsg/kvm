package com.ff.kvm.mall.service;

import com.ff.kvm.dao.model.UserLv;

public interface UserLvService {

    UserLv findUserLvByLv(int lv);

    void loadLv();
}
