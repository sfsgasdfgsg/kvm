package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.ProductRob;

public interface ProductRobService {

    ProductRob findTopRob();

    Response<PageResult<ProductActiveDV>> productRobList(ProductPageDV productPageDV);

    ProductRob findProductRob(int productId);

    void updateStock(int productId,int quantity);
}
