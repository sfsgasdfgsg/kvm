package com.ff.kvm.mall.helper;

import com.ff.kvm.common.dv.payment.PayOrderDV;

public interface PayChannelHelper {

    void createPrePayOrder(PayOrderDV payOrderDV);
}
