package com.ff.kvm.mall.provider;

import com.ff.kvm.common.dv.user.ComplaintDV;
import com.ff.kvm.common.dv.user.HelperDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.service.ComplaintService;
import com.ff.kvm.mall.service.HelperCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OtherProvider {

    @Autowired
    private ComplaintService complaintService;

    @Autowired
    private HelperCenterService helperCenterService;

    @RequestMapping("/helperList")
    public Response<List<HelperDV>> helperList(){

        return helperCenterService.helperList();
    }

    @RequestMapping("/helperDetail")
    public Response<HelperDV> helperDetail(@RequestParam("id") int id){

        return helperCenterService.helperDetail(id);
    }

    @RequestMapping("/about")
    public Response<HelperDV> about(){
        return helperCenterService.about();
    }

    @RequestMapping("/addComplaint")
    public Response<Boolean> addComplaint(@RequestBody ComplaintDV complaintDV){

        return complaintService.addComplaint(complaintDV);
    }

    @RequestMapping("/complaintList")
    public Response<List<ComplaintDV>> complaintList(@RequestParam("userId")int userId){

        return complaintService.complaintList(userId);
    }

    @RequestMapping("/complaintDetail")
    public Response<ComplaintDV> complaintDetail(@RequestParam("id") int id){

        return complaintService.complaintDetail(id);
    }



}
