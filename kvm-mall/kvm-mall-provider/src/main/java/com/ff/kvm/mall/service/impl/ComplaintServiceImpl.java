package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.user.ComplaintDV;
import com.ff.kvm.common.eunm.ComplaintTypeEnum;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.ComplaintMapper;
import com.ff.kvm.dao.model.Complaint;
import com.ff.kvm.mall.service.ComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ComplaintServiceImpl implements ComplaintService {

    @Autowired
    private ComplaintMapper complaintMapper;

    @Override
    public Response<Boolean> addComplaint(ComplaintDV complaintDV) {
        Response<Boolean> response = new Response<>();
        Complaint complaint = new Complaint();
        complaint.setUserId(complaintDV.getUserId());
        complaint.setContent(complaintDV.getContent());
        complaint.setImages(complaintDV.getImages());
        complaint.setType(complaintDV.getType());
        complaint.setCreateTime(new Date());
        complaintMapper.insert(complaint);
        response.setData(true);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<List<ComplaintDV>> complaintList(int userId) {
        Response<List<ComplaintDV>> response = new Response<>();
        List<ComplaintDV> listDV = new ArrayList<>();
        List<Complaint> list = complaintMapper.findComplaint(userId);
        for (Complaint complaint : list){
            ComplaintDV complaintDV = new ComplaintDV();
            complaintDV.setId(complaint.getId());
            complaintDV.setUserId(complaint.getUserId());
            complaintDV.setTitle(complaint.getTitle());
            complaintDV.setType(complaint.getType());
            complaintDV.setTypeDes(ComplaintTypeEnum.getName(complaint.getType()));
            complaintDV.setCreateTime(MyUtil.formatDateYMDhms(complaint.getCreateTime()));
            listDV.add(complaintDV);
        }
        response.setData(listDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<ComplaintDV> complaintDetail(int id) {
        Response<ComplaintDV> response = new Response<>();
        Complaint complaint = complaintMapper.selectByPrimaryKey(id);

        ComplaintDV complaintDV = new ComplaintDV();
        complaintDV.setId(complaint.getId());
        complaintDV.setContent(complaint.getContent());
        complaintDV.setImages(complaint.getImages());
        complaintDV.setType(complaint.getType());
        complaintDV.setTitle(complaint.getTitle());
        complaintDV.setTypeDes(ComplaintTypeEnum.getName(complaint.getType()));
        complaintDV.setCreateTime(MyUtil.formatDateYMDhms(complaint.getCreateTime()));
        response.setData(complaintDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
