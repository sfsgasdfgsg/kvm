package com.ff.kvm.mall.service.impl;

import com.ff.kvm.dao.mapper.SysCodeMapper;
import com.ff.kvm.dao.model.SysCode;
import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.SysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysCodeServiceImpl implements SysCodeService {

    @Autowired
    private SysCodeMapper sysCodeMapper;

    @Autowired
    private RedisService redisService;

    @Override
    public void loadSysCode() {
        List<SysCode> list = sysCodeMapper.listSysyCode();
        for (SysCode sysCode : list)
            redisService.putCache(sysCode.getName(), sysCode.getCode());
    }
}
