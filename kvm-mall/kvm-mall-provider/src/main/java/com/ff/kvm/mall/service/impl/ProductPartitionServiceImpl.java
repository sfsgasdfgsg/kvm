package com.ff.kvm.mall.service.impl;

import com.ff.kvm.dao.mapper.ProductPartitionMapper;
import com.ff.kvm.dao.model.ProductPartition;
import com.ff.kvm.mall.service.ProductPartitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductPartitionServiceImpl implements ProductPartitionService {

    @Autowired
    private ProductPartitionMapper productPartitionMapper;

    @Override
    public List<ProductPartition> findProductPartition() {
        return productPartitionMapper.findProductPartition();
    }

    @Override
    public List<ProductPartition> findProductPartitionTop() {
        return productPartitionMapper.findProductPartitionTop();
    }
}
