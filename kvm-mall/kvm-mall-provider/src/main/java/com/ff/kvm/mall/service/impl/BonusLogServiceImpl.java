package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.team.BonusLogDV;
import com.ff.kvm.common.dv.team.TeamInfoDV;
import com.ff.kvm.common.dv.team.TeamPageDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.eunm.UserTeamGiftLvEnum;
import com.ff.kvm.common.eunm.UserTeamProductLvEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.BonusLogMapper;
import com.ff.kvm.dao.model.BonusLog;
import com.ff.kvm.dao.model.User;
import com.ff.kvm.mall.service.BonusLogService;
import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.UserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BonusLogServiceImpl implements BonusLogService {

    @Autowired
    private BonusLogMapper bonusLogMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisService redisService;

    @Override
    public Response<TeamInfoDV> teamInfo(int userId) {

        Double teamProductTotal = bonusLogMapper.sumBonusTotal(userId,0);
        teamProductTotal = teamProductTotal == null?0:teamProductTotal;

        Double teamGiftBoxTotal = bonusLogMapper.sumBonusTotal(userId,1);
        teamGiftBoxTotal = teamGiftBoxTotal == null?0:teamGiftBoxTotal;

        User user = userService.findNormalUser(userId);
        TeamInfoDV teamInfoDV = new TeamInfoDV();
        teamInfoDV.setTeamGiftBoxLv(teamInfoDV.getTeamGiftBoxLv());
        teamInfoDV.setTeamGiftBoxName(UserTeamGiftLvEnum.getName(teamInfoDV.getTeamGiftBoxLv()));
        teamInfoDV.setTeamGiftBoxTotal(teamGiftBoxTotal);

        teamInfoDV.setTeamProductLv(user.getTeamProductLv());
        teamInfoDV.setTeamProductName(UserTeamProductLvEnum.getName(user.getTeamProductLv()));
        teamInfoDV.setTeamProductTotal(teamProductTotal);

        Response<TeamInfoDV> response = new Response<>();
        response.setData(teamInfoDV);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public Response<PageResult<BonusLogDV>> bonusLog(TeamPageDV teamPageDV) {
        int pageSize = Integer.valueOf(redisService.getCache(Constants.PAGE_SIZE));
        PageResult<BonusLogDV> result = new PageResult<>();
        List<BonusLogDV> listDV = new ArrayList<>();
        Page page = PageHelper.startPage(teamPageDV.getPageNum(),pageSize);
        List<BonusLog> list = bonusLogMapper.findBonusLog(teamPageDV.getUserId(),teamPageDV.getType());
        for(BonusLog bonusLog : list){
            BonusLogDV bonusLogDV = new BonusLogDV();
            bonusLogDV.setUserId(bonusLog.getDownUserId());
            bonusLogDV.setName(userService.findNormalUser(teamPageDV.getUserId()).getName());
            bonusLogDV.setTotal(bonusLog.getMd());
            bonusLogDV.setCreateTime(MyUtil.formatDateYMDhms(bonusLog.getCreateTime()));
            listDV.add(bonusLogDV);
        }
        result.setTotal(page.getTotal());
        result.setPageSize(pageSize);
        result.setData(listDV);
        Response<PageResult<BonusLogDV>> response = new Response<>();
        response.setData(result);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
