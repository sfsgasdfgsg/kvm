package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductPageDV;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.ProductHotMapper;
import com.ff.kvm.dao.model.ProductHot;
import com.ff.kvm.dao.model.po.ProductActivePO;
import com.ff.kvm.mall.service.ProductHotService;
import com.ff.kvm.mall.service.ProductService;
import com.ff.kvm.mall.service.RedisService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductHotServiceImpl implements ProductHotService {

    @Autowired
    private ProductHotMapper productHotMapper;

    @Autowired
    private ProductService productService;

    @Override
    public ProductHot findTopHot() {
        return productHotMapper.findTopHot();
    }

    @Autowired
    private RedisService redisService;

    @Override
    public Response<PageResult<ProductActiveDV>> productHotList(ProductPageDV productPageDV) {

        Response<PageResult<ProductActiveDV>> response = new Response<>();
        PageResult<ProductActiveDV> result = new PageResult<>();
        int pageSize = Integer.valueOf(redisService.getCache(Constants.PAGE_SIZE));
        List<ProductActiveDV> listDV = new ArrayList<>();
        Page page = PageHelper.startPage(productPageDV.getPageNum(), pageSize);
        List<ProductActivePO> list = productHotMapper.findProductHotList();
        for (ProductActivePO productActivePO : list) {
            ProductActiveDV productActiveDV = new ProductActiveDV();
            productActiveDV.setProductId(productActivePO.getProductId());
            productActiveDV.setName(productActivePO.getName());
            productActiveDV.setCurrentPrice(productActivePO.getCurrentPrice());
            List<String> imgs = ConvertUtil.jsonToList(productActivePO.getImg(), String.class);
            if (!MyUtil.isEmpty(imgs)) {
                productActiveDV.setIcon(imgs.get(0));
            }
            productActiveDV.setPrice(productActivePO.getPrice());
            productActiveDV.setMonthSale(productActivePO.getMonthSale());
            productActiveDV.setRemark(productActivePO.getRemark());
            productActiveDV.setStock(productActivePO.getStock());
            listDV.add(productActiveDV);
        }
        result.setData(listDV);
        result.setPageSize(pageSize);
        result.setTotal(page.getTotal());
        response.setData(result);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }

    @Override
    public ProductHot findProductHot(int productId) {
        return productHotMapper.findProductHot(productId);
    }
}
