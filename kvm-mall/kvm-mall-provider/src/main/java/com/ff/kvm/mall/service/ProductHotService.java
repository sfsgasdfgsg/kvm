package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.model.ProductHot;

public interface ProductHotService {

    ProductHot findTopHot();

    Response<PageResult<ProductActiveDV>> productHotList(ProductPageDV productPageDV);

    ProductHot findProductHot(int productId);
}
