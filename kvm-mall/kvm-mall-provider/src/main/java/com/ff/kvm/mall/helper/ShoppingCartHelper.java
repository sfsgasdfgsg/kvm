package com.ff.kvm.mall.helper;

import com.ff.kvm.common.dv.shopping.CheckOrderDV;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.shopping.ShoppingProductDV;
import com.ff.kvm.common.vo.Response;

import java.util.List;

public interface ShoppingCartHelper {

    Response<CheckOrderResultDV> checkOrder(CheckOrderDV checkOrderDV);

    Response<List<ShoppingProductDV>> iteamsList(int userId,String productIds);
}
