package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.payment.AddCashDV;
import com.ff.kvm.common.dv.payment.CashDV;
import com.ff.kvm.common.eunm.CashStatusEnum;
import com.ff.kvm.common.eunm.StatusEnum;
import com.ff.kvm.common.util.Constants;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.dao.mapper.WithdrawalMapper;
import com.ff.kvm.dao.model.User;
import com.ff.kvm.dao.model.Withdrawal;
import com.ff.kvm.mall.service.AccountService;
import com.ff.kvm.mall.service.CashService;
import com.ff.kvm.mall.service.RedisService;
import com.ff.kvm.mall.service.UserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CashServiceImpl implements CashService {

    @Autowired
    private WithdrawalMapper withdrawalMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserService userService;

    @Transactional
    @Override
    public synchronized Response<Boolean> addCash(AddCashDV addCashDV) {
        Response<Boolean> response = new Response<>();
        if(addCashDV.getTotal() <= 0){
            response.setCode(StatusEnum.FAIL.code());
            response.setMsg(StatusEnum.FAIL.description());
            response.setData(false);
            return response;
        }
        Double point = Double.valueOf(redisService.getCache(Constants.CASH_FEE_POINT));
        double total = MyUtil.float2(addCashDV.getTotal()/(1+point));
        double fee = MyUtil.float2(addCashDV.getTotal() - total);
        User user = userService.findNormalUser(addCashDV.getUserId());
        if(!user.getDealPwd().equals(MyUtil.MD5encode(addCashDV.getDealPwd()))){
            response.setCode(StatusEnum.DEAL_PWD_ERROR.code());
            response.setMsg(StatusEnum.DEAL_PWD_ERROR.description());
            response.setData(false);
            return response;
        }

        //记录日志
        if(!accountService.cashMD(addCashDV.getUserId(),addCashDV.getTotal(),total, fee)){
            response.setCode(StatusEnum.ACCOUNT_NOT_ENOUGH.code());
            response.setMsg(StatusEnum.ACCOUNT_NOT_ENOUGH.description());
            response.setData(false);
            return response;
        }


        Withdrawal withdrawal = new Withdrawal();
        withdrawal.setUserId(addCashDV.getUserId());
        withdrawal.setMobile(user.getMobile());
        withdrawal.setFcPg(point);
        withdrawal.setFcNum(fee);
        withdrawal.setTotal(addCashDV.getTotal());
        withdrawal.setType(addCashDV.getType());
        withdrawal.setAccount(addCashDV.getAccount());
        withdrawal.setAccountName(addCashDV.getAccountName());
        withdrawal.setBankName(addCashDV.getBankName());
        withdrawal.setCreateTime(new Date());
        withdrawal.setStatus(CashStatusEnum.APPROVAL.type());
        withdrawalMapper.insert(withdrawal);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        response.setData(true);
        return response;
    }

    @Override
    public Response<PageResult<CashDV>> cashList(int userId, int pageNum) {
        Response<PageResult<CashDV>> response = new Response<>();
        PageResult<CashDV> result = new PageResult<>();
        List<CashDV> listDV  = new ArrayList<>();
        int pageSize = Integer.valueOf(redisService.getCache(Constants.PAGE_SIZE));
        Page page = PageHelper.startPage(pageNum,pageSize);
        List<Withdrawal> list = withdrawalMapper.findCashList(userId);
        for (Withdrawal withdrawal : list){
            CashDV cashDV = new CashDV();
            cashDV.setFee(withdrawal.getFcNum());
            cashDV.setTotal(withdrawal.getTotal());
            cashDV.setZfbAccount(withdrawal.getAccount());
            cashDV.setUserName(withdrawal.getAccountName());
            cashDV.setStatus(withdrawal.getStatus());
            cashDV.setStatusDes(CashStatusEnum.getName(withdrawal.getStatus()));
            cashDV.setRemark(withdrawal.getDes());
            cashDV.setCreateTime(MyUtil.formatDateYMDhms(withdrawal.getCreateTime()));
            cashDV.setCompleteTime(MyUtil.formatDateYMDhms(withdrawal.getWdTime()));
            listDV.add(cashDV);
        }
        result.setData(listDV);
        result.setTotal(page.getTotal());
        result.setPageSize(pageSize);
        response.setData(result);
        response.setCode(StatusEnum.SUCCESS.code());
        response.setMsg(StatusEnum.SUCCESS.description());
        return response;
    }
}
