package com.ff.kvm.mall.service.impl;

import com.ff.kvm.common.dv.order.OrderItemsDV;
import com.ff.kvm.common.dv.shopping.ItemsDV;
import com.ff.kvm.common.eunm.ProductTypeEnum;
import com.ff.kvm.common.util.BeanConvertUtils;
import com.ff.kvm.common.util.ConvertUtil;
import com.ff.kvm.common.util.MyUtil;
import com.ff.kvm.dao.mapper.OrderItemsMapper;
import com.ff.kvm.dao.model.OrderItems;
import com.ff.kvm.dao.model.Product;
import com.ff.kvm.mall.service.OrderItemsService;
import com.ff.kvm.mall.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderItemsServiceImpl implements OrderItemsService {

    @Autowired
    private OrderItemsMapper orderItemsMapper;

    @Autowired
    private ProductService productService;


    @Override
    public void createOrderItems(int orderId, ItemsDV itemsDV,double discount,double price) {

        OrderItems orderItems = new OrderItems();
        orderItems.setOrderId(orderId);
        orderItems.setProductId(itemsDV.getProductId());
        orderItems.setQuantity(itemsDV.getQuantity());
        Product product = productService.findProductNormal(itemsDV.getProductId());
        if(product.getType() == ProductTypeEnum.PRODUCT.type()){
            orderItems.setPrice(MyUtil.float2(product.getPrice()*discount));
        }else{
            orderItems.setPrice(price);
        }
        orderItems.setType(product.getType());
        orderItems.setCreateTime(new Date());
        orderItemsMapper.insert(orderItems);
    }

    @Override
    public List<OrderItems> findOrderItemsByOrderId(int orderId) {

        return orderItemsMapper.findOrderItemsByOrderId(orderId);
    }

    @Override
    public List<OrderItemsDV> getItemsDV(int orderId) {

        return BeanConvertUtils.convertList(orderItemsMapper.findOrderItemsByOrderId(orderId),OrderItemsDV.class);
    }

    @Override
    public List<OrderItemsDV> getGiftBoxItemsDV(int orderId) {

        String giftItems = productService.getGiftItemsByOrderId(orderId);
        List<OrderItemsDV> listDV = ConvertUtil.jsonToList(giftItems,OrderItemsDV.class);
        for (OrderItemsDV orderItemsDV : listDV){
            Product product = productService.findProductById(orderItemsDV.getProductId());
            orderItemsDV.setName(product.getName());
            orderItemsDV.setIcon(product.getIcon());
            orderItemsDV.setParam(product.getParam());
        }
        return listDV;
    }

    @Override
    public int findGiftLvByOrderId(int orderId) {

        return orderItemsMapper.findGiftLvByOrderId(orderId);
    }

    @Override
    public List<OrderItems> finOrderItems(int orderId) {
        return orderItemsMapper.finOrderItems(orderId);
    }

    @Override
    public double findGiftBoxPriceByOrderId(int orderId) {
        return orderItemsMapper.findGiftBoxPriceByOrderId(orderId);
    }

    @Override
    public OrderItems findGiftBoxByOrderId(int orderId) {
        return orderItemsMapper.findGiftBoxByOrderId(orderId);
    }

}
