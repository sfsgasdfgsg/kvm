package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.dao.model.Roll;

import java.util.List;

public interface RollService {

    List<RollListDV> rollList();
}
