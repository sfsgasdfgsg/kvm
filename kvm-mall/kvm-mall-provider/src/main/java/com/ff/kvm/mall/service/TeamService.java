package com.ff.kvm.mall.service;

import com.ff.kvm.common.dv.team.TeamDV;
import com.ff.kvm.common.vo.Response;

public interface TeamService {

    Response<TeamDV> team(int userId);
}
