package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.AddressDV;
import com.ff.kvm.common.dv.AreaDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.AddressConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AddressHystrix implements AddressConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<AddressDV> defaultAddress(int userId) {
        log.error("/kvm-mall-provider/defaultAddress  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<AddressDV>> userAddress(int valueOf) {
        log.error("/kvm-mall-provider/userAddress  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> newAddress(AddressDV addressDV) {
        log.error("/kvm-mall-provider/newAddress  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }


    @Override
    public Response<Boolean> updateAddress(AddressDV addressDV) {
        log.error("/kvm-mall-provider/updateAddress  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> updateDefaultAddress(int userId, int id) {
        log.error("/kvm-mall-provider/updateDefaultAddress  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<AreaDV>> areaList(int id) {
        log.error("/kvm-mall-provider/areaList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> delAddress(int userId, int id) {
        log.error("/kvm-mall-provider/delAddress  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
