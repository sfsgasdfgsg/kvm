package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.user.DealPwdDV;
import com.ff.kvm.common.dv.user.ForgetDealPwdDV;
import com.ff.kvm.common.dv.user.UpdateDealPwdDV;
import com.ff.kvm.common.dv.user.UserInfoDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.UserHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value="kvm-mall-provider", fallback= UserHystrix.class)
public interface UserConsumer {

    @RequestMapping("/checkMobile")
    Response<Boolean> checkMobile(@RequestParam("mobile") String mobile);

    @RequestMapping("/directGift")
    Response<CheckOrderResultDV> directGift(@RequestParam("userId") int userId, @RequestParam("productId") int productId);

    @RequestMapping("/bindPhone")
    Response<Boolean> bindPhone(@RequestParam("userId") int userId,@RequestParam("mobile") String mobile);

    @RequestMapping("/updateDealPwd")
    Response<Boolean> updateDealPwd(@RequestBody UpdateDealPwdDV updateDealPwdDV);

    @RequestMapping("/forgetDealPwd")
    Response<Boolean> forgetDealPwd(@RequestParam("userId") int userId,@RequestParam("dealPwd") String dealPwd);

    @RequestMapping("/getMobile")
    Response<String> getMobile(@RequestParam("userId")int userId);

    @RequestMapping("/userInfo")
    Response<UserInfoDV> userInfo(@RequestParam("userId")int userId);

    @RequestMapping("/dealPwd")
    Response<Boolean> dealPwd(@RequestBody DealPwdDV dealPwdDV);

    @RequestMapping("/spreadQRCode")
    Response<String> spreadQRCode(@RequestParam("userId")int userId);

}
