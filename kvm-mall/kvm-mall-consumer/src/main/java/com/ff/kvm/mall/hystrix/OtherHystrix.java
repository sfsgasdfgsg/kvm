package com.ff.kvm.mall.hystrix;


import com.ff.kvm.common.dv.user.ComplaintDV;
import com.ff.kvm.common.dv.user.HelperDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.OtherConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OtherHystrix implements OtherConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<List<HelperDV>> helperList() {
        log.error("/kvm-mall-provider/helperList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<HelperDV> helperDetail(int id) {
        log.error("/kvm-mall-provider/helperDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> addComplaint(ComplaintDV complaintDV) {
        log.error("/kvm-mall-provider/addComplaint  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<ComplaintDV>> complaintList(int userId) {
        log.error("/kvm-mall-provider/complaintList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<ComplaintDV> complaintDetail(int id) {
        log.error("/kvm-mall-provider/complaintDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<HelperDV> about() {
        log.error("/kvm-mall-provider/about  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
