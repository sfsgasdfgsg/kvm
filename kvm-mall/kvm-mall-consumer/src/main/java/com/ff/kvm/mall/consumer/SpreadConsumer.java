package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.SpreadHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "kvm-mall-provider", fallback = SpreadHystrix.class)
public interface SpreadConsumer {

    @RequestMapping("/spreadQRCode")
    Response<String> spreadQRCode(@RequestParam("userId") int userId);

    @RequestMapping("/spread")
    Response<Boolean> spread(@RequestParam("pollCode")String pollCode, @RequestParam("openId")String openId);
}
