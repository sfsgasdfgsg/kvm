package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.team.*;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.TeamHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="kvm-mall-provider", fallback= TeamHystrix.class)
public interface TeamConsumer {

    @RequestMapping("/teamUser")
    Response<PageResult<TeamUserDV>> teamUser(@RequestBody TeamPageDV teamPageDV);

    @RequestMapping("/achieInfo")
    Response<AchieInfoDV> achieInfo(@RequestBody TeamPageDV teamPageDV);

    @RequestMapping("/teamInfo")
    Response<TeamInfoDV> teamInfo(@RequestParam("userId")int userId);

    @RequestMapping("/bonusLog")
    Response<PageResult<BonusLogDV>> bonusLog(@RequestBody TeamPageDV teamPageDV);

    @RequestMapping("/team")
    Response<TeamDV> team(@RequestParam("userId")int userId);
}
