package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.order.OrderDV;
import com.ff.kvm.common.dv.order.OrderDetailDV;
import com.ff.kvm.common.dv.order.OrderExpressDV;
import com.ff.kvm.common.dv.order.OrderPageDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.OrderConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class OrderHystrix implements OrderConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<PageResult<OrderDV>> orderList(OrderPageDV orderPageDV) {
        log.error("/kvm-mall-provider/orderList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<OrderDetailDV> orderDetail(int orderId) {
        log.error("/kvm-mall-provider/orderDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<OrderExpressDV> expressDetail(int orderId) {
        log.error("/kvm-mall-provider/expressDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> signOrder(int userId, int orderId) {
        log.error("/kvm-mall-provider/expressDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> cancleOrder(int userId, int orderId) {
        log.error("/kvm-mall-provider/cancleOrder  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
