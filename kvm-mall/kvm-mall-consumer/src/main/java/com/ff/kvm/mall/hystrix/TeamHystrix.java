package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.team.*;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.TeamConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TeamHystrix implements TeamConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<PageResult<TeamUserDV>> teamUser(TeamPageDV teamPageDV) {
        log.error("/kvm-mall-provider/teamUser  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<AchieInfoDV> achieInfo(TeamPageDV teamPageDV) {
        log.error("/kvm-mall-provider/achieInfo  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<TeamInfoDV> teamInfo(int userId) {
        log.error("/kvm-mall-provider/teamInfo  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<PageResult<BonusLogDV>> bonusLog(TeamPageDV teamPageDV) {
        log.error("/kvm-mall-provider/bonusLog  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<TeamDV> team(int userId) {
        log.error("/kvm-mall-provider/team  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
