package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.shopping.CheckOrderDV;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.shopping.ShoppingProductDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.ShoppingCartHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value="kvm-mall-provider", fallback= ShoppingCartHystrix.class)
public interface ShoppingCartConsumer {

    @RequestMapping("/checkOrder")
    Response<CheckOrderResultDV> checkOrder(@RequestBody CheckOrderDV checkOrderDV);

    @RequestMapping("/iteamsList")
    Response<List<ShoppingProductDV>> iteamsList(@RequestParam("userId")int userId,@RequestParam("productIds") String productIds);
}
