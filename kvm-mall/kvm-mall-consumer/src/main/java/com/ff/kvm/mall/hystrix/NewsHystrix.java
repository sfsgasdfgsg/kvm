package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.NewsDetailDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.NewsConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NewsHystrix implements NewsConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());


    @Override
    public Response<List<NewsDV>> newsList() {
        log.error("/kvm-mall-provider/newsList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<NewsDetailDV> newsDetail(int id) {
        log.error("/kvm-mall-provider/newsDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
