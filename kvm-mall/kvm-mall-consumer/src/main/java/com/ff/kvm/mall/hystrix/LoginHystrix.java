package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.LoginConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class LoginHystrix implements LoginConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());


    @Override
    public Response<Boolean> register(LoginDV loginDV) {
        log.error("/kvm-mall-provider/register  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<String> login(LoginDV loginDV) {
        log.error("/kvm-mall-provider/login  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }


    @Override
    public Response<Boolean> forgetPwd(LoginDV loginDV) {
        log.error("/kvm-mall-provider/forgetPwd  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> spread(LoginDV loginDV) {
        log.error("/kvm-mall-provider/spread  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<String> wxLogin(LoginDV loginDV) {
        log.error("/kvm-mall-provider/wxLogin  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<String> phoneLogin(LoginDV loginDV) {
        log.error("/kvm-mall-provider/phoneLogin  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
