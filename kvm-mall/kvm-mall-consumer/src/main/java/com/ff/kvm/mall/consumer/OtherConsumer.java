package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.user.ComplaintDV;
import com.ff.kvm.common.dv.user.HelperDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.OtherHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(value="kvm-mall-provider", fallback= OtherHystrix.class)
public interface OtherConsumer {


    @RequestMapping("/helperList")
    Response<List<HelperDV>> helperList();

    @RequestMapping("/helperDetail")
    Response<HelperDV> helperDetail(@RequestParam("id") int id);

    @RequestMapping("/addComplaint")
    Response<Boolean> addComplaint(@RequestBody ComplaintDV complaintDV);

    @RequestMapping("/complaintList")
    Response<List<ComplaintDV>> complaintList(@RequestParam("userId")int userId);

    @RequestMapping("/complaintDetail")
    Response<ComplaintDV> complaintDetail(@RequestParam("id") int id);

    @RequestMapping("/about")
    Response<HelperDV> about();
}
