package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.gift.GiftBoxDV;
import com.ff.kvm.common.dv.gift.GiftBoxItemsDV;
import com.ff.kvm.common.dv.product.*;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.ProductConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductHystrix implements ProductConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<PageResult<ProductDV>> productList(ProductPageDV productPageDV) {
        log.error("/kvm-mall-provider/productList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<ProductDetailDV> productDetail(int userId,int id) {

        log.error("/kvm-mall-provider/productDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<CheckOrderResultDV> buyProduct(BuyProductDV buyProductDV) {
        log.error("/kvm-mall-provider/buyProduct  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<CheckOrderResultDV> buySigninGiftProduct(BuyProductDV buyProductDV) {
        log.error("/kvm-mall-provider/buySigninGiftProduct  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<GiftBoxDV>> giftBoxList() {
        log.error("/kvm-mall-provider/giftBoxList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<GiftBoxItemsDV>> giftBoxItems(int id) {
        log.error("/kvm-mall-provider/giftBoxItems  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }


    @Override
    public Response<List<CategoryDV>> categoryList() {
        log.error("/kvm-mall-provider/categoryList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<PageResult<ProductActiveDV>> productActiveList(ProductPageDV productPageDV) {
        log.error("/kvm-mall-provider/productActiveList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<PageResult<ProductDV>> productDiscountList(ProductPageDV productPageDV) {
        log.error("/kvm-mall-provider/productDiscountList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

}
