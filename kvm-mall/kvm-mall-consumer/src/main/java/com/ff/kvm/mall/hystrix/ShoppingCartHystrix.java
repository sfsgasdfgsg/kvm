package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.shopping.CheckOrderDV;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.shopping.ShoppingProductDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.ShoppingCartConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShoppingCartHystrix implements ShoppingCartConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<CheckOrderResultDV> checkOrder(CheckOrderDV checkOrderDV) {
        log.error("/kvm-mall-provider/checkOrder  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<ShoppingProductDV>> iteamsList(int userId,String productIds) {
        log.error("/kvm-mall-provider/iteamsList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
