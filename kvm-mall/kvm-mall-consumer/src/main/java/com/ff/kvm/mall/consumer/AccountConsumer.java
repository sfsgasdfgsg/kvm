package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.account.AccountDV;
import com.ff.kvm.common.dv.account.AccountLogDV;
import com.ff.kvm.common.dv.account.AccountPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.AccountHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="kvm-mall-provider", fallback= AccountHystrix.class)
public interface AccountConsumer {

    @RequestMapping("/financialDetail")
    Response<PageResult<AccountLogDV>> financialDetail(@RequestBody AccountPageDV accountPageDV);

    @RequestMapping("/account")
    Response<AccountDV> account(@RequestParam("userId") int userId);

}
