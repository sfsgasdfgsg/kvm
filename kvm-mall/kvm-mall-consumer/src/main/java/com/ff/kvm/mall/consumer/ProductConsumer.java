package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.gift.GiftBoxDV;
import com.ff.kvm.common.dv.gift.GiftBoxItemsDV;
import com.ff.kvm.common.dv.product.*;
import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.ProductHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "kvm-mall-provider",fallback = ProductHystrix.class)
public interface ProductConsumer {

    @RequestMapping("/productList")
    Response<PageResult<ProductDV>> productList(@RequestBody ProductPageDV productPageDV);

    @RequestMapping("/productDetail")
    Response<ProductDetailDV> productDetail(@RequestParam("userId") int userId,@RequestParam("id") int id);

    @RequestMapping("/buyProduct")
    Response<CheckOrderResultDV> buyProduct(@RequestBody BuyProductDV buyProductDV);

    @RequestMapping("/buySigninGiftProduct")
    Response<CheckOrderResultDV> buySigninGiftProduct(@RequestBody BuyProductDV buyProductDV);

    @RequestMapping("/giftBoxList")
    Response<List<GiftBoxDV>> giftBoxList();

    @RequestMapping("/giftBoxItems")
    Response<List<GiftBoxItemsDV>> giftBoxItems(@RequestParam("id") int id);

    @RequestMapping("/categoryList")
    Response<List<CategoryDV>> categoryList();

    @RequestMapping("/productActiveList")
    Response<PageResult<ProductActiveDV>> productActiveList(@RequestBody ProductPageDV productPageDV);

    @RequestMapping("/productDiscountList")
    Response<PageResult<ProductDV>> productDiscountList(@RequestBody ProductPageDV productPageDV);
}
