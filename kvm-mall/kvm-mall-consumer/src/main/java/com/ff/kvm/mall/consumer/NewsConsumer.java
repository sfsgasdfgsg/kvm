package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.NewsDetailDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.NewsHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value="kvm-mall-provider", fallback= NewsHystrix.class)
public interface NewsConsumer {

    @RequestMapping("/newsList")
    Response<List<NewsDV>> newsList();

    @RequestMapping("/newsDetail")
    Response<NewsDetailDV> newsDetail(@RequestParam("id") int id);
}
