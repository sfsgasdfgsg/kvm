package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.LoginDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.LoginHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="kvm-mall-provider", fallback= LoginHystrix.class)
public interface LoginConsumer {

    @RequestMapping("/register")
    Response<Boolean> register(@RequestBody LoginDV loginDV);

    @RequestMapping("/login")
    Response<String> login(@RequestBody LoginDV loginDV);

    @RequestMapping("/forgetPwd")
    Response<Boolean> forgetPwd(@RequestBody LoginDV loginDV);

    @RequestMapping("/spread")
    Response<Boolean> spread(@RequestBody LoginDV loginDV);

    @RequestMapping("/wxLogin")
    Response<String> wxLogin(@RequestBody LoginDV loginDV);

    @RequestMapping("/phoneLogin")
    Response<String> phoneLogin(@RequestBody LoginDV loginDV);

}
