package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.payment.*;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.PaymentConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PaymentHystrix implements PaymentConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());


    @Override
    public Response<PaymentResult> confirmPayOrder(PayOrderDV payOrderDV) {
        log.error("/kvm-mall-provider/confirmPayOrder  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> wxPayRT(WXPayRTDV wxPayRTDV) {
        log.error("/kvm-mall-provider/wxPayRT  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> zfbPayRT(ZFBPayRTDV zfbPayRTDV) {
        log.error("/kvm-mall-provider/zfbPayRT  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> addCash(AddCashDV addCashDV) {
        log.error("/kvm-mall-provider/addCash  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<PageResult<CashDV>> cashList(int userId, int pageNum) {
        log.error("/kvm-mall-provider/cashList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
