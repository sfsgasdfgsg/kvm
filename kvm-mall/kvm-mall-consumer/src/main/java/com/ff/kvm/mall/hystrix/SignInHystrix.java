package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.signin.GetGiftDV;
import com.ff.kvm.common.dv.signin.PersonalProductDV;
import com.ff.kvm.common.dv.signin.SignStatusDV;
import com.ff.kvm.common.dv.signin.SigninGiftTimeNodeDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.SignInConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class SignInHystrix implements SignInConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<String> getGiftPostage(int id) {
        log.error("/kvm-mall-provider/getGiftPostage  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response getGift(int userId, int giftId) {
        log.error("/kvm-mall-provider/getGift  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<GetGiftDV> signIn(int userId) {
        log.error("/kvm-mall-provider/signIn  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<SignStatusDV> signinStatus(int userId) {
        log.error("/kvm-mall-provider/signinStatus  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<SigninGiftTimeNodeDV> signinGiftNode() {
        log.error("/kvm-mall-provider/signinGiftNode  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }


    @Override
    public Response<PageResult<PersonalProductDV>> personalGiftList(int userId, int PageNum, int status) {
        log.error("/kvm-mall-provider/personalGiftList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
