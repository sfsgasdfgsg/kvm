package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.signin.GetGiftDV;
import com.ff.kvm.common.dv.signin.PersonalProductDV;
import com.ff.kvm.common.dv.signin.SignStatusDV;
import com.ff.kvm.common.dv.signin.SigninGiftTimeNodeDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.SignInHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "kvm-mall-provider", fallback = SignInHystrix.class)
public interface SignInConsumer {

    @RequestMapping("/getGiftPostage")
    Response<String> getGiftPostage(@RequestParam("id") int id);

    @RequestMapping("/getGift")
    Response getGift(@RequestParam("userId") int userId, @RequestParam("giftId") int giftId);

    @RequestMapping("/signIn")
    Response<GetGiftDV> signIn(@RequestParam("userId") int userId);

    @RequestMapping("/signinStatus")
    Response<SignStatusDV> signinStatus(@RequestParam("userId") int userId);

    @RequestMapping("/signinGiftNode")
    Response<SigninGiftTimeNodeDV> signinGiftNode();

    @RequestMapping("/personalGiftList")
    Response<PageResult<PersonalProductDV>> personalGiftList(@RequestParam("userId") int userId, @RequestParam("PageNum") int PageNum, @RequestParam("status") int status);

}
