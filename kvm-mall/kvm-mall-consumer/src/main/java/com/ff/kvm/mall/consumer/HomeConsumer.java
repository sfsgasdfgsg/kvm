package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.dv.home.HomeDV;
import com.ff.kvm.common.dv.product.CategoryDV;
import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductSpecialDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.HomeHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value="kvm-mall-provider", fallback= HomeHystrix.class)
public interface HomeConsumer {


    @RequestMapping("/rollList")
    Response<List<RollListDV>> rollList();

    @RequestMapping("/hotList")
    Response<List<NewsDV>> hotList();

    @RequestMapping("/propVideo")
    Response<String> propVideo();

    @RequestMapping("/category")
    Response<List<CategoryDV>> category();

    @RequestMapping("/active")
    Response<ProductActiveDV> active(@RequestParam("type")int type);

    @RequestMapping("/special")
    Response<List<ProductSpecialDV>> special();

    @RequestMapping("/home")
    Response<HomeDV> home();
}
