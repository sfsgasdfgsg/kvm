package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.account.AccountDV;
import com.ff.kvm.common.dv.account.AccountLogDV;
import com.ff.kvm.common.dv.account.AccountPageDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.AccountConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountHystrix implements AccountConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<PageResult<AccountLogDV>> financialDetail(AccountPageDV accountPageDV) {
        log.error("/kvm-mall-provider/financialDetail  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<AccountDV> account(int userId) {
        log.error("/kvm-mall-provider/account  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
