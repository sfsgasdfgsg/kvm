package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.AddressDV;
import com.ff.kvm.common.dv.AreaDV;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.AddressHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value="kvm-mall-provider", fallback= AddressHystrix.class)
public interface AddressConsumer {

    @RequestMapping("/defaultAddress")
    Response<AddressDV> defaultAddress(@RequestParam("userId") int userId);

    @RequestMapping("/userAddress")
    Response<List<AddressDV>> userAddress(@RequestParam("userId") int userId);

    @RequestMapping("/newAddress")
    Response<Boolean> newAddress(@RequestBody AddressDV addressDV);

    @RequestMapping("/updateAddress")
    Response<Boolean> updateAddress(@RequestBody AddressDV addressDV);

    @RequestMapping("/updateDefaultAddress")
    Response<Boolean> updateDefaultAddress(@RequestParam("userId") int userId, @RequestParam("id") int id);

    @RequestMapping("/areaList")
    Response<List<AreaDV>> areaList(@RequestParam("id")int id);

    @RequestMapping("/delAddress")
    Response<Boolean> delAddress(@RequestParam("userId") int userId, @RequestParam("id") int id);
}
