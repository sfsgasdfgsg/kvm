package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.shopping.CheckOrderResultDV;
import com.ff.kvm.common.dv.user.DealPwdDV;
import com.ff.kvm.common.dv.user.ForgetDealPwdDV;
import com.ff.kvm.common.dv.user.UpdateDealPwdDV;
import com.ff.kvm.common.dv.user.UserInfoDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.UserConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class UserHystrix implements UserConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<Boolean> checkMobile(String mobile) {
        log.error("/kvm-mall-provider/checkMobile  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<CheckOrderResultDV> directGift(int userId, int productId) {
        log.error("/kvm-mall-provider/directGift  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> bindPhone(int userId,String mobile) {
        log.error("/kvm-mall-provider/bindPhone  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> updateDealPwd(UpdateDealPwdDV updateDealPwdDV) {
        log.error("/kvm-mall-provider/updateDealPwd  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> forgetDealPwd(int userId, String dealPwd) {
        log.error("/kvm-mall-provider/forgetDealPwd  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<String> getMobile(int userId) {
        log.error("/kvm-mall-provider/getMobile  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<UserInfoDV> userInfo(int userId) {
        log.error("/kvm-mall-provider/userInfo  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> dealPwd(DealPwdDV dealPwdDV) {
        log.error("/kvm-mall-provider/dealPwd  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<String> spreadQRCode(int userId) {
        log.error("/kvm-mall-provider/spreadQRCode  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

}
