package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.payment.*;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.PaymentHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "kvm-mall-provider",fallback = PaymentHystrix.class)
public interface PaymentConsumer {

    @RequestMapping("/confirmPayOrder")
    Response<PaymentResult> confirmPayOrder(@RequestBody PayOrderDV payOrderDV);

    @RequestMapping("/wxPayRT")
    Response<Boolean> wxPayRT(@RequestBody WXPayRTDV wxPayRTDV);

    @RequestMapping("/zfbPayRT")
    Response<Boolean> zfbPayRT(@RequestBody ZFBPayRTDV zfbPayRTDV);

    @RequestMapping("/addCash")
    Response<Boolean> addCash(@RequestBody AddCashDV addCashDV);

    @RequestMapping("/cashList")
    Response<PageResult<CashDV>> cashList(@RequestParam("userId") int userId, @RequestParam("pageNum") int pageNum);
}
