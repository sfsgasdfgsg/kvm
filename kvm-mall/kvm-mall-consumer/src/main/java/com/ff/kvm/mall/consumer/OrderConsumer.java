package com.ff.kvm.mall.consumer;

import com.ff.kvm.common.dv.order.OrderDV;
import com.ff.kvm.common.dv.order.OrderDetailDV;
import com.ff.kvm.common.dv.order.OrderExpressDV;
import com.ff.kvm.common.dv.order.OrderPageDV;
import com.ff.kvm.common.vo.PageResult;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.hystrix.OrderHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value="kvm-mall-provider", fallback= OrderHystrix.class)
public interface OrderConsumer {

    @RequestMapping("/orderList")
    Response<PageResult<OrderDV>> orderList(@RequestBody OrderPageDV orderPageDV);

    @RequestMapping("/orderDetail")
    Response<OrderDetailDV> orderDetail(@RequestParam("orderId") int orderId);

    @RequestMapping("/expressDetail")
    Response<OrderExpressDV> expressDetail(@RequestParam("orderId") int orderId);

    @RequestMapping("/signOrder")
    Response<Boolean> signOrder(@RequestParam("userId") int userId, @RequestParam("orderId") int orderId);

    @RequestMapping("/cancleOrder")
    Response<Boolean> cancleOrder(@RequestParam("userId") int userId, @RequestParam("orderId") int orderId);
}
