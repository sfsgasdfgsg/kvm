package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.dv.NewsDV;
import com.ff.kvm.common.dv.RollListDV;
import com.ff.kvm.common.dv.home.HomeDV;
import com.ff.kvm.common.dv.product.CategoryDV;
import com.ff.kvm.common.dv.product.ProductActiveDV;
import com.ff.kvm.common.dv.product.ProductSpecialDV;
import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.HomeConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HomeHystrix implements HomeConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());


    @Override
    public Response<List<RollListDV>> rollList() {
        log.error("/kvm-mall-provider/rollList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<NewsDV>> hotList() {
        log.error("/kvm-mall-provider/hotList  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<String> propVideo() {
        log.error("/kvm-mall-provider/propVideo  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<CategoryDV>> category() {
        log.error("/kvm-mall-provider/category  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<ProductActiveDV> active(int type) {
        log.error("/kvm-mall-provider/active  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<List<ProductSpecialDV>> special() {
        log.error("/kvm-mall-provider/special  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<HomeDV> home() {
        log.error("/kvm-mall-provider/home  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

}
