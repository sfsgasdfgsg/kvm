package com.ff.kvm.mall.hystrix;

import com.ff.kvm.common.util.ResponseUtil;
import com.ff.kvm.common.vo.Response;
import com.ff.kvm.mall.consumer.SpreadConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SpreadHystrix implements SpreadConsumer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Response<String> spreadQRCode(int userId) {
        log.error("/kvm-mall-provider/spreadQRCode  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }

    @Override
    public Response<Boolean> spread(String pollCode, String openId) {
        log.error("/kvm-mall-provider/spread  SERVICE UNAVAILABLE!!! ");
        return ResponseUtil.fail();
    }
}
