package com.ff.kvm.dao.model.po;

/**
 * 礼物发放时间节点
 */
public class SigninGiftNodePO {

    private Integer signinNo;
    private Integer giftProductId;

    public Integer getSigninNo() {
        return signinNo;
    }

    public void setSigninNo(Integer signinNo) {
        this.signinNo = signinNo;
    }

    public Integer getGiftProductId() {
        return giftProductId;
    }

    public void setGiftProductId(Integer giftProductId) {
        this.giftProductId = giftProductId;
    }
}
