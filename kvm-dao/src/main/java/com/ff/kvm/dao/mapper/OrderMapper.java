package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    List<Order> findOrderListByStatus(@Param("userId") Integer userId, @Param("status") Integer status);

    Integer getOrderIdByOrderNo(@Param("no") String no);

    int updateOrderStatus(@Param("userId")int userId, @Param("id")int id, @Param("status") int status);

    Order findOrderByNo(@Param("no")String no);
}