package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.ProductPartition;

import java.util.List;

public interface ProductPartitionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductPartition record);

    int insertSelective(ProductPartition record);

    ProductPartition selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductPartition record);

    int updateByPrimaryKey(ProductPartition record);

    List<ProductPartition> findProductPartition();

    List<ProductPartition> findProductPartitionTop();
}