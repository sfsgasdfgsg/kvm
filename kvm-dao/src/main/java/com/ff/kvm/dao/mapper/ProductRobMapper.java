package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.ProductRob;
import com.ff.kvm.dao.model.po.ProductActivePO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductRobMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductRob record);

    int insertSelective(ProductRob record);

    ProductRob selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductRob record);

    int updateByPrimaryKey(ProductRob record);

    ProductRob findTopRob();

    List<ProductActivePO> findProductRobList();

    ProductRob findProductRob(@Param("productId") int productId);

    int updateStock(@Param("productId") int productId,@Param("quantity") int quantity);
}