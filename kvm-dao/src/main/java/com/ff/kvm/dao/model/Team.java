package com.ff.kvm.dao.model;

import java.io.Serializable;
import java.util.Date;

public class Team implements Serializable {
    private static final long serialVersionUID = -1707483609595583875L;
    private Integer id;

    private Integer upUserId;

    private Integer userId;

    private Integer lv;

    private Integer gLv;

    private Integer pLv;

    private Integer dLv0;

    private Integer dLv1;

    private Integer dLv2;

    private Integer dLv3;

    private Integer tLv0;

    private Integer tLv1;

    private Integer tLv2;

    private Integer tLv3;


    private Integer dGLv1;

    private Integer dGLv2;

    private Integer dGLv3;

    private Integer dGLv4;

    private Integer dGLv5;

    private Integer dGLv6;

    private Integer dGLv7;


    private Integer tGLv1;

    private Integer tGLv2;

    private Integer tGLv3;

    private Integer tGLv4;

    private Integer tGLv5;

    private Integer tGLv6;

    private Integer tGLv7;

    private Integer dPLv1;

    private Integer dPLv2;

    private Integer dPLv3;

    private Integer dPLv4;

    private Integer tPLv1;

    private Integer tPLv2;

    private Integer tPLv3;

    private Integer tPLv4;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLv() {
        return lv;
    }

    public void setLv(Integer lv) {
        this.lv = lv;
    }

    public Integer getgLv() {
        return gLv;
    }

    public void setgLv(Integer gLv) {
        this.gLv = gLv;
    }

    public Integer getpLv() {
        return pLv;
    }

    public void setpLv(Integer pLv) {
        this.pLv = pLv;
    }

    public Integer getdLv0() {
        return dLv0;
    }

    public void setdLv0(Integer dLv0) {
        this.dLv0 = dLv0;
    }

    public Integer getdLv1() {
        return dLv1;
    }

    public void setdLv1(Integer dLv1) {
        this.dLv1 = dLv1;
    }

    public Integer getdLv2() {
        return dLv2;
    }

    public void setdLv2(Integer dLv2) {
        this.dLv2 = dLv2;
    }

    public Integer getdLv3() {
        return dLv3;
    }

    public void setdLv3(Integer dLv3) {
        this.dLv3 = dLv3;
    }

    public Integer gettLv0() {
        return tLv0;
    }

    public void settLv0(Integer tLv0) {
        this.tLv0 = tLv0;
    }

    public Integer gettLv1() {
        return tLv1;
    }

    public void settLv1(Integer tLv1) {
        this.tLv1 = tLv1;
    }

    public Integer gettLv2() {
        return tLv2;
    }

    public void settLv2(Integer tLv2) {
        this.tLv2 = tLv2;
    }

    public Integer gettLv3() {
        return tLv3;
    }

    public void settLv3(Integer tLv3) {
        this.tLv3 = tLv3;
    }


    public Integer getdGLv1() {
        return dGLv1;
    }

    public void setdGLv1(Integer dGLv1) {
        this.dGLv1 = dGLv1;
    }

    public Integer getdGLv2() {
        return dGLv2;
    }

    public void setdGLv2(Integer dGLv2) {
        this.dGLv2 = dGLv2;
    }

    public Integer getdGLv3() {
        return dGLv3;
    }

    public void setdGLv3(Integer dGLv3) {
        this.dGLv3 = dGLv3;
    }

    public Integer getdGLv4() {
        return dGLv4;
    }

    public void setdGLv4(Integer dGLv4) {
        this.dGLv4 = dGLv4;
    }

    public Integer getdGLv5() {
        return dGLv5;
    }

    public void setdGLv5(Integer dGLv5) {
        this.dGLv5 = dGLv5;
    }

    public Integer getdGLv6() {
        return dGLv6;
    }

    public void setdGLv6(Integer dGLv6) {
        this.dGLv6 = dGLv6;
    }

    public Integer getdGLv7() {
        return dGLv7;
    }

    public void setdGLv7(Integer dGLv7) {
        this.dGLv7 = dGLv7;
    }

    public Integer gettGLv1() {
        return tGLv1;
    }

    public void settGLv1(Integer tGLv1) {
        this.tGLv1 = tGLv1;
    }

    public Integer gettGLv2() {
        return tGLv2;
    }

    public void settGLv2(Integer tGLv2) {
        this.tGLv2 = tGLv2;
    }

    public Integer gettGLv3() {
        return tGLv3;
    }

    public void settGLv3(Integer tGLv3) {
        this.tGLv3 = tGLv3;
    }

    public Integer gettGLv4() {
        return tGLv4;
    }

    public void settGLv4(Integer tGLv4) {
        this.tGLv4 = tGLv4;
    }

    public Integer gettGLv5() {
        return tGLv5;
    }

    public void settGLv5(Integer tGLv5) {
        this.tGLv5 = tGLv5;
    }

    public Integer gettGLv6() {
        return tGLv6;
    }

    public void settGLv6(Integer tGLv6) {
        this.tGLv6 = tGLv6;
    }

    public Integer gettGLv7() {
        return tGLv7;
    }

    public void settGLv7(Integer tGLv7) {
        this.tGLv7 = tGLv7;
    }


    public Integer getdPLv1() {
        return dPLv1;
    }

    public void setdPLv1(Integer dPLv1) {
        this.dPLv1 = dPLv1;
    }

    public Integer getdPLv2() {
        return dPLv2;
    }

    public void setdPLv2(Integer dPLv2) {
        this.dPLv2 = dPLv2;
    }

    public Integer getdPLv3() {
        return dPLv3;
    }

    public void setdPLv3(Integer dPLv3) {
        this.dPLv3 = dPLv3;
    }

    public Integer getdPLv4() {
        return dPLv4;
    }

    public void setdPLv4(Integer dPLv4) {
        this.dPLv4 = dPLv4;
    }


    public Integer gettPLv1() {
        return tPLv1;
    }

    public void settPLv1(Integer tPLv1) {
        this.tPLv1 = tPLv1;
    }

    public Integer gettPLv2() {
        return tPLv2;
    }

    public void settPLv2(Integer tPLv2) {
        this.tPLv2 = tPLv2;
    }

    public Integer gettPLv3() {
        return tPLv3;
    }

    public void settPLv3(Integer tPLv3) {
        this.tPLv3 = tPLv3;
    }

    public Integer gettPLv4() {
        return tPLv4;
    }

    public void settPLv4(Integer tPLv4) {
        this.tPLv4 = tPLv4;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getTotal(){
        return dLv0+dLv1+dLv2+dLv3+tLv0+tLv1+tLv2+tLv3;
    }

    public Integer getUpUserId() {
        return upUserId;
    }

    public void setUpUserId(Integer upUserId) {
        this.upUserId = upUserId;
    }
}