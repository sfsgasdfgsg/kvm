package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.ProductHot;
import com.ff.kvm.dao.model.po.ProductActivePO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductHotMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ProductHot record);

    int insertSelective(ProductHot record);

    ProductHot selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductHot record);

    int updateByPrimaryKey(ProductHot record);

    ProductHot findTopHot();

    List<ProductActivePO> findProductHotList();

    ProductHot findProductHot(@Param("productId") int productId);
}