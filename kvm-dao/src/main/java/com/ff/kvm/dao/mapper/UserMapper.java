package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.User;
import com.ff.kvm.dao.model.po.UpdateLoginTimePO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User findUserByMobile(@Param("mobile") String mobile);

    Integer getUserIdByMobile(@Param("mobile") String mobile);

    Integer updateLoginPwd(@Param("id") int id, @Param("loginPwd") String loginPwd);

    int getUserLvByUserId(@Param("id")int id);

    int isExistsBlack(@Param("userId")int userId);

    User findUserByUserCode(@Param("userCode")String userCode);

    int upLv(@Param("id")int id, @Param("lv")int lv);

    Integer findUpUserLvById(@Param("id")int id);

    User findUpUserById(@Param("id")int id);

    int getFloorByUserCode(@Param("userCode")String userCode);

    Integer getIdByUserCode(@Param("userCode")String userCode);

    int updateLoginTime(@Param("id")int id);

    Integer getIdByOpOpenId(@Param("opOpenId")String opOpenId);

    String getSpreadUrlByUserId(@Param("openId")int userId);

    int updateUserTeamGiftLv(@Param("id")int id, @Param("lv")int lv);

    int updateUserTeamProductLv(@Param("id")int id, @Param("lv")int lv);

    int bindPhone(@Param("id")int id, @Param("mobile")String mobile);

    List<User> findDirectUser(@Param("id")int id,@Param("lv")int lv,@Param("type")int type);

    String getNameByUserId(@Param("id")int id);

    User findNormalUser(@Param("id")int id);

    String getDealPwdById(@Param("id")int id);

    int updateDealPwd(@Param("id")int id, @Param("dealPwd")String dealPwd);

    String getMobileById(@Param("id")int id);

    User userInfo(@Param("id")int id);

    String getUserCodeById(@Param("id")int id);

    Integer countDirectPeople(@Param("pollCode")String pollCode,@Param("lv")int lv);


    int countDirectUser(@Param("pollCode")String pollCode);

    int addDirectGiftTimes(@Param("id")int id);

    List<Integer> findUserIds();

    int updateSpread(@Param("id")int id, @Param("spread")String spread);

    Integer getUpUserId(@Param("id")int id);

    Integer getIdByUnionId(@Param("unionId")String unionId);

    int getMaxFloor();
}