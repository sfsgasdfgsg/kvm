package com.ff.kvm.dao.model;

import java.io.Serializable;
import java.util.Date;

public class Withdrawal implements Serializable {
    private Integer id;

    private Integer userId;

    private String mobile;

    private Integer type;

    private Double fcPg;

    private Double fcNum;

    private Double total;

    private String account;

    private String accountName;

    private Integer status;

    private Date wdTime;

    private String des;

    private Date createTime;

    private String bankName;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getFcPg() {
        return fcPg;
    }

    public void setFcPg(Double fcPg) {
        this.fcPg = fcPg;
    }

    public Double getFcNum() {
        return fcNum;
    }

    public void setFcNum(Double fcNum) {
        this.fcNum = fcNum;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName == null ? null : accountName.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getWdTime() {
        return wdTime;
    }

    public void setWdTime(Date wdTime) {
        this.wdTime = wdTime;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des == null ? null : des.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}