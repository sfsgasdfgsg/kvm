package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.UserLv;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserLvMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserLv record);

    int insertSelective(UserLv record);

    UserLv selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserLv record);

    int updateByPrimaryKey(UserLv record);

    UserLv findUserLvByLv(@Param("lv") int lv);

    List<UserLv> findAll();
}