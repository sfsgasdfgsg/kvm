package com.ff.kvm.dao.model.po;

import java.io.Serializable;

public class UpdateLoginTimePO implements Serializable {

    private static final long serialVersionUID = 4585565456295655703L;

    private String mobile;
    private String openId;
    private String id;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
