package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.PreRegister;
import org.apache.ibatis.annotations.Param;

public interface PreRegisterMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PreRegister record);

    int insertSelective(PreRegister record);

    PreRegister selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PreRegister record);

    int updateByPrimaryKey(PreRegister record);

    PreRegister findByOpenId(@Param("openId") String openId);

    PreRegister findByUnionId(@Param("unionId") String unionId);
}