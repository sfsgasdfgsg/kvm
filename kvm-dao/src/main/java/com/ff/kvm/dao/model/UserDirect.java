package com.ff.kvm.dao.model;

import java.io.Serializable;
import java.util.Date;

public class UserDirect implements Serializable {

    private static final long serialVersionUID = 5176617947512930905L;

    private Integer id;

    private Integer userId;

    private Integer directUserId;

    private Integer status;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDirectUserId() {
        return directUserId;
    }

    public void setDirectUserId(Integer directUserId) {
        this.directUserId = directUserId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}