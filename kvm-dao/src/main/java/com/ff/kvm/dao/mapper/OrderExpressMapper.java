package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.OrderExpress;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface OrderExpressMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderExpress record);

    int insertSelective(OrderExpress record);

    OrderExpress selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderExpress record);

    int updateByPrimaryKey(OrderExpress record);

    OrderExpress findOrderExpressByOrderId(@Param("orderId") int orderId);

    int sign(@Param("orderId") int orderId,@Param("status") int status);

    Date getSendTime(@Param("orderId") int orderId);

    int updateExpressStatus(@Param("orderId") int orderId,@Param("status")  int status);
}