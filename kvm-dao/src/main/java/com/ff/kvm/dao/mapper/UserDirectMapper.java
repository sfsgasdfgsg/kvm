package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.UserDirect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDirectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserDirect record);

    int insertSelective(UserDirect record);

    UserDirect selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserDirect record);

    int updateByPrimaryKey(UserDirect record);

    List<UserDirect> findByUserId(@Param("userId") int userId);
}