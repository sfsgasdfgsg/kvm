package com.ff.kvm.dao.model;

import java.io.Serializable;
import java.util.Date;

public class BonusLog implements Serializable {

    private static final long serialVersionUID = 4405543457429714599L;

    private Integer id;

    private Integer userId;

    private Integer userLv;

    private Double userPoint;

    private Integer downUserId;

    private Integer downUserLv;

    private Double downUserPoint;

    private Double total;

    private Double md;

    private Integer type;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserLv() {
        return userLv;
    }

    public void setUserLv(Integer userLv) {
        this.userLv = userLv;
    }

    public Double getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(Double userPoint) {
        this.userPoint = userPoint;
    }

    public Integer getDownUserId() {
        return downUserId;
    }

    public void setDownUserId(Integer downUserId) {
        this.downUserId = downUserId;
    }

    public Integer getDownUserLv() {
        return downUserLv;
    }

    public void setDownUserLv(Integer downUserLv) {
        this.downUserLv = downUserLv;
    }

    public Double getDownUserPoint() {
        return downUserPoint;
    }

    public void setDownUserPoint(Double downUserPoint) {
        this.downUserPoint = downUserPoint;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getMd() {
        return md;
    }

    public void setMd(Double md) {
        this.md = md;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}