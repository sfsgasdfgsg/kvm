package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Withdrawal;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WithdrawalMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Withdrawal record);

    int insertSelective(Withdrawal record);

    Withdrawal selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Withdrawal record);

    int updateByPrimaryKey(Withdrawal record);

    List<Withdrawal> findCashList(@Param("userId") int userId);
}