package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Recharge;
import org.apache.ibatis.annotations.Param;

public interface RechargeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Recharge record);

    int insertSelective(Recharge record);

    Recharge selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Recharge record);

    int updateByPrimaryKey(Recharge record);

    int updateRechargeStatus(@Param("rechargeNo") String rechargeNo);
}