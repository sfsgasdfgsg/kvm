package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.OrderPrice;
import org.apache.ibatis.annotations.Param;

public interface OrderPriceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderPrice record);

    int insertSelective(OrderPrice record);

    OrderPrice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderPrice record);

    int updateByPrimaryKey(OrderPrice record);

    OrderPrice findOrderPriceByOrderId(@Param("orderId") int orderId);

    double findOrderPriceTotalByOrderId(@Param("orderId") int orderId);
}