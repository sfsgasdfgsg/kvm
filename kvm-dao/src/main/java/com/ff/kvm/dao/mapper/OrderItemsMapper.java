package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.OrderItems;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderItemsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderItems record);

    int insertSelective(OrderItems record);

    OrderItems selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderItems record);

    int updateByPrimaryKey(OrderItems record);

    List<OrderItems> findOrderItemsByOrderId(@Param("orderId") Integer orderId);

    int findGiftLvByOrderId(@Param("orderId")int orderId);

    List<OrderItems> finOrderItems(@Param("orderId")int orderId);

    double findGiftBoxPriceByOrderId(@Param("orderId")int orderId);

    OrderItems findGiftBoxByOrderId(@Param("orderId")int orderId);
}