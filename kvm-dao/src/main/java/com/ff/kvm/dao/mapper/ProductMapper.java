package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    List<Product> findProductList(@Param("category") Integer category,@Param("type") int type);

    List<Product> findGiftBox();

    double getPriceById(@Param("id") int id);

    List<Product> findProductTopByType(@Param("type")int type);

    List<Product> findProductSpecialTop();

    List<Product> findProductTopByCategory(@Param("category")int category);

    Product findProductByIdForCheckOrder(@Param("id")int id);

    Product findProductNormal(@Param("id")int id);

    Product findProductNormalByOrderId(@Param("orderId")int orderId);

    String getGiftItemsByOrderId(@Param("orderId") int orderId);

    String getGiftNameByOrderId(@Param("orderId")int orderId);
}