package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.SignIn;
import com.ff.kvm.dao.model.po.SigninGiftNodePO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SignInMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SignIn record);

    int insertSelective(SignIn record);

    SignIn selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SignIn record);

    int updateByPrimaryKey(SignIn record);

    /**
     * 获取昨天签到数据
     *
     * @param userId
     * @return
     */
    SignIn getLastSinginInfo(@Param("userId") Integer userId);

    /**
     * 获取今天签到状态
     *
     * @param userId
     * @return
     */
    SignIn signTodayStatus(@Param("userId") Integer userId);

    /**
     * 获取签到礼物发放时间节点
     *
     * @return
     */
    List<SigninGiftNodePO> signinGiftSeting();

    /**
     * 通过奖id获取商品id
     *
     * @param id
     * @return
     */
    Integer getPrdcutId(@Param("giftId") Integer id);

}