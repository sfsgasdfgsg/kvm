package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.OrderPayment;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface OrderPaymentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OrderPayment record);

    int insertSelective(OrderPayment record);

    OrderPayment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OrderPayment record);

    int updateByPrimaryKey(OrderPayment record);

    int findOrderPaymentIdByOrderNO(@Param("orderNo") String orderNo, @Param("payChannel") int payChannel);

    OrderPayment findOrderPaymentByOrderNO(@Param("orderNo") String orderNo);

    Date getPayTime(@Param("orderId")int orderId);
}