package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.BonusLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BonusLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BonusLog record);

    int insertSelective(BonusLog record);

    BonusLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BonusLog record);

    int updateByPrimaryKey(BonusLog record);

    Double sumBonusTotal(@Param("userId")int userId,@Param("type") int type);

    List<BonusLog> findBonusLog(@Param("userId")int userId, @Param("type")int type);
}