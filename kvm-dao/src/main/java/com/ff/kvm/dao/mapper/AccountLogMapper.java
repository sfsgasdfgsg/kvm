package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.AccountLog;

import java.util.List;
import java.util.Map;

public interface AccountLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AccountLog record);

    int insertSelective(AccountLog record);

    AccountLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AccountLog record);

    int updateByPrimaryKey(AccountLog record);

    List<AccountLog> findAccountLog(Map<String, Object> condition);
}