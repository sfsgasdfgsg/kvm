package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.SysCode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysCodeMapper {
    int deleteByPrimaryKey(String name);

    int insert(SysCode record);

    int insertSelective(SysCode record);

    SysCode selectByPrimaryKey(String name);

    int updateByPrimaryKeySelective(SysCode record);

    int updateByPrimaryKey(SysCode record);

    int updateValue(@Param("name") String name,@Param("code") String code);

    List<SysCode> listSysyCode();
}