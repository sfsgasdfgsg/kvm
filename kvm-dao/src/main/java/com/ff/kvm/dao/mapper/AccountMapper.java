package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Account;
import org.apache.ibatis.annotations.Param;

public interface AccountMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

    Account findAccountByUserId(@Param("userId") int userId);

    int gwjPay(@Param("userId") int userId,@Param("total") double total);

    double getGwjByUserId(@Param("userId") int userId);

    double getMdByUserId(@Param("userId") int userId);

    int income(@Param("userId")int userId,@Param("total") double total);

    int bonus(@Param("userId")int userId, @Param("total")double total);

    int mdRecharge(@Param("userId")int userId, @Param("total")double total);

    int wxzfgRecharge(@Param("userId")int userId, @Param("total")double total);

    int mdCash(@Param("userId")int userId, @Param("total")double total);
}