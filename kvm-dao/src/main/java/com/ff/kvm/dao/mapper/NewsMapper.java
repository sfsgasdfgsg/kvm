package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.News;

import java.util.List;

public interface NewsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKey(News record);

    List<News> hotList();

    List<News> findVideo();

    List<News> newsList();
}