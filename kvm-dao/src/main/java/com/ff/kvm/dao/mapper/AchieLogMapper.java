package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.AchieLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AchieLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AchieLog record);

    int insertSelective(AchieLog record);

    AchieLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AchieLog record);

    int updateByPrimaryKey(AchieLog record);

    Double sumTotal(@Param("type") int type);

    List<AchieLog> findAchieLog(@Param("userId")int userId,@Param("type") int type);
}