package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Address record);

    int insertSelective(Address record);

    Address selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKey(Address record);

    Address findDefaultAddress(@Param("userId") int userId);

    List<Address> findAddressByUserId(@Param("userId") int userId);

    Integer countAddress(@Param("userId") int userId);

    Integer findDefaultAddressId(@Param("userId") Integer userId);

    Integer cancleDefaultAddress(@Param("id") Integer id);

    Integer updateDefaultAddress(@Param("id")  Integer id);
}