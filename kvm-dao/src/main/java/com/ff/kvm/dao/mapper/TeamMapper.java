package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Team;
import org.apache.ibatis.annotations.Param;

public interface TeamMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Team record);

    int insertSelective(Team record);

    Team selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Team record);

    int updateByPrimaryKey(Team record);

    Team findTeamByUserId(@Param("userId") int userId);

    Integer getIdByUserId(@Param("userId") int userId);
}