package com.ff.kvm.dao.model;

import java.util.Date;

public class SignIn {
    private Integer id;

    private Integer userId;

    private Date signinTime;

    private Integer signinMaxNo;

    private Integer signinNo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getSigninTime() {
        return signinTime;
    }

    public void setSigninTime(Date signinTime) {
        this.signinTime = signinTime;
    }

    public Integer getSigninMaxNo() {
        return signinMaxNo;
    }

    public void setSigninMaxNo(Integer signinMaxNo) {
        this.signinMaxNo = signinMaxNo;
    }

    public Integer getSigninNo() {
        return signinNo;
    }

    public void setSigninNo(Integer signinNo) {
        this.signinNo = signinNo;
    }
}