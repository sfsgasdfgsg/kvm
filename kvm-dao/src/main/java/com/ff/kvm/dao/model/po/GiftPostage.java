package com.ff.kvm.dao.model.po;

public class GiftPostage {
    Integer productId;
    Double postage;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getPostage() {
        return postage;
    }

    public void setPostage(Double postage) {
        this.postage = postage;
    }
}
