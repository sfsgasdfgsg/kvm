package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.HelpCenter;

import java.util.List;

public interface HelpCenterMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(HelpCenter record);

    int insertSelective(HelpCenter record);

    HelpCenter selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HelpCenter record);

    int updateByPrimaryKey(HelpCenter record);

    List<HelpCenter> findHelperCenter();

    List<HelpCenter> findAbout();
}