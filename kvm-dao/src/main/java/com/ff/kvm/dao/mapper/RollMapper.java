package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.Roll;

import java.util.List;

public interface RollMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Roll record);

    int insertSelective(Roll record);

    Roll selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Roll record);

    int updateByPrimaryKey(Roll record);

    List<Roll> rollList();
}