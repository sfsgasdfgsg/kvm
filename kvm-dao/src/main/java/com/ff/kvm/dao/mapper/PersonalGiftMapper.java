package com.ff.kvm.dao.mapper;

import com.ff.kvm.dao.model.PersonalGift;
import com.ff.kvm.dao.model.po.GiftPostage;
import com.ff.kvm.dao.model.po.PersonalProductPO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PersonalGiftMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PersonalGift record);

    int insertSelective(PersonalGift record);

    PersonalGift selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PersonalGift record);

    int updateByPrimaryKey(PersonalGift record);

    int getGiveGiftId();

    /**
     * 查看个人奖品列表
     *
     * @param userId
     * @param status
     * @return
     */
    List<PersonalProductPO> personalGiftList(@Param("userId") Integer userId, @Param("status") Integer status);

    Long personalGiftListTotal(@Param("userId") Integer userId, @Param("status") Integer status);

    /**
     * 奖品计数
     *
     * @param giftId
     * @return
     */
    int giftNumCumulative(@Param("giftId") Integer giftId);

    /**
     * 获取奖品邮费
     *
     * @param id
     * @return
     */
    GiftPostage getGiftPostage(@Param("id") Integer id);

}