package com.ff.kvm.dao.model;

import java.io.Serializable;
import java.util.Date;

public class AchieLog implements Serializable {
    private static final long serialVersionUID = 7579963472972230629L;
    private Integer id;

    private Integer userId;

    private Integer downUserId;

    private Double total;

    private Integer type;
    private String orderNo;
    private Integer orderId;

    private Date createTime;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDownUserId() {
        return downUserId;
    }

    public void setDownUserId(Integer downUserId) {
        this.downUserId = downUserId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}